/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9215.3.0  Starts(1st April 2015)         */
/*                              Features & Bugs for V2.9215.3.0  Ends (1st April 2015)          */
/*                              Features & Bugs for V2.9215.3.1  Starts (1st April 2015)        */
/*                              Features & Bugs for V2.9215.3.1  Ends (21st April 2015)         */
/*                              Features & Bugs for V2.9215.3.2  Starts (22nd April 2015)       */
/*                              Features & Bugs for V2.9215.3.2  Ends (27th April 2015)         */
/*                              Features & Bugs for V2.9215.3.3  Starts (28th April 2015)       */
/*                                                                                              */
/* ******************************************************************************************** */

/* ************************************ ZD 103263 Start ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE [dbo].[BJN_Settings_D](
	[Id] [int] IDENTITY(1,1),
	[BJNAppKey] [nvarchar](MAX) NULL,
	[BJNAppSecret] [nvarchar](MAX) NULL,
	[BJNAccessToken] [nvarchar](MAX) NULL,
	[BJNExpiry] [int] NULL,
	[Lastmodifieddatetime] [datetime] NULL,
	[ExpiryDateTime] [datetime] NULL
) ON [PRIMARY]
GO
COMMIT

update BJN_Settings_D set BJNAppKey = '',BJNAppSecret = '',BJNAccessToken = '', BJNExpiry = '0', Lastmodifieddatetime = '',ExpiryDateTime=''



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	RoomAdminEmail nvarchar(MAX) NULL,
	BJNUserId int NULL,
	EnableBJN int NULL,
	BJNUserName nvarchar(MAX) NULL,
	BJNPassword nvarchar(MAX) NULL
GO
COMMIT


update Loc_Room_D set RoomAdminEmail = '',BJNUserId=0, BJNUserName='',EnableBJN=0,BJNPassword=''



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	isBJNConf int NULL,
	BJNUniqueid nvarchar(100) NULL,
	BJNPasscode nvarchar(100) NULL,
	BJNMeetingid nvarchar(100) NULL,
	BJNUserId nvarchar(100) NULL,
	BJNHostPasscode nvarchar(100) NULL
GO
COMMIT


update Conf_Conference_D set isBJNConf=0,BJNUniqueid = '',BJNPasscode = '',BJNMeetingid = '',BJNUserId = '',BJNHostPasscode=''



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	isBJNRoom int NULL
GO
COMMIT


update Conf_Room_D set isBJNRoom = 0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_SyncMCUAdj_D ADD
	BJNUniqueid nvarchar(100) NULL,
	BJNUserId nvarchar(100) NULL
GO
COMMIT


update Conf_SyncMCUAdj_D set BJNUniqueid = '', BJNUserId = ''

/* ************************************ ZD 103263 End ********************************************* */


/* ************************************ ZD 103424 Start ********************************************* */

update loc_room_d set name = REPLACE(REPLACE (name, ',' , ''), '''','')

update Loc_Tier2_D set name = REPLACE(REPLACE (name, ',' , ''), '''','')

update Loc_Tier3_D set name = REPLACE(REPLACE (name, ',' , ''), '''','')

/* ************************************ ZD 103424 End ********************************************* */



-- 103493


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	PersonnelAlert nvarchar(MAX) NULL
GO
COMMIT

update Org_Settings_D set PersonnelAlert = ''

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9215.3.3  Ends (18th May 2015)           */
/*                              Features & Bugs for V2.9215.3.4  Starts (19th May 2015)         */
/*                                                                                              */
/* ******************************************************************************************** */
--ZD 103416
Update Org_Settings_D set SurveyOption = 2


/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9215.3.4  Ends (20th May 2015)           */
/*                              Features & Bugs for V2.9215.3.5  Starts (21st May 2015)         */
/*                                                                                              */
/* ******************************************************************************************** */



--103585

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	RecurencePatternText nvarchar(4000) NULL
GO
COMMIT



--ZD 103398 

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	VideoRefreshTimer int NULL
GO
COMMIT


update Org_Settings_D set VideoRefreshTimer = 0

--ZD 103595 Operations - Remove MPI Functionality and Verbiage

Delete from Gen_VideoProtocol_S where VideoProtocolID = 4 and VideoProtocolType ='MPI' 

Delete from Gen_AddressType_S where [id]=5 and name='MPI'

update Ept_List_D set addresstype=6 where addresstype=5 
update Ept_List_D set protocol= 3 where protocol=4

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9215.3.5  Ends (04th June 2015)          */
/*                              Features & Bugs for V2.9215.3.6  Starts (05th June 2015)        */
/*                                                                                              */
/* ******************************************************************************************** */

/* ************************************ ZD 102514 Starts ********************************************* */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	OnSiteAVSupportBuffer int NULL,
	CallMonitoringBuffer int NULL,
	DedicatedVNOCOperatorBuffer int NULL
GO
COMMIT

update Org_Settings_D set OnSiteAVSupportBuffer = 0, CallMonitoringBuffer = 0, DedicatedVNOCOperatorBuffer=0


/* ************************************ ZD 102514 Ends ********************************************* */

/* ************************************ ZD 103674 Starts ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	UserRolename nvarchar(50)  NULL
GO
ALTER TABLE dbo.Usr_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Usr_List_D set UserRolename = a.roleName from Usr_Roles_D as a where Usr_List_D.roleID = a.roleID



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	UserRolename nvarchar(50)  NULL
GO
ALTER TABLE dbo.Usr_Inactive_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Usr_Inactive_D set UserRolename = a.roleName from Usr_Roles_D as a where Usr_Inactive_D.roleID = a.roleID

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_GuestList_D ADD
	UserRolename nvarchar(50)  NULL
GO
ALTER TABLE dbo.Usr_GuestList_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Usr_GuestList_D set UserRolename = a.roleName from Usr_Roles_D as a where Usr_GuestList_D.roleID = a.roleID
/* ************************************ ZD 103674 Ends ********************************************* */

/* ************************************ ZD 103673 Starts ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Templates_D ADD
	DateFormat nvarchar(50) NULL,
	TimeFormat int NULL
GO
ALTER TABLE dbo.Usr_Templates_D ADD CONSTRAINT
	DF_Usr_Templates_D_DateFormat DEFAULT (N'MM/dd/yyyy') FOR DateFormat
GO
ALTER TABLE dbo.Usr_Templates_D ADD CONSTRAINT
	DF_Usr_Templates_D_TimeFormat DEFAULT ((0)) FOR TimeFormat
GO
ALTER TABLE dbo.Usr_Templates_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* ************************************ ZD 103673 Ends ********************************************* */

/*                              Features & Bugs for V2.9215.3.6  Ends(09th June 2015)        */
/*                              Features & Bugs for V2.9215.3.7 Starts(10th June 2015)        */

/* ************************************ ZD 103692 Starts ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableTravelAvoidTrack int NULL
GO
COMMIT

update Org_Settings_D set EnableTravelAvoidTrack = 0


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	HostRoom int NULL,
	NoofAttendee int NULL
GO
ALTER TABLE dbo.Conf_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Conf_Room_D set HostRoom = 0, NoofAttendee = 0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_Room_D ADD
	HostRoom int NULL,
	NoofAttendee int NULL
GO
ALTER TABLE dbo.Archive_Conf_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Archive_Conf_Room_D set HostRoom = 0, NoofAttendee = 0


/* ************************************ ZD 103692 Ends ********************************************* */

/* ************************************ ZD 103469 Starts ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D
	DROP CONSTRAINT DF_Usr_List_D_PreferedRoom
GO
COMMIT


update Usr_List_D set PreferedRoom = REPLACE (PreferedRoom, ',0,' , ',') where PreferedRoom like '%,0,%'
update Usr_List_D set PreferedRoom = REPLACE (PreferedRoom, '0,' , '') where PreferedRoom like '0,%'
update Usr_List_D set PreferedRoom = REPLACE (PreferedRoom, ',0' , '') where PreferedRoom like '%,0'
update Usr_List_D set PreferedRoom = REPLACE (PreferedRoom, ',1,' , ',') where PreferedRoom like '%,1,%'
/* ************************************ ZD 103469 Ends ********************************************* */
--ZD 102688
update Sys_TimeZonePref_S set name = 'Worldwide Time Zones' where systemid = 0


/* **************************************************************************************** */

/*                              Features & Bugs for V2.9215.3.7  Ends(18th June 2015)       */
/*                              Features & Bugs for V2.9215.3.8 Starts(18th June 2015)      */

/* **************************************************************************************** */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	LoginBackgroundNormalId int NULL
GO
COMMIT

update Sys_Settings_D set LoginBackgroundNormalId = 0
/* ************************************ ZD 103581 Starts ********************************************* */


/* ************************************ ZD 103469 Ends ********************************************* */

/* ************************************ ZD 103801 Starts ********************************************* */

update Ept_List_D set Extendpoint = 0 where Extendpoint = 1 
and endpointId in (select endpointId from Loc_Room_D where RoomCategory = 3 and Extroom = 0)

update Loc_Room_D set RoomCategory = 1 where RoomCategory = 3 and Extroom = 0
/* ************************************ ZD 103801 Starts ********************************************* */


/* ZD 103723- Add Additional Supported Cisco Models to Web and Documentation and 103644 START */
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (82,'Cisco DX650',2,'DX650')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (83,'Cisco DX70',2,'DX70')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (84,'Cisco DX80',2,'DX80')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (85,'Cisco MX700',2,'MX700')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (86,'Cisco MX800',2,'MX800')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (87,'Cisco SX80',2,'SX80')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (88,'Cisco SX10',2,'SX10')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (89,'Cisco Telepresence Profile 55',2,'Telepresence Profile 55')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (90,'Cisco Telepresence Profile 65',2,'Telepresence Profile 65')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (91,'Cisco Telepresence T1',2,'Telepresence T1')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (92,'Cisco Telepresence T3',2,'Telepresence T3')
Insert into Gen_VideoEquipment_S (VEid,VEname,Familyid, DisplayName) values (93,'Cisco IX5000',2,'IX5000')
update Gen_VideoEquipment_S set DisplayName = 'CTS 3000 series' where VEid = 56
update Gen_VideoEquipment_S set DisplayName = 'SX20 *' where VEid = 52
update Gen_VideoEquipment_S set DisplayName = 'TX1300 series' where VEid = 57
update Gen_VideoEquipment_S set DisplayName = 'CTS 1300 series' where VEid = 78
/*ZD 103723 and 103644 END*/

/* ************************************ ZD 103737 Starts ********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.conf_user_d ADD	
	MeetingSigninTime DateTime NULL
GO
COMMIT

/* ************************************ ZD 103737 End ********************************************* */
/* **************************************************************************************** */

/*                              Features & Bugs for V2.9215.3.8  Ends(06 July 2015)        */
/*                              Features & Bugs for  V2.9315.3.0 Starts(07 July 2015)      */

/* **************************************************************************************** */

--ZD 103880
Update Loc_Room_D set TopTier =a.Name from Loc_Tier3_D as a where Loc_Room_D.L3LocationId = a.Id and RoomCategory = 3

Update Loc_Room_D set MiddleTier =a.Name from Loc_Tier2_D as a where Loc_Room_D.L2LocationId = a.Id and RoomCategory = 3  


/* ************************************ ZD 103999 Update RMX Versions in MCU Profile Page- START ********************************************* */

INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants])
 VALUES (17, N'Polycom RMX 1000', 1, 20, 20)

 INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants])
 VALUES (18, N'Polycom RMX 1500', 1, 20, 20)

 INSERT INTO [dbo].[Mcu_Vendor_S] ([id], [name], [BridgeInterfaceId], [videoparticipants], [audioparticipants])
 VALUES (19, N'Polycom RMX 4000', 1, 20, 20)



Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom RMX 1000',17,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom RMX 1500',18,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Polycom RMX 4000',19,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

update MCU_Params_D set confMuteAllExcept =0 , confUnmuteAll=0,partyLeader=1,LoginURL='',PassPhrase='',activeSpeaker=0,snapshotSupport=0 where BridgeTypeid in(17,18,19)

update Mcu_List_D set SoftwareVer='4.x' where BridgeTypeId=8 and SoftwareVer='3.x'

/* ************************************ ZD 103999 END********************************************* */

/* **************************************************************************************** */

/*                              Features & Bugs for  V2.9315.3.0  Ends(25 July 2015)        */
/*                              Features & Bugs for  V2.9315.3.1 Starts(27 July 2015)       */

/* **************************************************************************************** */

--ZD 103987

Update Usr_Roles_D set locked = 1 where roleID in (select roleID from Usr_List_D) and createType = 2 and locked = 0

/* ************************************ ZD 103496 Starts********************************************* */
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Sys_Settings_D ADD
	MemcacheEnabled smallint NULL
GO
COMMIT


Update Sys_Settings_D set MemcacheEnabled = 0
/* ************************************ ZD 103496 Ends********************************************* */


--- For saving images in local working folders, have to save the images with the help SaveImage Tool in VSS

/* ************************************ ZD 103496 Starts********************************************* */
ALTER TABLE Loc_Room_D ALTER COLUMN RoomIconTypeId nvarchar(250) NULL
ALTER TABLE Loc_Room_D ALTER COLUMN MiscImage1Id nvarchar(250) NULL
ALTER TABLE Loc_Room_D ALTER COLUMN MiscImage2Id nvarchar(250) NULL
ALTER TABLE Loc_Room_D ALTER COLUMN MapImage1Id nvarchar(250) NULL
ALTER TABLE Loc_Room_D ALTER COLUMN MapImage2Id nvarchar(250) NULL

ALTER TABLE Audit_Loc_Room_D ALTER COLUMN RoomIconTypeId nvarchar(250) NULL
ALTER TABLE Audit_Loc_Room_D ALTER COLUMN MiscImage1Id nvarchar(250) NULL
ALTER TABLE Audit_Loc_Room_D ALTER COLUMN MiscImage2Id nvarchar(250) NULL
ALTER TABLE Audit_Loc_Room_D ALTER COLUMN MapImage1Id nvarchar(250) NULL
ALTER TABLE Audit_Loc_Room_D ALTER COLUMN MapImage2Id nvarchar(250) NULL

update Loc_Room_D set RoomIconTypeId ='Room.jpg'  where RoomIconTypeId ='24'
update Loc_Room_D  set RoomIconTypeId ='Audio.jpg'   where RoomIconTypeId ='25'
update Loc_Room_D  set RoomIconTypeId ='Video.jpg'  where RoomIconTypeId ='26'
update Loc_Room_D set RoomIconTypeId ='Telepresence.jpg'  where RoomIconTypeId ='27'
update Loc_Room_D  set RoomIconTypeId ='HotdeskingAudio.jpg'  where RoomIconTypeId ='28'
update Loc_Room_D set RoomIconTypeId ='HotdeskingVideo.jpg'  where RoomIconTypeId ='29'
update Loc_Room_D  set RoomIconTypeId ='GuestVideo.jpg'  where RoomIconTypeId ='30'
update Loc_Room_D  set RoomIconTypeId =''  where RoomIconTypeId ='0'

update Audit_Loc_Room_D set RoomIconTypeId ='Room.jpg'  where RoomIconTypeId ='24'
update Audit_Loc_Room_D  set RoomIconTypeId ='Audio.jpg'   where RoomIconTypeId ='25'
update Audit_Loc_Room_D  set RoomIconTypeId ='Video.jpg'  where RoomIconTypeId ='26'
update Audit_Loc_Room_D set RoomIconTypeId ='Telepresence.jpg'  where RoomIconTypeId ='27'
update Audit_Loc_Room_D  set RoomIconTypeId ='HotdeskingAudio.jpg'  where RoomIconTypeId ='28'
update Audit_Loc_Room_D set RoomIconTypeId ='HotdeskingVideo.jpg'  where RoomIconTypeId ='29'
update Audit_Loc_Room_D  set RoomIconTypeId ='GuestVideo.jpg'  where RoomIconTypeId ='30'
update Audit_Loc_Room_D  set RoomIconTypeId =''  where RoomIconTypeId ='0'
/* ************************************ ZD 103496 Ends********************************************* */

/*                              Features & Bugs for  V2.9315.3.1 ENDS(31 July 2015)       */

/*                              Features & Bugs for  V2.9315.3.2 Starts(1 August 2015)       */




/* ************************************ ZD 103784 Enhancement - Provide Ability to Have Duplicate Logins in Multi-Forested AD START********************************************* */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
Alter Table Usr_List_D 
Add UserDomain varchar(50) Null
GO
Alter Table Usr_Inactive_D 
Add UserDomain varchar(50) Null
Go
Alter Table Usr_GuestList_D 
Add UserDomain varchar(50) Null
Go

COMMIT

Update Usr_List_D set UserDomain='' 
Update Usr_Inactive_D set UserDomain='' 
Update Usr_GuestList_D set UserDomain='' 

/* ************************************ ZD 103784 END********************************************* */


/* **************************************************************************************** */

/*                              Features & Bugs for  V2.9315.3.2  Ends(06 Aug 2015)        */
/*                              Features & Bugs for  V2.9315.3.3 Starts(07 Aug 2015)       */

/* **************************************************************************************** */


/* ************************************ ZD 103550 START********************************************* */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableBJNIntegration int NULL,	
	BJNMeetingType int NULL,
	BJNSelectOption int NULL,
	BlueJeansUserLimit int NULL
GO
COMMIT

update Org_Settings_D set EnableBJNIntegration = 0, BJNMeetingType = 1, BJNSelectOption = 0, BlueJeansUserLimit =0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	BJNMeetingType int NULL,
	CascadeBJN int NULL
GO
COMMIT

update Conf_Conference_D set BJNMeetingType = 0 , CascadeBJN = 0


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_Conference_D ADD
	BJNMeetingType int NULL,
	CascadeBJN int NULL,
	isBJNConf int NULL,
	BJNUniqueid nvarchar(100) NULL,
	BJNPasscode nvarchar(100) NULL,
	BJNMeetingid nvarchar(100) NULL,
	BJNUserId nvarchar(100) NULL,
	BJNHostPasscode nvarchar(100) NULL
GO
COMMIT

update Audit_Conf_Conference_D set BJNMeetingType = 0 , CascadeBJN = 0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_Conference_D ADD
	BJNMeetingType int NULL,
	CascadeBJN int NULL,
	isBJNConf int NULL,
	BJNUniqueid nvarchar(100) NULL,
	BJNPasscode nvarchar(100) NULL,
	BJNMeetingid nvarchar(100) NULL,
	BJNUserId nvarchar(100) NULL,
	BJNHostPasscode nvarchar(100) NULL
GO
COMMIT

update Archive_Conf_Conference_D set BJNMeetingType = 0 , CascadeBJN = 0

update Archive_Conf_Conference_D set isBJNConf=0,BJNUniqueid = '',BJNPasscode = '',BJNMeetingid = '',BJNUserId = '',BJNHostPasscode=''


Insert into Mcu_Vendor_S (id,name,BridgeInterfaceId,videoparticipants,audioparticipants) values (20,'Blue Jeans',11,50,50)

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	BJNAppKey nvarchar(250) NULL,
	BJNAppSecret nvarchar(250) NULL,
	BJNAppToken nvarchar(250) NULL,
	BJNExpiry int NULL,
	BJNEnterpriseID int NULL,
	BJNExpiryDateTime datetime NULL
GO
ALTER TABLE dbo.Mcu_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Mcu_List_D ADD
	BJNAppKey nvarchar(250) NULL,
	BJNAppSecret nvarchar(250) NULL,
	BJNAppToken nvarchar(250) NULL,
	BJNExpiry int NULL,
	BJNEnterpriseID int NULL,
	BJNExpiryDateTime datetime NULL
GO
ALTER TABLE dbo.Audit_Mcu_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_List_D ADD
	BJNUserName nvarchar(50) NULL,
	BJNUserId int NULL,
	BJNMeetingId nvarchar(250) NULL,
	BJNPassCode nvarchar(250) NULL,
	BJNUserEmail nvarchar(250) NULL,
	EnableBJNConf int NULL,
	BJNPartyPassCode nvarchar(250) NULL
GO
ALTER TABLE dbo.Usr_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Usr_Inactive_D ADD
	BJNUserName nvarchar(50) NULL,
	BJNUserId int NULL,
	BJNMeetingId nvarchar(250) NULL,
	BJNPassCode nvarchar(250) NULL,
	BJNUserEmail nvarchar(250) NULL,
	EnableBJNConf int NULL,
	BJNPartyPassCode nvarchar(250) NULL
GO
ALTER TABLE dbo.Usr_Inactive_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Loc_Room_D ADD
	BJNPartyPassCode nvarchar(250) NULL
GO
COMMIT


Drop Table Bjn_Settings_D

ALTER TABLE Loc_Room_D DROP COLUMN BJNPassword


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.License_Defaults_D ADD
	BlueJeansUserLimit int NULL
GO
ALTER TABLE dbo.License_Defaults_D
	DROP COLUMN MaxenableBlueJeans
GO
ALTER TABLE dbo.License_Defaults_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update License_Defaults_D set MaxiControlRooms=0,BlueJeansUserLimit=0


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Usr_List_D ADD
	BJNUserName nvarchar(50) NULL,
	EnableBJNConf int NULL
GO
COMMIT


Insert into mcu_params_d (BridgeName,BridgeTypeid,ConfLockUnLock,confMessage,confAudioTx,confAudioRx,confVideoTx,confVideoRx,
confLayout,confCamera,confPacketloss,confRecord,partyBandwidth,partySetfocus,partyMessage,partyAudioTx,partyAudioRx,partyVideoTx,
partyVideoRx ,partyLayout ,partyCamera,partyPacketloss,partyImagestream,partyLockUnLock,partyRecord,partyLecturemode) values 
('Blue Jeans',20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

update MCU_Params_D set confMuteAllExcept =0 , confUnmuteAll=0,partyLeader=0,LoginURL='',PassPhrase='',activeSpeaker=0,snapshotSupport=0 where BridgeTypeid = 20


-- Added new Tag <MaxBlueJeansUsers>10</MaxBlueJeansUsers>

--<myVRMSiteLicense><IR>1-Qss05</IR><Site><ExpirationDate>1/31/2018</ExpirationDate><MaxOrganizations>5</MaxOrganizations><Cloud>0</Cloud><PublicRoomService>0</PublicRoomService><IsLDAP>0</IsLDAP><ServerActivation><ProcessorIDs><ID>BFEBFBFF0001067A</ID><ID>BFEBFBFF0001067A</ID></ProcessorIDs><MACAddresses><ID>00:25:11:6F:A1:5D</ID><ID>00:25:11:6F:A1:5D</ID></MACAddresses></ServerActivation></Site><Organizations><Rooms><MaxNonVideoRooms>200</MaxNonVideoRooms><MaxVideoRooms>200</MaxVideoRooms><MaxVMRRooms>50</MaxVMRRooms><MaxGuestRooms>20</MaxGuestRooms><MaxROHotdesking>20</MaxROHotdesking><MaxVCHotdesking>10</MaxVCHotdesking><MaxiControlRooms>5</MaxiControlRooms></Rooms><Hardware><MaxStandardMCUs>20</MaxStandardMCUs><MaxEnhancedMCUs>20</MaxEnhancedMCUs><MaxMCUs>3</MaxMCUs><MaxEndpoints>50</MaxEndpoints><MaxCDRs></MaxCDRs></Hardware><Users><MaxTotalUsers>400</MaxTotalUsers><MaxGuestsPerUser>10</MaxGuestsPerUser><MaxExchangeUsers>100</MaxExchangeUsers><MaxDominoUsers>100</MaxDominoUsers><MaxMobileUsers>5</MaxMobileUsers><MaxPCUsers>10</MaxPCUsers><MaxWebexUsers>5</MaxWebexUsers><MaxBlueJeansUsers>10</MaxBlueJeansUsers></Users><Modules><MaxFacilitiesModules>10</MaxFacilitiesModules><MaxCateringModules>10</MaxCateringModules><MaxHousekeepingModules>10</MaxHousekeepingModules><MaxAPIModules>10</MaxAPIModules><MaxJabber>10</MaxJabber><MaxLync>10</MaxLync><MaxVidtel>10</MaxVidtel><MaxAdvancedReport>10</MaxAdvancedReport></Modules><Languages><MaxSpanish></MaxSpanish><MaxMandarin></MaxMandarin><MaxHindi></MaxHindi><MaxFrench></MaxFrench></Languages></Organizations></myVRMSiteLicense>

/* ************************************ ZD 103550 END********************************************* */


/* **************************************************************************************** */

/*                              Features & Bugs for  V2.9315.3.3  Ends(07 Aug 2015)        */
/*                              Features & Bugs for  V2.9315.3.4 Starts(10 Aug 2015)       */

/* **************************************************************************************** */


/* ************************************ ZD 103878/10418 Error in deleting the user if he is the host of the completed conferences- START********************************************* */
/****** Object:  Table [dbo].[Usr_Deletelist_D]    Script Date: 08/07/2015 00:38:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Usr_Deletelist_D](
	[Id] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Financial] [int] NULL,
	[Admin] [smallint] NULL,
	[Login] [nvarchar](256) NULL,
	[Password] [nvarchar](1000) NULL,
	[Company] [nvarchar](512) NULL,
	[TimeZone] [smallint] NULL,
	[Language] [smallint] NULL,
	[PreferedRoom] [varchar](150) NULL,
	[AlternativeEmail] [nvarchar](512) NULL,
	[DoubleEmail] [smallint] NULL,
	[PreferedGroup] [smallint] NULL,
	[CCGroup] [smallint] NULL,
	[Telephone] [nvarchar](50) NULL,
	[RoomID] [int] NULL,
	[PreferedOperator] [int] NULL,
	[lockCntTrns] [int] NULL,
	[DefLineRate] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[Deleted] [int] NOT NULL,
	[DefAudio] [int] NULL,
	[DefVideoSession] [int] NULL,
	[MenuMask] [nvarchar](200) NULL,
	[initialTime] [int] NULL,
	[EmailClient] [smallint] NULL,
	[newUser] [smallint] NULL,
	[PrefTZSID] [int] NULL,
	[IPISDNAddress] [nvarchar](256) NULL,
	[DefaultEquipmentId] [smallint] NOT NULL,
	[connectionType] [smallint] NULL,
	[companyId] [int] NULL,
	[securityKey] [nvarchar](256) NULL,
	[LockStatus] [smallint] NOT NULL,
	[LastLogin] [datetime] NULL,
	[outsidenetwork] [smallint] NULL,
	[emailmask] [bigint] NULL,
	[roleID] [varchar](100) NOT NULL,
	[addresstype] [smallint] NULL,
	[accountexpiry] [datetime] NULL,
	[approverCount] [int] NULL,
	[BridgeID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[LevelID] [int] NULL,
	[preferredISDN] [nvarchar](50) NULL,
	[portletId] [int] NULL,
	[searchId] [int] NULL,
	[endpointId] [int] NOT NULL,
	[DateFormat] [nvarchar](50) NULL,
	[enableAV] [int] NULL,
	[timezonedisplay] [int] NULL,
	[timeformat] [int] NULL,
	[enableParticipants] [int] NULL,
	[TickerStatus] [int] NULL,
	[TickerSpeed] [int] NULL,
	[TickerPosition] [int] NULL,
	[TickerBackground] [nchar](10) NULL,
	[TickerDisplay] [int] NULL,
	[RSSFeedLink] [nvarchar](50) NULL,
	[TickerStatus1] [int] NULL,
	[TickerSpeed1] [int] NULL,
	[TickerPosition1] [int] NULL,
	[TickerBackground1] [nchar](10) NULL,
	[TickerDisplay1] [int] NULL,
	[RSSFeedLink1] [nvarchar](50) NULL,
	[enableExchange] [int] NULL,
	[enableDomino] [int] NULL,
	[Audioaddon] [nvarchar](50) NULL,
	[WorkPhone] [nvarchar](50) NULL,
	[CellPhone] [nvarchar](50) NULL,
	[ConferenceCode] [nvarchar](50) NULL,
	[LeaderPin] [nvarchar](50) NULL,
	[DefaultTemplate] [smallint] NULL,
	[EmailLangId] [int] NULL,
	[MailBlocked] [smallint] NULL,
	[MailBlockedDate] [datetime] NULL,
	[enableMobile] [int] NULL,
	[PluginConfirmations] [smallint] NULL,
	[InternalVideoNumber] [nvarchar](50) NOT NULL,
	[ExternalVideoNumber] [nvarchar](50) NOT NULL,
	[HelpReqEmailID] [nvarchar](512) NULL,
	[HelpReqPhone] [nvarchar](50) NULL,
	[SendSurveyEmail] [int] NULL,
	[PrivateVMR] [nvarchar](max) NULL,
	[EnableVNOCselection] [int] NULL,
	[EntityID] [int] NULL,
	[Extension] [nvarchar](50) NULL,
	[Pin] [nvarchar](50) NULL,
	[MemberID] [int] NULL,
	[isLocked] [int] NULL,
	[VidyoURL] [nvarchar](max) NULL,
	[Type] [nvarchar](50) NULL,
	[allowPersonalMeeting] [int] NULL,
	[allowCallDirect] [int] NULL,
	[ESUserID] [int] NULL,
	[ParentReseller] [int] NULL,
	[CorpUser] [int] NULL,
	[Region] [int] NULL,
	[Secured] [smallint] NULL,
	[PreConfCode] [nvarchar](50) NULL,
	[PreLeaderPin] [nvarchar](50) NULL,
	[PostLeaderPin] [nvarchar](50) NULL,
	[AudioDialInPrefix] [nvarchar](50) NULL,
	[enablePCUser] [smallint] NULL,
	[RFIDValue] [nvarchar](250) NULL,
	[BrokerRoomNum] [nvarchar](50) NULL,
	[PerCalStartTime] [datetime] NULL,
	[PerCalEndTime] [datetime] NULL,
	[RoomCalStartTime] [datetime] NULL,
	[RoomCalEndime] [datetime] NULL,
	[PerCalShowHours] [smallint] NULL,
	[RoomCalShowHours] [smallint] NULL,
	[RequestID] [nvarchar](max) NULL,
	[StaticID] [varchar](255) NULL,
	[IsStaticIDEnabled] [int] NULL,
	[enableWebexUser] [smallint] NULL,
	[WebexAddress] [nvarchar](max) NULL,
	[WebexUserName] [nvarchar](50) NULL,
	[WebexPassword] [nvarchar](50) NULL,
	[LandingPageTitle] [nvarchar](200) NULL,
	[LandingPageLink] [nvarchar](200) NULL,
	[ExternalUserId] [nvarchar](200) NULL,
	[VirtualRoomId] [nvarchar](200) NULL,
	[PasswordTime] [datetime] NULL,
	[IsPasswordReset] [smallint] NULL,
	[LastModifiedUser] [int] NULL,
	[LastModifiedDate] [datetime] NULL,
	[RoomViewType] [smallint] NULL,
	[enableAdditionalOption] [int] NULL,
	[EnableAVWO] [smallint] NULL,
	[EnableCateringWO] [smallint] NULL,
	[EnableFacilityWO] [smallint] NULL,
	[RoomRecordsView] [int] NULL,
	[UserOrgin] [smallint] NULL,
	[ParticipantCode] [nvarchar](50) NULL,
	[UserRolename] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE Usr_Deletelist_D
  ALTER COLUMN LastName
    VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
    
ALTER TABLE Usr_Deletelist_D
  ALTER COLUMN FirstName
    VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
    
ALTER TABLE Usr_Deletelist_D
  ALTER COLUMN Email
    VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
    
      
ALTER TABLE Usr_Deletelist_D
  ALTER COLUMN roleID
    VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
    

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Conference_D ADD
	UserIDRefText nvarchar(10) NULL
GO
ALTER TABLE dbo.Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_Conference_D ADD
	UserIDRefText nvarchar(10) NULL
GO
ALTER TABLE dbo.Archive_Conf_Conference_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* ************************************ ZD 103878/10418 END********************************************* */


/* **************************************************************************************** */

/*                              Features & Bugs for  V2.9315.3.4  Ends(20 Aug 2015)         */

/* **************************************************************************************** */




