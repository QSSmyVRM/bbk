
delete from Launguage_Translation_Text where LanguageID =3

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(Open for Registration)','(Abierto para Registro)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(Work orders deliver by date/time do not match with conference date/time.)','(Fecha/hora de entrega de las �rdenes de trabajo  no coinciden con la fecha/hora de conferencia.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(Work orders start/end date/time do not match with conference start/end date/time.)','(Fecha/hora de inicio/fin de las �rdenes de trabajo  no coinciden con la fecha/hora de inicio/fin de la conferencia.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(No custom options)','(Sin opciones personalizadas)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(No participants)','(Sin participantes)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(No rooms)','(Sin salones)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(No work orders)','(Sin �rdenes de trabajo)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address Type and Protocol don t match. Please verify','Tipo de Direcci�n y Protocolo no coinciden. Por favor, verif�quelos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Aggregate Usage Report','Informe sobre el uso conjunto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Records Found','No se encontraron registros')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'All Audio/Visual Work Orders','Todas las �rdenes de  trabajo Audio/Visual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'All Housekeeping Work Orders','Todas las �rdenes de Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Apply Theme','Aplicar Tema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'At least one item should have a requested quantity greater than 0.','Al menos un elemento debe tener una cantidad solicitada mayor que 0.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'At least one menu should have a requested quantity > 0.','Al menos un men� debe tener una cantidad solicitada > 0.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'At most 5 profiles can be created.','Se pueden crear un m�ximo de 5 perfiles.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attach Files','Adjuntar Archivos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio Only Conference','Conferencia de Audio Solamente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio/Video Conference','Conferencia de Audio/V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio/Visual','Audio/Visual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio/Visual Work Orders','�rdenes de trabajo Audio/Visual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Auto select...','Selecci�n autom�tica ...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Billing Code already exists in the list','C�digo de facturaci�n ya existe en la lista')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Billing Code description should be within 35 characters','Descripci�n del c�digo de facturaci�n debe ser de hasta 35 caracteres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Cancel','Cancelar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Cascade','Cascada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering ','Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Providers','Proveedores de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Work Orders','Ordenes de trabajo de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'CC','CC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Edit/Create button to save these changes.','Haga clic en el bot�n Editar/Crear para guardar los cambios.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Update in order to save changes. Otherwise click Cancel to return to the catering work orders summary page for this conference.','Haga clic en el bot�n Actualizar para guardar los cambios. De lo contrario haga clic en cancelar para regresar a la p�gina resumen de las �rdenes de trabajo para esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Update to re-calculate total OR click Edit to update this Workorder.','Haga clic en Actualizar para volver a calcular totales o haga clic en Editar para actualizar esta Orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Clone All','Clonar Todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Close','Cerrar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Codian MCU Configuration','Configuraci�n Codian MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Compare Conference Room Resource','Comparar Recursos de Sal�n de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Completed','Completado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Details','Detalles de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference End','Fin de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Room Resource','Recursos de Sal�n de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Start','Inicio de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Title','Titulo de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Your Conference has been scheduled successfully.','Su conferencia ha sido programada con exitosa.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Your conference has been scheduled successfully. Email notifications have been sent to participants.','Su conferencia ha sido programada con exitosa. Se han enviado notificaciones por correo electr�nico a los participantes.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Congratulations! Your conference has been scheduled successfully. Email notifications have been sent to participants.','�Enhorabuena! Su conferencia ha sido programada con exitosa. Se han enviado notificaciones por correo electr�nico a los participantes.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connect','Conectar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create','Crear ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create Group','Crear Grupo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New ','Crear Nuevo ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New A/V Set','Crear Nuevo Grupo de A/V')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Catering Provider','Crear Nuevo Proveedor de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Endpoint','Crear Nuevo Punto Final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Crear Nuevo Punto Final','Crear Nuevo Grupo de H/K')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New MCU','Crear Nuevo MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Organization','Crear Nueva Organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New User','Crear Nuevo Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New User Template','Crear Nueva Plantilla de Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Date Conferences Total Usage Report','Informe de uso total de conferencias de fechas personalizadas  ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom option description should be within 35 characters','Descripci�n de la opci�n personalizada debe ser de hasta 35 caracteres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom option title already exists in the list','Opci�n personalizada ya existe en la lista')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Option Value','Valor de opci�n personalizada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default Protocol does not match with selected Address Type.','Protocolo por defecto no coincide con el tipo de direcci�n seleccionada. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delete All','Borrar Todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delete Log','Borrar Registro')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deleted','Borrado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disconnected','Desconectado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'DLL Name','Nombre DLL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit','Editar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Existing User','Editar Usuario Existente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Group','Editar Grupo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Organization Profile','Editar Perfil de Organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit User Template','Editar Plantilla de Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Enabled','Habilitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Enter requested quantity for corresponding items.','Introduzca la cantidad solicitada para los elementos correspondientes.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error deleteting work order.','Error al borrar la orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error Uploading Image. Please check your VRM Administrator.Error al borrar la orden de trabajo','Error al cargar la imagen. Por favor, consulte con su administrador del VRM.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Event level to be logged','Nivel de evento para ser registrado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External Attendee','Asistentes externos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'File attachment is greater than 10MB. File has not been uploaded.','Archivo adjunto mayor que 10 MB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'File trying to upload already exists','El Archivo que est� tratando cargar ya existe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Guest','Invitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Guest Address Book','Libro de Direcciones de Invitados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Housekeeping ','Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Housekeeping Groups','Grupos Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Housekeeping Work Orders','�rdenes de trabajo de Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Illegal Search','B�squeda ilegal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Image size is greater than 5MB. File has not been uploaded.','Tama�o de la imagen es mayor que 5 MB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Immediate Conference','Conferencia Inmediata')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Include in Email','Incluir en correo-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Date Range: FROM date should be prior to TO date.','Rango de Fecha Invalido: Fecha DESDE debe ser anterior a  la  fecha HASTA.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid duration','Duraci�n Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Duration.Conference duration should be minimum of 15 mins.','Duraci�n Inv�lida. Duraci�n de Conferencia debe ser por lo menos de 15 minutos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Interface Type and Address Type.','Tipo de Interface y Tipo Direcci�n Inv�lidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid MPI Address','Direcci�n MPI Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid selection for Address Type, Protocol and Connection Type.','Selecci�n inv�lida de tipo de direcci�n, protocolo y tipo de conexi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI protocol.','Selecci�n de tipo de conexi�n y protocolo inv�lida. El MCU es obligatorio para las llamadas directas con protocolo MPI.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Selection.','Selecci�n inv�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Start and End Range','Rango de Inicio y Fin inv�lidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Start Date or Time. It should be greater than current time in','Fecha/Hora de Inicio inv�lida. Debe ser mayor que la hora actual en')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid User Type','Tipo de Usuario Inv�lido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Set has been changed and no longer belongs to the selected room.','Grupo de inventario ha sido cambiado y ya no pertenece al sal�n elegido.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Sets','Grupos de Inventario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Item Name already exists in the list','Nombre de elemento ya existe en la lista')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Month Conferences Total Usage Report','Informe de uso total de conferencias del mes pasado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Updated Date','�ltima fecha actualizada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Week Conferences Total Usage Report','Informe de uso total de conferencias de la semana pasada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Departments','Administrar Departamentos ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Guests','Administrar Invitados ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Inactive Users','Administrar Usuarios Inactivos ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage MCUs','Administrar MCUs ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Tiers','Administrar Niveles ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Users','Administrar Usuarios ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max. length for start and end range is 22 characters.','La longitud m�x. para el rango de inicio y el final es de 22 caracteres.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU','MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU is mandatory for MPI protocol','MCU es obligatorio para el protocolo MPI')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Modify work order to conform to your room changes. Work orders and Room names are as follows:','Modificar la orden de trabajo para adaptarse a los cambios de su habitaci�n. Las �rdenes de trabajo y los nombres de los salones son los siguientes:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Module Type','Tipo de M�dulo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'More than 10 IP Services can not be added.','M�s de 10 servicios IP no se pueden a�adir.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'More than 10 ISDN Services can not be added.','M�s de 10 servicios ISDN no se pueden a�adir.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Incomplete Audio/Visual Work Orders','Mis �rdenes de trabajo de audio/visual incompletas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Incomplete Catering Work Orders','Mis �rdenes de trabajo de catering incompletas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Incomplete Housekeeping Work Orders','Mis �rdenes de trabajo de mantenimiento incompletas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Pending Audio/Visual Work Orders','Mis �rdenes de trabajo de audio/visual pendientes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Pending Catering Work Orders','Mis �rdenes de trabajo de catering pendientes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Pending Housekeeping Work Orders','Mis �rdenes de trabajo de mantenimiento pendientes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Preferences','Mis Preferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'myVRM Address Book','Libro de direcciones myVRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Conference-AV','Sin Conferencia AV')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Custom Options found.','No se encontraron opciones personalizadas.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Data Found','No se encontraron datos.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Logs Found.','No se encontraron registros')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Recurrence','Sin recurrencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'None','Ninguno')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Not Started','No ha empezado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'On MCU','En MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Ongoing','En ejecuci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Only one image can be uploaded for footer information.','S�lo se puede cargar un imagen para la informaci�n del pi� de p�gina')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Only two Endpoints can be selected for Point-To-Point Conference.','S�lo dos puntos finales pueden ser seleccionados para la Conferencia de punto a punto.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful','Operaci�n Exitosa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful! Please logout and re-login to see the changes.','�Operaci�n Exitosa!. Por favor, cierre la sesi�n y vuelva a iniciarla para ver los cambios.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful! Reminder email has been sent to selected participant.','�Operaci�n Exitosa!. Correo recordatorio ha sido enviado a los participantes seleccionados.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful! Reminder email has been sent to the person-in-charge of the work order.','�Operaci�n Exitosa!. Correo recordatorio ha sido enviado a la persona encargada de la orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Option name already exists in the list','Nombre de opci�n ya existe en la lista')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Overwrite events older than','Sobrescribir eventos m�s antiguos que')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Page(s):','P�gina(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Paused','En pausa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pending','Pendiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please add atleast one option to the custom option.','Por favor, a�ada al menos una opci�n a la opci�n personalizada.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please associate at least one menu with each work order.','Por favor asocie, al menos, un men� con cada orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check Start and Completed Date/Time for this workorder.','Por favor, revise la fecha/hora de inicio y finalizaci�n para esta orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the password. It must be unique','Por favor, revise la contrase�a. Debe ser �nica')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the start and end time for this workorder.','Por favor, revise la hora de inicio y finalizaci�n para esta orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter a quantity for each selected item. Each item should have a quantity > 0','Por favor, introduzca la cantidad de cada elemento seleccionado. Cada elemento debe tener una cantidad > 0')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter email address for test mail','Por favor, introduzca la direcci�n de correo electr�nico para el correo de prueba')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter the ExchangeID for the profile name ','Por favor, introduzca la ID de Intercambio para el nombre de perfil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter TO and FROM dates.','Por favor, introduzca las fechas DESDE y HASTA.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter valid date range of at most a week.','Por favor, introduzca un rango de d�as valido de al menos una semana.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please fill in all the values for Change Request','Por favor, llene todos los valores para la solicitud de cambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please respond to the following conference','Por favor, responda a la siguiente conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please save or cancel current workorder prior to editing or deleting existing workorders.','Por favor, guarde o cancele la orden de trabajo actual antes de editar o eliminar �rdenes de trabajo existentes.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please save/update or cancel current workorder prior to editing or deleting existing workorders.','Por favor, guarde/actualice o cancele la orden de trabajo actual antes de editar o eliminar �rdenes de trabajo existentes.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Housekeeping Group now.','Por favor, seleccione ahora un grupo de mantenimiento.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Room','Por favor, seleccione un sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a valid IP Address.','Por favor, seleccione una direcci�n de IP v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a valid ISDN Address.','Por favor, seleccione una direcci�n de ISDN v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select an Address Type.','Por favor, seleccione un tipo de direcci�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select an entity','Por favor, seleccione una entidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select at least one room from "Select Rooms" tab in order to create a work order.','Por favor, seleccione por lo menos un sal�n de la pesta�a "Seleccionar Sal�n" para crear la orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select atleast one Department.','Por favor, seleccione por lo menos un departamento.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select atleast one Endpoint.','Por favor, seleccione por lo menos un punto final.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select atleast one MCU.','Por favor, seleccione por lo menos un MCU.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select atleast one Room.','Por favor, seleccione por lo menos un sal�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select atleast one Tier.','Por favor, seleccione por lo menos un nivel.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select MCU.','Por favor, seleccione un MCU.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select the corresponding menu for a valid quantity.','Por favor, seleccione el men� correspondiente a una cantidad v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select...','Por favor, seleccione�')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder.','Por favor, especifique la cantidad y,/o  haga clic en Actualizar para calcular el precio total de esta orden de trabajo o haga clic en Crear para agregar una orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please visit the Advanced Audio/Video Settings tab','Por favor, entre en la pesta�a de Audio/Video Avanzado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point-To-Point Conference','Conferencia de Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Polycom MGC Configuration','Configuraci�n de Polycom MGC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Polycom RMX Configuration','Configuraci�n de Polycom RMX')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Post Conference End','Despu�s del Final de la Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pre Conference Start','Antes del Inicio de la Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PRI Allocation Report','Informe de asignaci�n de PRI')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Radvision MCU Configuration','Configuraci�n de Radvision MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Report is not ready to view.','El informe no est� listo para ver')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room','Salon/ Sala')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Attendee','Asistentes al Sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Conference','sala/salon de conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rooms Found :','Salones Encontrados:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Running','Ejecut�ndose')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled','Programado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Result(s) for Audio/Visual Work Orders','Resultados de la b�squeda(s) para �rdenes de trabajo Audio/Visual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Result(s) for Catering Work Orders','Resultados de la b�squeda(s) para �rdenes de trabajo de catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Result(s) for Housekeeping Work Orders','Resultados de la b�squeda(s) para �rdenes de trabajo de mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Results','Resultados de la b�squeda')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'See Recurring Pattern','Ver Patr�n de recurrencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Select a Set from the Drop down list.','Seleccione un grupo de la lista desplegable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Select Caller/Callee','Seleccione al remitente / destinatario de la llamada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Selected bridge for this template does not support MPI calls.','Puente seleccionado para esta plantilla no admite llamadas MPI.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Set Customized Instances','Establecer instancias personalizadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Show Uploaded Files','Mostrar archivos cargados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Some Additional Comments fields are missing. Please contact your VRM Administrator.','Faltan algunas casillas de comentarios adicionales. Por favor, p�ngase en contacto con el administrador del VRM.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Status being Updated.','El estado se est� actualizando')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Stopped','Parado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Submit','Entregar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Submit Conference','Entregar Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tandberg MCU Configuration','Configuraci�n Tandberg MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Terminate','Finalizar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Terminated','Finalizado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Audio/Visual work orders associated with this conference.','No hay �rdenes de trabajo de audio/visual asociadas con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Catering work orders associated with this conference.','No hay �rdenes de trabajo de Catering asociadas con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Housekeeping Groups available for this room. Please select another room.','No hay grupos de mantenimiento asociados con este sal�n. Por favor, seleccione otro sal�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Housekeeping work orders associated with this conference.','No hay �rdenes de trabajo de mantenimiento asociadas con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no menus associated with the selected Service Type for this location. Please select a new Service Type for this location OR click Cancel to return to the catering work orders summary page for this conference.','No hay men�s asociados con el tipo de servicio seleccionado para esta ubicaci�n. Por favor, seleccione un nuevo tipo de servicio para esta ubicaci�n o haga clic en Cancelar para volver a la p�gina de resumen de las �rdenes de trabajo de catering para esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Workorders associated with this conference','No hay �rdenes de trabajo asociadas con esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'This value has already been taken.','Este valor ya se us�.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'This Week Conferences Total Usage Report',' Informe de uso total de conferencias de esta semana ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Title','T�tulo ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.','Para modificar una orden de trabajo actual haga clic en el enlace Editar o Borrar asociados con esa orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the button below.','Para a�adir una nueva orden de trabajo haga clic en el bot�n de abajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the button below. OR click EDIT to edit an existing work order.','Para a�adir una nueva orden de trabajo haga clic en el bot�n de abajo o haga clic en EDITAR para modificar una orden de trabajo actual.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new work order, click on Add New Work Order button.','Para crear una nueva orden de trabajo, haga clic en el bot�n Agregar Nueva Orden de Trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.','Para crear una nueva orden de trabajo haga clic en el bot�n Agregar nueva orden de trabajo. Para modificar una orden de trabajo actual haga clic en los enlaces Editar o Borrar asociados con esa orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new work order, click the button below.','Para crear una nueva orden de trabajo haga clic en el bot�n de abajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new work order, please click on the button below.','Para crear una nueva orden de trabajo, por favor haga clic en el bot�n de abajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Todays Audio/Visual Work Orders','�rdenes de trabajo de Audio/Visual de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Todays Catering Work Orders','�rdenes de trabajo de Catering de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Todays Housekeeping Work Order','�rdenes de trabajo de Mantenimiento de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Totals:','Totales:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Un Mute','Activar sonido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'UnBlock','Quitar Bloqueo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Undefined','Indefinido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Undelete','Recuperar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Update','Actualizar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Usage By Room','Uso por sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User','Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Version Numbe','N�mero de Versi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Version Number','N�mero de Versi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Conference','Ver conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Conflict','Ver conflicto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Menus','Ver Men�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VRM Service is not installed','El Servicio VRM no est� instalado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VTC Calendar','Calendario VTC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VTC Contact List','Lista de Contactos VTC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VTC Daily Schedule','Horario Diario VTC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VTC Resource Allocation Report','Informe de Asignaci�n de Recursos VTC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'We are sorry that you will not be able to join the following conference','Lamentamos que usted no podr� unirse a la siguiente conferencia ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Workorders can only be created with rooms selected for this conference.','Las �rdenes de trabajo s�lo se pueden crear con salones seleccionados para esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Year To Date Conferences Total Usage Report','Informe de uso total de conferencias en el a�o hasta la fecha')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Yesterday Conference Total Usage Report','Informe de uso total de conferencias de ayer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'You have no Pending or Future Conferences.','No tiene conferencias pendientes ni futuras.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Items...','No hay elementos�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Already on last page','Ya est� en la �ltima p�gina')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'next page','P�gina Siguiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Already on first page','Ya est� en la primera pagina')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'N/A','N/D')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Session expired','Sesi�n caducada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again. Hours of operation:','El sistema VRM no est� disponible en la fecha/hora elegida. Por favor, compruebe la disponibilidad del sistema VRM y vuelva a intentarlo. Horario de funcionamiento:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Closed','Cerrado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Any','Cualquier')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connecting','Conectando')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disconnecting','Desconectando')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Records:','Registros totales:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Room first from the drop down below.','Por favor, primero seleccione un sal�n en el men� desplegable.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'If there are no rooms in the list, click on Select Rooms option available on your left.','Si no hay salones en la lista, haga clic en la opci�n Seleccionar Salones disponible a su izquierda.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Cancel before you proceed to another step.','Haga clic en Cancelar antes de seguir al siguiente paso.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'If there are no rooms in the list, click on "Select Rooms" option available on your left.','Si no hay salones en la lista, haga clic en la opci�n "Seleccionar Salones" disponible a su izquierda.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Select a Set from the A/V Set list.','Seleccione un grupo de la lista A/V')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'If there are no items in the list, then there are no sets associated with this room. ','Si no hay elementos en la lista, entonces no hay grupos asociados a esta sala.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Duration:','Duraci�n:')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.','para asistencia. <br />Una vez que la configuraci�n sea confirmada, por favor haga clic en la pesta�a "Vista Previa" y vuelva a enviar la conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please confirm settings below. If changes are required, please edit the fields accordingly OR contact technical support ','Por favor, confirme la configuraci�n siguiente. Si se requieren cambios, por favor, edite los campos correspondientes o contacte con el soporte t�cnico')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'for assistance.<br />Once settings are confirmed please click Preview tab and resubmit the conference.','para asistencia. <br />Una vez que la configuraci�n sea confirmada por favor haga clic en la pesta�a "Vista Previa" y vuelva a enviar la conferencia.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Audio/Visual work orders associated with this conference.<br />To create a new work order, click the button below.','No hay �rdenes de trabajo de audio/visual asociadas con esta conferencia. Para crear una orden de trabajo nueva, haga clic en el siguiente bot�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Item','Elemento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' is duplicated!','�est� duplicado!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation failed: Invalid security permissions on ','Error en la operaci�n: Permisos de seguridad inv�lidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'You have successfully logged out of myVRM.','Ha cerrado satisfactoriamente la sesi�n de myVRM .')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error 122: Please contact your VRM Administrator','Error 122: Por favor, contacte con su administrador VRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New','Crear Nuevo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Locations','Sin ubicaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Restore User','Restaurar Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inactive Users','Usuario Inactivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Floor / room number','Numero de piso/salon/sala')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address and Directions','Direcci�n e indicaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Parking directions','Indicaciones para estacionamiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Map Link','V�nculo del Mapa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Time Zone','Zona horaria')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Phone number','Numero de telefono de la sala/salon')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Maximum capacity','Capacidad M�xima')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Setup time buffer','Configurar Regulador de la Hora')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Teardown time buffer','Regulador de Hora de Desmontaje')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Projector Available','Proyector Disponible')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room images','Im�genes del Sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Assistant-in-charge','Asistente encargado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Map Images','Im�genes del Mapa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Security images','Im�genes de Seguridad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Misc. Attachments','Adjuntos Miscel�neos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Approvers','Aprobadores')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints','Puntos finales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Departments','Departamentos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Top > Middle Tier','Nivel superior>Medio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Records per page','Registros por p�gina')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Video Rooms','M�ximo de salones de v�deo permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Non-Video Rooms','M�ximo de salones sin v�deo permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed MCU','M�ximo de MCU permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Endpoints','M�ximo de puntos finales permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Users','M�ximo de usuarios permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Exchange Users','M�ximo de usuarios de intercambio permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Domino Users','M�ximo de usuarios de domino permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Mobile Users','M�ximo de usuarios de m�viles permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Catering','Catering m�ximo permitido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed HouseKeeping','Mantenimiento m�ximo permitido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed APIs','APIs m�ximos permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Facilities','M�ximo n�mero de Instalaciones permitidas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Your Email has been blocked since ','Su correo ha sido bloqueado desde')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Email has been blocked since ','Correo de Organizaci�n ha sido bloqueado desde')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'. Please contact administrator for further assistance.','Por favor, contacte con el administrador para obtener m�s ayuda.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio/Video<br />Settings','Configuraci�n <br />Audio/Visual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio/Video','Audio/V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this MCU?','�Est� seguro de que quiere eliminar este MCU?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this hearing?','�Est� seguro de que quiere eliminar esta audiencia?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this conference?','�Est� seguro de que quiere eliminar esta conferencia?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete and Clone this conference?','�Est� seguro de que quiere eliminar y clonar esta conferencia?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Workorder?','�Est� seguro de que quiere eliminar esta orden de trabajo?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this department?','�Est� seguro de que quiere eliminar este departamento?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Menu?','�Est� seguro de que quiere eliminar este men�?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Billing Code?','�Est� seguro de que quiere eliminar este c�digo de facturaci�n?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this inventory?','�Est� seguro de que quiere eliminar este inventario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Item?','�Est� seguro de que quiere eliminar este elemento?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this user?','�Est� seguro de que quiere eliminar este usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this endpoint?','�Est� seguro de que quiere eliminar este punto final?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to mute/unmute this endpoint?','�Est� seguro de que quiere desactivar/activar el sonido de este punto final?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to Connect/disconnect this endpoint ?','�Esta seguro de que quiere conectar/desconectar este punto final?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Option?','�Est� seguro de que quiere eliminar esta opci�n?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Group ?','�Est� seguro de que quiere eliminar este grupo?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this SP Guest?','�Est� seguro de que quiere eliminar este Invitado SP?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this organization profile?','�Est� seguro de que quiere eliminar el perfil de esta organizaci�n?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Template ?','�Est� seguro de que quiere eliminar esta plantilla?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this user template?','�Est� seguro de que quiere eliminar esta plantilla de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this middle tier?','�Est� seguro de que quiere eliminar este nivel medio?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this tier ?','�Est� seguro de que quiere eliminar este nivel?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to ACTIVATE this user account ?','�Est� seguro de que quiere ACTIVAR esta cuenta de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to DELETE this user account. This user will not longer be available to restore ?','�Est� seguro de que quiere ELIMINAR esta cuenta de usuario? Este usuario no estar� disponible para su recuperaci�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Custom Option?','�Est� seguro de que quiere eliminar esta Opci�n personalizada?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to edit this Custom Option?','�Est� seguro de que quiere editar esta Opci�n personalizada?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new work order, click the Add New Work Order button.','Para crear una nueva orden de trabajo, haga clic en el bot�n Agregar Nueva Orden De Trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last','�ltimo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'First','Primero')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Prev','Anterior')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Next','Siguiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Activate','Activar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deactivate','Desactivar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select MCU for user:','Por favor, seleccione el MCU para el usuario:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Selected bridge for user ','Puente seleccionado para el usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' does not support MPI calls.','No soporta llamadas MPI.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'An audio or video conference cannot be created with room ','Una conferencia de Audio o V�deo no puede ser creada con un sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' since there is no endpoint associated with it','ya que no hay un punto final asociado con �l')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Profile for room: ','Por Favor, Seleccione el perfil del sal�n:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit All','editar todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No conflict','sin conflicto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Facility','Instalaci�n de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint','Punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No SP Guest found','No se encuentra Invitado SP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Work Order','Crear nueva orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rejected','Denegado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Accepted','Aceptado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Undecided','Pendiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Request for Reschedule','Solicitud para reprogramaci�nRequest for Reschedule')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'rooms','Salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Page','P�gina')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint<br>Name','Nombre de<br>Punto final')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room/Attendee<br>Name','Nombre de<br>Sala/Invitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Unreachable','Inalcanzable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Online','En l�nea')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disconnect','Desconectar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connected','Conectado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Endpoints:','Puntos finales totales:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Updated:','�ltima actualizaci�n:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'hour(s)','Hora(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'min(s)','Minuto(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delete','Eliminar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Records Found.','No se encontraron registros')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Provider','Proveedor de catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Entity Codes displayed in','C�digos de entidad mostrados en')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Day Color','Administrar Color del D�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Date.','Fecha inv�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Activated','Activado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deactivated','Desactivado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage','Administrar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'A/V Workorders','�rdenes de trabajo de A/V')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Bulk Tool Facility','Funci�n para Herramientas al por mayor')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Customize Organization Emails','Personalizar Correos-e de la organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Cutomize my Emails','Personalizar mis correos-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dashboard','Tablero de mandos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express form','Formulario expr�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'LDAP Directory Import','Importaci�n del directorio LDAP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Active Users','Administrar Usuarios activos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Catering items','Administrar elementos de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Email Queue','Administrar la cola de correos-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Endpoints','Administrar puntos finales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Groups','Administrar grupos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Housekeeping items','Administrar elementos de mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Inventory Sets','Administrar grupos de inventario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage MCU','Administrar MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage my Lobby UI','Administrar mi Lobby IU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Organization Custom Options','Administrar las opciones de personalizaci�n de la organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Organization Set-up','Administrar la configuraci�n de la organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Organization UI Text','Administrar el texto UI de la organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Roles','Administrar roles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Rooms','Administrar salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Templates','Administrar plantillas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Advance Reports','Mis Informes de Avance')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Default Search','Mi b�squeda por defecto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Organization Options','Mis Opciones de Organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Organization UI','Mi UI de Organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Reports','Mis Informes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Settings','Mi Configuraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Restore Users','Restaurar Usuarios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Settings','Configuraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Settings','Configuraci�n del sitio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Systems Diagnostics','Diagn�stico de sistemas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Approval Pending','Ver Aprobaci�n pendiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Catering Workorder','Ver Orden de trabajo de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Housekeeping workorders','Ver �rdenes de trabajo de mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View My Reservations','Ver Mis Reservas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Ongoing','Ver En ejecuci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Pending','Ver Pendientes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Personal Calendar','Ver Calendario Personal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Public Conferences','Ver Conferencias p�blicas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Room Calendar','Ver Calendario de salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Scheduled Calls','Ver Llamadas programadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Updated for Spanish','Actualizado para Espa�ol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'% usage (Avail. Hrs)','% de uso (Horas disponibles)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(Applicable for all instances)','(Aplicable a todas las instancias)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'account expiry','vencimiento de cuenta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Active','Activo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Actual','Real')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Additional Information','Informaci�n adicional')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'address','direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'address type','tipo de direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'admin','admin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Aggregate Usage','Uso conjunto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ago','hace')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'All','Todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'and','y')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Approval','Aprobaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'April','Abril')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'are invalid characters.','son caracteres no v�lidos.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'are linked to the selected entity option and data will be lost.','est�n enlazados a la opci�n de entidad seleccionada y los datos se perder�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to BLOCK this user account?','�Est� seguro de que quiere BLOQUEAR esta cuenta de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Day Color?','�Est� seguro de que quiere eliminar este Color del D�a?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Entity Code?','�Est� seguro de que quiere eliminar este c�digo de Entidad?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to DELETE this user account?','�Est� seguro de que quiere ELIMINAR esta cuenta de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to edit this Entity Code?','�Est� seguro de que quiere editar este c�digo de Entidad?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to UNBLOCK this user account?','�Est� seguro de que quiere DESBLOQUEAR esta cuenta de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to UNDELETE this user account?','�Est� seguro de que quiere RECUPERAR esta cuenta de usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'At least one Inventory item should NOT be marked for deletion.','Al menos un elemento del Inventario NO deber�a estar marcado para su eliminaci�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attachment 1 is greater than 10MB. File has not been uploaded.','Archivo adjunto 1 es mayor que 10 MB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attachment 2 has already been uploaded.','El archivo adjunto 2 ya ha sido cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attachment 3 has already been uploaded.','El archivo adjunto 3 ya ha sido cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attachment 3 is greater than 10MB. File has not been uploaded.','Archivo adjunto 3 es mayor que 10 MB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Attachments','Archivos Adjuntos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio Info','informaci�n de Audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio-Only','S�lo audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'August','Agosto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'available hrs','horas disponibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Bill to host','Factura al anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Bill to individual','Factura al individuo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Blocked ','Bloqueado ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Both','Ambos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Breakfast','Desayuno')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'bridge name','nombre del puentes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Browser Agent','Agente de navegaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Browser Language','Idioma de Navegaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Admin','Admin de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Remainder','Restos de Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'cell','telf. m�vil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'cell #','n� m�vil')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Choose Add Audio Bridge button and select the desired audio bridge','Elija el bot�n A�adir Puente de Audio y seleccione el puente de audio que desee')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click to show room resources','Haga clic para ver los recursos del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Coffee Service','Servicio de caf�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Comment','Comentario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Completed Conference(s)','Conferencia(s) completada(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'concierge support search','B�squeda de soporte de Conserje')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'conf #','n� conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf Datetime','Fecha/Hora de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference','Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'conference date','Fecha de conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Name','Nombre de la conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Report','Informe de conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Room B','Sal�n de conferencia B')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Status','Estado de la conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Type','Tipo de conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conferences','Conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conferences Usage Report','Informe de uso de Conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connection Details','Detalles de Conexi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'contact name','nombre de contacto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'contact no','n� de contacto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Color Day','Crear nuevo D�a de Color')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Currency Format','Formato de divisa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Current Status','Estado actual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'CurrentIsdnCost','Coste actual de Isdn')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom','Personalizado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Date','Fecha personalizada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Options','Opciones Personalizadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Customized','Personalizado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Daily','Diariamente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Data Table has been generated successfully!','�La tabla de datos se ha generado correctamente!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'date','fecha')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'day','d�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Debug','Depurar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'December','Diciembre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'dedicated VNOC operator','operador VNOC dedicado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'default profile','perfil por defecto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'deleted conference','conferencia eliminada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery and Pickup','Enviar y Recoger')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery Cost','Coste de Env�o')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery Datetime','Fecha/Hora de Entrega')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery Only','solo enviar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery Type','Tipo de entrega')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Equipment pickup','recoger equipo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Description','Descripcion ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'dialing option','opci�n de marcaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dinner','Cena')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disabled','Inhabilitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Drag a column header here to group by that column','Arrastre un encabezamiento de columna aqu� para agrupar por dicha columna')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Duration','Duracion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'duration(min)','duraci�n (min)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Audio Bridge','Editar Puente de Audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Color Day Details','Editar Detalles del D�a de color')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'end','fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'End_by Datetime','Terminar_Fecha/Hora')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'endpoint name','nombre de punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint Reports','Informes de Punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint Selection','Selecci�n de Punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'entity code','c�digo de entidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express Conference','Conferencia expr�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'February','Febrero')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Feedback','Comentario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'File attachment is greater than 100kB. File has not been uploaded.','El archivo adjunto es mayor que 100kB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'firmware version','versi�n del firmware')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Forgot Password','Contrase�a olvidada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'fourth','cuarto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Friday','Viernes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Future','Futuro')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Future Conference(s)','Conferencia(s) futura(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Gold','Oro')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Halal','Halal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'High Resolution Banner exceeds the Resolution Limit','El cartel de alta resoluci�n excede el l�mite de la resoluci�n ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HK Admin','Admin HK')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HK Remainder','Resto de HK')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host','Anfitrion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'hours per working day','horas por d�a laborable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Ical_Emails','correos-e_Ical')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inactive ','Inactivo ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Information','Informaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'instances','ejemplos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Conference Service ID','ID del Servicio de conferencia inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid IP Address for user','Direcci�n IP inv�lida para el usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid ISDN Address for user','Direcci�n ISDN inv�lida para el usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Time (HH:mm)','Hora inv�lida (HH:mm)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Admin','Admin de Inventario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Remainder','Restos de Inventario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Set Used','Grupo de Inventario Usado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IsdnThresholdCost','Coste Umbral Isdn')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'January','Enero')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'July','Julio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'June','Junio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Kosher','Kosher')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Month','�ltimo mes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Week','�ltima semana')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Lobby Management','Administraci�n de vest�bulo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Location','Ubicaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Location Info','informaci�n de ubicaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Location(s)','Ubicaci�n(es)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Location(s) Report','Informe de Ubicaci�n(es)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Locations','Ubicaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Locations(Country/State) Report','Informe de Ubicaciones (Pa�s/Estado)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Locations(Zip Code) Report','Informe de Ubicaciones (C�digo postal)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Login','Inicio de Sesi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Low-Cal.','Baja-Cal.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Lunch','Almuerzo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mail Logo','Logotipo de Correo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mail Server Test Connection','Conexi�n de prueba del Servidor de correo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Maintenance','Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Conference','Administrar Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manage Email Domains','Administrar Dominios de Correo-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'March','Marzo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Notes Users','M�ximo de usuarios de Notes permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed Outlook Users','M�ximo de usuarios Outlook permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed PC','N�mero m�x PCs permitidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'May','Mayo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Admin','Admin MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Alert To Host','Alerta MCU al Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Alert To MCU Admin','Alerta MCU al Admin MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Approval Request','Solicitud de aprobaci�n MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Details','Detalles MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Info','informaci�n de MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Names','Nombres MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Reports','Informes MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Selection','Selecci�n MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'mcu usage','Uso MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU(All) Report','Informe MCU (todos)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'media','medios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'meet and greet','reuni�n informal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'meeting tilte','t�tulo de la reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Meeting Type','Tipo de reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'middle tier','nivel medio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'min','min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'model','modelo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Monday','Lunes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Month(s)','Mes(es)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Monthly','mensual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Monthly Room Usage','Uso mensual del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Name','Nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Endpoint(s) associated with room: ','No hay punto(s) final(es) asociado(s) con este sal�n:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Endpoint(s) associated with this room','No hay punto final asociado con este sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Endpoints selected','Sin punto final seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Records','Sin Registros')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No room layouts defined','Sin distribuci�n del sal�n definida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'None...','Ninguno...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Note: to add an audio bridge to your conference, 1) complete the information on this page, 2) under the Select Participants tab, ','Nota: para a�adir un puente de audio a su conferencia, 1) completo la informaci�n de esta p�gina, 2) en la pesta�a "Seleccionar Participantes", ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Note: to complete request to add an audio bridge to your conference, ','Nota: para completar la solicitud para a�adir un puente de audio a su conferencia, ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Note: to complete request to add an audio bridge to your conference, choose Add Audio Add-On Bridge button and select the desired audio bridge','Nota: para completar la solicitud para a�adir un puente de audio a su conferencia, elija "A�adir Audio" en el bot�n "Puente para A�adir" y seleccione el puente de audio deseado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'November','Noviembre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'October','Octubre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'office phone #','n� telf. de la oficina')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Ongoing conference','Conferencia en curso')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful! Please switch to respective organization to see the changes.','�Operaci�n Exitosa! Por favor, cambie a la organizaci�n respectiva para ver los cambios.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Option Name already exists in the list for Language','Nombre de opci�n ya existe en la lista para el Idioma')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Reports','Informes de organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant','Participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant Acceptance Email','Correo-e de aceptaci�n del participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant Remainder','Restos de Participantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant Request Change','Cambio de solicitud de participantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Password','Contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Past Conference(s)','Conferencia(s) pasada(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pending conference','Conferencia pendiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pending Status','Estado Pendiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'percentage of total audio conferences','porcentaje del total de conferencias de audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'percentage of total audio-video conferences','porcentaje del total de conferencias de audio-v�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'percentage of total point-to-point conferences','porcentaje del total de conferencias punto a punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'percentage of total room conferences','porcentaje del total de conferencias de sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'percentage of usage','porcentaje de uso')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Person_in_charge','Persona encargada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Personal report','Informe personal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pick up only','solo recoger')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Platinum','Platino')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please add option Name to Language English.','Por favor, a�ada el Nombre de opci�n al Idioma Ingl�s.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please contact administrator for further assistance.','Por favor, contacte con el administrador para obtener m�s ayuda.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select one...','Por favor, seleccione una�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please set the values for the mandatory custom options.','Por favor, establezca los valores para las opciones personalizadas obligatorias.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please Upload the File','Por favor, cargue el archivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point to Point','Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Preferred Login ID','ID de Inicio de Sesi�n Preferida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PRI','PRI')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'primary approver','Aprobador principal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Private','Privado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'protocol','protocolo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Public','P�blico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'public conference','conferencia p�blica')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Recipient','Destinatario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Recurrence Pattern','Forma de repetici�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Recurring Hearing','Audiencia recurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'remaining','restante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'remarks','observaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Report name is already exists.','El nombre del informe ya existe.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Request Account','Solicitar Cuenta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requested Datetime','Fecha/Hora Solicitada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requested Duration','Duraci�n Solicitada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requested Item','Art�culo Solicitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Required','Necesario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Reservation conference','Conferencia de reserva ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'role','role')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Approval Request','Solicitud de aprobaci�n del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room In-charge','Sal�n encargado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'room name','nombre del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Only','S�lo sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'room phone #','n� telf. del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Reports','Informes del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Selection','Selecci�n de sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'room type','tipo de sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'room usage','uso del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ROOM USAGE BY SITE','USO DEL SAL�N POR SITIO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rooms Assigned','Sal�n Asignado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Saturday','S�bado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Saved Successfully.','Guardado correctamente.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled Conference(s)','Conferencia(s) programada(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduler','Planificador')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'second','segundo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Security Desk Only','S�lo Mesa de seguridad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'September','Septiembre')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Service Charge','Cargo de Servicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Service Type','Tipo de servicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Setup','Instalacion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Silver','Plata')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Logo  exceeds the Resolution Limit','El logo del sitio excede el l�mite de la resoluci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Logo attachment is greater than 10MB. File has not been uploaded.','El logo del sitio adjunto es mayor que 10 MB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Reports','Informes del sitio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Snack','Tentempi�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'special instructions','instrucciones especiales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Standard Resolution Banner attachment is greater than 100kB. File has not been uploaded.','El cartel de resoluci�n est�ndar adjunto es mayor que 100kB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Standard Resolution Banner exceeds the Resolution Limit','El cartel de resoluci�n est�ndar excede el l�mite de la resoluci�n ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'start','inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start - End','Inicio - Fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start Datetime','Fecha/Hora de Inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start_by Datetime','Empezar_Fecha/Hora')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sunday','Domingo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Survey Email','Correo-e de encuesta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Survey URL','URL de encuesta')
/*==========================================================================================*/
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System','Sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Approval Request','Solicitud de aprobaci�n de sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Error.Please contact your VRM Administrator and supply this error code.','Error del sistema. Por favor, contacte con su Administrador VRM y d�gale este c�digo de error.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Teardown','Desmontaje')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tech Contact ','Contacto Tecnico ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tech Email','Correo Electronico Tecnico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tech Phone','Telefono Tecnico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'terminated conference','conferencia finalizada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Test Mail Connection','Conexi�n del correo de prueba')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Thank you for the feedback.','Gracias por sus comentarios.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Audio/Visual work orders associated with the selected Room','No hay �rdenes de trabajo de audio/visual asociadas con el sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Catering work orders associated with the selected Room','No hay �rdenes de trabajo de Catering asociadas con el sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Housekeeping work orders associated with the Selected Room','No hay �rdenes de trabajo de mantenimiento asociadas con el sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no user templates available. Please create at least one user template and proceed with LDAP Import','No hay plantilla de usuarios disponibles. Por favor, cree al menos una plantilla de usuario y proceda con la Importaci�n LADP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There is no location for this conference','No hay ubicaci�n para esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There were too many matches in your search,please narrow your search','Hay demasiadas coincidencias en su b�squeda, por favor ajuste m�s la b�squeda')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'third','tercero')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'This Week','Esta semana')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Thursday','Jueves')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'tier1','nivel1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'tier2','nivel2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Time Remaining','Tiempo restante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'to','para')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To be completed','Para ser completado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'to Week','para la semana')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'top tier','nivel superior')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Top Tier(s) are imported successfully!','�El nivel superior ha sido importado correctamente!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total','Total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Audio Conferences','Total de Conferencias de audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Audio-Video Conferences','Total de Conferencias de Audio-V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Conferences','Total de conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Cost','Coste Total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Duration(Hours)','Duraci�n total (horas)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'total hrs','horas totales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Minutes','Minutos totales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Participants','Total de participantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Point-to-Point Conferences','Total de Conferencias Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Room Conferences','Total de Conferencias en Sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total(Week ','Total(Semana ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tuesday','Martes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'type','tipo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Type instructions here','Escriba las instrucciones aqu�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Update Search Parameters for Template:','Actualizar los Par�metros de B�squeda para la plantilla:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Usage By Room - ','Uso por Sal�n - ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User only','S�lo usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Selection','Selecci�n de usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Users Datetime','Fecha/Hora de Usuarios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Users IP Address','Direcci�n IP del Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Vegetarian','Vegetariano')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'video conferences','v�deo-conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Inventory workorders','Ver �rdenes de trabajo de Inventario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Public','Ver P�blico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Reservations','Ver Reservas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual','Virtual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Warning','Advertencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Warning: This is an IRREVERSIBLE process. Once purged the data cannot be recovered.Are you sure you want to continue ?','Advertencia: Este es un proceso IRREVERSIBLE, Una vez purgado los datos no se prodr�n recuperar. �Est� seguro de querer continuar?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Web Page','Pagina Web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Website URL','URL de Sitio Web ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Wednesday','Mi�rcoles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Week ','Semana ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'weekday','D�a laborable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'weekend','Fin de semana')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Weekly','Semanalmente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Weekly Room Usage','Uso semanal del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'work','trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work Order Name','Nombre de Orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'work orders','�rdenes de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work.Dys','D�as de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Workorder Comment','Comentario de Orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Year to Date','A�o hasta la fecha')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Yearly','Anualmente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Yesterday','Ayer')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'You must UNBLOCK this user before editing their profile.','Debe DESBLOQUEAR este usuario antes de editar su perfil.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid IP Address:','Direcci�n IP Inv�lida:')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Successful!','Operaci�n Exitosa!')

-- V2.8
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Protocol does not match with selected Address Type.','Protocolo no se ajusta con el tipo de direcci�n seleccionada.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Address.','Direcci�n no v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'validation','validaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'CancelEndpoint','Cancelar_punto_final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Submit Endpoint ','Entregar punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Call Monitor','Monitor de Llamadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Call Monitor (P2P)','Monitor de Llamadas (P2P)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View VNOC Conference','Ver Conferencia VNOC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'new','nuevo')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'English','Ingl�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Approver','Aprobador del Sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Send Reminder','Enviar Recordatorio')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery<br>Cost($)','Coste de<br>Entrega ($)')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Service<br>Charge($)','Tarifa del<br>Servicio ($)')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'EM7 Online<br> Status','Estado<br> EM7 en l�nea')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address<br>Type','Tipo de<br>Direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total MCUs','MCUs totales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Send Survey','Enviar encuesta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'are invalid characters','son caracteres no v�lidos.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'invalid','no v�lido')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Force MCU Pre start & end','Forzar el inicio y final del Previo MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU pre end time is not allowed more than 15 mins','No est� autorizada una hora final del Previo MCU superior a 15 minutos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Pre End by','Final del Previo MCU antes de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU pre start time is not allowed more than 15 mins','No est� autorizado una duraci�n del Previo MCU mayor de 15 minutos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Pre Start by','Inicio del Previo MCU antes de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tear-Down Time','Hora de desmontaje')



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference duration range 15 to 1440 minutes.','La duraci�n de las conferencias es de 15 a 1440 minutos.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default Conference Duration','Duraci�n predeterminada de la Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Enable Smart Point-to-Point','�Habilitar Punto a Punto inteligente?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Send MCU Alert Mail','Enviar Mensaje de Alerta MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default LineRate','Velocidad_L�nea predeterminada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External','Externa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Summary','Resumen de Conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The selected time is already defined for other message','El tiempo seleccionado ya est� definido para otro mensaje')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Action','Acci�n')



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio Settings','Configuraci�n de audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Add Audio Bridge','A�adir puente de audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'instances/occurrences in the recurring series','casos/sucesos en las series recurrentes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Note: Maximum limit of','Nota: L�mite m�ximo de ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'of','de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'hours including buffer period','horas incluyendo  periodo de almacenamiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Manual','Manual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Automatic','Autom�tico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start Mode','Modo inicial')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'This conference type is being changed to point-to-point and will not use an MCU connection','Se cambiar� en tipo de conferencia a Punto-a-Punto y no usar� conexi�n MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'day(s)','d�a(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please click Create/Edit or Cancel prior to clicking Next','Por favor, haga ''clic'' en Crear/Editar antes de hacer ''clic'' en Siguiente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Less','Menos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Date/Time','Fecha/Hora')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work Orders Management','Administraci�n de �rdenes de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'UNSUCCESS','FALLO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'SUCCESS','�XITO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Join','Unirse')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Recurring Conference','Conferencia recurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf Mode','Modo Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Silo Name','Nombre del Silo')





Insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Meeting Title','T�tulo de la reuni�n')
Insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'No Data to display','No hay datos para presentar')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Message','Mensaje')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Duration (Min)','duraci�n (min)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Call Detail Records','Registros de detalles de llamada')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Requestor','Solicitante')  
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'VNOC operator','Operador VNOC')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Call URI','Llamada URI')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Hours','horas')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Minutes','minutos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Connects','Conecta')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Max Allowed Guest Rooms','M�ximo de salones de Invitados permitidos')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'More','m�s')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'User Reports','Informes de Usuario')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Create New H/K Group','Crear Nuevo Grupo de H/K')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Callee','Destinatario')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Caller','Remitente')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Select','Seleccionar')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Timezone','Zona horaria')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'Unique ID','ID unica')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Status','Estado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference ID','ID de la conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Task Type <br/> Trans Id','Task Type <br/> Trans Id')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Customer Id','ID del Cliente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Issued By <br/> Issued On','Generado por <br/> generado a')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid account expiration date.','La fecha de caducidad de la cuenta no es v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid account expiration date. Please check the license.','La fecha de caducidad de la cuenta no es v�lida.  Por favor, revise la licencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter a valid Duration.','Por favor, introduzca una Duraci�n v�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error Event Report','Informe de eventos de error')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Event Failure Mail','Correo de fallo de evento')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'AVOnsiteSupportEmail','Correo-e de soporte del sitio AVO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Report Email','Correo-e de informe de usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU ISDN Threshold alert','Alerta de umbral MCU ISDN')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room/Attendee','Sala/Invitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Email','Correo-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'First Name','Nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Last Name','Apellido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requestor Name','Nombre del solicitante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Personal Audio Info','Informaci�n de audio personal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Accept/Decline','Aceptar/Rechazar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Post Conf Scheduling','Programaci�n post-Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Name','Nombre de Usuario')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in to MCU','marque al MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-out from MCU','Marque afuera desde el MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Direct to MCU','Directamente al MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'General User','usuario general')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Administrator','Administrador de organizacion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Administrator','Administrador del sitio web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Administrator','Administrador del servicio de comida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inventory Administrator','Administrador del inventorio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Housekeeping Administrator','Administrador de limpieza')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express Profile','Perfil express')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express Profile Manage','maneje el perfil express')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express Profile Advanced','maneje el perfil avanzado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View-Only','solo-ver')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host Name','Nombre del organizador')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'OS Name','Nombre OS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'OS Manufacturer','fabricador OS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'OS Configuration','Configuracion OS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'OS Build Type','tipo construente OS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Registered Owner','Propietario Registrado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Registered Organization','organizacion registrada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Product ID','Identificacion del Producto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Original Install Date','duracion inicial del sistema modelo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Manufacturer','Fabricador del sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Model','modelo del sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Type','tipo de sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Processor(s)','procesor(es)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'BIOS Version','Versi�n BIOS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Windows Directory','Directorio de Windows')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Directory','directorio del sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Boot Device','maneje el perfil express')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Locale','sistema regional')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Input Locale','entrada regional')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Physical Memory','Memoria fisica en total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Page File Location(s)','localizacion del archivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Domain','Domininio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Logon Server','tarjeta(s) del la red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Network Card(s)','tarjeta(s) del la red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IP address(es)','Direcci�n IP')
----
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'World Wide Time Zones','zona horaria Mundial')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Hotfix(s)','urgente revision(es)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'U.S Time Zones','zona horaria EEUU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please associate at least one Menu Item with all Menus.','Por favor asocie aunque sea un articulo del menu con el resto del los menus')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Computer Name','Nombre de ordenador')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Housekeeping Group','grupo de housekeeping')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'OS Name','Nombre del SO')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'OS Version','Versi�n del SO')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'.Net Version','Versi�n .Net')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Computer Manufacturer','Fabricante de ordenadores')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Computer Model','Modelo de ordenador')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Not Specified','No especificado')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Off','apagado')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'1 day','1 dia')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'3 days','3 dias')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'7 days','7 dias')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Sytem Errors','Errores del sistema')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'System and User Errors','El Sistema y Errores del Usuario')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Warnings (and all above)','Advertencia ( y todo en total)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Information (and all above)','Informacion ( y todo en total)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Debug (and all above)','depurar ( y todo en total)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Audio-Only Conference','Solo-Audio Conferencia')
--
-- ZD 100288 

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Ongoing Conferences','Conferencias en curso')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Reservations','Reservas')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Public Conferences','Conferencias P�blicas')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Pending Conferences','Conferencias Pendientes')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Approval Pending','Aprobaciones pendientes')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Manage My Lobby','Administrar ''Mi Vest�bulo''')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Bulk User Management','Administraci�n de usuarios al por mayor')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Audiovisual Work Orders','�rdenes de trabajo audiovisuales')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Facility Services Work Orders','�rdenes de trabajo de Servicios de las instalaciones')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Audiovisual Inventories','Inventorios audiovisuales')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Catering Menus','Men�s de catering')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Facility Services','Servicios de las instalaciones')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'New Conference','Conferencia Nueva')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Conference Dashboard','Tablero de control de la conferencia')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Customize My Email','Personalizar Mi Correo electr�nico')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Diagnostics','Diagn�sticos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Express New Conference','Nueva Conferencia expr�s')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Reports','Informes')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Organization Options','Opciones de organizaci�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Manage Blocked Email','Administrar correos electr�nicos bloqueados')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Manage Organizations','Administrar organizaciones')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Manage User Roles','Administrar funciones del usuario')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Organization Custom Options','Opciones personalizadas de la Organizaci�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Organization Settings','Ajustes de la Organizaci�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Personal Calendar','Calendario Personal')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Room Calendar','Calendario del Salon')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'GuestLocations','Localizaci�n de invitados')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Automatic Call Launch','Lanzamiento de llamada autom�tico')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Personal/Room/External VMR','VMR Personal/Sal�n/Externa')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Concierge Support','Soporte de Conserje')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Internal Number','N�mero interno')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'External Number','N�mero externo')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'StartDate','Fecha_de_inicio')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'EndDate','Fecha_de_fin')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'BatchReportName','Nombre_Informe_Conjunto')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'PC Details','Detalles del PC')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'VMR Room Info','Info del Sal�n VMR')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Desktop Link','Enlace al Escritorio')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'The room does not have any Endpoint associated with it. An audio or video Conference cannot be created with this room.','El sal�n no tiene ning�n punto final asociado a �l. No se puede crear una conferencia de Audio o V�deo con este sal�n.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'User Endpoint','Punto final de usuario')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Guest Room','Sal�n de Invitados')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Guest Endpoint Address','Direcci�n de punto final de invitados')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Edit Existing MCU','Editar MCU existente')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Meet Type','Tipo de reuni�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'The server is not operational','El servidor no est� operativo')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Thank you! Your response has been recorded','�Gracias! Su respuesta ha sido grabada')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Please select a valid H323 ID Address.','Por favor, seleccione una direcci�n H323 ID v�lida.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'SetupTIme','Tiempo_Configuraci�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'TearDownTime','Hora_Desmontaje')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Max Allowed Standard MCU','M�ximo de MCU est�ndar permitidos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Max Allowed Enhanced MCU','M�ximo de MCU mejorados permitidos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Maintanance','Mantenimiento')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Please check the start and end time for the conference','Por favor, revise la hora de inicio y finalizaci�n para la conferencia')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'The user is In-Active. Please make sure the user is Active.','El usuario est� inactivo. Por favor, cerci�rese de que el usuario est� activo.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Invalid Protocol, Connection Type and Address Type selected for user: ','Protocolo, tipo de conexi�n y tipo de conexi�n seleccionados para el usuario son inv�lidos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Invalid Conference Code for user','C�digo de conferencia para el usuario no es v�lido')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Outside office hours.','Fuera de horas de oficina.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Room(s) not available.','Sal�n(es) no disponible(s).')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'No of Occurrence Exceeds color dates','N�m. de sucesos supera las fechas de color')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Special reccurence failed : Building reccurence','Fall� la recurrencia especial : Recurrencia del edificio')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Please select Dedicated VNOC Operator.','Por favor, seleccione el Operador VNOC dedicado.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Status not available','Estado no disponible')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'MSE 8000 MCU Configuration','Configuraci�n de la MCU MSE 8000')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Conference should have atleast two Endpoints','La conferencia deber�a tener al menos dos puntos finales')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'At least one endpoint is required for a Remote meeting.','Es necesario al menos un punto final para una reuni�n remota.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Operation UnSuccessful','Operaci�n fallida')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Type ID','ID del Tipo')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Status Date','Fecha del estado')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Status Message','Mensaje de estado')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'In-Active','Inactivo')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Hotfix(s)','Hotfix(s)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'System Boot Time','Hora de arranque del sistema')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Available Physical Memory','Memoria f�sica disponible')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Virtual Memory','Memoria virtual')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'DHCP Enabled','DHCP habilitado')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Connection Name','Nombre de la conexi�n')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Please enter the Whygo Integration Settings.','Por favor, introduzca la configuraci�n de integraci�n de Whygo.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Create New Catering Menus','Crear Nuevos Men�s de Catering')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Create New Audiovisual Inventories','Crear nuevos inventorios audiovisuales')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Create New Facility Services','Crear nuevos Servicios de instalaciones')
--insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'User Interface Banner &amp; Theme','Usar la Interfaz ''Cartel y Tema''')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values (3,'Participants','participantes')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Room(s)','Sal�n(es)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Create New Hotdesking','Crear nueva Escritorios Compartidos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Edit Hotdesking','Editar Escritorios Compartidos')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Concierge Conferences','Conferencias de Conserje')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'File type is invalid. Please select a new file and try again.','Tipo de archivo no v�lido. Por favor, seleccione un archivo nuevo y pruebe otra vez.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'MCU Connect / Disconnect','Conectar / Desconectar MCU')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'MCU Connect','Conectar / Desconectar MCU')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'MCU Disconnect','Desconexi�n MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'On-Site A/V Support','Soporte A/V sobre el terreno')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Welcome Mail','Correo de Bienvenida')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Password Request Change','Cambio de Solicitud Contrase�a')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'View','Ver')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'Import','Importar')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'Start Date/Time','Fecha/Hora de Inicio')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'End Date/Time','Hora/Fecha Final')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'SecurityDesk Only','S�lo Escritorio de Seguridad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(Custom)','(Personalizado)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(System)','(Sistema)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rooms','Salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Actions','Acciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Administrator 1','Administrador de organizacion 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Administrator 2','Administrador de organizacion 2')
insert into Launguage_Translation_Text(LanguageID, Text, TranslatedText) values (3,'Manage Batch Report','Administrar informe del conjunto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Public Participant','Participante p�blico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Network Switching','Cambio de red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Cancellation','Cancelaci�n de conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Creation Failed','Fallo en la creaci�n de la Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Batch Report','Informe del Conjunto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant Multi Request Change','Cambio de m�ltiples solicitudes de participantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Exportable Conference Report','Informe de conferencia exportable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Contact List','Lista de Contactos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The selected role is currently assigned to a group template and cannot be edited or deleted.','El rol seleccionado est� actualmente asignado a una plantilla de grupo y no puede ser editado ni eliminado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Call Monitoring','Supervisi�n de llamada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RoomName','Nombre de sala/salon')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Conference','Editar Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Support','Soporte de la Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Audiovisual work orders associated with this conference.','No hay �rdenes de trabajo audiovisuales asociadas con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Delivery','Coste de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Cost','Entrega')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Service','Tarifa del')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Charge','Servicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Price','Precio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Facility work orders associated with this conference.','No hay �rdenes de trabajo de instalaciones asociadas con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Set-up (Minutes)','Configuraci�n (Minutos)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tear Down (Minutes)','Desmontaje (Minutos)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new catering work order, click on Add New Work Order button. To modify an existing work order, click the Edit OR Delete link associated with that work order.','Para crear una nueva orden de trabajo de catering, haga ''clic'' en el bot�n Agregar nueva orden de trabajo. Para modificar una orden de trabajo existente, haga ''clic'' en los enlaces Editar o Borrar asociados con esa orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Confirmation','confirmaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Number','N�mero de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Reservation','Inicio de la reserva')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation unsuccessful.','Operaci�n fallida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Create New Day Color','Crear Nuevo D�a de Color')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid IP Address.','Direcci�n IP Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid ISDN Address.','Direcci�n ISDN Inv�lida.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid ISDN Address','Direcci�n ISDN Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mute','Silenciado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Events','Sin eventos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error Code','C�digo de Error')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select at least one room from ''Select Rooms'' tab in order to create a work order.','Por favor, seleccione por lo menos un sal�n de la pesta�a "Seleccionar Sal�n" para crear una orden de trabajo nueva.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Other','Otro')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Advanced Reports','Informes avanzados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room','Sal�n de Reuniones Virtuales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'InternalNumber','N�mero interno')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ExternalNumber','N�mero externo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Yes','Si')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No','No')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Map','Mapa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Select one...','Seleccione uno...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Sets','No hay series')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Rooms Selected','No hay sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' rooms',' Salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Items','No hay elementos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Groups','No hay grupos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External VMR','VMR externo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Request Change','Solicitar Cambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Lotus Plugin','Conexi�n Lotus')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Outlook Plugin','Conexi�n Outlook')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Services','servicios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Website','Sitio Web')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'System Up Time','Hora del Sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Page File','Localizar Archivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HotDesking Reservation','Reserva de ''Escritorios Compartidos''')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Radio Button','Bot�n Radio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'List Box','Cuadro de Listas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Drop-Down List','Lista desplegable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'URL','URL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Text-Multiline','Texto-Multil�nea')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Check Box','Casilla de verificaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mute All','Silenciar todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Clone','Clonar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click to see more details','Clic para ver m�s detalles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Insufficient Seats. Please Contact Your VRM Administrator.','Insuficientes lugares disponibles. Por favor contacte a su administrador de VRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Available Seats','Lugares disponibles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Image dimension too large','Dimensi�n de Imagen demasiado grande')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sorry the required log file is not found','Lo sentimos, el archivo de registro requerido no se encuentra')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'EmailService is not installed','El servicio de Email no est� instalado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Service is not installed','El servicio no est� instalado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting','N�mero de minutos antes del inicio de la conferencia, que se utilizar�n para preparar la sala para la reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes after the conference has ended to prepare the room for the next meeting','N�mero de minutos despu�s que la conferencia ha terminado, para preparar la sala para la pr�xima reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference','N�mero de minutos que el MCU dispone para conectar los equipos terminales, antes de la llegada de los participantes a la conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Day Color Details','Editar D�a, Color y Detalles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select at least one user to import','Por favor, seleccione al menos un usuario para importar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a date','Por favor, seleccione una fecha')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a bridge with at least one associated MPI Service.','Por favor, seleccione un Bridge con al menos un servicio de MPI asociado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Account Expiration Date','Fecha inv�lida para vencimiento de cuenta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid MPI Address - Alphanumeric characters only','Direcci�n inv�lida para MPI - Solo caracteres alfanum�ricos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid conflict date','Fecha de conflicto inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No data to print','No hay datos para imprimir')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter a valid date time','Por favor introduzca una fecha y hora v�lidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Special reccurence failed : Holidays not defined','Fall� la recurrencia especial: No se han definido los d�as feriados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Special reccurence failed : Reccurence string is not in right format','Fall� la recurrencia especial: La sintaxis de Recurrencia no tiene el formato correcto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select the ticker background color','Por favor, seleccione el color de fondo del dispositivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter the RSS Feed link','Por favor introduzca el enlace para el RSS Feed')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Both Tickers cannot have the same background color','Ambos dispositivos no pueden tener el mismo color de fondo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Hotdesking','Escritorios Compartidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point-to-Point','Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio Only','S�lo audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Timezone','Zona horaria de la conferencia')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HotDesking Reservation','Reserva de Escritorios Compartidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.','N�mero de minutos antes del inicio de la conferencia, que se utilizar�n para preparar la sala para la reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes after the conference has ended to prepare the room for the next meeting.','N�mero de minutos despu�s que la conferencia ha terminado, para preparar la sala para la pr�xima reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes that the MCU is to connect the endpoints, prior to the arrival of the participants into the conference.','N�mero de minutos que el MCU dispone para conectar los equipos terminales, antes de la llegada de los participantes a la conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The number of minutes that the MCU is to disconnect the endpoints, prior to the departure of the participants from the conference.','N�mero de minutos que el MCU dispone para desconectar los equipos terminales, antes de la salida de los participantes de la conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View User Details','Ver detalles del usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Virtual Meeting Rooms','Ver Sal�n de Reuniones Virtuales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Custom Option Details','Ver Opciones Personalizadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Endpoint Details','Ver Detalles del Punto Final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View MCU Details','Ver Detalles MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Audio Bridge Details','Ver Puente de Audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View User Template Details','Ver  Nueva Plantilla de Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Video Conferencing Systems','Sistemas de video-conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VMR Attendee','VMR Asistente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in IP','Marcar IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Meeting ID','ID de la reuni�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Phone Conferencing','Conferencia por tel�fono')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in Toll Number','Marcar n�mero de pago')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in Toll Free Number','Marcar n�mero gratuito')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'First time joining a Lync Video Meeting','�Es su primera vez en una reuni�n Lync')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'For detailed instructions','Para las instrucciones detalladas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'First time joining a Blue Jeans Video Meeting','�Es su primera vez en una reuni�n por v�deo Blue Jeans')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'First time joining a Jabber Video Meeting','�Es su primera vez en una reuni�n Jabber')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in H.323','Marcar H.323')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in SIP','Marcar SIP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Issued By','Generado por')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Issued On','generado a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audiovisual Administrator','Administrador audiovisuales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Facility Administrator','administrador de la instituci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audiovisual Admin','Administrador audiovisuales')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Facility Admin','Administrador de la instituci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audiovisual Remainder','Remanente Audiovisual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Facility Remainder','Remanente Instalaciones')
--100288
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'hour','hora');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HotDesking Conference','Conferencia de Escritorios Compartidos');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Insufficient Audio/Video ports.','Insuficientes puertos de audio/video.');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'UnMute','silenciado');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Result(s) for Audiovisual Work Orders','Resultados de la b�squeda(s) para �rdenes de trabajo audiovisual');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search Result(s) for Facility Services Work Orders','Resultados de la b�squeda(s) para �rdenes de trabajo de Servicios de instalaciones');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Design','dise�o')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Preview','Vista previa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Your conference has been submitted successfully and is currently in pending status awaiting administrative approval.','Su conferencia se ha entregado con exitosa y actualmente se encuentra en estado pendiente esperando aprobaci�n administrativa.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Outside','Fuera de')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mon','Lun')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tue','Mar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Wed','Mi�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Thu','Jue')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Fri','Vie')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sat','S�b')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sun','Dom')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Existing','Existente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'NA','N/D')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'IP(Hours)','IP(horas)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Facility','instalaciones')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this job name?','�est�s seguro de que quieres eliminar este nombre de trabajo?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'English','Ingl�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Spanish','espa�ol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Specific','espec�fico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Information','hay informaci�n')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The server is not operational.','El servidor no est� operativo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Active Directory or LDAP Directory not Configured in Site Settings','El directorio activo o Directorio LDAP no configurados en la Configuraci�n del Sitio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation Sucessful!','�Operaci�n Exitosa!')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'An operation error occurred.','Se ha producido un error de operaci�n')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Text Box','Cuadro de texto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RadioButton List','Lista de Botones Radio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'AudioVideo','AudioV�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Entity_Code','c�digo de entidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Dial-out (Hours)','Marcaci�n ISDN (horas)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Dial-out %','ISDN% de llamada saliente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Dial-in (Hours)','ISDN de acceso telef�nico (horas)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RoomOnly','S�lo sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Confirmed','confirmado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'AudioOnly','S�lo audio')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IP Address','Direcci�n IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Phone Number','N�mero de tel�fono ISDN')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid MCU Address Type, Connection Type or Protocol Selected for profile #:','Tipo inv�lido MCU Direcci�n, Tipo de conexi�n o protocolo seleccionado para el perfil #:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Address Type, Connection Type or Protocol Selected for profile #:','Tipo de direcci�n no v�lida, Tipo de conexi�n o protocolo seleccionado para el perfil #:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid MCU Address Type, Address Type or Protocol Selected for profile #:','Tipo inv�lido MCU Direcci�n, Tipo de direcci�n o protocolo seleccionado para el perfil #:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Address Types, Connection Type or MCU Address Type Selected for profile #:','Tipos no es v�lida, el tipo de conexi�n o MCU Tipo de direcci�n seleccionada para el perfil #:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'You cannot attend this conference as a room attendee since there are no rooms associated with this conference','Usted no puede asistir a esta conferencia como un asistente de habitaci�n, ya que no hay habitaciones asociadas con esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Dial-in Number','Conferencia Discar el n�mero')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point to Point Conference should not have Telepresence endpoint.. Telepresence Room Name:','Punto a Punto Conferencia no deber�a tener telepresencia punto final.. Sal�n de Telepresencia Nombre:')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Authentication Failed','Error de autenticaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'TestConnection failed','Probar conexi�n fall�.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Date and Time','Fecha y hora')





insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'AudioVideo','AudioV�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'State','Estado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'City','Ciudad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Confirmed','Confirmado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Inbound','Entrante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Outbound','Outbound')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf Start','Inicio de Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio,Video','Audio,V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Video','V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IP Address','Direcci�n IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Phone number','N�mero de tel�fono ISDN')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'AudioOnly','S�lo audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point-to- Point',' Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Video conferences','V�deo-conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio Conferences','Audio Conferencias')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Conferences','Sala/Salon de Conferencias')



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization','organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Department Name','nombre de departamento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'CTS Numeric ID','ID num�rico CTS')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start Time','Hora de inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Duration (Mins)','Duraci�n (mins)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf Speed','Velocidad de la conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf.Title','Titulo de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf.ID','ID de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Date Of Conf.','Fecha de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Start Time','Fecha de Inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints in Conf','puntos finales de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rooms in Conf','Salon en Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Used','MCU usados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host Role','Rol Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host First Name','Nombre de Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host Last Name','Apellido de Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host Email','Correo-e de Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf Protocol','Protocolo de la conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Occurrence','Suceso')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'WO Person-in-charge','WO Persona encargada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rm.Asst.in-Charge','Sal�n encargado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rm.Pri.Appr.Name','Sal.Apr.principal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rm.Sec.Appr.Name 1','Sal.Apr.secundario 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Rm.Sec.Appr.Name 2','Sal.Apr.secundario 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU.Pri.Appr.Name','MCU.Apr.principal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU.Sec.Appr.Name 1','MCU.Apr.secundario 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU.Sec.Appr.Name 2','MCU.Apr.secundario 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'On-Site A/V Support(Mins)','Soporte A/V sobre el terreno(Mins)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Meet and Greet(Mins)','reuni�n informal(Mins)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Call Monitoring(Mins)','Supervisi�n de llamada')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dedicated VNOC Operator(Mins)','operador VNOC dedicado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Secondary email','correo-e secundario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Role Name','Nombre del Rol')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Account Expiration','Expiraci�n de Cuenta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Exchange','Intercambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Domino','Domin�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RoomPhone','Tel�fono del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'capacity','Capacidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address1','Direcci�n 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address2','Direcci�n 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RoomFloor','Piso Sala')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'RoomNumber','Numero de Sala')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'City','Ciudad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'State','Estado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Country','Pa�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Zipcode','C�digo Postal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf ID','ID de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Single/Recuring','Individual/Recurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default Profile Name','Nombre de perfil predeterminado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Number of Profiles','N�mero de Perfiles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Preferred Bandwidth','Ancho de banda preferido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pre.Dialing Option','Pre Opci�n de marcaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default Protocol','Protocolo por defecto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Web Access URL','URL de acceso a la red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Network Location','Ubicaci�n de la red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Telnet Enabled','Telnet habilitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Email ID','ID de Correo-e')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'API port','Puerto API')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ICAL Invite','Invitado ICAL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tier 1','Nivel 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tier 2','Nivel 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints in Conf > Name','puntos finales de Conf>Nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints in Conf > Name - IP ','puntos finales de Conf>Nombre - IP ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints in Conf > Name - IP  - Add ','puntos finales de Conf>Nombre - IP - Direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoints in Conf > Name - IP  - Add  - Dial.Opt. ','puntos finales de Conf>Nombre - IP - Direcci�n - opci�n de marcaci�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Used in Conf','MCU Usado en Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Used in Conf > Address','MCU Usado en Conferencia > Direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default MCU','MCU Por defecto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Assignment','Asignaci�n MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Name','Nombre MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Org Name','Org Nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Next Poll in Mins','Siguiente Encuesta en minutos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'my VRM Conf ID','myVRM ID de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Conf ID','MCU ID de Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled Start','programado inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled End','programado fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connect Time','Conectar Inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disconnect Time','Desconectar Inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Disconnect Reason','Desconectar raz�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connection Type','Tipo de conexi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'CDR Scheduled Report','Informe CDR Programado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Part. First Name','Nombre del Participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Part. Last Name','Apellidos del Participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Video','V�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Single','Individual')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Recurring','Recurrente')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Part. Email','Correo-e de Part.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Part. Role','Rol Part.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Company Relationship','Enlace de la relaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Role','Rol Personalizado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IP Address','Direcci�n IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deleted Successfully.','Eliminado Correctamente.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pt-to-Pt','Punto-Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-Out','marcaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-In','Marcar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sec. Approver 1','aprobador secundario 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Sec. Approver 2','aprobador secundario 2')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Other...','Otro...')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please Enter the Comments','Por favor, introduzca los Comentarios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Protocol and Address type doesn''t match','Protocolo y Direcci�n de tipo no coincide')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Hotdesking Room Details','Ver Hot Desking Informaci�n de las habitaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Entity Code Details','Ver c�digo Entidad Detalles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Color Day Details','Ver detalles Color D�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'View Room Details','Ver detalles de la habitaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'conference already deleted or expired','conferencia ya eliminado o expirado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'please associate at least one Catering Service with all Menus','por favor asociar al menos un servicio de catering con todos los men�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Characters Left','Caracteres restantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Continuous Presence','Presencia continua')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Username','Nombre de usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'pending decision at SYSTEM level','decisi�n pendiente del sistema')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'pending decision at MCU level','decisi�n pendiente del MCU')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'pending decision at ROOM level','decisi�n pendiente del administrador del Sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'pending decision at Department level','decisi�n pendiente del departamento')

update Launguage_Translation_Text set TranslatedText='Mensual' where LanguageID=3 and TEXT='Monthly'

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Open 24 hours a day. ',' Abre las 24 horas del d�a. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Hours of operation: ',' Horario de funcionamiento: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Closed: ',' Cerrado: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' To ',' a ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Limit','L�mite de salones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Enhanced MCU Limit','El l�mite MCU mejorado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Domino User','M�x. usuario domin�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'API Module','M�dulo API')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Limit','L�mite Organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'End Point Limit','L�mite del punto final')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Mobile User','M�x.Usuarios de m�viles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Video Rooms','M�x. Salones de Video')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Public Room Service','Servicio Sal�n P�blico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Limit','L�mite de usuarios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max PC User','M�x. PC del usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max VMR Rooms','M�x. Salones VMR')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Catering Module','M�dulo Catering')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Guests Per User','M�x. Invitados Por Usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max RO Hotdesking Rooms','M�x. Salones de Escritorios Compartidos RO')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Facilities Module','M�dulo de instalaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max VC Hotdesking Rooms','M�x. Salones de Escritorios Compartidos VC')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'HouseKeeping Module','M�dulo Mantenimiento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Exchange User','M�x. Usuarios de intercambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Guest Room Limit','El l�mite de salones de invitados')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Standard MCU Limit','L�mite MCU est�ndar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Expiration Date','Fecha de expiraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Non Video Rooms','Max no v�deo Habitaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Adv Rep Mod','Informe de Avance del m�dulo')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To join or start the meeting,go to','Para unirse o iniciar la conferencia, visite')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Or join directly with the following options:','O unirse directamente con las siguientes opciones:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Test your video connection,talk to our video tester by clicking here','Pruebe su conexi�n de v�deo, consulte con nuestro probador v�deo haciendo clic aqu�')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(c) Blue Jeans Network 2012','(c) Blue Jeans de la Red 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(c) Jabber Network 2012','(c) Jabber de la Red 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(c) Lync Network 2012','(c) Lync de la Red 2012')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'(c) Vidtel Network 2012','(c) Vidtel de la Red 2012')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Facility Services available for this room. Please select another room.','No hay grupos de Servicios de instalaciones disponibles para este sal�n. Por favor, seleccione otro sal�n.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Facility Services now.','Por favor, seleccione ahora un Servicio de instalaciones.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Work Order','Editar orden de trabajo')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Switched Video','Switched V�deo')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Transcoding','Transcodificando')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Facility work orders associated with the Selected Room','No hay �rdenes de trabajo de instalaciones asociadas con el sal�n seleccionado')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Modify requested quantity and click EDIT to edit this work order.','Modificar la cantidad solicitada y haga clic en Editar para modificar esta orden de trabajo.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No details available','No se encontraron Detalles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Summary','Resumen')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Phone','Tel�fono')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Entity Name','Nombre de Entidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Need Entity Code','Necesidad C�digo de entidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Report Title','Titulo de Informe')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Duration Total','Duraci�n total')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Point-to-Point Conferences','Conferencias Punto a Punto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audio-Video Conferences','Conferencia Audio-Visual')

--ZD 100288

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Maximum limit is 24 hours including buffer period. Please enter a valid duration.','El l�mite m�ximo es de 24 horas incluyendo el periodo regulador. Esta podr�a ser debido al ''Periodo de carga por defecto'' en la ''Opciones de Organizaci�n''')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid MPI Address - Alphanumeric characters only.','Direcci�n inv�lida para MPI - Solo caracteres alfanum�ricos.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Today','hoy')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,' does not have any Endpoint associated with it. An audio or video Conference cannot be created with this room.','El sal�n no tiene ning�n punto final asociado a �l. No se puede crear una conferencia de Audio o V�deo con este sal�n.')




insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Audio/Video Seats','Totales Asientos Audio / Video')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Audio Only Seats','Total de s�lo audio Asientos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Time (HHmmZ)','Hora inv�lida (HHmmZ)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Polycom CMA Configuration','Configuraci�n Polycom CMA')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Polycom RPRM Configuration','Configuraci�n Polycom RPRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Lifesize MCU Configuration','Configuraci�n MCU tama�o natural')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'iView MCU Configuration','Configuraci�n MCU iView')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Address Type selected for IP Service #','Tipo de direcci�n no v�lida seleccionada para el Servicio IP #')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' remaining','restante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' min',' min')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' ago ','hace')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Partially Connected','Parcialmente Conectado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Status not available.','Estado no disponible.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Department','Departamento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a room to join this conference as a VMR attendee.','Por favor seleccione una habitaci�n para unirse a esta conferencia como un asistente VMR.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a room to join this conference as a room attendee.','Por favor seleccione una habitaci�n para unirse a esta conferencia como un asistente de habitaciones.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Pending Audiovisual Work Orders','Mis �rdenes de trabajo audiovisual pendientes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Pending Facility Services Work Orders','Mis �rdenes de trabajo pendientes de Servicios de las instalaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Incomplete Audiovisual Work Orders','Mis �rdenes de trabajo audiovisual incompletas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'My Incomplete Facility Services Work Orders','Mis �rdenes de trabajo incompletas de Servicios de las instalaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Today''s Audiovisual Work Orders','�rdenes de trabajo audiovisuales de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Today''s Catering Work Orders','�rdenes de trabajo de Catering de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Today''s Facility Services Work Orders','�rdenes de trabajo de Servicios de instalaciones de Hoy')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'E164/SIP Address','Direcci�n E164/SIP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click ''Cancel'' before you proceed to another step.','Haga clic en ''Cancelar'' antes de seguir al siguiente paso.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'If there are no rooms in the list, click on ''Select Rooms'' option available on your left.','Si no hay salones en la lista, haga clic en la opci�n ''Seleccionar Salones'' disponible a su izquierda.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Either you are not entitled to use the selected room or room no longer belongs to the conference.','Ya sea que usted no tiene derecho a utilizar la sala o habitaci�n seleccionada ya no pertenece a la conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'This audiovisual inventories no longer belongs to selected room.','Este inventorio audiovisual ya no pertenece al sal�n seleccionado.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Edit Work Order','Editar orden de trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Modify requested quantity and click EDIT to edit this work order.','Modificar la cantidad solicitada y haga clic en Editar para modificar esta orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Select a Set from the Audiovisual Inventories list.','Seleccione un grupo de la lista de Inventorios audiovisuales.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a Facility Services now.','Por favor, seleccione ahora un Servicio de instalaciones.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Facility Services available for this room. Please select another room.','No hay grupos de Servicios de instalaciones disponibles para este sal�n. Por favor, seleccione otro sal�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the start and end Date/time for this workorder.','Por favor, compruebe el inicio y el final de fecha / hora para esta orden de trabajo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' (Applicable for all instances)','(Aplicable para todas las instancias)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Duration: ','Duraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' (Work orders deliver by date/time do not match with conference date/time.)','(Fecha/hora de entrega de las �rdenes de trabajo  no coinciden con la fecha/hora de conferencia.)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Workorders can only be created with rooms selected for this conference. ','Las �rdenes de trabajo s�lo se pueden crear con salones seleccionados para esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the start and end time for the conference.','Por favor, revise la hora de inicio y finalizaci�n para la conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Duration. Conference duration should be minimum of 15 mins.','Duraci�n Inv�lida. Duraci�n de Conferencia debe ser por lo menos de 15 minutos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Duration. Conference duration should be maximum of 24 hours.','Duraci�n Inv�lida.Duraci�n Conferencia debe ser m�ximo de 24 horas.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Setup Time','Tiempo de configuraci�n no v�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Teardown Time','Inv�lido Tiempo Teardown')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Setup/Teardown Time','Configuraci�n no v�lida / Teardown Tiempo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'is In-Active. Please make sure the user is Active.','El usuario est� inactivo. Por favor, cerci�rese de que el usuario est� activo.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' for assistance.<br>Once settings are confirmed please click Preview tab and resubmit the conference.','para asistencia. <br>Una vez que la configuraci�n sea confirmada, por favor haga clic en la pesta�a ''Vista Previa'' y ')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Leader Pin for user','Pin L�der no v�lido para el usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Network Protocol selected for user','Protocolo de red no v�lida seleccionada para el usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Network access is restricted to IP only.','Acceso a la red se limita a IP �nica.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Audiovisual work orders associated with the selected Room','No hay �rdenes de trabajo audiovisuales asociadas con el sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no Facility work orders associated with the Selected Room','No hay �rdenes de trabajo de instalaciones asociadas con el sal�n seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Name has already been used','Nombre de la sala ya se ha utilizado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To join or start the meeting,go to','Para unirse o iniciar la conferencia, visite:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Or join directly with the following options:','O unirse directamente con las siguientes opciones:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Video Conferencing Systems:','Sistemas de video-conferencias:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PIN','PIN')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'& < and > are invalid characters.','<br>& < y > no son caracteres v�lidos.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Maximum limit is 4000 characters.','El l�mite m�ximo es 4000 caracteres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Un Mute All','Un Mute All')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mute All','Silenciar todo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to Connect/disconnect this endpoint?','�Esta seguro de que quiere conectar/desconectar este punto final?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Audiovisual ','Audiovisual ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room No longer belongs to this conference','Habitaci�n Ya no pertenece a esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'There are no work orders currently associated with this conference.','No hay �rdenes de trabajo que actualmente est� asociada con esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'�(Open for Registration)','(Abierto para Registro)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the telepresence status of all profiles','Por favor, compruebe el estado de todos los perfiles de telepresencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please associate at least one location with this Provider.','Por favor asocie aunque sea un articulo del menu con el resto del los menus')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please associate at least one Menu with this Provider.','Por favor, asocie al menos un m�nu con este Proveedor.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please associate at least one Catering Service with all Menus.','por favor asociar al menos un servicio de catering con todos los men�s')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter unique menu names for all menus.','Por favor ingrese los nombres de men�s �nicos para todos los men�s.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Billing Code Name should be within 35 characters','Billing Code Name debe ser dentro de los 35 caracteres')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Task Type','Tipo de tarea')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Type ID','ID del Tipo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Operation failed: Invalid security permission. Please contact your VRM Administrator','Error en la operaci�n: el permiso de seguridad no v�lido. Por favor, p�ngase en contacto con el administrador del VRM')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Standard Resolution Banner exceeds the Resolution Limit','El cartel de resoluci�n est�ndar excede el l�mite de la resoluci�n ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'automatically on daily basis.','autom�ticamente a diario')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'at','a')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Department','Departamento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'automatically.','cada viernes a las')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'every Month End at','cada fin de mes a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Hardware Interaction layer','Capa Interacci�n Hardware')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Data Access Layer','Capa de acceso a datos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Business Logic Layer','Capa de l�gica de negocios')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Business Facade layer','Capa Business Facade')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Front-End','Frontal y Fin')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select....','por favor seleccione....')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Setup time cannot be less than the MCU pre start time.','La hora de establecimiento no puede ser anterior a la hora de previa al inicio del MCU.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Teardown time cannot be less than the MCU pre end time.','La hora del desmontaje no puede ser anterior a la hora de previa a la finalizaci�n del MCU.')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Ishost','Ishost')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this user ?','�Est� seguro de que quiere eliminar este rol del usuario?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PC Attendee','PC Asistente')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Collapse','Colapso')
--insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Expand','Espaciar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom option name should be within 35 characters','Nombre de la opci�n personalizada debe estar dentro de 35 caracteres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Option name already exists in the list for Language ','Nombre de opci�n ya existe en la lista para el Idioma')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this Custom Option ?','�Est� seguro de que quiere eliminar esta Opci�n personalizada?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Both Tickers cannot have the same background color.','Ambos dispositivos no pueden tener el mismo color de fondo.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter the RSS Feed link.','Por favor introduzca el enlace para el RSS Feed.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select the ticker background color.','Por favor, seleccione el color de fondo del dispositivo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No Tiers found.','No se encontraron niveles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this middle tier ?','�Est� seguro de que quiere eliminar este nivel medio?')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Login Background Image attachment is greater than 100kB. File has not been uploaded.','Login archivo adjunto de imagen es mayor de 100 kB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Logo attachment is greater than 100kB. File has not been uploaded.','El logo del sitio adjunto es mayor que 100KB. El archivo no se ha cargado.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Update Search Parameters for Template: ','Actualizaci�n de la b�squeda Par�metros de la plantilla: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter a valid date time.','Por favor introduzca una fecha y hora v�lidos.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'TearDown Dur','Desmontaje Duraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Setup Dur','Instalaci�n Duraci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' to Week','para la semana')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Conferences Usage Report',' Informe de uso de Conferencias')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid IP Address','Direcci�n IP Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid ISDN Address','Direcci�n ISDN Inv�lida')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU is mandatory for MPI protocol','MCU es obligatorio para el protocolo MPI')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid selection for connection type and protocol. MCU is mandatory for direct calls with MPI protocol.','Selecci�n de tipo de conexi�n y protocolo inv�lida. El MCU es obligatorio para las llamadas directas con protocolo MPI.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Selected bridge for this template does not support MPI calls.','Puente seleccionado para esta plantilla no admite llamadas MPI.')

update Launguage_Translation_Text set TranslatedText ='Conferencia se t�rmina' where Text ='Conference End' and LanguageID=3

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Host','Organizador de Evento')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'<error><errorCode>264</errorCode><message>Your VRM account is about to expire on %s. Please contact your VRM Administrator for further assistance Your account expires in {0} days.</message><Description>VRM Account Expiration warning</Description><level>M         </level></error>','<error><errorCode>264</errorCode><message>Su cuenta VRM est� a punto de expirar el %s. Por favor, contacte a su administrador de VRM para obtener m�s ayuda. Su cuenta expira en {0} d�as.</message><Description>VRM Account Expiration warning</Description><level>M         </level></error>')

-------------------------------- ZD 100622 (23 Dec 2013) starts -------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click to see Map','Clic'' para ver el Mapa')

-------------------------------- ZD 100622 (23 Dec 2013) ends-------------------------------------------------
--ZD 100507 (06 Jan 2013)
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Layout','Distribuci�n del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Layout Image','Estructura Sala de Imagen')

-------------------------------- ZD 100777 (29 Jan 2014) starts -------------------------------------------------


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External Scheduling Number','N�mero de planificaci�n externa')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Expiry Date','Fecha de caducidad')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Expiry Remaining Days','Caducidad d�as restantes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Name','Nombre de la organizaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Alernate Location','Alernate Ubicaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Alernate MCU','Alernate MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Fail Status Alert','Estado de Alerta MCU Falla')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Pass Status Alert','Estado de Alerta MCU Pass ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Event Edit Notification','Editar evento de notificaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Event Delete Notification','Evento Eliminar Notificaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Site Expiration','Vencimiento del sitio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Client Site Expiration','Vencimiento del sitio del cliente ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Organization Expiry','Organizaci�n de caducidad ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Alternate Host Mail','Host de correo alternativo de habitaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Alternate Schedular Mail','Habitaci�n alternativo cedular correo ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Alternate Host Mail','MCU alternativo Mail Host ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Alternate Schedular Mail','MCU alternativo cedular correo ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deny Room Host Mail','Denegar Habitaci�n Mail Host ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deny Room Schedular Mail','Denegar Habitaci�n cedular correo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conf.Type','conf.tipo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' min(s)',' Minuto(s)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Address Book','Libreta de direcciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'New','Nuevo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VMR Password','VMR Contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Company Logo Banner exceeds the Resolution Limit','Logotipo de la empresa Banner excede el l�mite de resoluci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Company logo attachment is greater than 100KB. File has not been uploaded','Logotipo de la empresa adjunto es mayor a 100 KB. El archivo no se ha subido')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Banner attachment is greater than 500KB. File has not been uploaded.','Apego Banner es mayor de 500KB. El archivo no se ha subido.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Banner attachment for high resolution is greater than 500KB. File has not been uploaded.','Apego Banner para alta resoluci�n es mayor que 500 KB. El archivo no se ha subido.')

-------------------------------- ZD 100777 (29 Jan 2014) ends-------------------------------------------------

-------------------------------- ZD 100788 (4 Feb 2013) starts -------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Telepresence','Tele-presencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Hotdesking Audio','Escritorios Compartidos audio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Hotdesking Video','Escritorios Compartidos v�deo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Media Type is None','Tipo de soporte es Ninguno')

-------------------------------- ZD 100788 (4 Feb 2014) ends-------------------------------------------------

-------------------------------- ZD 100909 (6 Feb 2014) starts -------------------------------------------------

update Launguage_Translation_Text set TranslatedText='Administrar Departamentos' where Text = 'Manage Departments' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Invitados' where Text = 'Manage Guests' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios Inactivos' where Text = 'Manage Inactive Users' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar MCUs' where Text = 'Manage MCUs' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Niveles' where Text = 'Manage Tiers' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios' where Text = 'Manage Users' and languageid= 3

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search','Buscar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'FAIL','FALLAR')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PASS','PASAR')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host URL','Anfitri�n URL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant URL','URL Participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'WebEx Password','WebEx Contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Failure Message','Mensaje de error')

-------------------------------- ZD 100909 (6 Feb 2014) ends-------------------------------------------------
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Allowed  Desktop Video ','M�ximas permitidas de v�deo de escritorio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Virtual Meeting Room ','M�ximas Virtual Sala de reuniones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room','Virtual Sala de reuniones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Desktop Video User','Max de video de escritorio del usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room Conference' ,'Virtual Sala de reuniones  Conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room Attendee','Virtual Sala de reuniones de asistentes');
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a room to join this conference as a Virtual Meeting Room attendee.','Por favor seleccione una habitaci�n para unirse a esta conferencia como un asistente virtual Sala de reuniones.');

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Event Edit failure Notification','Fracaso Editar evento de notificaci�n')

--ZD 100825
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values 
(3,'Not Monitored','Sin supervisi�n')

-- ZD 100909 Starts --
update Launguage_Translation_Text set TranslatedText='Administrar Departamentos' where Text = 'Manage Departments' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Invitados' where Text = 'Manage Guests' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios Inactivos' where Text = 'Manage Inactive Users' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar MCUs' where Text = 'Manage MCUs' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Niveles' where Text = 'Manage Tiers' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Administrar Usuarios' where Text = 'Manage Users' and languageid= 3

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search','Buscar')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'FAIL','FALLAR')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'PASS','PASAR')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host URL','Anfitri�n URL')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Participant URL','URL Participante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'WebEx Password','WebEx Contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Failure Message','Mensaje de error')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max WebEx User','M�x. WebEx del usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deny MCU Host Mail','Denegar MCU Mail Host')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Deny MCU Scheduler Mail','Denegar MCU cedular correo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Started','Comenzado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Home','Inicio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User interface banner and Theme','Usar la Interfaz Cartel y Tema')
-- ZD 100909 End --

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room Link','Sala Virtual Meeting Enlace')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Note','Nota')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have first name','El usuario no tiene nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have Last name','El usuario no tiene Apellidos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have Email Id','El usuario no tiene ID de correo electr�nico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have Password','El usuario no tiene contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have Timezone','El usuario no tiene Zona Horaria')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User doesn''t have valid Email Id','El usuario no tiene validez Identificaci�n del email')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room doesn''t have the name','Habitaci�n no tiene el nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room doesn''t have the Tier 1','Habitaci�n no tiene el nivel 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room doesn''t have the Tier 2','Habitaci�n no tiene el nivel 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Given Tier 1 doesn''t exists.','Tier Dada 1 no existe.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Given Tier 2 doesn''t exists.','Tier Dada 2 no existe.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint doesn''t have name','Punto final no tiene nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint doesn''t have address','Punto final no tiene direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint doesn''t have address type','Punto final no tiene el tipo de direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint name exceeds 20 character','Nombre de punto final excede de 20 caracteres')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU doesn''t have name','MCU no tiene nombre')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU doesn''t have IP Address','MCU no tiene direcci�n IP')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3
,'For New Entry :  Leave the first column (''ID'') value as blank (ie., no need to enter any value for the ''ID'' field).'
,'Para Nueva entrada: Salga de la primera columna (''ID'') como valor en blanco (es decir, sin necesidad de introducir ning�n valor para el campo ''ID'').')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3
,'For Modifying Existing Record : Please never modify /remove the value in the first column (''ID'').'
,'Para Modificar registro existente: Por favor, nunca modificar / quitar el valor de la primera columna ("ID").')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Data','Data Room')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'MCU Data','MCU Data')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Endpoint Data','Punto final de datos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Data','datos del usuario')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User Role (User/Admin/Super Admin etc)','Funci�n de usuario(Usuario / Admin / Superadministrador etc)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Assigned MCU (only in case of multiple)','Asignado MCU(s�lo en caso de m�ltiple)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Notes','Notas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Vendor Type','Tipo de proveedor')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Control Port IP Address','Direcci�n de Control de IP Port')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tier One','Nivel Uno')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Tier Two','Nivel Dos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Floor','Piso')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Phone','Tel�fono Habitaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room Administrator','Administrador de habitaciones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Media (None Audio-only Audio-Video)','Medios (Ninguno, De s�lo audio, Audio-Video)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Department','Departamento')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Preferred Dialing Option','Opci�n preferente Marcaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Located Outside the Network','Situado fuera de la red')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Data Import','Importar datos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Initial Password','Contrase�a inicial')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Desktop Video Details','Video Conferencia de Escritorio Detalles')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room Password','Virtual Contrase�a Sala de reuniones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External Virtual Meeting Room','Externo Virtual Sala de reuniones')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Virtual Meeting Room Info','Informaci�n de la sala de reuniones virtual')

update Launguage_Translation_Text set TranslatedText='Max Desktop Video User' where Text = 'Max de video conferencia de escritorio del usuario' and languageid= 3

--ZD 100528
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'hours per working day','hrs por d�a laborable')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Total Duration(Hrs)','Duraci�n total (hrs)')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'hrs','hrs')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'mins','mins')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Outside office hrs.','Fuera de hrs de oficina.')
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'IP(hrs)','IP(hrs)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Dial-out (Hrs)','Marcaci�n ISDN (hrs)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'ISDN Dial-in (Hrs)','ISDN de acceso telef�nico (hrs)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Open 24 hrs a day. ',' Abre las 24 hrs del d�a. ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Hrs of operation: ',' Hrs de funcionamiento: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Maximum limit is 24 hrs including buffer period. Please enter a valid duration.',
'El l�mite m�ximo es de 24 horas incluyendo el periodo regulador. Esta podr�a ser debido al ''Periodo de carga por defecto'' en la ''Opciones de Organizaci�n''')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Duration. 
Conference duration should be maximum of 24 hrs.','Duraci�n Inv�lida.Duraci�n Conferencia debe ser m�ximo de 24 hrs.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values 
(3,'VRM system is unavailable during the chosen date and time. Please check VRM system availability and try again. 
Hrs of operation:','El sistema VRM no est� disponible en la fecha/hora elegida. Por favor, compruebe la 
disponibilidad del sistema VRM y vuelva a intentarlo. Hrs de funcionamiento:' )

--ZD 100981
update Launguage_Translation_Text set TranslatedText='Soporte A/V en el Sitio(Mins)' where Text = 'On-Site A/V Support(Mins)' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Soporte A/V en el Sitio' where Text = 'On-Site A/V Support' and languageid= 3

--ZD 100959
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Dial-in Information','Informaci�n de la Conferencia de acceso telef�nico')

/* **********************************ZD 100781 Starts 26 Feb 2014************************** */
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'You will receive the password shortly. Administrator(s) are notified about your request.','Usted recibir� la contrase�a en breve. Administrador (s) son notificados acerca de su petici�n.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'User password has been changed and email notification has been sent to user.','Contrase�a de usuario se ha cambiado y notificaci�n por correo electr�nico se ha enviado a los usuarios.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Request Password','Solicitar contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Password Change Admin Notification','Notificaci�n de cambio de contrase�a de administrador')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'New Password Notification ','Contrase�a nueva Notificaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'New Password Admin Notification','Nueva notificaci�n contrase�a de administrador')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Password Change Request','Contrase�a Solicitud de Cambio')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'<error><errorCode>724</errorCode><message>Your VRM account password is about to expire on %s. Please contact your VRM Administrator for further assistance.</message><Description>Your VRM account password is about to expire on %s. Please contact your VRM Administrator for further assistance Your account password expires in {0} days.</Description><level>E         </level></error>','<error><errorCode>724</errorCode><message>Tu contrase�a de la cuenta VRM est� a punto de expirar el %s. Por favor, p�ngase en contacto con el administrador del VRM para obtener m�s ayuda.</message><Description>Tu contrase�a de la cuenta VRM est� a punto de expirar el %s. Por favor, p�ngase en contacto con el administrador del VRM para obtener m�s ayuda.</Description><level>E         </level></error>')


/* **********************************ZD 100781 Ends 26 Feb 2014************************** */


/* **********************************ZD 100963 START 28 Feb 2014************************** */
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Reserved','Reservado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Host','Anfitri�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work','trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Cell','telf. m�vil')
/* **********************************ZD 100963 END 28 FEB 2014************************** */



insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Apr','Abr')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Aug','Ago')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dec','Dic')

--ZD 100036
Insert into Launguage_Translation_Text (LanguageID, [Text], TranslatedText) values ( 3, 'On MCU Conferences', 'En las Conferencias de MCU')


/* **********************************ZD 100973 START 05 MAR 2014************************** */

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Google Authentication Failed','Google Error de autenticaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid user Authentication','Autenticaci�n de usuario no v�lido')

/* **********************************ZD 100973 END 05 MAR 2014************************** */


/* **********************************ZD 101026 Starts 13 MAR 2014************************** */

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Changes Made','Cambios realizados')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'No Changes Made','No hay cambios realizados')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'New Profile Created','Nueva Fecha de creaci�n')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Created','Creado')

/* **********************************ZD 101026 End 13 MAR 2014************************** */

/*********************************** ZD 100619  Starts 10 Apr 2014 **************************/
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Dial-in to Conference','Dial-in de Conferencia')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Dial-out to Location','Dial-a Ubicaci�n')

/*********************************** ZD 100619  Ends 10 Apr 2014 **************************/

--ZD 101304
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'An e-mail has been sent to the address entered with instructions to reset your password','Un e-mail ha sido enviado a la direcci�n introducida con instrucciones para restablecer tu contrase�a')

--ZD 101322
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External Participant(s) Info','Participante externo (s) Informaci�n')

--ZD 101344


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Resource br Availability','Disponibilidad br de Recurso')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Workorders br Calendar','Detalles de las br �rdenes de trabajo')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Search br Workorders','Buscar �rdenes  br de trabajo')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Workorders br Calendar','Calendario de  br �rdenes de Trabajo')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Basic br Details','Detalles  br B�sicos')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Select br Rooms','Seleccionar br Salones')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Select br Audiovisual','Seleccione br Audiovisual')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Select br Catering','Seleccionar  br Catering')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Select br Facility','Seleccionar  br instalaciones')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Additional br Options','Opciones  br Adicionales')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Review amp;br Submit','Revisar  br y Entregar')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Workorders br Details','Detalles de las br �rdenes de trabajo')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Search br Workorders','Buscar �rdenes de br trabajo')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Workorders br Calendar','Calendario de br �rdenes de Trabajo')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'CISCO TMS Configuration','Configuraci�n TMS CISCO')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'MGC Accord MCU Configuration','Configuraci�n MCU ''MGC Accord''')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'No Rooms','Sin salones')

--ZD 101176
Delete from Launguage_Translation_Text where [Text]='entity code' and LanguageID =3
Delete from Launguage_Translation_Text where [Text]='special instructions' and LanguageID =3


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Entity Code','C�digo de Entidad')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Special Instructions','Instrucciones Especiales')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Assigned to MCU','Asignado a MCU')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The caller endpoint must be a certified endpoint.','El criterio de valoraci�n llamador debe ser un criterio de valoraci�n certificada.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The caller endpoint must use an IP address as the address type.','El punto final que llama debe utilizar una direcci�n IP como el tipo de direcci�n.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'The caller endpoint cannot be a Guest Attendee.','El punto final de la persona que llama no puede ser un asistente Visitante.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Only an IP address is permitted for this profile type.','S�lo una direcci�n IP est� permitido para este tipo de perfil.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'A Default Profile should be selected.','Un perfil por defecto debe ser seleccionado.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Profile type cannot be the same.','Tipo de perfil no puede ser la misma.')


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Max iControl Room','M�xima Habitaci�n iControl')

-- ZD 101344
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Minimum Length is','Longitud m�nima es')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'First digit should be non-zero.','El primer digito no debe ser cero.')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Assigned to MCU','Asignado a MCU')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Excel is not Uploaded','Excel no est� Subido')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Middle Tier(s) are imported successfully!','�El Nivel Medio ha sido importado correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Department(s) are imported successfully!','�El Departamento(s) ha(n) sido importado(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Room(s) are imported successfully!','�El Sal�n(es) ha(n) sido importado(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Failed to create Room''s are listed below','No se pudo crear la habitaci�n de se enumeran a continuaci�n')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'User(s) are imported successfully!','�El Usuario(s) ha(n) sido importado(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Failed to create User''s are listed below','No se pudo crear de usuario se enumeran a continuaci�n')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'MCU(s) are imported successfully!','�MCU(s) ha(n) sido importado(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Failed to create/update MCU''s are listed below','Error al crear / actualizar MCU se enumeran a continuaci�n')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'EndPoint(s) are imported successfully!','�El Punto(s) final(es) ha(n) sido importado(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Failed to create/update Endpoint''s are listed below','Error al crear / actualizar Endpoint se enumeran a continuaci�n')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Conference(s) are imported successfully!','�La Conferencia(s) ha(n) sido importada(s) correctamente!')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Purge Completed','Purga completada')

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'(Fly Room)','(Sala de Volar)')

--ZD 101411

update Launguage_Translation_Text set TranslatedText='Sal�n' where Text = 'Room' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n de conferencia' where Text = 'Room Conference' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de minutos antes del inicio de la conferencia, que se utilizar�n para preparar el sal�n para la reuni�n.' where Text = 'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting.' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de minutos despu�s de que la conferencia ha terminado, para preparar el sal�n para la pr�xima reuni�n.' where Text = 'The number of minutes after the conference has ended to prepare the room for the next meeting.' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n de Conferencias' where Text = 'Room Conferences' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n de Piso' where Text = 'RoomFloor' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de Sal�n' where Text = 'RoomNumber' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Estructura de Imagen del Sal�n ' where Text = 'Room Layout Image' and languageid= 3
update Launguage_Translation_Text set TranslatedText='El nombre del sal�n ya se ha utilizado' where Text = 'Room Name has already been used' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Si no hay elementos en la lista, entonces no hay grupos asociados a este sal�n.' where Text = 'If there are no items in the list, then there are no sets associated with this room. ' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de piso/sal�n' where Text = 'Floor / room number' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de telefono del sal�n' where Text = 'Room Phone number' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de minutos antes del inicio de la conferencia, que se utilizar�n para preparar el sal�n para la reuni�n.' where Text = 'The number of minutes prior to the start of the conference that will be used to prepare the room for the meeting' and languageid= 3
update Launguage_Translation_Text set TranslatedText='N�mero de minutos despu�s de que la conferencia ha terminado, para preparar el sal�n para la pr�xima reuni�n.' where Text = 'The number of minutes after the conference has ended to prepare the room for the next meeting' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Ya sea que usted no tiene derecho a utilizar el sal�n seleccionado o ya no pertenece a la conferencia.' where Text = 'Either you are not entitled to use the selected room or room no longer belongs to the conference.' and languageid= 3
update Launguage_Translation_Text set TranslatedText='M�ximo de Salones de Reuniones Virtuales' where Text = 'Max Virtual Meeting Room ' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Conferencia de Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room Conference' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Asistentes de Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room Attendee' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Por favor seleccione un salon para unirse a esta conferencia como un asistente de Sal�n de Reuni�nes Virtuales' where Text = 'Please select a room to join this conference as a Virtual Meeting Room attendee.' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Enlace de Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room Link' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Nombre de sal�n' where Text = 'RoomName' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n/Invitado' where Text = 'Room/Attendee' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Contrase�a de Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room Password' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n de Reuni�nes Virtuales Externo' where Text = 'External Virtual Meeting Room' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Informaci�n del Sal�n de Reuni�nes Virtuales' where Text = 'Virtual Meeting Room Info' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Calendario del Sal�n' where Text = 'Room Calendar' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Por favor seleccione un Sal�n para unirse a esta conferencia como un asistente de Sal�n de Reuni�nes Virtuales.' where Text = 'Please select a room to join this conference as a Virtual Meeting Room attendee.' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Sal�n en Conf' where Text = 'Rooms in Conf' and languageid= 3
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Set-up (mins)','Configuraci�n (mins)')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Tear Down (mins)','Desmontaje (mins)')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'A Default Profile should be selected','Un perfil por defecto debe ser seleccionado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please check the profile type','Por favor, compruebe el tipo de perfil')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Login Background Image exceeds the Resolution Limit','Ingresar la imagen de fondo es superior al l�mite de resoluci�n')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Total Audio/Video Conferences','Total de Conferencias de Audio/V�deo')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Percentage of Total Audio/Video Conferences','Porcentaje del Total de Conferencias de Audio/V�deo')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'OBTP Conference','Conferencia OBTP')
--ZD 101244 Start
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Security Email','Seguridad de correo electr�nico')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Acceptance Security Email','Seguridad Aceptaci�n Email')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invited Partcipant','Invitada Partcipant')
--ZD 101244 End
--ZD 101443
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Active directory enabled','Active Directory habilitado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Whygo Portal Error','Whygo Error Portal')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connection Detail','Detalles de Conexcion')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Party Address','Fiesta Direcci�nFiesta Direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'VMR Room Link','VMR Room Enlace')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Bridge Address','Bridge Direcci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Protocol Type','Tipo de Protocolo')
--ZD 101429
update Launguage_Translation_Text set TranslatedText='Aprobador 1' where Text='primary approver' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Aprobador 2' where Text='Sec. Approver 1' and languageid= 3
update Launguage_Translation_Text set TranslatedText='Aprobador 3' where Text='Sec. Approver 2' and languageid= 3
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values (3,'Participant(s)','Participante(s)')
--ZD 101611
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Room image width and height should not exceed 250 pixels.','Habitaci�n imagen anchura y la altura no debe superar los 250 p�xeles.')
--ZD 101720
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conferences must be scheduled at least 20 minutes from the current time. Use the ''Start Now'' option or ''Instant Conference'' to schedule a meeting immediately','Las conferencias deben ser programadas por lo menos 20 minutos de la hora actual. Utilice la opci�n ''Empezar Inmediatemente'' o "Conferencia Instant�nea ''para programar una reuni�n inmediatamente')
--ZD 101730
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'No file selected','No hay archivos seleccionados')
--ZD 101808
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Notification on Pending Rooms/Endpoints','Notificaci�n en pendientes Habitaciones / Puntos Finales')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Please select a record for import.','Por favor, seleccione un registro para la importaci�n.')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'No RPRM MCUs to Poll Rooms/Endpoints. Error Code : 100','No MCUs RPRM al voto Habitaciones / puntos finales. C�digo de error: 100')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'No RPRM MCUs to Poll Rooms/Endpoints.','No MCUs RPRM al voto Habitaciones / puntos finales.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No records found','No se encontraron registros')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid Participant code for user','C�digo de participante no v�lida para el usuario')
--ZD 101913 
update Launguage_Translation_Text 
set TranslatedText='Administrar Mi Vest�bulo' 
where Text='Manage My Lobby' and Languageid = 3
--ZD 101803
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Default Family','Familia por defecto')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Family 1','Familia 1')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Family 2','Familia 2')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Family 3','Familia 3')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Family 4','Familia 4')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Family 5','Familia 5')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'VMR Type','Tipo VMR')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Note: The selected video display layout is not supported by the MCU in conference.','Nota: El dise�o de pantalla de v�deo seleccionada no es compatible con el MCU en conferencia.')
--ZD 102032
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Pexip MCU Configuration','Configuraci�n Pexip MCU')
----ZD 101930
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Account Settings','Ajustes de la Cuenta')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Created Date','Creado fecha')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled Usage','Uso Programado')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Scheduled Minutes','Minutos programadas')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'LDAP Groups','Grupos LDAP')
--ZD 102054
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Express Form','Formulario Expreso')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Advanced Form','Formulario Avanzada')
--ZD 102085
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference failure Notification','Fracaso Conferencia Notificaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error Code','C�digo de error')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Error Message','Mensaje de error')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'New/Edit Conference','Nuevo / Editar Conferencia')

--ZD 102333
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To edit an existing work order, click the Edit button.','Para modificar una orden de trabajo existente, haga clic en el bot�n Editar.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the Add New Audiovisual Work Order button.','Para a�adir una nueva orden de trabajo, haga clic en el bot�n Nuevo Audiovisual Orden de Trabajo en Agregar.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the Add New Catering Work Order button.','Para a�adir una nueva orden de trabajo, haga clic en el bot�n Nuevo Catering Orden de Trabajo en Agregar.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To add a new work order, click the Add New Facility Work Order button.','Para a�adir una nueva orden de trabajo, haga clic en el bot�n Agregar nuevo trabajo Facilidad Orden.')

--ZD 102263
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Invalid login background image dimension.','Entrar fondo no v�lida dimensi�n de la imagen.')
--ZD 102331
insert into Launguage_Translation_Text ( LanguageID, Text, TranslatedText) values (3,'Please specify the Requested Quantity of each item and select either Update to calculate the total price of this work order or select Create to add the work order to the conference.','Por favor, especifique la cantidad solicitada de cada elemento y seleccione Actualizar para calcular el precio total de esta orden de trabajo o seleccione Crear para agregar la orden de trabajo de la conferencia.')

--ZD 102432
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'A system error has occurred. Please contact your myVRM system administrator and give them the following error code.','Se ha producido un error del sistema. Por favor, p�ngase en contacto con el administrador del sistema myVRM y darles el c�digo de error.')
--ZD 102332
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Edit button to save these changes.','Haga clic en el bot�n Editar para guardar los cambios.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Click Create button to save these changes.','Haga clic en el bot�n Crear para guardar los cambios.')

--ZD 102337
insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'To create a new catering work order, click on the Add New Catering Work Order button. To modify an existing work order, click the Edit button. To delete an existing work order, click the Delete button.','Para crear una nueva orden de trabajo de restauraci�n, haga clic en el bot�n Agregar nuevo Catering Orden de Trabajo. Para modificar una orden de trabajo existente, haga clic en el bot�n Editar. Para eliminar una orden de trabajo existente, haga clic en el bot�n Eliminar.')

--ZD 102334

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Are you sure you want to delete this work order?','�Seguro que quieres borrar este orden de trabajo?')
--Spanish
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'To create a new facility work order, click on the Add New Facility Work Order button. To modify an existing work order, click the Edit button. To delete an existing work order, click the Delete button.'
,'Para crear una nueva orden de trabajo instalaci�n, haga clic en el bot�n A�adir nuevo trabajo Facilidad Orden. Para modificar una orden de trabajo existente, haga clic en el bot�n Editar. Para eliminar una orden de trabajo existente, haga clic en el bot�n Eliminar.')

--ZD 102499

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work Orders br Details','Detalles de las br �rdenes de trabajo')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Search br Work Orders','Buscar �rdenes de br trabajo')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Work Orders br Calendar','Calendario de br �rdenes de Trabajo')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Dial-in Information(Customized)','Informaci�n de la Conferencia de acceso telef�nico(Personalizado)')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Report type','Nombre del informe')

-- ZD 102727

update Launguage_Translation_Text set TranslatedText='conferencias' where LanguageID=3 and TEXT='Conferences'
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'every friday at','todos los viernes a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'at the end of every month at','al final de cada mes a las')

--ZD 102759
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'French','Franc�s')

-- ZD 102473
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IBM Notes Forms','IBM Notes Formas')

--ZD 102647
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Custom Options Report','Informes opci�n personalizada')

--ZD 102710

Update Launguage_Translation_Text Set Text='There are no work orders associated with this conference',
 TranslatedText='No hay �rdenes de trabajo asociadas con esta conferencia' 
Where Text='There are no Workorders associated with this conference' and LanguageID=3

Update Launguage_Translation_Text Set Text='Work orders can only be created with rooms selected for this conference.',
 TranslatedText='Las �rdenes de trabajo s�lo se pueden crear con salones seleccionados para esta conferencia.' 
Where Text='Workorders can only be created with rooms selected for this conference.' and LanguageID=3

Update Launguage_Translation_Text Set Text='Are you sure you want to delete this work order?',
 TranslatedText='�Est� seguro de que quiere eliminar esta orden de trabajo?' 
Where Text='Are you sure you want to delete this Workorder?' and LanguageID=3

Update Launguage_Translation_Text Set Text='A/V work orders',
 TranslatedText='�rdenes de trabajo de A/V' 
Where Text='A/V Workorders' and LanguageID=3

Update Launguage_Translation_Text Set Text='work orders can only be created with rooms selected for this conference. ',
 TranslatedText='Las �rdenes de trabajo s�lo se pueden crear con salones seleccionados para esta conferencia.' 
Where Text='Workorders can only be created with rooms selected for this conference. ' and LanguageID=3

Update Launguage_Translation_Text Set Text='View Catering work order',
 TranslatedText='Ver Orden de trabajo de Catering' 
Where Text='View Catering Workorder' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please check the start and end Date/time for this work order.',
 TranslatedText='Por favor, compruebe el inicio y el final de fecha / hora para esta orden de trabajo.' 
Where Text='Please check the start and end Date/time for this workorder.' and LanguageID=3

Update Launguage_Translation_Text Set Text='View Housekeeping work orders',
 TranslatedText='Ver �rdenes de trabajo de mantenimiento' 
Where Text='View Housekeeping workorders' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please check Start and Completed Date/Time for this work order.',
 TranslatedText='Por favor, revise la fecha/hora de inicio y finalizaci�n para esta orden de trabajo.' 
Where Text='Please check Start and Completed Date/Time for this workorder.' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please check the start and end time for this work order.',
 TranslatedText='Por favor, revise la hora de inicio y finalizaci�n para esta orden de trabajo' 
Where Text='Please check the start and end time for this workorder.' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please save or cancel current work order prior to editing or deleting existing work orders.',
 TranslatedText='Por favor, guarde o cancele la orden de trabajo actual antes de editar o eliminar �rdenes de trabajo existentes.' 
Where Text='Please save or cancel current workorder prior to editing or deleting existing workorders.' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please save/update or cancel current work order prior to editing or deleting existing work orders.',
 TranslatedText='Por favor, guarde/actualice o cancele la orden de trabajo actual antes de editar o eliminar �rdenes de trabajo existentes.' 
Where Text='Please save/update or cancel current workorder prior to editing or deleting existing workorders.' and LanguageID=3

Update Launguage_Translation_Text Set Text='Please specify Quantity and either click update to calculate total price for this work order or click create to add work order.',
 TranslatedText='Por favor, especifique la cantidad y,/o  haga clic en Actualizar para calcular el precio total de esta orden de trabajo o haga clic en Crear para agregar una orden de trabajo.' 
Where Text='Please specify Quantity and either click update to calculate total price for this work order or click create to add workorder.' and LanguageID=3

Update Launguage_Translation_Text Set Text='work orders br Calendar',
 TranslatedText='Detalles de las br �rdenes de trabajo' 
Where Text='Workorders br Calendar' and LanguageID=3

Update Launguage_Translation_Text Set Text='Search br work orders',
 TranslatedText='Buscar �rdenes  br de trabajo' 
Where Text='Search br Workorders' and LanguageID=3

Update Launguage_Translation_Text Set Text='View Inventory work orders',
 TranslatedText='Ver �rdenes de trabajo de Inventario' 
Where Text='View Inventory workorders' and LanguageID=3

Update Launguage_Translation_Text Set Text='work order Comment',
 TranslatedText='Comentario de Orden de trabajo' 
Where Text='Workorder Comment' and LanguageID=3

Update Launguage_Translation_Text Set Text='Click Update to re-calculate total OR click Edit to update this work order.',
 TranslatedText='Haga clic en Actualizar para volver a calcular totales o haga clic en Editar para actualizar esta Orden de trabajo.' 
Where Text='Click Update to re-calculate total OR click Edit to update this Workorder.' and LanguageID=3

--ZD 102706

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Username','Nom d''utilisateur')

insert into Launguage_Translation_Text(LanguageID,Text,TranslatedText) values(3,'Requester','Solicitante')  
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference #','Conferencia #')



-- ZD 103095 - HD
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Floor Plans','Piso Planos')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select a valid Image Format like JPG GIF or PNG','Por favor, seleccione un formato de imagen v�lida como JPG GIF o PNG')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Su conferencia se ha presentado con �xito y se encuentra actualmente en estado de lista de espera.','Su conferencia se ha presentado con �xito y se encuentra actualmente en estado de lista de espera.')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Wait List Conference Notification','Attendez notification Conf�rence Liste')


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Wait List Conferences','Lista de Espera Conferencias')

-- ZD 102835
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'WO Admin-in-charge','WO Administrator encargado')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'WO Type','WO Tipo')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Room Host','Anfitri�n de Salon')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Inventory Item','Inventario de elemento')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Quantity','Cantidad')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'WO Name','WO Nombre')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3,'Work Orders Report','�rdenes de trabajo Informe')

--ZD 103181
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'No items found','No se encontraron art�culos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Mentioned entity code could not be linked with conference as it does not match.','C�digo mencionada entidad no podr�a estar vinculado con la conferencia, ya que no coincide.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter specific entity code.','Por favor, introduzca el c�digo espec�fico entidad.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter the valid entity code as it does not match.','Por favor, introduzca el c�digo de entidad v�lida, ya que no coincide.')

--ZD 102490

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in Information','Informaci�n de Marcaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Dial-in Information:','Informaci�n de Marcaci�n:')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,' Password:','Contrase�a')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'External Participant(s) :','Participante externo (s) :')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'IP/ISDN address: ','Direcci�n IP/ISDN: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connection type: ','Tipo de conexi�n: ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Connection','Conexi�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Direct','Directo')



--ZD 103268
    
update Launguage_Translation_Text set text = 'Catering Reminder' , TranslatedText = 'Recordatorio Catering'
where text = 'Catering Remainder' and LanguageID = 3


update Launguage_Translation_Text set text = 'Facility Reminder' , TranslatedText = 'Recordatorio Fondo'
where text = 'Facility Remainder' and LanguageID = 3


update Launguage_Translation_Text set text = 'Audiovisual Reminder' , TranslatedText = 'Recordatorio Audiovisual'
where text = 'Audiovisual Remainder' and LanguageID = 3

update Launguage_Translation_Text set text = 'Participant Reminder' , TranslatedText = 'Recordatorio Participante'
where text = 'Participant Remainder' and LanguageID = 3

update Launguage_Translation_Text set text = 'Inventory Reminder' , TranslatedText = 'Recordatorio Inventario'
where text = 'Inventory Remainder' and LanguageID = 3


update Launguage_Translation_Text set text = 'HK Reminder' , TranslatedText = 'Recordatorio HK'
where text = 'HK Remainder' and LanguageID = 3

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please select at least one room to the conference.','Por favor, seleccione al menos una habitaci�n a la conferencia.' )

--ZD 103263

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter Blue Jeans password','por favor entre Blue Jeans contrase�a ')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please enter Virtual Meeting Room password','Introduzca virtual contrase�a Sala de reuniones')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'BlueJeans Meeting ID','BlueJeans Reuni�n ID')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'BlueJeans Meeting Passcode','BlueJeans Reuni�n Contrase�a')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'BlueJeans Meeting URL','BlueJeans Reuni�n URL')


-- 103493

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Personnel Alert','Alerta Personal')


--Spanish --ZD 103531
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'An e-mail has been sent to the address entered with instructions to reset your password.','Un e-mail ha sido enviado a la direcci�n introducida con instrucciones para restablecer tu contrase�a.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please provide the information below and select a room to attend this conference.','Por favor, proporcione la siguiente informaci�n y seleccione un sal�n para asistir a esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Please provide the information below to attend this conference.','Por favor, proporcione la siguiente informaci�n para asistir a esta conferencia.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Your session expired. Please sign in to continue...','Su sesi�n ha caducado. Por favor, inicie sesi�n para continuar...')

--ZD 103692

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3, 'The value in the # of Attendees field must be greater than zero per room', 'El valor en el campo N�mero de asistentes debe ser mayor que cero por habitaci�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3, 'A room MUST be selected as the host location of this conference', 'Un cuarto DEBEN ser seleccionado como la ubicaci�n de host de esta conferencia')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Total Travel Avoided', 'Evitada Viajes total')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Room Name', 'Nombre del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, '# of Attendees', 'N� de asistentes')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Room Name', 'Nombre del sal�n')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Rooms In Conf.', 'Sal�n en Conf')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Host Room', 'Nombre del sal�n')


--Spanish
update Launguage_Translation_Text set TranslatedText = 'N�mero de asistentes'
where LanguageID = 3 and Text = '# of Attendees'

update Launguage_Translation_Text set TranslatedText = 'Total de viajes evitados'
where LanguageID = 3 and Text = 'Total Travel Avoided'

update Launguage_Translation_Text set TranslatedText = 'Una habitaci�n DEBE ser seleccionada como la ubicaci�n anfitriona de esta conferencia'
where LanguageID = 3 and Text = 'A room MUST be selected as the host location of this conference'

update Launguage_Translation_Text set TranslatedText = 'El valor en el campo de n�mero de asistentes debe ser mayor que cero'
where LanguageID = 3 and Text = 'The value in the # of Attendees field must be greater than zero per room'

update Launguage_Translation_Text set TranslatedText = 'Sal�n en Conf - N�mero de asistentes'
where LanguageID = 3 and Text = 'Rooms In Conf - # of Attendees'

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) 
values (3, 'Rooms In Conf - # of Attendees', 'Sal�n en Conf - N�mero de asistentes')

--ZD 102700
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Map 1 attachment width and height should not exceed 250 pixels.', 'Mapa 1 anchura y la altura de fijaci�n no deben superar los 250 p�xeles.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Map 2 attachment width and height should not exceed 250 pixels.', 'Mapa 2 anchura y la altura de fijaci�n no deben superar los 250 p�xeles.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Misc 1 attachment width and height should not exceed 250 pixels.', 'Varios 1 anchura y la altura de fijaci�n no deben superar los 250 p�xeles.')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3, 'Misc 2 attachment width and height should not exceed 250 pixels.', 'Varios 2 anchura y la altura de fijaci�n no deben exceder 250 p�xeles.')

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requester Phone','Tel�fono del solicitante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requester Cell','M�vil del solicitante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Requester Email','Correo-e del solicitante')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Numeric values only','S�lo valores num�ricos' )
Update Launguage_Translation_Text set Text='Unblock' Where Text='UnBlock'

--ZD 104116

Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Blue Jeans License does not permit this operation','La licencia de Blue Jeans no permite esta operaci�n')
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Limited to Active Users','Limitado para Usuarios Activos')
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Max Blue Jeans User','Max Usuarios de Blue Jeans')

-- Spanish
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Create MCU Load Balance Group','Crear Grupo de Balance de Carga de MCU',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Edit MCU Load Balance Group','Editar Grupo de Balance de Carga de MCU',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Please select the lead MCU.','Por favor seleccione la MCU plomo.',1)


Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (3,'BlueJeans Meeting ID(Customized)','BlueJeans Reuni�n ID(Personalizado)',1)
Insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (3,'BlueJeans Meeting Passcode(Customized)','BlueJeans Reuni�n Contrase�a(Personalizado)',1)

--ZD 102131
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Date/Time Signed-In','Fecha/Hora Registrada',1)


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Conference Room Removal Notification','Notificaci�n de eliminaci�n de una sala de conferencia ',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (3,'Please enter a valid email address','Por favor ingrese un correo electronico valido',1)

--ZD 102131 104474
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Attended Conference','Conferencia Atendida',1)

--ZD 104169
insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText) values (3,'Conference Time Zone','Zona horaria de la conferencia')

--ZD 104163

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (3,'Invalid Time (HH:MM)','Hora inv�lida (HH:MM)', 1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) values (3,'Invalid Date','Fecha inv�lida', 1)

update Launguage_Translation_Text set TranslatedText ='Hora inv�lida (HH:MM))', TextType = 1 where Text ='Invalid Time (HH:mm)' and languageid = 3


--ZD 104542

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (3,'The myVRM system is unavailable during the date and time selected. Please check the myVRM system availability and try again. Hours of operation: ','El sistema myVRM no est� disponible durante la fecha y hora seleccionada. Por favor compruebe la disponibilidad del sistema myVRM y vuelva a intentarlo. Horas de funcionamiento: ', 1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText, TextType) values (3,' Error Code : ',' C�digo de error : ', 1)




--Spanish

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Maximum Capacity between 0 and 10000','Capacidad m�xima entre 0 y 10000',1)

insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'Image missing for','Imagen faltante para',1)


insert into Launguage_Translation_Text (LanguageID, Text, TranslatedText,TextType) 
values (3,'List of rooms are imported without images.','La lista de salones es importada sin im�genes',1)