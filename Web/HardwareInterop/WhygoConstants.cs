﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RTC_v181
{
    public class WhygoConstants
    {
        
         //Countries (<<WhygoCountryID, myVRMCountryID>, Whygo Country Name>)


         public static Dictionary<KeyValuePair<int, int>, string> CountriesDict = new Dictionary<KeyValuePair<int, int>, string>()
                                                                       {

                                                                             {new KeyValuePair<int, int>( 1, 13) ,"Australia"},

                                                                             {new KeyValuePair<int, int>( 2, 152) , "New Zealand"},

                                                                              {new KeyValuePair<int, int>( 3, 225) , "USA"},

                                                                             {new KeyValuePair<int, int>( 82, 161), "Pakistan"},

                                                                             {new KeyValuePair<int, int>( 160, 10), "Argentina"},

                                                                             {new KeyValuePair<int, int>( 161, 14), "Austria"},

                                                                             {new KeyValuePair<int, int>( 162, 19), "Barbados"},

                                                                             {new KeyValuePair<int, int>( 163, 21), "Belgium"},

                                                                             {new KeyValuePair<int, int>( 164, 26), "Bolivia"},

                                                                             {new KeyValuePair<int, int>( 165, 30), "Brazil"},

                                                                             {new KeyValuePair<int, int>( 167, 33), "Bulgaria"},

                                                                             {new KeyValuePair<int, int>( 168, 38), "Canada"},

                                                                             {new KeyValuePair<int, int>( 171, 43), "Chile"},

                                                                             {new KeyValuePair<int, int>( 172, 44), "China"},

                                                                             {new KeyValuePair<int, int>( 173, 47), "Colombia"},

                                                                             {new KeyValuePair<int, int>( 175, 53), "Croatia"},

                                                                             {new KeyValuePair<int, int>( 176, 55), "Cyprus"},

                                                                             {new KeyValuePair<int, int>( 177, 56), "Czech Republic"},

                                                                             {new KeyValuePair<int, int>( 178, 57), "Denmark"},

                                                                             {new KeyValuePair<int, int>( 179, 62), "Egypt"},

                                                                             {new KeyValuePair<int, int>( 180, 66), "Estonia"},

                                                                             {new KeyValuePair<int, int>( 181, 70), "Fiji"},

                                                                             {new KeyValuePair<int, int>( 182, 71), "Finland"},

                                                                             {new KeyValuePair<int, int>( 183, 72), "France"},

                                                                             {new KeyValuePair<int, int>( 184, 73), "French Guiana"},

                                                                             {new KeyValuePair<int, int>( 185, 74), "French Polynesia"},

                                                                             {new KeyValuePair<int, int>( 186, 79), "Germany"},

                                                                             {new KeyValuePair<int, int>( 187, 82), "Greece"},

                                                                             {new KeyValuePair<int, int>( 188, 85), "Guadeloupe"},

                                                                             {new KeyValuePair<int, int>( 189, 96), "Hungary"},

                                                                             {new KeyValuePair<int, int>( 190, 98), "India"},

                                                                             {new KeyValuePair<int, int>( 191, 99), "Indonesia"},

                                                                             {new KeyValuePair<int, int>( 192, 102), "Ireland, Republic of"},

                                                                             {new KeyValuePair<int, int>( 193, 103), "Israel"},

                                                                             {new KeyValuePair<int, int>( 194, 104), "Italy"},

                                                                             {new KeyValuePair<int, int>( 195, 105), "Jamaica"},

                                                                             {new KeyValuePair<int, int>( 196, 106), "Japan"},

                                                                             {new KeyValuePair<int, int>( 197, 107), "Jordan"},

                                                                             {new KeyValuePair<int, int>( 198, 123), "Luxembourg"},

                                                                             {new KeyValuePair<int, int>( 199, 125), "Macedonia"},

                                                                             {new KeyValuePair<int, int>( 200, 128), "Malaysia"},

                                                                             {new KeyValuePair<int, int>( 201, 131), "Malta"},

                                                                             {new KeyValuePair<int, int>( 202, 133), "Martinique"},

                                                                             {new KeyValuePair<int, int>( 203, 135), "Mauritius"},

                                                                             {new KeyValuePair<int, int>( 204, 143), "Morocco"},

                                                                             {new KeyValuePair<int, int>( 205, 146), "Namibia"},

                                                                             {new KeyValuePair<int, int>( 206, 149), "Netherlands"},

                                                                             {new KeyValuePair<int, int>( 207, 159), "Norway"},

                                                                             {new KeyValuePair<int, int>( 208, 160), "Oman"},

                                                                             {new KeyValuePair<int, int>( 209, 164), "Panama"},

                                                                             {new KeyValuePair<int, int>( 210, 167), "Peru"},

                                                                             {new KeyValuePair<int, int>( 211, 168), "Philippines"},

                                                                             {new KeyValuePair<int, int>( 212, 170), "Poland"},

                                                                             {new KeyValuePair<int, int>( 213, 171), "Portugal"},

                                                                             {new KeyValuePair<int, int>( 214, 172), "Puerto Rico"},

                                                                             {new KeyValuePair<int, int>( 215, 184), "Republic of San Marino"},

                                                                             {new KeyValuePair<int, int>( 216, 174), "La Reunion"},

                                                                             {new KeyValuePair<int, int>( 217, 175), "Romania"},

                                                                             {new KeyValuePair<int, int>( 218, 176), "Russian Federation"},

                                                                             {new KeyValuePair<int, int>( 219, 186), "Saudi Arabia"},

                                                                             {new KeyValuePair<int, int>( 220, 191), "Singapore"},

                                                                             {new KeyValuePair<int, int>( 221, 196), "South Africa"},

                                                                             {new KeyValuePair<int, int>( 222, 112), "Korea, Republic of"},

                                                                             {new KeyValuePair<int, int>( 223, 198), "Spain"},

                                                                             {new KeyValuePair<int, int>( 224, 199), "Sri Lanka"},

                                                                             {new KeyValuePair<int, int>( 225, 204), "Sweden"},

                                                                             {new KeyValuePair<int, int>( 226, 205), "Switzerland"},

                                                                             {new KeyValuePair<int, int>( 227, 207), "Taiwan"},

                                                                             {new KeyValuePair<int, int>( 228, 210), "Thailand"},

                                                                             {new KeyValuePair<int, int>( 230, 217), "Turkey"},

                                                                             {new KeyValuePair<int, int>( 231, 223), "United Arab Emirates"},

                                                                             {new KeyValuePair<int, int>( 232, 224), "United Kingdom"},

                                                                             {new KeyValuePair<int, int>( 233, 227), "Uruguay"},

                                                                             {new KeyValuePair<int, int>( 234, 230), "Venezuela"},

                                                                             {new KeyValuePair<int, int>( 237, 231), "Vietnam"},

                                                                             {new KeyValuePair<int, int>( 238, 173), "Qatar"},

                                                                             {new KeyValuePair<int, int>( 240, 221), "Uganda"},

                                                                             {new KeyValuePair<int, int>( 241, 215), "Trinidad & Tobago"},

                                                                             {new KeyValuePair<int, int>( 242, 117), "Lebanon"},

                                                                             {new KeyValuePair<int, int>( 243, 113), "Kuwait"},

                                                                             {new KeyValuePair<int, int>( 244, 18), "Bangladesh"},

                                                                             {new KeyValuePair<int, int>( 245, 209), "Tanzania"},

                                                                             {new KeyValuePair<int, int>( 246, 102), "Ireland, Northern"},

                                                                             {new KeyValuePair<int, int>( 247, 239), "Scotland"},

                                                                             {new KeyValuePair<int, int>( 248, 224), "England"},

                                                                             {new KeyValuePair<int, int>( 249, 240), "Wales"},

                                                                             {new KeyValuePair<int, int>( 250, 17), "Bahrain"},

                                                                             {new KeyValuePair<int, int>( 251, 87), "Guatemala"},

                                                                             {new KeyValuePair<int, int>( 253, 192), "Slovakia"},

                                                                             {new KeyValuePair<int, int>( 254, 216), "Tunisia"},

                                                                             {new KeyValuePair<int, int>( 255, 222), "Ukraine"},

                                                                             {new KeyValuePair<int, int>( 256, 60), "Dominican Republic"},

                                                                             {new KeyValuePair<int, int>( 257, 116), "Latvia"},

                                                                             {new KeyValuePair<int, int>( 258, 137), "Mexico"},

                                                                             {new KeyValuePair<int, int>( 259, 80), "Ghana"},

                                                                             {new KeyValuePair<int, int>( 260, 28), "Botswana"},

                                                                             {new KeyValuePair<int, int>( 261, 165), "Papua New Guinea"},

                                                                             {new KeyValuePair<int, int>( 262, 36), "Cambodia"},

                                                                             {new KeyValuePair<int, int>( 263, 1), "Afghanistan"},

                                                                             {new KeyValuePair<int, int>( 264, 144), "Mozambique"},

                                                                             {new KeyValuePair<int, int>( 266, 100), "Iran"},

                                                                             {new KeyValuePair<int, int>( 267, 101), "Iraq"},

                                                                             {new KeyValuePair<int, int>( 268, 236), "Yemen"},

                                                                             {new KeyValuePair<int, int>( 270, 206), "Syria"},

                                                                             {new KeyValuePair<int, int>( 271, 108), "Kazakhstan"},

                                                                             {new KeyValuePair<int, int>( 272, 141), "Mongolia"},

                                                                             {new KeyValuePair<int, int>( 273, 52), "Costa Rica"},

                                                                             {new KeyValuePair<int, int>( 274, 155), "Nigeria"},

                                                                             {new KeyValuePair<int, int>( 275, 122), "Lithuania"},

                                                                             {new KeyValuePair<int, int>( 276, 81), "Gibraltar"},

                                                                             {new KeyValuePair<int, int>( 277, 24), "Bermuda"},

                                                                             {new KeyValuePair<int, int>( 278, 27), "Bosnia-Herzegovina"},

                                                                             {new KeyValuePair<int, int>( 279, 2), "Albania"},

                                                                             {new KeyValuePair<int, int>( 280, 179), "Saint Kitts and Nevis"},

                                                                             {new KeyValuePair<int, int>( 281, 129), "Maldives"},

                                                                             {new KeyValuePair<int, int>( 282, 129), "Serbia"},

                                                                             {new KeyValuePair<int, int>( 283, 109), "Kenya"},

                                                                             {new KeyValuePair<int, int>( 284, 241), "Cote d'Ivoire"},

                                                                             {new KeyValuePair<int, int>( 285, 140), "Monaco"},

                                                                             {new KeyValuePair<int, int>( 286, 233), "US Virgin Islands"},

                                                                             {new KeyValuePair<int, int>( 287, 75), "French West Indies"},

                                                                             {new KeyValuePair<int, int>( 288, 193), "Slovenia"},

                                                                             {new KeyValuePair<int, int>( 289, 238), "Zimbabwe"},

                                                                             {new KeyValuePair<int, int>( 290, 200), "Sudan"},

                                                                             {new KeyValuePair<int, int>( 291, 67), "Ethiopia"},

                                                                             {new KeyValuePair<int, int>( 292, 34), "Burkino Faso"},

                                                                             {new KeyValuePair<int, int>( 293, 187), "Senegal"},

                                                                             {new KeyValuePair<int, int>( 294, 134), "Mauritania"},

                                                                             {new KeyValuePair<int, int>( 295, 61), "Ecuador"},

                                                                             //{new KeyValuePair<int, int>( 296, 225), "Ireland"},

                                                                             {new KeyValuePair<int, int>( 297, 150), "Netherlands Antilles"},

                                                                             {new KeyValuePair<int, int>( 298, 180), "Saint Lucia"},

                                                                             {new KeyValuePair<int, int>( 299, 163), "Palestinian Authority"},

                                                                             {new KeyValuePair<int, int>( 300, 211), "Timor Leste"},

                                                                             {new KeyValuePair<int, int>( 301, 78), "Georgia"},

                                                                             {new KeyValuePair<int, int>( 302, 242), "Kosovo"},

                                                                             {new KeyValuePair<int, int>( 303, 11), "Armenia"},

                                                                             {new KeyValuePair<int, int>( 304, 15), "Azerbaijan"},

                                                                             {new KeyValuePair<int, int>( 305, 126), "Madagascar"},

                                                                             {new KeyValuePair<int, int>( 306, 153), "Nicaragua"},

                                                                             {new KeyValuePair<int, int>( 307, 23), "Benin"},

                                                                             {new KeyValuePair<int, int>( 308, 243), "Isle of Man"},

                                                                             {new KeyValuePair<int, int>( 309, 40), "Cayman Islands"},

                                                                             {new KeyValuePair<int, int>( 310, 63), "El Salvador"},

                                                                             {new KeyValuePair<int, int>( 312, 139), "Moldova"},

                                                                             {new KeyValuePair<int, int>( 313, 244), "Channel Islands"},

                                                                             {new KeyValuePair<int, int>( 314, 121), "Liechtenstein"},

                                                                             {new KeyValuePair<int, int>( 316, 32), "Brunei Darussalam"},

                                                                             {new KeyValuePair<int, int>( 317, 97), "Iceland"},

                                                                             {new KeyValuePair<int, int>( 318, 130), "Mali"},

                                                                             {new KeyValuePair<int, int>( 319, 37), "Cameroon"},

                                                                             {new KeyValuePair<int, int>( 321, 120), "Libya"},

                                                                             {new KeyValuePair<int, int>( 322, 86), "Guam"},

                                                                             {new KeyValuePair<int, int>( 323, 16), "Bahamas"},

                                                                             {new KeyValuePair<int, int>( 324, 245), "Isle of Wight"},

                                                                             {new KeyValuePair<int, int>( 325, 148), "Nepal"},

                                                                             {new KeyValuePair<int, int>( 326, 20), "Belarus"},

                                                                             {new KeyValuePair<int, int>( 327, 190), "Sierra Leone"},

                                                                             {new KeyValuePair<int, int>( 328, 237), "Zambia"},

                                                                             {new KeyValuePair<int, int>( 329, 229), "Vanuatu"},

                                                                             //{new KeyValuePair<int, int>( 3, 225)330, "Virtual Room"}

                                                                       };

 

            //Countries (<<stateID, state name>, countryID>)

         public static Dictionary<KeyValuePair<int, string>, int> StatesDict = new Dictionary<KeyValuePair<int, string>, int>()

            {

                        {new KeyValuePair<int, string>( 1, "New South Wales"), 1},

                        {new KeyValuePair<int, string>( 2, "Queensland"), 1},

                        {new KeyValuePair<int, string>( 3, "South Australia"), 1},

                        {new KeyValuePair<int, string>( 4, "Northern Territory"), 1},

                        {new KeyValuePair<int, string>( 5, "Western Australia"), 1},

                        {new KeyValuePair<int, string>( 6, "Australian Capital Territory"), 1},

                        {new KeyValuePair<int, string>( 7, "Victoria"), 1},

                        {new KeyValuePair<int, string>( 8, "Tasmania"), 1},

                        {new KeyValuePair<int, string>( 9, "North Island"), 2},

                        {new KeyValuePair<int, string>( 10, "South Island"), 2},

                        {new KeyValuePair<int, string>( 657, "Buenos Aires"), 160},

                        {new KeyValuePair<int, string>( 660, "San Isidro"), 160},

                        {new KeyValuePair<int, string>( 661, "Feldkirch"), 161},

                        {new KeyValuePair<int, string>( 662, "Innsbruck"), 161},

                        {new KeyValuePair<int, string>( 663, "Klagenfurt"), 161},

                        {new KeyValuePair<int, string>( 664, "Linz"), 161},

                        {new KeyValuePair<int, string>( 665, "Salzburg"), 161},

                        {new KeyValuePair<int, string>( 667, "Wieu"), 161},

                        {new KeyValuePair<int, string>( 669, "St Michael"), 162},

                        {new KeyValuePair<int, string>( 670, "Antwerp"), 163},

                        {new KeyValuePair<int, string>( 671, "Antwerp (Aartselaar)"), 163},

                        {new KeyValuePair<int, string>( 672, "Brussels"), 163},

                        {new KeyValuePair<int, string>( 673, "Charleroi"), 163},

                        {new KeyValuePair<int, string>( 674, "Vilvoorde"), 163},

                        {new KeyValuePair<int, string>( 675, "La Paz"), 164},

                        {new KeyValuePair<int, string>( 676, "Santa Cruz"), 164},

                        {new KeyValuePair<int, string>( 677, "Belo Horizonte"), 165},

                        {new KeyValuePair<int, string>( 678, "Brasilia"), 165},

                        {new KeyValuePair<int, string>( 679, "Curitiba"), 165},

                        {new KeyValuePair<int, string>( 680, "Porto Alegre"), 165},

                        {new KeyValuePair<int, string>( 681, "Rio De Janeiro"), 165},

                        {new KeyValuePair<int, string>( 682, "Salvador"), 165},

                        {new KeyValuePair<int, string>( 683, "Sao Paulo"), 165},

                        {new KeyValuePair<int, string>( 684, "Vitoria"), 165},

                        {new KeyValuePair<int, string>( 686, "Sofia"), 167},

                        {new KeyValuePair<int, string>( 696, "Alberta"), 168},

                        {new KeyValuePair<int, string>( 697, "British Columbia"), 168},

                        {new KeyValuePair<int, string>( 698, "New Brunswick"), 168},

                        {new KeyValuePair<int, string>( 699, "Northwest Territories"), 168},

                        {new KeyValuePair<int, string>( 700, "Nova Scotia"), 168},

                        {new KeyValuePair<int, string>( 701, "Ontario"), 168},

                        {new KeyValuePair<int, string>( 702, "Yukon Territory"), 168},

                        {new KeyValuePair<int, string>( 704, "Alderney"), 232},

                        {new KeyValuePair<int, string>( 705, "Guernsey"), 232},

                        {new KeyValuePair<int, string>( 706, "Santiago"), 171},

                        {new KeyValuePair<int, string>( 707, "Beijing"), 172},

                        {new KeyValuePair<int, string>( 708, "Hong Kong"), 172},

                        {new KeyValuePair<int, string>( 709, "Kowloon"), 172},

                        {new KeyValuePair<int, string>( 711, "Bogota"), 173},

                        {new KeyValuePair<int, string>( 712, "Cali"), 173},

                        {new KeyValuePair<int, string>( 713, "Medellin"), 173},

                        {new KeyValuePair<int, string>( 715, "Sv. Nedjecja"), 175},

                        {new KeyValuePair<int, string>( 716, "Zagreb"), 175},

                        {new KeyValuePair<int, string>( 717, "Nicosia"), 176},

                        {new KeyValuePair<int, string>( 718, "Jablonec Nad Nisou"), 177},

                        {new KeyValuePair<int, string>( 719, "Prague"), 177},

                        {new KeyValuePair<int, string>( 720, "Ballerup"), 178},

                        {new KeyValuePair<int, string>( 722, "Farum"), 178},

                        {new KeyValuePair<int, string>( 723, "Cairo"), 179},

                        {new KeyValuePair<int, string>( 724, "Giza"), 179},

                        {new KeyValuePair<int, string>( 725, "Tallinn"), 180},

                        {new KeyValuePair<int, string>( 726, "Suva"), 181},

                        {new KeyValuePair<int, string>( 727, "Helsinki"), 182},

                        {new KeyValuePair<int, string>( 728, "Turku"), 182},

                        {new KeyValuePair<int, string>( 729, "Agen"), 183},

                        {new KeyValuePair<int, string>( 730, "Ajaccio"), 183},

                        {new KeyValuePair<int, string>( 731, "Albi"), 183},

                        {new KeyValuePair<int, string>( 732, "Annonay"), 183},

                        {new KeyValuePair<int, string>( 733, "Auch"), 183},

                        {new KeyValuePair<int, string>( 734, "Aurillac"), 183},

                        {new KeyValuePair<int, string>( 735, "Bastia"), 183},

                        {new KeyValuePair<int, string>( 736, "Belfort"), 183},

                        {new KeyValuePair<int, string>( 737, "Besancon"), 183},

                        {new KeyValuePair<int, string>( 738, "Bordeaux"), 183},

                        {new KeyValuePair<int, string>( 739, "Bordeaux Centre"), 183},

                        {new KeyValuePair<int, string>( 740, "Bourges"), 183},

                        {new KeyValuePair<int, string>( 741, "Brive"), 183},

                        {new KeyValuePair<int, string>( 742, "Caen"), 183},

                        {new KeyValuePair<int, string>( 743, "Castres"), 183},

                        {new KeyValuePair<int, string>( 744, "Clermont-Ferrand"), 183},

                        {new KeyValuePair<int, string>( 746, "Colmar"), 183},

                        {new KeyValuePair<int, string>( 747, "Dijon"), 183},

                        {new KeyValuePair<int, string>( 748, "Grenoble"), 183},

                        {new KeyValuePair<int, string>( 750, "Guyanne Cayenne"), 183},

                        {new KeyValuePair<int, string>( 751, "Guyanne St Laurent"), 183},

                        {new KeyValuePair<int, string>( 752, "Jonzac"), 183},

                        //{new KeyValuePair<int, string>( 753, "La Martinique Fort De France"), 183},

                        {new KeyValuePair<int, string>( 754, "La Reunion St Denis"), 183},

                        {new KeyValuePair<int, string>( 755, "La Reunion St Pierre"), 183},

                        //{new KeyValuePair<int, string>( 756, "La Rochelle 2 sites"), 183},

                        {new KeyValuePair<int, string>( 757, "Labege Cedex"), 183},

                        {new KeyValuePair<int, string>( 758, "Les Herbiers"), 183},

                        {new KeyValuePair<int, string>( 759, "Libourne"), 183},

                        {new KeyValuePair<int, string>( 760, "Lille"), 183},

                        {new KeyValuePair<int, string>( 762, "Limoges"), 183},

                        {new KeyValuePair<int, string>( 763, "Loudun (Nord Poitiers)"), 183},

                        {new KeyValuePair<int, string>( 765, "Marseille"), 183},

                        {new KeyValuePair<int, string>( 766, "Montpellier"), 183},

                        {new KeyValuePair<int, string>( 767, "Nancy"), 183},

                        {new KeyValuePair<int, string>( 768, "Nantes"), 183},

                        {new KeyValuePair<int, string>( 769, "Nice"), 183},

                        {new KeyValuePair<int, string>( 771, "Nice-Antibes"), 183},

                        {new KeyValuePair<int, string>( 772, "Orsay"), 183},

                        {new KeyValuePair<int, string>( 773, "Paris"), 183},

                        {new KeyValuePair<int, string>( 774, "Paris - La Defense"), 183},

                        {new KeyValuePair<int, string>( 775, "Paris - Les Halles"), 183},

                        {new KeyValuePair<int, string>( 776, "Paris 17e"), 183},

                        {new KeyValuePair<int, string>( 777, "Paris Boulogne Pont de Sevre"), 183},

                        {new KeyValuePair<int, string>( 778, "Paris Boulogne Porte de St Cloud"), 183},

                        {new KeyValuePair<int, string>( 779, "Paris Est Porte de Montreuil"), 183},

                        {new KeyValuePair<int, string>( 780, "Paris Montparnasse 14e"), 183},

                        {new KeyValuePair<int, string>( 781, "Paris Suresnes"), 183},

                        {new KeyValuePair<int, string>( 782, "Pau"), 183},

                        {new KeyValuePair<int, string>( 783, "Poitiers"), 183},

                        {new KeyValuePair<int, string>( 784, "Quimper"), 183},

                        {new KeyValuePair<int, string>( 785, "Reims"), 183},

                        {new KeyValuePair<int, string>( 786, "Rennes"), 183},

                        {new KeyValuePair<int, string>( 787, "Rouen"), 183},

                        {new KeyValuePair<int, string>( 788, "Rouen (Mont Saint Aignan)"), 183},

                        {new KeyValuePair<int, string>( 789, "St Brieux"), 183},

                        {new KeyValuePair<int, string>( 790, "St Malo"), 183},

                        {new KeyValuePair<int, string>( 791, "Strabourg Sud"), 183},

                        {new KeyValuePair<int, string>( 792, "Strasbourg"), 183},

                        {new KeyValuePair<int, string>( 793, "Toulouse"), 183},

                        {new KeyValuePair<int, string>( 794, "Toulouse center"), 183},

                        {new KeyValuePair<int, string>( 795, "Toulouse Labege"), 183},

                        {new KeyValuePair<int, string>( 796, "Tours"), 183},

                        {new KeyValuePair<int, string>( 797, "Valence"), 183},

                        {new KeyValuePair<int, string>( 798, "Vannes"), 183},

                        {new KeyValuePair<int, string>( 799, "Vouneuil Sous Biard (East Poitiers)"), 183},

                        {new KeyValuePair<int, string>( 800, "Cayenne"), 184},

                        {new KeyValuePair<int, string>( 801, "Tahiti Papeete"), 185},

                        {new KeyValuePair<int, string>( 803, "Berlin"), 186},

                        {new KeyValuePair<int, string>( 804, "Berlin - Chalottenstrasse"), 186},

                        {new KeyValuePair<int, string>( 805, "Berlin-Mitte"), 186},

                        {new KeyValuePair<int, string>( 806, "Bonn"), 186},

                        {new KeyValuePair<int, string>( 807, "Bremen"), 186},

                        {new KeyValuePair<int, string>( 808, "Cologne"), 186},

                        {new KeyValuePair<int, string>( 809, "Dortmund"), 186},

                        {new KeyValuePair<int, string>( 810, "Dresden"), 186},

                        {new KeyValuePair<int, string>( 811, "Dusseldorf"), 186},

                        {new KeyValuePair<int, string>( 812, "Erfurt"), 186},

                        {new KeyValuePair<int, string>( 813, "Eschborn"), 186},

                        {new KeyValuePair<int, string>( 814, "Frankfurt"), 186},

                        {new KeyValuePair<int, string>( 817, "Freiburg"), 186},

                        {new KeyValuePair<int, string>( 818, "Halle"), 186},

                        {new KeyValuePair<int, string>( 819, "Hamburg"), 186},

                        {new KeyValuePair<int, string>( 820, "Hannover"), 186},

                        {new KeyValuePair<int, string>( 822, "Koln"), 186},

                        {new KeyValuePair<int, string>( 823, "Leipzig"), 186},

                        {new KeyValuePair<int, string>( 824, "Mannheim"), 186},

                        {new KeyValuePair<int, string>( 826, "Munich"), 186},

                        {new KeyValuePair<int, string>( 827, "Neckarslum"), 186},

                        {new KeyValuePair<int, string>( 828, "Nuremberg"), 186},

                        {new KeyValuePair<int, string>( 830, "Saarbrucken"), 186},

                        {new KeyValuePair<int, string>( 831, "Stuttgart"), 186},

                        {new KeyValuePair<int, string>( 832, "Wiesbaden"), 186},

                        {new KeyValuePair<int, string>( 833, "Zella-Mehlis"), 186},

                        {new KeyValuePair<int, string>( 834, "Athens"), 187},

                        {new KeyValuePair<int, string>( 835, "Thessaloniki"), 187},

                        {new KeyValuePair<int, string>( 836, "Pointe A Pitre"), 188},

                        {new KeyValuePair<int, string>( 837, "Budapest"), 189},

                        {new KeyValuePair<int, string>( 838, "Bangalore"), 190},

                        {new KeyValuePair<int, string>( 839, "Calcutta"), 190},

                        {new KeyValuePair<int, string>( 840, "Chennai"), 190},

                        {new KeyValuePair<int, string>( 841, "Hyderabad"), 190},

                        {new KeyValuePair<int, string>( 842, "Mumbai"), 190},

                        {new KeyValuePair<int, string>( 843, "New Delhi"), 190},

                        {new KeyValuePair<int, string>( 844, "Noida"), 190},

                        {new KeyValuePair<int, string>( 845, "Pune"), 190},

                        {new KeyValuePair<int, string>( 846, "Bali"), 191},

                        {new KeyValuePair<int, string>( 847, "Jakarta"), 191},

                        {new KeyValuePair<int, string>( 848, "Cork"), 192},

                        {new KeyValuePair<int, string>( 850, "Galway"), 192},

                        {new KeyValuePair<int, string>( 851, "Limerick"), 192},

                        {new KeyValuePair<int, string>( 852, "Barak"), 193},

                        {new KeyValuePair<int, string>( 853, "Eshel Hanassi"), 193},

                        {new KeyValuePair<int, string>( 854, "Jerusalem"), 193},

                        {new KeyValuePair<int, string>( 855, "Petach Tikva"), 193},

                        {new KeyValuePair<int, string>( 856, "Rosh Ha-Ayin"), 193},

                        {new KeyValuePair<int, string>( 857, "Tel Aviv"), 193},

                        {new KeyValuePair<int, string>( 858, "Bologna"), 194},

                        {new KeyValuePair<int, string>( 859, "Catanzaro"), 194},

                        {new KeyValuePair<int, string>( 860, "Florence"), 194},

                        {new KeyValuePair<int, string>( 861, "Genoa"), 194},

                        {new KeyValuePair<int, string>( 862, "Milan"), 194},

                        {new KeyValuePair<int, string>( 864, "Naples"), 194},

                        {new KeyValuePair<int, string>( 865, "Padova"), 194},

                        {new KeyValuePair<int, string>( 866, "Rome"), 194},

                        {new KeyValuePair<int, string>( 867, "Salerno"), 194},

                        {new KeyValuePair<int, string>( 868, "Torino"), 194},

                        {new KeyValuePair<int, string>( 869, "Trieste"), 194},

                        {new KeyValuePair<int, string>( 870, "Venice"), 194},

                        //{new KeyValuePair<int, string>( 871, "Kingston 10"), 195},

                        {new KeyValuePair<int, string>( 872, "Osaka"), 196},

                        {new KeyValuePair<int, string>( 873, "Tokyo"), 196},

                        {new KeyValuePair<int, string>( 874, "Amman"), 197},

                        {new KeyValuePair<int, string>( 875, "Luxembourg"), 198},

                        {new KeyValuePair<int, string>( 876, "Skopje"), 199},

                        {new KeyValuePair<int, string>( 877, "Kuala Lumpur"), 200},

                        {new KeyValuePair<int, string>( 878, "Subang Jaya Selangor"), 200},

                        {new KeyValuePair<int, string>( 879, "Floriana"), 201},

                        {new KeyValuePair<int, string>( 880, "Fort-de-France"), 202},

                        {new KeyValuePair<int, string>( 882, "Port Louis"), 203},

                        {new KeyValuePair<int, string>( 883, "Mohammedia"), 204},

                        {new KeyValuePair<int, string>( 884, "Windhoek"), 205},

                        {new KeyValuePair<int, string>( 887, "Breda"), 206},

                        {new KeyValuePair<int, string>( 889, "Groningen"), 206},

                        {new KeyValuePair<int, string>( 894, "Oslo"), 207},

                        {new KeyValuePair<int, string>( 895, "Stavanger"), 207},

                        {new KeyValuePair<int, string>( 896, "Troms"), 207},

                        {new KeyValuePair<int, string>( 897, "Trondheim"), 207},

                        {new KeyValuePair<int, string>( 898, "Muscat"), 208},

                        {new KeyValuePair<int, string>( 899, "Toronto"), 168},

                        {new KeyValuePair<int, string>( 900, "Karachi"), 82},

                        {new KeyValuePair<int, string>( 901, "Lahore"), 82},

                        {new KeyValuePair<int, string>( 902, "Panama"), 209},

                        {new KeyValuePair<int, string>( 903, "Lima"), 210},

                        {new KeyValuePair<int, string>( 904, "Pasig City"), 211},

                        {new KeyValuePair<int, string>( 905, "Poznan"), 212},

                        {new KeyValuePair<int, string>( 906, "Warsaw"), 212},

                        {new KeyValuePair<int, string>( 907, "Lisbon"), 213},

                        {new KeyValuePair<int, string>( 908, "Porto"), 213},

                        {new KeyValuePair<int, string>( 909, "San Juan"), 214},

                        {new KeyValuePair<int, string>( 910, "Rovereta"), 215},

                        {new KeyValuePair<int, string>( 911, "Saint-Denis"), 216},

                        {new KeyValuePair<int, string>( 912, "Bucharest"), 217},

                        {new KeyValuePair<int, string>( 913, "Moscow"), 218},

                        {new KeyValuePair<int, string>( 914, "Saint Petersburg"), 218},

                        {new KeyValuePair<int, string>( 916, "Riyadh"), 219},

                        {new KeyValuePair<int, string>( 917, "Singapore"), 220},

                        {new KeyValuePair<int, string>( 938, "Seoul"), 222},

                        {new KeyValuePair<int, string>( 939, "Barcelona"), 223},

                        {new KeyValuePair<int, string>( 940, "Bilbao"), 223},

                        {new KeyValuePair<int, string>( 941, "Castellon"), 223},

                        {new KeyValuePair<int, string>( 942, "Granada"), 223},

                        {new KeyValuePair<int, string>( 943, "Ibiza"), 223},

                        {new KeyValuePair<int, string>( 944, "Las Palmas"), 223},

                        {new KeyValuePair<int, string>( 946, "Malaga"), 223},

                        {new KeyValuePair<int, string>( 947, "Orense"), 223},

                        {new KeyValuePair<int, string>( 948, "Palma De Mallorca"), 223},

                        {new KeyValuePair<int, string>( 949, "Pontevedra"), 223},

                        {new KeyValuePair<int, string>( 950, "Rocafort"), 223},

                        {new KeyValuePair<int, string>( 951, "Sabadell"), 223},

                        {new KeyValuePair<int, string>( 952, "Santa Cruz De Tenerife"), 223},

                        {new KeyValuePair<int, string>( 953, "Santander"), 223},

                        {new KeyValuePair<int, string>( 954, "Valencia"), 223},

                        {new KeyValuePair<int, string>( 955, "Vigo"), 223},

                        {new KeyValuePair<int, string>( 956, "Zaragoza"), 223},

                        {new KeyValuePair<int, string>( 957, "Colombo"), 224},

                        {new KeyValuePair<int, string>( 958, "Fargelanda"), 225},

                        {new KeyValuePair<int, string>( 959, "Haparanda"), 225},

                        {new KeyValuePair<int, string>( 960, "Kalmar"), 225},

                        {new KeyValuePair<int, string>( 961, "Stockholm"), 225},

                        {new KeyValuePair<int, string>( 962, "Uppsala"), 225},

                        {new KeyValuePair<int, string>( 963, "Agno"), 226},

                        {new KeyValuePair<int, string>( 964, "Basel"), 226},

                        {new KeyValuePair<int, string>( 965, "Geneva"), 226},

                        {new KeyValuePair<int, string>( 966, "Lucerne (Ebikon)"), 226},

                        {new KeyValuePair<int, string>( 967, "Tagelswangen"), 226},

                        {new KeyValuePair<int, string>( 968, "Zurich"), 226},

                        {new KeyValuePair<int, string>( 969, "Kaohsiung"), 227},

                        {new KeyValuePair<int, string>( 970, "Tainan"), 227},

                        {new KeyValuePair<int, string>( 971, "Taipei"), 227},

                        {new KeyValuePair<int, string>( 972, "Bangkok"), 228},

                        {new KeyValuePair<int, string>( 973, "Pattaya"), 228},

                        {new KeyValuePair<int, string>( 975, "Istanbul"), 230},

                        {new KeyValuePair<int, string>( 976, "Izmir"), 230},

                        {new KeyValuePair<int, string>( 977, "Abu Dhabi"), 231},

                        {new KeyValuePair<int, string>( 978, "Dubai"), 231},

                        {new KeyValuePair<int, string>( 979, "Aberdeen"), 232},

                        {new KeyValuePair<int, string>( 980, "Aberystwyth"), 232},

                        {new KeyValuePair<int, string>( 981, "Ascot"), 232},

                        {new KeyValuePair<int, string>( 982, "Barnsley"), 232},

                        {new KeyValuePair<int, string>( 983, "Bedford"), 232},

                        {new KeyValuePair<int, string>( 985, "Birmingham"), 232},

                        {new KeyValuePair<int, string>( 986, "Bottisham (Cambridge)"), 232},

                        {new KeyValuePair<int, string>( 988, "Bournemouth"), 232},

                        {new KeyValuePair<int, string>( 989, "Bracknell"), 232},

                        {new KeyValuePair<int, string>( 990, "Branston"), 232},

                        {new KeyValuePair<int, string>( 991, "Brecon"), 232},

                        {new KeyValuePair<int, string>( 992, "Brentford"), 232},

                        {new KeyValuePair<int, string>( 993, "Brighton"), 232},

                        {new KeyValuePair<int, string>( 994, "Bristol"), 232},

                        {new KeyValuePair<int, string>( 995, "Bungay"), 232},

                        {new KeyValuePair<int, string>( 996, "Burton upon Trent"), 232},

                        {new KeyValuePair<int, string>( 997, "Caernarfon"), 232},

                        {new KeyValuePair<int, string>( 998, "Cambridgeshire"), 248},

                        {new KeyValuePair<int, string>( 999, "Canterbury"), 232},

                        {new KeyValuePair<int, string>( 1001, "Cardiff"), 232},

                        {new KeyValuePair<int, string>( 1002, "Carlisle"), 232},

                        {new KeyValuePair<int, string>( 1004, "Castle Donnington"), 232},

                        {new KeyValuePair<int, string>( 1005, "Cheadle"), 232},

                        {new KeyValuePair<int, string>( 1006, "Chelmsford"), 232},

                        {new KeyValuePair<int, string>( 1007, "Cheltenham"), 232},

                        {new KeyValuePair<int, string>( 1008, "Chertsey"), 232},

                        {new KeyValuePair<int, string>( 1009, "Chester"), 232},

                        {new KeyValuePair<int, string>( 1010, "Chippenham"), 232},

                        {new KeyValuePair<int, string>( 1011, "Colchester"), 232},

                        {new KeyValuePair<int, string>( 1012, "Coventry"), 232},

                        {new KeyValuePair<int, string>( 1014, "Croydon"), 232},

                        {new KeyValuePair<int, string>( 1016, "Dartford"), 232},

                        {new KeyValuePair<int, string>( 1017, "Doncaster"), 232},

                        {new KeyValuePair<int, string>( 1018, "Dover"), 232},

                        {new KeyValuePair<int, string>( 1019, "Dundee"), 232},

                        {new KeyValuePair<int, string>( 1020, "Dunfermline"), 232},

                        {new KeyValuePair<int, string>( 1021, "Ebbow Vale"), 232},

                        {new KeyValuePair<int, string>( 1023, "Exeter"), 232},

                        {new KeyValuePair<int, string>( 1024, "Falmouth"), 232},

                        {new KeyValuePair<int, string>( 1025, "Fetcham"), 232},

                        {new KeyValuePair<int, string>( 1026, "Fleet"), 232},

                        {new KeyValuePair<int, string>( 1027, "Gatwick Airport"), 232},

                        {new KeyValuePair<int, string>( 1028, "Glasgow"), 232},

                        {new KeyValuePair<int, string>( 1030, "Guildford"), 232},

                        {new KeyValuePair<int, string>( 1031, "Hammersmith"), 232},

                        {new KeyValuePair<int, string>( 1032, "Harrow"), 232},

                        {new KeyValuePair<int, string>( 1033, "Haverfordwest"), 232},

                        {new KeyValuePair<int, string>( 1034, "Heathrow"), 232},

                        {new KeyValuePair<int, string>( 1035, "Hengoed"), 232},

                        {new KeyValuePair<int, string>( 1036, "High Wycombe"), 232},

                        {new KeyValuePair<int, string>( 1037, "Hove"), 232},

                        {new KeyValuePair<int, string>( 1038, "Inverness"), 232},

                        {new KeyValuePair<int, string>( 1039, "Kettering"), 232},

                        {new KeyValuePair<int, string>( 1040, "Lampeter"), 232},

                        {new KeyValuePair<int, string>( 1041, "Lancaster"), 232},

                        {new KeyValuePair<int, string>( 1042, "Leamington Spa"), 232},

                        {new KeyValuePair<int, string>( 1043, "Leatherhead"), 232},

                        {new KeyValuePair<int, string>( 1044, "Leeds"), 232},

                        {new KeyValuePair<int, string>( 1045, "Leicester"), 232},

                        {new KeyValuePair<int, string>( 1046, "Liverpool"), 232},

                        {new KeyValuePair<int, string>( 1047, "Llandrindod Wells"), 232},

                        {new KeyValuePair<int, string>( 1048, "Llangefni"), 232},

                        {new KeyValuePair<int, string>( 1050, "Luton"), 232},

                        {new KeyValuePair<int, string>( 1051, "Maidenhead"), 232},

                        {new KeyValuePair<int, string>( 1052, "Manchester"), 232},

                        {new KeyValuePair<int, string>( 1053, "Manchester (Sale)"), 232},

                        {new KeyValuePair<int, string>( 1054, "Manchester Airport"), 232},

                        {new KeyValuePair<int, string>( 1055, "Mendlesham"), 232},

                        {new KeyValuePair<int, string>( 1056, "Middlesborough"), 232},

                        {new KeyValuePair<int, string>( 1057, "Milton Keynes"), 232},

                        {new KeyValuePair<int, string>( 1058, "Newcastle"), 232},

                        {new KeyValuePair<int, string>( 1059, "Newport"), 232},

                        //{new KeyValuePair<int, string>( 1060, "Northampton"), 232},

                        {new KeyValuePair<int, string>( 1061, "Norwich"), 232},

                        //{new KeyValuePair<int, string>( 1062, "Nottingham"), 232},

                        {new KeyValuePair<int, string>( 1063, "Okehampton"), 232},

                        {new KeyValuePair<int, string>( 1064, "Omagh"), 232},

                        {new KeyValuePair<int, string>( 1065, "Orpington"), 232},

                        //{new KeyValuePair<int, string>( 1066, "Oxford"), 232},

                        {new KeyValuePair<int, string>( 1067, "Pembroke"), 232},

                        {new KeyValuePair<int, string>( 1068, "Plymouth"), 232},

                        {new KeyValuePair<int, string>( 1069, "Pontypool"), 232},

                        {new KeyValuePair<int, string>( 1070, "Poole"), 232},

                        {new KeyValuePair<int, string>( 1071, "Reading"), 232},

                        {new KeyValuePair<int, string>( 1072, "Reigate"), 232},

                        {new KeyValuePair<int, string>( 1073, "Rickmansworth"), 232},

                        {new KeyValuePair<int, string>( 1074, "Rosyth (Dunfermline)"), 232},

                        {new KeyValuePair<int, string>( 1075, "Rotherham"), 232},

                        {new KeyValuePair<int, string>( 1076, "Rugby"), 232},

                        {new KeyValuePair<int, string>( 1077, "Selby"), 232},

                        {new KeyValuePair<int, string>( 1078, "Sheffield"), 232},

                        {new KeyValuePair<int, string>( 1079, "Slough"), 232},

                        {new KeyValuePair<int, string>( 1080, "Southampton (Fareham)"), 232},

                        {new KeyValuePair<int, string>( 1081, "St. Albans"), 232},

                        {new KeyValuePair<int, string>( 1083, "Stirling"), 232},

                        {new KeyValuePair<int, string>( 1084, "Stockton-on-Tees"), 232},

                        {new KeyValuePair<int, string>( 1085, "Sudbrooke"), 232},

                        {new KeyValuePair<int, string>( 1086, "Sunbury on Thames"), 232},

                        {new KeyValuePair<int, string>( 1087, "Sunderland"), 232},

                        {new KeyValuePair<int, string>( 1088, "Swindon"), 232},

                        {new KeyValuePair<int, string>( 1089, "Taunton"), 232},

                        {new KeyValuePair<int, string>( 1090, "Thurso"), 232},

                        {new KeyValuePair<int, string>( 1091, "Warrington"), 232},

                        {new KeyValuePair<int, string>( 1092, "Watford"), 232},

                        {new KeyValuePair<int, string>( 1093, "Welshpool"), 232},

                        {new KeyValuePair<int, string>( 1094, "Wigan"), 232},

                        {new KeyValuePair<int, string>( 1095, "Witney"), 232},

                        {new KeyValuePair<int, string>( 1096, "Woking"), 232},

                        {new KeyValuePair<int, string>( 1097, "Montevideo"), 233},

                        {new KeyValuePair<int, string>( 1098, "Caracas"), 234},

                        {new KeyValuePair<int, string>( 1102, "Mpumalanga"), 221},

                        {new KeyValuePair<int, string>( 1108, "Alabama"), 3},

                        {new KeyValuePair<int, string>( 1109, "Alaska"), 3},

                        {new KeyValuePair<int, string>( 1110, "American Samoa"), 3},

                        {new KeyValuePair<int, string>( 1111, "Arizona"), 3},

                        {new KeyValuePair<int, string>( 1112, "Arkansas"), 3},

                        {new KeyValuePair<int, string>( 1113, "California"), 3},

                        {new KeyValuePair<int, string>( 1114, "Colorado"), 3},

                        {new KeyValuePair<int, string>( 1115, "Connecticut"), 3},

                        {new KeyValuePair<int, string>( 1116, "Delaware"), 3},

                        {new KeyValuePair<int, string>( 1117, "District Of Columbia"), 3},

                        {new KeyValuePair<int, string>( 1118, "Federated States of Micronesia"), 3},

                        {new KeyValuePair<int, string>( 1119, "Florida"), 3},

                        {new KeyValuePair<int, string>( 1120, "Georgia"), 3},

                        {new KeyValuePair<int, string>( 1121, "Guam"), 3},

                        {new KeyValuePair<int, string>( 1122, "Hawaii"), 3},

                        {new KeyValuePair<int, string>( 1123, "Idaho"), 3},

                        {new KeyValuePair<int, string>( 1124, "Illinois"), 3},

                        {new KeyValuePair<int, string>( 1125, "Indiana"), 3},

                        {new KeyValuePair<int, string>( 1126, "Iowa"), 3},

                        {new KeyValuePair<int, string>( 1127, "Kansas"), 3},

                        {new KeyValuePair<int, string>( 1128, "Kentucky"), 3},

                        {new KeyValuePair<int, string>( 1129, "Louisiana"), 3},

                        {new KeyValuePair<int, string>( 1130, "Maine"), 3},

                        {new KeyValuePair<int, string>( 1131, "Marshall Islands"), 3},

                        {new KeyValuePair<int, string>( 1132, "Maryland"), 3},

                        {new KeyValuePair<int, string>( 1133, "Massachusetts"), 3},

                        {new KeyValuePair<int, string>( 1134, "Michigan"), 3},

                        {new KeyValuePair<int, string>( 1135, "Minnesota"), 3},

                        {new KeyValuePair<int, string>( 1136, "Mississippi"), 3},

                        {new KeyValuePair<int, string>( 1137, "Missouri"), 3},

                        {new KeyValuePair<int, string>( 1138, "Montana"), 3},

                        {new KeyValuePair<int, string>( 1139, "Nebraska"), 3},

                        {new KeyValuePair<int, string>( 1140, "Nevada"), 3},

                        {new KeyValuePair<int, string>( 1141, "New Hampshire"), 3},

                        {new KeyValuePair<int, string>( 1142, "New Jersey"), 3},

                        {new KeyValuePair<int, string>( 1143, "New Mexico"), 3},

                        {new KeyValuePair<int, string>( 1144, "New York"), 3},

                        {new KeyValuePair<int, string>( 1145, "North Carolina"), 3},

                        {new KeyValuePair<int, string>( 1146, "North Dakota"), 3},

                        {new KeyValuePair<int, string>( 1147, "Northern Mariana Islands"), 3},

                        {new KeyValuePair<int, string>( 1148, "Ohio"), 3},

                        {new KeyValuePair<int, string>( 1149, "Oklahoma"), 3},

                        {new KeyValuePair<int, string>( 1150, "Oregon"), 3},

                        {new KeyValuePair<int, string>( 1151, "Palau"), 3},

                        {new KeyValuePair<int, string>( 1152, "Pennsylvania"), 3},

                        {new KeyValuePair<int, string>( 1153, "Puerto Rico"), 3},

                        {new KeyValuePair<int, string>( 1154, "Rhode Island"), 3},

                        {new KeyValuePair<int, string>( 1155, "South Carolina"), 3},

                        {new KeyValuePair<int, string>( 1156, "South Dakota"), 3},

                        {new KeyValuePair<int, string>( 1157, "Tennessee"), 3},

                        {new KeyValuePair<int, string>( 1158, "Texas"), 3},

                        {new KeyValuePair<int, string>( 1159, "Utah"), 3},

                        {new KeyValuePair<int, string>( 1160, "Vermont"), 3},

                        {new KeyValuePair<int, string>( 1161, "Virgin Islands"), 3},

                        {new KeyValuePair<int, string>( 1162, "Virginia"), 3},

                        {new KeyValuePair<int, string>( 1163, "Washington"), 3},

                        {new KeyValuePair<int, string>( 1164, "West Virginia"), 3},

                        {new KeyValuePair<int, string>( 1165, "Wisconsin"), 3},

                        {new KeyValuePair<int, string>( 1166, "Wyoming"), 3},

                        {new KeyValuePair<int, string>( 1167, "Hanoi"), 237},

                        {new KeyValuePair<int, string>( 1170, "Shibuya"), 196},

                        {new KeyValuePair<int, string>( 1171, "Lyon"), 183},

                        {new KeyValuePair<int, string>( 1172, "Kyoto"), 196},

                        {new KeyValuePair<int, string>( 1173, "Danang"), 237},

                        {new KeyValuePair<int, string>( 1175, "Solihull"), 232},

                        {new KeyValuePair<int, string>( 1176, "Manila"), 211},

                        {new KeyValuePair<int, string>( 1181, "Doha"), 238},

                        {new KeyValuePair<int, string>( 1183, "Kampala"), 240},

                        {new KeyValuePair<int, string>( 1187, "Friesland"), 206},

                        {new KeyValuePair<int, string>( 1188, "Drenthe"), 206},

                        {new KeyValuePair<int, string>( 1189, "Overijssel"), 206},

                        {new KeyValuePair<int, string>( 1190, "Flevoland"), 206},

                        {new KeyValuePair<int, string>( 1192, "Utrecht"), 206},

                        {new KeyValuePair<int, string>( 1195, "Zeeland"), 206},

                        {new KeyValuePair<int, string>( 1197, "Limburg"), 206},

                        {new KeyValuePair<int, string>( 1198, "Gelderland"), 206},

                        {new KeyValuePair<int, string>( 1205, "Trinidad"), 241},

                        {new KeyValuePair<int, string>( 1206, "Tobago"), 241},

                        {new KeyValuePair<int, string>( 1209, "Beirut"), 242},

                        {new KeyValuePair<int, string>( 1210, "Safat"), 243},

                        {new KeyValuePair<int, string>( 1211, "Kuwait City"), 243},

                        {new KeyValuePair<int, string>( 1212, "Bitola"), 199},

                        {new KeyValuePair<int, string>( 1216, "Kyushu"), 196},

                        {new KeyValuePair<int, string>( 1218, "British Virgin Islands"), 232},

                        {new KeyValuePair<int, string>( 1219, "Dhaka"), 244},

                        {new KeyValuePair<int, string>( 1220, "Phuket"), 228},

                        {new KeyValuePair<int, string>( 1221, "Bath"), 232},

                        {new KeyValuePair<int, string>( 1223, "Dar Es Salaam"), 245},

                        {new KeyValuePair<int, string>( 1224, "Derby"), 232},

                        {new KeyValuePair<int, string>( 1226, "Scotland"), 247},

                        {new KeyValuePair<int, string>( 1228, "South West"), 248},

                        {new KeyValuePair<int, string>( 1230, "London"), 248},

                        {new KeyValuePair<int, string>( 1231, "North"), 248},

                        {new KeyValuePair<int, string>( 1232, "North East"), 248},

                        {new KeyValuePair<int, string>( 1234, "East Midlands"), 248},

                        {new KeyValuePair<int, string>( 1235, "West Midlands"), 248},

                        {new KeyValuePair<int, string>( 1238, "Wales"), 249},

                        {new KeyValuePair<int, string>( 1239, "Bahrain"), 250},

                        {new KeyValuePair<int, string>( 1241, "Guatemala"), 251},

                        //{new KeyValuePair<int, string>( 1242, "Slovakia"), 253},

                        {new KeyValuePair<int, string>( 1243, "Tunisia"), 254},

                        {new KeyValuePair<int, string>( 1251, "Sydney"), 1},

                        {new KeyValuePair<int, string>( 1253, "Bergen"), 207},

                        {new KeyValuePair<int, string>( 1256, "Newfoundland"), 168},

                        {new KeyValuePair<int, string>( 1257, "Santo Domingo"), 256},

                        {new KeyValuePair<int, string>( 1258, "Marne La Vallee"), 183},

                        {new KeyValuePair<int, string>( 1259, "Gundelfingen"), 186},

                        {new KeyValuePair<int, string>( 1260, "Mainz"), 186},

                        {new KeyValuePair<int, string>( 1261, "Walldorf"), 186},

                        {new KeyValuePair<int, string>( 1262, "Azur"), 193},

                        {new KeyValuePair<int, string>( 1263, "Riga"), 257},

                        {new KeyValuePair<int, string>( 1264, "Guadalajara"), 258},

                        {new KeyValuePair<int, string>( 1265, "Mexico City"), 258},

                        //{new KeyValuePair<int, string>( 1266, "Cd Juarez"), 258},

                        {new KeyValuePair<int, string>( 1268, "Monterrey"), 258},

                        {new KeyValuePair<int, string>( 1271, "Puebla"), 258},

                        {new KeyValuePair<int, string>( 1272, "Tijuana"), 258},

                        {new KeyValuePair<int, string>( 1273, "Amsterdam"), 206},

                        {new KeyValuePair<int, string>( 1274, "Eindhoven"), 206},

                        {new KeyValuePair<int, string>( 1275, "Rotterdam"), 206},

                        {new KeyValuePair<int, string>( 1278, "Makati City"), 211},

                        {new KeyValuePair<int, string>( 1280, "Bratislava"), 253},

                        {new KeyValuePair<int, string>( 1281, "Marbella"), 223},

                        {new KeyValuePair<int, string>( 1282, "Gothenburg"), 225},

                        {new KeyValuePair<int, string>( 1283, "Malmo"), 225},

                        {new KeyValuePair<int, string>( 1284, "Solna"), 225},

                        {new KeyValuePair<int, string>( 1285, "Zug"), 226},

                        {new KeyValuePair<int, string>( 1286, "Kiev"), 255},

                        {new KeyValuePair<int, string>( 1287, "Shenzhen"), 172},

                        {new KeyValuePair<int, string>( 1289, "Gurgaon"), 190},

                        {new KeyValuePair<int, string>( 1290, "Yokohama"), 196},

                        {new KeyValuePair<int, string>( 1292, "Belfast"), 246},

                        {new KeyValuePair<int, string>( 1296, "Edinburgh"), 247},

                        {new KeyValuePair<int, string>( 1303, "Mid Wales"), 249},

                        {new KeyValuePair<int, string>( 1305, "Northern Scotland"), 247},

                        {new KeyValuePair<int, string>( 1307, "South West Wales"), 249},

                        {new KeyValuePair<int, string>( 1308, "Southern Scotland"), 247},

                        {new KeyValuePair<int, string>( 1313, "West Wales"), 249},

                        {new KeyValuePair<int, string>( 1314, "Yorkshire"), 248},

                        {new KeyValuePair<int, string>( 1315, "Accra"), 259},

                        {new KeyValuePair<int, string>( 1409, "Noord-Brabant"), 206},

                        {new KeyValuePair<int, string>( 1477, "Arnhem"), 206},

                        //{new KeyValuePair<int, string>( 1536, "City"), 256},

                        {new KeyValuePair<int, string>( 1571, "Brabant"), 206},

                        {new KeyValuePair<int, string>( 1588, "Nuevo Leon"), 256},

                        {new KeyValuePair<int, string>( 1599, "Schiphol-Rijk"), 206},

                        //{new KeyValuePair<int, string>( 1606, "Cd Mexico"), 256},

                        {new KeyValuePair<int, string>( 1609, "Catalunya"), 223},

                        {new KeyValuePair<int, string>( 1645, "Noord-Holland"), 206},

                        {new KeyValuePair<int, string>( 1653, "Nove Mesto"), 177},

                        {new KeyValuePair<int, string>( 1654, "Zuid-Holland"), 206},

                        {new KeyValuePair<int, string>( 1664, "Nordrhein-Westfalen"), 186},

                        {new KeyValuePair<int, string>( 1665, "Bavaria"), 186},

                        {new KeyValuePair<int, string>( 1669, "Hesse"), 186},

                        {new KeyValuePair<int, string>( 1670, "Baden-Wuerttemberg"), 186},

                        {new KeyValuePair<int, string>( 1684, "Hellerup"), 178},

                        {new KeyValuePair<int, string>( 1691, "Sweden"), 225},

                        {new KeyValuePair<int, string>( 1695, "Lower-Saxony"), 186},

                        {new KeyValuePair<int, string>( 1701, "Guangdong"), 172},

                        {new KeyValuePair<int, string>( 1703, "Karnataka"), 190},

                        {new KeyValuePair<int, string>( 1704, "Haryana"), 190},

                        {new KeyValuePair<int, string>( 1706, "Kanagawa"), 196},

                        {new KeyValuePair<int, string>( 1707, "Neuilly"), 183},

                        {new KeyValuePair<int, string>( 1709, "Attica"), 187},

                        {new KeyValuePair<int, string>( 1726, "Las Condes"), 171},

                        {new KeyValuePair<int, string>( 1751, "Vlaams Brabant"), 163},

                        {new KeyValuePair<int, string>( 1759, "East of England"), 248},

                        {new KeyValuePair<int, string>( 1768, "Bruxelles-Capitale"), 163},

                        {new KeyValuePair<int, string>( 1968, "Hilversum"), 206},

                        {new KeyValuePair<int, string>( 1987, "Vienna"), 161},

                        {new KeyValuePair<int, string>( 2013, "Nordic"), 182},

                        {new KeyValuePair<int, string>( 2032, "The Hague"), 206},

                        {new KeyValuePair<int, string>( 2043, "Madrid"), 223},

                        {new KeyValuePair<int, string>( 2110, "Espoo"), 182},

                        {new KeyValuePair<int, string>( 2124, "Quebec"), 168},

                        //{new KeyValuePair<int, string>( 2125, "Central"), 219},

                        {new KeyValuePair<int, string>( 2153, "Copenhagen"), 178},

                        {new KeyValuePair<int, string>( 2168, "Mazowieckie"), 212},

                        {new KeyValuePair<int, string>( 2250, "Shanghai"), 172},

                        {new KeyValuePair<int, string>( 2252, "Limassol"), 176},

                        {new KeyValuePair<int, string>( 2261, "Brabant Wallon"), 163},

                        {new KeyValuePair<int, string>( 2273, "Dalian"), 172},

                        {new KeyValuePair<int, string>( 2285, "Guangzhou"), 172},

                        {new KeyValuePair<int, string>( 2299, "Tianjin"), 172},

                        {new KeyValuePair<int, string>( 2300, "Ireland"), 192},

                        {new KeyValuePair<int, string>( 2301, "Northern Ireland"), 246},

                        {new KeyValuePair<int, string>( 2302, "Andhra Pradesh"), 190},

                        {new KeyValuePair<int, string>( 2304, "Gujarat"), 190},

                        {new KeyValuePair<int, string>( 2306, "Kerala"), 190},

                        {new KeyValuePair<int, string>( 2307, "Maharashtra"), 190},

                        {new KeyValuePair<int, string>( 2308, "Madhya Pradesh"), 190},

                        {new KeyValuePair<int, string>( 2309, "Punjab"), 190},

                        {new KeyValuePair<int, string>( 2310, "Rajasthan"), 190},

                        {new KeyValuePair<int, string>( 2311, "Tamil Nadu"), 190},

                        {new KeyValuePair<int, string>( 2312, "Uttar Pradesh"), 190},

                        {new KeyValuePair<int, string>( 2313, "West Bengal"), 190},

                        {new KeyValuePair<int, string>( 2314, "Gaborone"), 260},

                        //{new KeyValuePair<int, string>( 2315, "Kingston 6"), 195},

                        {new KeyValuePair<int, string>( 2316, "Goa"), 190},

                        {new KeyValuePair<int, string>( 2318, "Saskatchewan"), 168},

                        {new KeyValuePair<int, string>( 2319, "Manitoba"), 168},

                        {new KeyValuePair<int, string>( 2320, "Port Moresby"), 261},

                        {new KeyValuePair<int, string>( 2321, "Lautoka"), 181},

                        {new KeyValuePair<int, string>( 2322, "Phnom Penh"), 262},

                        {new KeyValuePair<int, string>( 2324, "Bihar"), 190},

                        {new KeyValuePair<int, string>( 2326, "Krakow"), 212},

                        {new KeyValuePair<int, string>( 2327, "Maputo"), 264},

                        {new KeyValuePair<int, string>( 2328, "Kathmandu"), 325},

                        {new KeyValuePair<int, string>( 2330, "Avignon"), 183},

                        {new KeyValuePair<int, string>( 2331, "The Highlands"), 247},

                        {new KeyValuePair<int, string>( 2332, "The Midland Valley"), 247},

                        {new KeyValuePair<int, string>( 2333, "The Southern Uplands"), 247},

                        {new KeyValuePair<int, string>( 2336, "North Wales"), 249},

                        {new KeyValuePair<int, string>( 2337, "South Wales"), 249},

                        {new KeyValuePair<int, string>( 2338, "Ceredigion"), 249},

                        {new KeyValuePair<int, string>( 2341, "Shannon West"), 192},

                        {new KeyValuePair<int, string>( 2342, "Jersey"), 232},

                        {new KeyValuePair<int, string>( 2343, "Islamabad"), 82},

                        {new KeyValuePair<int, string>( 2345, "San Jose"), 273},

                        {new KeyValuePair<int, string>( 2346, "Lagos"), 274},

                        {new KeyValuePair<int, string>( 2347, "Vilnius"), 275},

                        {new KeyValuePair<int, string>( 2348, "Gibraltar"), 276},

                        {new KeyValuePair<int, string>( 2349, "Medan"), 191},

                        {new KeyValuePair<int, string>( 2350, "Great Yarmouth"), 232},

                        {new KeyValuePair<int, string>( 2351, "Bermuda"), 277},

                        {new KeyValuePair<int, string>( 2352, "Hamilton"), 277},

                        {new KeyValuePair<int, string>( 2353, "Orissa"), 190},

                        {new KeyValuePair<int, string>( 2356, "Sarajevo"), 278},

                        {new KeyValuePair<int, string>( 2358, "Ankara"), 230},

                        {new KeyValuePair<int, string>( 2359, "Ho Chi Minh City"), 237},

                        {new KeyValuePair<int, string>( 2360, "Tirana"), 279},

                        {new KeyValuePair<int, string>( 2361, "Basseterre"), 280},

                        {new KeyValuePair<int, string>( 2362, "North Male Atoll"), 281},

                        {new KeyValuePair<int, string>( 2363, "Free State"), 221},

                        {new KeyValuePair<int, string>( 2366, "Aarhus"), 178},

                        {new KeyValuePair<int, string>( 2367, "Mahanagar Extension"), 190},

                        {new KeyValuePair<int, string>( 2368, "Makassar"), 191},

                        {new KeyValuePair<int, string>( 2369, "Cosenza"), 194},

                        {new KeyValuePair<int, string>( 2370, "Damascus"), 270},

                        {new KeyValuePair<int, string>( 2371, "Catania"), 194},

                        {new KeyValuePair<int, string>( 2372, "Estillac"), 183},

                        {new KeyValuePair<int, string>( 2373, "Treviso"), 194},

                        {new KeyValuePair<int, string>( 2374, "Ancona"), 194},

                        {new KeyValuePair<int, string>( 2375, "Caserta"), 194},

                        {new KeyValuePair<int, string>( 2376, "Imola"), 194},

                        {new KeyValuePair<int, string>( 2377, "Reggio"), 194},

                        {new KeyValuePair<int, string>( 2378, "Emilia"), 194},

                        {new KeyValuePair<int, string>( 2379, "Udine"), 194},

                        {new KeyValuePair<int, string>( 2380, "Vicenza"), 194},

                        {new KeyValuePair<int, string>( 2382, "Belgrade"), 282},

                        {new KeyValuePair<int, string>( 2383, "Nagoya"), 196},

                        {new KeyValuePair<int, string>( 2384, "Fukuoka"), 196},

                        {new KeyValuePair<int, string>( 2385, "Sabah"), 200},

                        {new KeyValuePair<int, string>( 2386, "Kedah"), 200},

                        {new KeyValuePair<int, string>( 2387, "Penang"), 200},

                        {new KeyValuePair<int, string>( 2388, "Johor"), 200},

                        {new KeyValuePair<int, string>( 2389, "Perak"), 200},

                        {new KeyValuePair<int, string>( 2390, "Melaka"), 200},

                        {new KeyValuePair<int, string>( 2391, "Sarawak"), 200},

                        {new KeyValuePair<int, string>( 2392, "Pahang"), 200},

                        {new KeyValuePair<int, string>( 2393, "Kelantan"), 200},

                        {new KeyValuePair<int, string>( 2394, "Terengganu"), 200},

                        {new KeyValuePair<int, string>( 2395, "Selangor"), 200},

                        {new KeyValuePair<int, string>( 2396, "Negeri Sembilan"), 200},

                        {new KeyValuePair<int, string>( 2397, "Wilayah Persekutuan"), 200},

                        {new KeyValuePair<int, string>( 2398, "Perlis"), 200},

                        {new KeyValuePair<int, string>( 2399, "Macau"), 172},

                        {new KeyValuePair<int, string>( 2400, "Graz"), 161},

                        {new KeyValuePair<int, string>( 2401, "Visakhapatnam"), 190},

                        {new KeyValuePair<int, string>( 2403, "Kabul"), 263},

                        {new KeyValuePair<int, string>( 2404, "Alicante"), 223},

                        {new KeyValuePair<int, string>( 2405, "Nairobi"), 283},

                        {new KeyValuePair<int, string>( 2406, "Northern Cape"), 221},

                        {new KeyValuePair<int, string>( 2407, "North West"), 221},

                        {new KeyValuePair<int, string>( 2408, "Gauteng"), 221},

                        {new KeyValuePair<int, string>( 2409, "Limpopo"), 221},

                        {new KeyValuePair<int, string>( 2411, "KwaZulu - Natal"), 221},

                        {new KeyValuePair<int, string>( 2413, "Eastern Cape"), 221},

                        {new KeyValuePair<int, string>( 2415, "Western Cape"), 221},

                        {new KeyValuePair<int, string>( 2417, "Cagliari"), 194},

                        {new KeyValuePair<int, string>( 2418, "Bari"), 194},

                        {new KeyValuePair<int, string>( 2419, "Delhi"), 190},

                        {new KeyValuePair<int, string>( 2420, "Abidjan"), 284},

                        {new KeyValuePair<int, string>( 2421, "Monte Carlo"), 285},

                        {new KeyValuePair<int, string>( 2422, "St. Thomas"), 286},

                        {new KeyValuePair<int, string>( 2423, "Angers"), 183},

                        {new KeyValuePair<int, string>( 2424, "Casablanca"), 204},

                        {new KeyValuePair<int, string>( 2425, "Guadeloupe"), 287},

                        {new KeyValuePair<int, string>( 2426, "Chihuahua"), 258},

                        {new KeyValuePair<int, string>( 2427, "Heidelburg"), 186},

                        {new KeyValuePair<int, string>( 2428, "Flanders"), 163},

                        {new KeyValuePair<int, string>( 2429, "Wallonie"), 163},

                        {new KeyValuePair<int, string>( 2430, "East Anglia"), 232},

                        {new KeyValuePair<int, string>( 2431, "Odense"), 178},

                        {new KeyValuePair<int, string>( 2433, "Sichuan Province"), 172},

                        {new KeyValuePair<int, string>( 2434, "Ljubljana"), 288},

                        {new KeyValuePair<int, string>( 2435, "Quetta"), 82},

                        {new KeyValuePair<int, string>( 2436, "Harare"), 289},

                        {new KeyValuePair<int, string>( 2437, "Lausanne"), 226},

                        {new KeyValuePair<int, string>( 2438, "Jeddah"), 219},

                        {new KeyValuePair<int, string>( 2439, "Khartoum"), 290},

                        {new KeyValuePair<int, string>( 2440, "Addis Ababa"), 291},

                        {new KeyValuePair<int, string>( 2441, "Ouagadougou"), 292},

                        {new KeyValuePair<int, string>( 2442, "Aix en Provence"), 183},

                        {new KeyValuePair<int, string>( 2443, "Dakar"), 293},

                        {new KeyValuePair<int, string>( 2444, "Nouakchott"), 294},

                        {new KeyValuePair<int, string>( 2445, "Quito"), 295},

                        {new KeyValuePair<int, string>( 2446, "Ireland, Northern"), 232},

                        {new KeyValuePair<int, string>( 2447, "Bedfordshire"), 248},

                        {new KeyValuePair<int, string>( 2448, "Berkshire"), 248},

                        {new KeyValuePair<int, string>( 2449, "Cheshire"), 248},

                        {new KeyValuePair<int, string>( 2450, "Cornwall"), 248},

                        {new KeyValuePair<int, string>( 2451, "Cumberland"), 248},

                        {new KeyValuePair<int, string>( 2452, "Derbyshire"), 248},

                        {new KeyValuePair<int, string>( 2453, "Devon"), 248},

                        {new KeyValuePair<int, string>( 2454, "Dorset"), 248},

                        {new KeyValuePair<int, string>( 2455, "Durham"), 248},

                        {new KeyValuePair<int, string>( 2456, "Essex"), 248},

                        {new KeyValuePair<int, string>( 2457, "Gloucestershire"), 248},

                        {new KeyValuePair<int, string>( 2458, "Hampshire"), 248},

                        {new KeyValuePair<int, string>( 2459, "Herefordshire"), 248},

                        {new KeyValuePair<int, string>( 2460, "Hertfordshire"), 248},

                        {new KeyValuePair<int, string>( 2461, "Huntingdonshire"), 248},

                        {new KeyValuePair<int, string>( 2462, "Kent"), 248},

                        {new KeyValuePair<int, string>( 2463, "Lancashire"), 248},

                        {new KeyValuePair<int, string>( 2464, "Leicestershire"), 248},

                        {new KeyValuePair<int, string>( 2465, "Lincolnshire"), 248},

                        {new KeyValuePair<int, string>( 2467, "Middlesex"), 248},

                        {new KeyValuePair<int, string>( 2468, "Norfolk"), 248},

                        {new KeyValuePair<int, string>( 2469, "Northamptonshire"), 248},

                        {new KeyValuePair<int, string>( 2470, "Northumberland"), 248},

                        {new KeyValuePair<int, string>( 2471, "Nottinghamshire"), 248},

                        {new KeyValuePair<int, string>( 2472, "Oxfordshire"), 248},

                        {new KeyValuePair<int, string>( 2473, "Rutland"), 248},

                        {new KeyValuePair<int, string>( 2474, "Shropshire"), 248},

                        {new KeyValuePair<int, string>( 2475, "Somerset"), 248},

                        {new KeyValuePair<int, string>( 2476, "Staffordshire"), 248},

                        {new KeyValuePair<int, string>( 2477, "Suffolk"), 248},

                        {new KeyValuePair<int, string>( 2478, "Surrey"), 248},

                        {new KeyValuePair<int, string>( 2479, "Sussex"), 248},

                        {new KeyValuePair<int, string>( 2480, "Warwickshire"), 248},

                        {new KeyValuePair<int, string>( 2481, "Westmorland"), 248},

                        {new KeyValuePair<int, string>( 2482, "Wiltshire"), 248},

                        {new KeyValuePair<int, string>( 2483, "Worcestershire"), 248},

                        {new KeyValuePair<int, string>( 2485, "Buckinghamshire"), 248},

                        {new KeyValuePair<int, string>( 2489, "Curacao"), 297},

                        {new KeyValuePair<int, string>( 2490, "La Reunion St Clotide"), 183},

                        {new KeyValuePair<int, string>( 2491, "St Clotide"), 216},

                        {new KeyValuePair<int, string>( 2493, "Castries Quarter"), 298},

                        {new KeyValuePair<int, string>( 2494, "Teheran"), 266},

                        {new KeyValuePair<int, string>( 2495, "Maastricht"), 206},

                        {new KeyValuePair<int, string>( 2497, "Pondicherry"), 190},

                        {new KeyValuePair<int, string>( 2498, "Gaza"), 193},

                        {new KeyValuePair<int, string>( 2499, "Birzeit"), 299},

                        {new KeyValuePair<int, string>( 2501, "Delft"), 206},

                        {new KeyValuePair<int, string>( 2502, "Cluj - Napoca"), 217},

                        {new KeyValuePair<int, string>( 2503, "Kaunas"), 275},

                        {new KeyValuePair<int, string>( 2504, "Cumbria"), 248},

                        {new KeyValuePair<int, string>( 2505, "Berne"), 226},

                        {new KeyValuePair<int, string>( 2506, "Peshawar"), 82},

                        {new KeyValuePair<int, string>( 2507, "Rawalpindi"), 82},

                        {new KeyValuePair<int, string>( 2508, "Sialkot"), 82},

                        {new KeyValuePair<int, string>( 2509, "Chengdu"), 172},

                        {new KeyValuePair<int, string>( 2510, "Hangzhou"), 172},

                        {new KeyValuePair<int, string>( 2511, "Dili"), 300},

                        {new KeyValuePair<int, string>( 2512, "Tbilisi"), 301},

                        {new KeyValuePair<int, string>( 2513, "Pristina"), 302},

                        {new KeyValuePair<int, string>( 2514, "Yerevan"), 303},

                        {new KeyValuePair<int, string>( 2515, "Baku"), 304},

                        {new KeyValuePair<int, string>( 2516, "Antananarivo"), 305},

                        {new KeyValuePair<int, string>( 2517, "Chandigarh"), 190},

                        {new KeyValuePair<int, string>( 2518, "Plimmerton"), 2},

                        {new KeyValuePair<int, string>( 2519, "Eskisehir"), 230},

                        {new KeyValuePair<int, string>( 2520, "Prince Edward Island"), 168},

                        {new KeyValuePair<int, string>( 2522, "Managua"), 306},

                        {new KeyValuePair<int, string>( 2523, "Cotonou"), 307},

                        {new KeyValuePair<int, string>( 2524, "Douglas"), 308},

                        {new KeyValuePair<int, string>( 2525, "Cayman Islands"), 309},

                        {new KeyValuePair<int, string>( 2526, "Jharkhand"), 190},

                        {new KeyValuePair<int, string>( 2527, "Mongolia"), 272},

                        {new KeyValuePair<int, string>( 2528, "San Salvador"), 310},

                        {new KeyValuePair<int, string>( 2530, "Lugano"), 226},

                        {new KeyValuePair<int, string>( 2531, "Christ Church"), 162},

                        {new KeyValuePair<int, string>( 2532, "Ticino"), 226},

                        {new KeyValuePair<int, string>( 2533, "Almere"), 206},

                        {new KeyValuePair<int, string>( 2534, "Nijmegen"), 206},

                        {new KeyValuePair<int, string>( 2535, "Den Bosch"), 206},

                        {new KeyValuePair<int, string>( 2536, "Chisinau"), 312},

                        {new KeyValuePair<int, string>( 2537, "Alexandria"), 179},

                        {new KeyValuePair<int, string>( 2538, "Jaipur"), 190},

                        {new KeyValuePair<int, string>( 2542, "Wanganui"), 2},

                        {new KeyValuePair<int, string>( 2543, "Sofia Antipolis"), 183},

                        {new KeyValuePair<int, string>( 2544, "Valbonne"), 183},

                        {new KeyValuePair<int, string>( 2545, "Channel Islands"), 313},

                        {new KeyValuePair<int, string>( 2547, "Nunavut"), 168},

                        {new KeyValuePair<int, string>( 2549, "Zwolle"), 206},

                        {new KeyValuePair<int, string>( 2550, "Triesen"), 314},

                        {new KeyValuePair<int, string>( 2551, "Austerlitz"), 206},

                        {new KeyValuePair<int, string>( 2552, "Jalisco"), 258},

                        {new KeyValuePair<int, string>( 2553, "Sudbury"), 168},

                        {new KeyValuePair<int, string>( 2554, "Ebene"), 203},

                        {new KeyValuePair<int, string>( 2555, "Amstelveen"), 206},

                        {new KeyValuePair<int, string>( 2557, "Bielefeld"), 186},

                        {new KeyValuePair<int, string>( 2558, "Esslingen"), 186},

                        {new KeyValuePair<int, string>( 2561, "Muenster"), 186},

                        {new KeyValuePair<int, string>( 2564, "Brunei"), 316},

                        {new KeyValuePair<int, string>( 2565, "Maugio"), 183},

                        {new KeyValuePair<int, string>( 2566, "Oulu"), 182},

                        {new KeyValuePair<int, string>( 2567, "Lappeenranta"), 182},

                        {new KeyValuePair<int, string>( 2568, "Jyvaskyla"), 182},

                        {new KeyValuePair<int, string>( 2569, "Kuopio"), 182},

                        {new KeyValuePair<int, string>( 2570, "Vantaa"), 182},

                        {new KeyValuePair<int, string>( 2571, "Tampere"), 182},

                        {new KeyValuePair<int, string>( 2572, "Mabolo"), 211},

                        {new KeyValuePair<int, string>( 2573, "Almaty"), 271},

                        {new KeyValuePair<int, string>( 2574, "Astan"), 271},

                        {new KeyValuePair<int, string>( 2577, "Reykjavik"), 317},

                        {new KeyValuePair<int, string>( 2578, "Qingdao"), 172},

                        {new KeyValuePair<int, string>( 2579, "Bamako"), 318},

                        {new KeyValuePair<int, string>( 2580, "Yaounde"), 319},

                        {new KeyValuePair<int, string>( 2581, "Vanuatu"), 329},

                        {new KeyValuePair<int, string>( 2582, "Elst"), 206},

                        {new KeyValuePair<int, string>( 2583, "Schiedam"), 206},

                        {new KeyValuePair<int, string>( 2584, "Bolivar"), 173},

                        {new KeyValuePair<int, string>( 2585, "Tripoli"), 321},

                        {new KeyValuePair<int, string>( 2586, "Vaslui"), 217},

                        {new KeyValuePair<int, string>( 2587, "Metz"), 183},

                        {new KeyValuePair<int, string>( 2588, "Gdansk"), 212},

                        {new KeyValuePair<int, string>( 2589, "Jiangsu Province"), 172},

                        {new KeyValuePair<int, string>( 2590, "Fujian Province"), 172},

                        {new KeyValuePair<int, string>( 2591, "Hunan Province"), 172},

                        {new KeyValuePair<int, string>( 2592, "Leeuwarden"), 206},

                        {new KeyValuePair<int, string>( 2593, "Yurakucho"), 196},

                        {new KeyValuePair<int, string>( 2595, "Mulhouse"), 183},

                        {new KeyValuePair<int, string>( 2596, "San Jorge"), 164},

                        {new KeyValuePair<int, string>( 2597, "Abuja"), 274},

                        {new KeyValuePair<int, string>( 2598, "Port Harcourt"), 274},

                        {new KeyValuePair<int, string>( 2599, "Issy-les-Moulineaux"), 183},

                        {new KeyValuePair<int, string>( 2600, "Nassau"), 323},

                        {new KeyValuePair<int, string>( 2601, "Cowes"), 324},

                        {new KeyValuePair<int, string>( 2602, "Ahmedabad"), 190},

                        {new KeyValuePair<int, string>( 2603, "Lalitpur"), 325},

                        {new KeyValuePair<int, string>( 2604, "Modena"), 194},

                        {new KeyValuePair<int, string>( 2606, "Minsk"), 326},

                        {new KeyValuePair<int, string>( 2607, "Nanan District"), 172},

                        {new KeyValuePair<int, string>( 2608, "Freetown"), 327},

                        {new KeyValuePair<int, string>( 2609, "Brno"), 177},

                        {new KeyValuePair<int, string>( 2610, "Gent"), 163},

                        {new KeyValuePair<int, string>( 2611, "Winterthur"), 226},

                        {new KeyValuePair<int, string>( 2612, "Eysins"), 226},

                        {new KeyValuePair<int, string>( 2615, "Madeira"), 213},

                        {new KeyValuePair<int, string>( 2616, "Rijswijk"), 206},

                        {new KeyValuePair<int, string>( 2617, "Lusaka"), 328},

                        {new KeyValuePair<int, string>( 2618, "Cannes"), 183},

                        {new KeyValuePair<int, string>( 2619, "Sendai"), 196},

                        {new KeyValuePair<int, string>( 2620, "Hiroshima"), 196},

                        {new KeyValuePair<int, string>( 2621, "Kobe"), 196},

                        {new KeyValuePair<int, string>( 2622, "Hyogo"), 196},

                        {new KeyValuePair<int, string>( 2624, "Recife"), 165},

                        {new KeyValuePair<int, string>( 2625, "Pernambuco"), 165},

                        {new KeyValuePair<int, string>( 2626, "Amersfoort"), 206}

            };


    }
}
