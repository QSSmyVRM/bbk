﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
#region References
using System;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using msxml4_Net;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMReminders
{
    public partial class myVRMRemindersService : ServiceBase
    {

        static String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        static String MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
        static String COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
        static String RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
        static System.Timers.Timer timerLaunchReminders = new System.Timers.Timer();
        static System.Timers.Timer timerBatchRpt = new System.Timers.Timer(); //FB 2410
        static System.Timers.Timer timerPollCDR = new System.Timers.Timer();
        static System.Timers.Timer timerFetchMCUInfo = new System.Timers.Timer();//FB 2501 Call Monitoring Dec6
        static System.Timers.Timer tmrUpdateRooms = new System.Timers.Timer();//FB 2392
        static System.Timers.Timer timerSiteExpiry = new System.Timers.Timer();//ZD 100230
        static System.Timers.Timer timerArchieveConference = new System.Timers.Timer();//ZD 100230
        static System.Timers.Timer timerUpdateCompletedCalls = new System.Timers.Timer(); //ZD 102532

        static NS_CONFIG.Config config = null;
        static NS_MESSENGER.ConfigParams configParams = null;
        static string configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        static string errMsg = null;
        static NS_LOGGER.Log log = null;
        static bool ret = false;
        static ASPIL.VRMServer myvrmCom = null;
        static VRMRTC.VRMRTC objRTC = null;
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);//FB 2501
        static double mcuInfoLaunch = 30000; //.5 Minutes 100369
        static double completedCalls = 30000; //.5 Minutes 102532

        public myVRMRemindersService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            double reminderLaunch = 90000; //1.5 Minutes
            double CDRLaunch = 30000; //FB 2593 15Minutes // FB 2944
            double rptSentInterval = 30000;// 12 * 60 * 60 * 1000; //FB 2410 //12 Hours //ZD 101823
            double ExpiryInterval = 90000; //ZD 100230 //24 Hours

            try
            {
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath); //FB 2944
                log = new NS_LOGGER.Log(configParams);

                log.Trace("Into The service started");
                log.Trace("Various Configs ASPIL:" + MyVRMServer_ConfigPath);


                timerLaunchReminders.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchReminders_Elapsed);
                timerLaunchReminders.Interval = reminderLaunch;
                timerLaunchReminders.Start();

                //FB 2501 - CDR - Starts
                timerPollCDR.Elapsed += new System.Timers.ElapsedEventHandler(timerPollCDR_Elapsed);
                timerPollCDR.Interval = CDRLaunch; //FB 2593
                timerPollCDR.Start();
                //FB 2501 - CDR - End

                //FB 2501 - Call Monitoring - Starts Dec6
                timerFetchMCUInfo.Elapsed += new System.Timers.ElapsedEventHandler(timerFetchMCUInfo_Elapsed);
                timerFetchMCUInfo.Interval = mcuInfoLaunch; // ZD 100369
                timerFetchMCUInfo.Start();
                //FB 2501 - Call Monitoring - End

                //FB 2392 Starts
                DateTime startDate = DateTime.Parse(DateTime.Now.AddDays(1).ToShortDateString() + " " + "12:00:00 AM");
                DateTime curDate = DateTime.Now;
                Double timeInterval = startDate.Subtract(curDate).TotalMilliseconds;

                tmrUpdateRooms.Elapsed += new System.Timers.ElapsedEventHandler(timerLaunchRoomUpdate_Elapsed);
                tmrUpdateRooms.Interval = timeInterval;
                tmrUpdateRooms.Start();
                //FB 2392 Ends

                timerBatchRpt.Elapsed += new System.Timers.ElapsedEventHandler(timerBatchRpt_Elapsed);
                timerBatchRpt.Interval = rptSentInterval;
                timerBatchRpt.Enabled = true;
                timerBatchRpt.Start();

                //ZD 102532 Start
                timerUpdateCompletedCalls.Elapsed += new System.Timers.ElapsedEventHandler(timerCompletedCalls_Elapsed);
                timerUpdateCompletedCalls.Interval = completedCalls;
                timerUpdateCompletedCalls.Start();
                //ZD 102532 End

                //ZD 100230 Start
                timerSiteExpiry.Elapsed += new System.Timers.ElapsedEventHandler(timerSiteExpiry_Elapsed);
                timerSiteExpiry.Interval = ExpiryInterval;
                timerSiteExpiry.Enabled = true;
                timerSiteExpiry.Start();
                timerSiteExpiry_Elapsed(null, null);
                //ZD 100230 End

                //ZD 101835
                timerArchieveConference.Elapsed += new System.Timers.ElapsedEventHandler(timerArchieveConf_Elapsed);
                timerArchieveConference.Interval = rptSentInterval;
                timerArchieveConference.Enabled = true;
                timerArchieveConference.Start();
                timerArchieveConf_timeset();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }

        #region timerLaunchReminders_Elapsed
        /// <summary>
        /// timerLaunchReminders_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerLaunchReminders_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501                   
                log.Trace("Into Reminders Lauch:" + DateTime.Now.ToString("F"));
                timerLaunchReminders.AutoReset = false;
                RemindersThread();
                System.Threading.Thread.Sleep(5000);
                timerLaunchReminders.AutoReset = true; ;

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region RemindersThread

        private static void RemindersThread()
        {
            try
            {

                myvrmCom = new ASPIL.VRMServer();
                String remindersConfInXML = "<Login><UserID>11</UserID></Login>";
                String remindersConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SendAutomatedReminders", remindersConfInXML);
                if (remindersConfOutXML.IndexOf("<success>") >= 0)
                    log.Trace("Reminders successfully Lauched:" + DateTime.Now.ToString("F"));
                myvrmCom = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }

        #endregion

        //FB 2501 - CDR - Starts
        #region timerPollCDR_Elapsed
        /// <summary>
        /// timerPollCDR_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerPollCDR_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            double CDRLaunch = 900000; //FB 2944
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501
                log.Trace("Into CDR Method:" + DateTime.Now.ToString("F"));
                timerPollCDR.AutoReset = false;
                PollCDRThread();
                System.Threading.Thread.Sleep(5000);
                timerPollCDR.Interval = CDRLaunch; // FB 2944
                timerPollCDR.AutoReset = true;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region PollCDRThread

        private static void PollCDRThread()
        {
            try
            {
                objRTC = new VRMRTC.VRMRTC();
                String dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location); //FB 2683
                String pollCDRInXML = "<Login><UserID>11</UserID><SeriveFilePath>" + dirPth + "</SeriveFilePath></Login>";//FB 2683
                String pollCDROutXML = objRTC.Operations(RTC_ConfigPath, "CallDetailRecords", pollCDRInXML);
                if (pollCDROutXML.IndexOf("<success>") >= 0)
                    log.Trace("Poll CDR command Lauched:" + DateTime.Now.ToString("F"));

                log.Trace("After PollCDRThread command:" + DateTime.Now.ToString("F"));

                objRTC = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion
        //FB 2501 - CDR - End

        //FB 2501 - Call Monitoring - Starts Dec6

        #region timerFetchMCUInfo_Elapsed
        /// <summary>
        /// timerFetchMCUInfo_Elapsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void timerFetchMCUInfo_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Into Fetch MCU Method:" + DateTime.Now.ToString("F"));
                timerFetchMCUInfo.AutoReset = false;
                timerFetchMCUInfo.Stop();
                FetchMCUThread();
                System.Threading.Thread.Sleep(5000);
                timerFetchMCUInfo.Interval = mcuInfoLaunch; // ZD 100369
                timerFetchMCUInfo.AutoReset = true;
                timerFetchMCUInfo.Start();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region FetchMCUThread
        /// <summary>
        /// FetchMCUThread
        /// </summary>
        private static void FetchMCUThread()
        {
            try
            {
                objRTC = new VRMRTC.VRMRTC();
                String FetchMCUInfoInXML = "<Login><UserID>11</UserID></Login>";
                String FetchMCUInfoOutXML = objRTC.Operations(RTC_ConfigPath, "FetchMCUInfo", FetchMCUInfoInXML);
                if (FetchMCUInfoOutXML.IndexOf("<success>") >= 0)
                    log.Trace("FetchMCUInfo command Lauched:" + DateTime.Now.ToString("F"));

                log.Trace("After FetchMCUInfo command:" + DateTime.Now.ToString("F"));

                objRTC = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion

        //FB 2501 - Call Monitoring - End


        protected override void OnStop()
        {
            timerLaunchReminders.Enabled = false;
            timerLaunchReminders.AutoReset = false;
            timerLaunchReminders.Stop();

            timerPollCDR.Enabled = false; //FB 2501 - CDR
            timerPollCDR.AutoReset = false;
            timerPollCDR.Stop();

            timerFetchMCUInfo.Enabled = false; //FB 2501 - Call Monitoring Dec6
            timerFetchMCUInfo.AutoReset = false;
            timerFetchMCUInfo.Stop();

            timerBatchRpt.Enabled = false; //ZD 101708
            timerBatchRpt.AutoReset = false;
            timerBatchRpt.Stop();
            //ZD 102532
            timerUpdateCompletedCalls.Enabled = false;
            timerUpdateCompletedCalls.AutoReset = false;
            timerUpdateCompletedCalls.Stop();
        }


        //FB 2392 Starts
        #region timerLaunchRoomUpdate_Elapsed

        static void timerLaunchRoomUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Into Room Update Lauch:" + DateTime.Now.ToString("F"));
                tmrUpdateRooms.AutoReset = false;
                if (objRTC == null)
                    objRTC = new VRMRTC.VRMRTC();

                GetLocationUpdate();

                tmrUpdateRooms.Interval = 24 * 60 * 60 * 1000;
                tmrUpdateRooms.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region GetLocationUpdate

        private static void GetLocationUpdate()
        {
            try
            {
                String inXML = "";
                log.Trace("COnfig path" + RTC_ConfigPath);
                String outXML = objRTC.Operations(RTC_ConfigPath, "GetLocationUpdate", inXML);
                if (outXML.IndexOf("<success>") >= 0)
                    log.Trace("Room Update successfully Lauched:" + DateTime.Now.ToString("F"));
                else
                    log.Trace(outXML + DateTime.Now.ToString("F"));
                myvrmCom = null;
            }
            catch (Exception ex)
            {
                log.Trace("GetLocationUpdate:" + ex.Message);
            }
        }
        #endregion

        //FB 2392 Ends
        //FB 2410

        #region timerActivation_Elapsed
        void timerBatchRpt_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;//FB 2501
                log.Trace("********** Entering into the GenerateBatchReport**************");
                timerBatchRpt.AutoReset = false;
                timerBatchRpt.Stop();
                GenerateBatchReport();
                System.Threading.Thread.Sleep(5000);
                timerBatchRpt.AutoReset = true;
                timerBatchRpt.Start();
                log.Trace("********** Finished GenerateBatchReport***********************");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion
        //FB 2410
        #region GenerateBatchReport
        /// <summary>
        /// GenerateBatchReport
        /// </summary>
        /// <returns></returns>
        private static bool GenerateBatchReport()
        {
            try
            {

                String inXML = "", OutXML = "";

                inXML = "<report>";
                inXML += "<configpath>" + MyVRMServer_ConfigPath + "</configpath>";
                inXML += "<Destination>" + configParams.reportFilePath + "</Destination>";
                inXML += "</report>";

                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                log.Trace(inXML);
                OutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GenerateBatchReport", inXML);
                log.Trace(OutXML);

                if (OutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Generate Batch Report: " + OutXML);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //ZD 100230 Start

        #region timerSiteExpiry_Elapsed
        void timerSiteExpiry_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                log.Trace("Into CheckSiteExpiry:" + DateTime.Now.ToString("F"));
                timerSiteExpiry.AutoReset = false;
                SiteExpiryThread();
                UserPasswordExpiry(); //ZD 100781
                System.Threading.Thread.Sleep(5000);
                timerSiteExpiry.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region SiteExpiryThread
        /// <summary>
        /// SiteExpiryThread
        /// </summary>
        /// <returns></returns>
        private static bool SiteExpiryThread()
        {
            try
            {

                myvrmCom = new ASPIL.VRMServer();
                String CheckSiteExpiryInXML = "<login><userID>11</userID></login>";
                String CheckSiteExpiryOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SendMailtoAdmin", CheckSiteExpiryInXML);
                if (CheckSiteExpiryOutXML.IndexOf("<success>") >= 0)
                    log.Trace("CheckSiteExpiry successfully Updated:" + DateTime.Now.ToString("F"));
                else
                    log.Trace("CheckSiteExpiry Not Sent");
                myvrmCom = null;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        //ZD 100230 End

        //ZD 100781 Starts
        #region UserPasswordExpiry
        /// <summary>
        /// UserPasswordExpiry
        /// </summary>
        /// <returns></returns>
        private static bool UserPasswordExpiry()
        {
            try
            {

                myvrmCom = new ASPIL.VRMServer();
                string UserPasswordExpiryIxml = "<login><userID>11</userID></login>";
                string UserPasswordExpiryOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SendPasswordRequest", UserPasswordExpiryIxml);
                log.Trace("SendPasswordRequest Not Sent" + UserPasswordExpiryOutXML);
                if (UserPasswordExpiryOutXML.IndexOf("<success>") >= 0)
                    log.Trace("UserPasswordExpiry successfully Updated:" + DateTime.Now.ToString("F"));
                else
                    log.Trace("UserPasswordExpiry Not Sent");
                myvrmCom = null;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 100781 Ends

        //ZD 101835
        #region ArchiveConferences

        private static void ArchiveConferences()
        {
            try
            {

                myvrmCom = new ASPIL.VRMServer();
                String confInXML = "<ConfArchiveConfiguration><From>FS</From></ConfArchiveConfiguration>"; //FS - From Service
                String confOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "GetConfArchiveConfiguration", confInXML);

                if (confOutXML.IndexOf("<success>") >= 0)
                    log.Trace("Conferences are successfully Archived:" + DateTime.Now.ToString("F"));

                log.Trace(confOutXML);

                if (confOutXML.IndexOf("<error>") >= 0)
                    log.Trace("Error in Archive Conferences: " + confOutXML);

                myvrmCom = null;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }

        void timerArchieveConf_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            TimeSpan tmrSpan = TimeSpan.FromMinutes(15);
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                log.Trace("Into ArchiveveConference:" + DateTime.Now.ToString("F"));
                timerArchieveConference.Stop();
                timerArchieveConference.AutoReset = false;
                timerArchieveConference.Interval = tmrSpan.TotalMilliseconds;
                ArchiveConferences();
                timerArchieveConference.AutoReset = true;
                timerArchieveConference.Start();

            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }

        void timerArchieveConf_timeset()
        {
            DateTime dtDate = DateTime.Now;
            TimeSpan tmrSpan = TimeSpan.FromMinutes(15);
            DateTime dtSetdate = DateTime.Now;
            TimeSpan tmrSpanSet;
            try
            {
                log.Trace("Into ArchiveTime set:" + DateTime.Now.ToString("F"));
                timerArchieveConference.Stop();
                dtSetdate = RoundUp(dtDate, tmrSpan);
                tmrSpanSet = dtSetdate.Subtract(dtDate);
                timerArchieveConference.Interval = tmrSpanSet.TotalMilliseconds;
                timerArchieveConference.AutoReset = true;
                timerArchieveConference.Start();

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        DateTime RoundUp(DateTime dtTime, TimeSpan tsSpan)
        {
            return new DateTime(((dtTime.Ticks + tsSpan.Ticks - 1) / tsSpan.Ticks) * tsSpan.Ticks);
        }

        #endregion

        //ZD 102532 Start

        #region timerCompletedCalls_Elapsed
        void timerCompletedCalls_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                log.Trace("Into timerCompletedCalls_Elapsed Method:" + DateTime.Now.ToLocalTime());
                timerUpdateCompletedCalls.AutoReset = false;
                timerUpdateCompletedCalls.Stop();
                UpdateCompletedConferences();
                System.Threading.Thread.Sleep(5000);
                timerUpdateCompletedCalls.Interval = completedCalls;
                timerUpdateCompletedCalls.AutoReset = true;
                timerUpdateCompletedCalls.Start();
                log.Trace("Exiting timerCompletedCalls_Elapsed...." + DateTime.Now.ToLocalTime());
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);

            }
        }
        #endregion

        #region UpdateCompletedConferences
        /// <summary>
        /// UpdateCompletedConferences
        /// </summary>
        private void UpdateCompletedConferences()
        {
            try
            {
                log.Trace("Into UpdateCompletedConferences... " + DateTime.Now.ToLocalTime());
                if (myvrmCom == null)
                    myvrmCom = new ASPIL.VRMServer();
                string confInXML = "<SearchConference><UserID>11</UserID></SearchConference>";
                string confOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "UpdateCompletedConference", confInXML);
                log.Trace("UpdateCompletedConferences OUTXML: " + confOutXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //ZD 102532 End

    }
}
