<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HolidayImport.aspx.cs" Inherits="ns_HolidayImport.HolidayImport" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 --><%--FB 2779--%>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function DataLoading()
{
    var obj = document.getElementById("tblDataImport");
    if (obj != null)
        obj.style.display="";
}
</script>
    <title>Database Import</title>
</head>
<body>
    <form id="frmDataImport" runat="server">
    <br /><br />
    <center>
        <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
    </center>
    <br /><br /><br />
        <h3 style="text-align: center">
            Holiday Import Tool</h3>
            <br /><br />            
            <table width="100%" bgcolor="white" cellpadding="1" cellspacing="0">
                <tr>
                    <td width="40%" align="center" style="display:none;">
                        <b>External Database Type:</b>&nbsp;
                        <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstDatabaseType" runat="server">
                            <asp:ListItem Selected="True" Text="Rendezvous" Value="1"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td align="left">
                        <asp:FileUpload ID="fleMasterCSV" Width="20%" EnableViewState="true" runat="server" CssClass="altText" />  
                        <asp:Button ID="btnGetDataTable" Width="20%" runat="server" OnClick="GenerateDataTable" CssClass="altShortBlueButtonFormat" Text="Upload Holidays" />
                    </td>
                </tr>
            </table>
            <br /><br />
            
        <br />
        <table>
            <tr>
                <td align="left">
                    <b>NOTE:</b> This section should only be accessed from the web server using localhost. The data file should reside on the web server it self.
                </td>
            </tr>
        </table>
        <table id="tblDataImport">
            <tr>
                <td>
                    <b><font color="#FF00FF" size="2">Data loading ...</font></b>&nbsp;&nbsp;&nbsp;&nbsp;<img border="0" src="image/wait1.gif" alt="Loading.." /><%-- FB2742 --%> <%--ZD 100419--%>
                </td>
            </tr>
        </table>
    </form>
<br />
<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<script language="javascript">
document.getElementById("tblDataImport").style.display="none";
</script>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" --> 
</body>
</html>
