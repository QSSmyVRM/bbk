<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_Conference.ConferenceChangeRequest" Buffer="true" ValidateRequest="false" %><%--ZD 100170--%>

<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls"
    TagPrefix="mbcbb" %>
<%@ Register TagPrefix="cc1" Namespace="myVRMWebControls" Assembly="myVRMWebControls" %>
<%--FB 2136--%>
<%--Edited for FF--%>
<%if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
{%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%}
else {%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%} %>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%--FB 2779--%>
<% 
    HttpContext.Current.Application.Lock(); //Added for FB 1648
    //Version and Year upgradation start 
    HttpContext.Current.Application.Remove("CopyrightsDur");
    HttpContext.Current.Application.Remove("Version");
    HttpContext.Current.Application.Add("Version", "2.9415.3.3");//Can Edit for version upgradation //FB 2864
    HttpContext.Current.Application.Add("CopyrightsDur", "2002-2015");//FB 1814//Can Edit for year upgradation //FB 2682
    //Version and Year upgradation end  
    HttpContext.Current.Application.UnLock();
    //Response.Write(Request.QueryString["t"].ToString());
    if (Request.QueryString["t"] != null) //FB 1628
    if (Request.QueryString["t"].ToString().Equals("hf")) { %>
<!-- #INCLUDE FILE="inc/maintop4.aspx" -->
<%-- FB 1861 --%>
<!--  #INCLUDE FILE="../en/inc/Holiday.aspx"  -->
<table width="100%">
    <tr>
        <td>
            <img src="../en/Organizations/Original/Image/lobbytop1024.jpg" width="100%" height="72" alt="Lobby Top Image" />
        </td>
    </tr>
</table>
<% } else { %>
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<% } %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Conference Change Request</title>
<script type="text/javascript" src="inc/functions.js"></script>

</head>
<body>
    <form id="frmResponse" runat="server" method="post" onsubmit="return true">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <asp:HiddenField ID="txtUserID" runat="server" />
    <asp:HiddenField ID="txtConfID" runat="server" />
    <asp:HiddenField ID="hdnLocation" runat="server" />
    <asp:HiddenField ID="hdnLcID" runat="server" />
    <asp:HiddenField ID="txtDecision" runat="server" />
    <asp:HiddenField ID="hdnImgName" runat="server" />
    <asp:HiddenField ID="hdnIsEnabledSecBadge" runat="server" />
    <asp:HiddenField ID="hdnObjAxis" runat="server" />
    <div>
        <table width="100%" id="tblMain">
            <tr>
                <td align="center">
                    <br />
                    <h3>
                            <asp:Literal Text="<%$ Resources:WebResources, ConferenceChangeRequest%>" runat="server"></asp:Literal> 
                            <br />
                        <asp:Label ID="lblHeader" runat="server" CssClass="lblMessage"></asp:Label>
                    </h3>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellspacing="5">
                        <tr>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_ConferenceDeta%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" border="0">
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext" valign="top" width="100px"> <%--ZD 100642_11jan2014--%>
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Title%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" valign="top" width="500px"> <%--ZD 100642_11jan2014--%>
                                <asp:Label ID="lblConfName" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="Medium" ForeColor="Green"></asp:Label>
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext" valign="top" width="100px"> <%--ZD 100642_11jan2014--%>
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_UniqueID%>" runat="server"></asp:Literal>
                            </td>
                            <td valign="top" style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" valign="top">
                                <asp:Label ID="lblConfUniqueID" runat="server" Font-Bold="True" Font-Names="Verdana"
                                    Font-Size="x-Small" ForeColor="Red"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Date%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" style="font-weight: normal; font-size: x-small; color: black; font-family: Verdana;"
                                valign="top" width="300">
                                <asp:Label ID="lblConfDate" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                                <asp:Label ID="lblConfTime" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                                <asp:Label ID="lblTimezone" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Duration%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left">
                                <asp:Label ID="lblConfDuration" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td id="tdpw" runat="server" align="left" style="font-weight: bold" class="blackblodtext"> 
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_tdpw%>" runat="server"></asp:Literal>
                            </td>
                            <td id="tdpwd" runat="server" style="width:1px"><b>:</b>&nbsp;</td>
                            <td id ="tdpw1" runat="server" align="left">
                                <asp:Label ID="lblPassword" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label>&nbsp;
                            </td>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Type%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left">
                                <asp:Label ID="lblConfType" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr> <%--ZD 100718--%>
                            <td align="left" style="font-weight: bold" class="blackblodtext" runat="server" id="tdChangeReq">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Location%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblLocation" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Files%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3">
                                <asp:Label ID="lblFiles" runat="server" Font-Bold="False" Font-Names="Verdana" Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                        <tr> <%--ZD 100642_11jan2014--%>
                            <td align="left" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ResponseConference_Description%>" runat="server"></asp:Literal>
                            </td>
                            <td style="width:1px"><b>:</b>&nbsp;</td>
                            <td align="left" colspan="3" valign="top">
                                <asp:Label ID="lblDescription" runat="server" Font-Bold="False" Font-Names="Verdana"
                                    Font-Size="Small"></asp:Label>&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trButtonsHeader" runat="server" class="btprint">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_ChooseanActio%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr runat="server" id="trButtons" class="btprint">
                <td align="center">
                    <table width="90%" border="0"><%--ZD 100718--%>
                        <tr id="trOption1" runat="server" align="left">
                            <td>
                                <asp:RadioButton id="Option1" Checked="true" Text="<%$ Resources:WebResources, CROption_1%>" GroupName="Action" runat="server" />
                            </td>
                        </tr>
                        <tr align="left"><%--ZD 100718--%>
                            <td>
                                <asp:RadioButton id="Option2" Text="<%$ Resources:WebResources, CROption_2%>" runat="server" GroupName="Action"  />
                            </td>
                        </tr>
                        <tr align="left"><%--ZD 100718--%>
                            <td>
                                <asp:RadioButton id="Option3" Text="<%$ Resources:WebResources, CROption_3%>" runat="server" GroupName="Action"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                               <button id="btnSubmit" class="altLongBlueButtonFormat" onserverclick="SubmitAction"  runat="server">
                               <asp:Literal Text="<%$ Resources:WebResources, Submit%>" runat="server"></asp:Literal></button>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trFooter1">
                <td>
                    <br />
                    <br />
                    <br />
                    <hr />
                </td>
            </tr>
            <tr id="trFooter">
                <td align="left" style="height: 52px">
                    <table border="0" cellpadding="2" cellspacing="2" class="btprint" width="100%">
                        <tr valign="bottom">
                            <td>
                                <span style="font-size: 7pt; color: #0000ff"><span class="srcstext2"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_TechSupportCo%>" runat="server"></asp:Literal>
                                    :
                                    <asp:Label ID="lblContactName" runat="server" Font-Names="Verdana" Font-Size="Small"></asp:Label></span><span
                                        class="contacttext" style="color: #0000ff"></span></span>
                            </td>
                            <td style="font-size: 7pt; color: #0000ff" width="10">
                            </td>
                            <td style="font-size: 7pt; color: #0000ff">
                                <span class="srcstext2"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_TechSupportEm%>" runat="server"></asp:Literal>
                                    <asp:Label ID="lblContactEmail" runat="server"></asp:Label></span>
                            </td>
                            <td style="font-weight: bold" width="10">
                            </td>
                            <td style="font-size: 7pt; color: #0000ff">
                                <span style="font-size: 7pt"><span class="srcstext2"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_TechSupportPh%>" runat="server"></asp:Literal>
                                    <asp:Label ID="lblContactPhone" runat="server"></asp:Label></span></span>
                            </td>
                        </tr>
                        <tr style="font-size: 7pt; color: #0000ff">
                            <td align="center" colspan="5">
                                <span class="srcstext2"><asp:Literal Text="<%$ Resources:WebResources, ResponseConference_myVRMVersion%>" runat="server"></asp:Literal>
                                    <%=Application["Version"].ToString()%>,(c)<asp:Literal Text="<%$ Resources:WebResources, Copyright%>" runat="server"></asp:Literal>
                                    <%=Application["CopyrightsDur"].ToString()%>
                                    <a href="http://www.myvrm.com" target="_blank"><strong>myVRM.com</strong></a>. 
                                    <asp:Literal Text="<%$ Resources:WebResources, AllRightsReserved%>" runat="server"></asp:Literal>.</span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input type="hidden" id="tempText" name="tempText" runat="server" />
        <input runat="server" id="hdnRecur" type="hidden" />
    </div>
    </form>
</body>
</html>

<script type ="text/javascript" >

    if (document.getElementById("errLabel") != null)
        var obj = document.getElementById("errLabel");
    
    if (obj != null) {

        var strInput = obj.innerHTML.toUpperCase();

        if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("EXITOSA") > -1) && !(strInput.indexOf("FALLIDA") > -1) && !(strInput.indexOf("ERROR") > -1))
            || ((strInput.indexOf("SUCC�S") > -1 || strInput.indexOf("�TABLIE") > -1) && !(strInput.indexOf("�CHEC") > -1) && !(strInput.indexOf("ERREUR") > -1)) //French            
            ) {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");  
            obj.setAttribute("className", "lblError");
        }
    } 
    
</script>

<script type="text/javascript" src="inc/softedge.js"></script>

<%
    if (!Request.QueryString["t"].ToString().Equals("hf"))
    { %>
<!-- #INCLUDE FILE="inc/mainBottomNet.aspx" -->

<script language="javascript">
    document.getElementById("trFooter").style.display="none";
    document.getElementById("trFooter1").style.display="none";
</script>

<%
}%>
