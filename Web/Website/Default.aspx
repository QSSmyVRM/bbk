﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<script type="text/javascript">
//    function SetBrowserlang() {
//        var Browserlang = navigator.language || navigator.userlanguage;
//        //'<%Session["BrowserLanguage"] = "' + Browserlang + '"; %>';
//        //alert('<%=Session["BrowserLanguage"]%>');
//    }
//    SetBrowserlang();
    var Browserlang = navigator.userLanguage || navigator.language; //ZD 103174        
    var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));  
    if (mobile) {  
        window.location.replace('Mobile/login.aspx');  // Mobile Site
    }
    else {
        //ZD 103174 - Start
        var curReq = '<%=Session["curReq"] %>';

        var qryStr = "?";
        if (curReq.indexOf('?') > -1)
            qryStr = "&";

        if (curReq != null && curReq != "") {
            if (Browserlang != "" && Browserlang != null)
                window.location.href = curReq + qryStr + 'lang="' + Browserlang + '"'; // PC Site
            else
                window.location.href = curReq + qryStr + 'lang="en-US"'; // PC Site
        }
        else {
            if (Browserlang != "" && Browserlang != null)
                window.location.replace('en/genlogin.aspx?lang="' + Browserlang + '"'); // PC Site
            else
                window.location.replace('en/genlogin.aspx?lang="en-US"'); // PC Site
        }
    }
    //ZD 103174 - End
</script>