/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
menunum = 0;
menus=new Array();
_d=document; 



function addmenu()
{
	menunum++;
	menus[menunum]=menu;
}

function dumpmenus()
{
	mt = "<script language=javascript>";
	for (a=1; a<menus.length; a++) {
		mt += " menu" + a + "=menus[" + a + "];"
	}
	mt += "<\/script>";
	_d.write(mt)
}
var menu1Val,menu5Val;

menu1Val = ["Home ","SettingSelect2.aspx",,"Go to the Home page",0];

//menu5Val = ["New Conference","ConferenceSetup.aspx?t=n&op=1",,"Create a New Meeting",0];
if(defaultConfTempJS == "0") //FB 1746
    menu5Val = ["New Conference","ConferenceSetup.aspx?t=n&op=1",,"Create a New Conference",0];
else
    menu5Val = ["New Conference","ConferenceSetup.aspx?t=t",,"Create a New Conference",0];

if((navigator.appVersion.indexOf("MSIE 7.0")>0) || (navigator.appVersion.indexOf("MSIE 6.0")>0) || (navigator.appVersion.indexOf("MSIE 8.0")>0))
{
    menu_0 = ["mainmenu",72,3,90,,,style1,1,"center",effect,,1,,,,,,,,,,]			// 24
    menu_0_2 = ["mainmenu", 70, 3, 90, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]		// 24
    if (isExpressUser != null)//FB 1779
        if (isExpressUser == 1)
            menu_0_2 = ["mainmenu", 72, 3, 110, , , style1_2, 1, "center", effect, , 1, , , , , , , , , , ]
        
    menu_1 = menu1Val //["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    //menu_3 = ["List&nbsp;","ConferenceList.aspx?t=2",,"Ongoing Conferences",0]
    menu_3 = menu5Val;
    //menu_4 = ["Search&nbsp;","dispatcher/gendispatcher.asp?cmd=GetSearchConference",,"Search Conference",0]
    menu_4 = ["Settings&nbsp;","show-menu=settings",,"My Settings",0]
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    //menu_5 = menu5Val //["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_5 = ["Organization","show-menu=organization",,"Organization",0]
    menu_6 = ["Site &nbsp;", "show-menu=site", , "Site", 0]
    menu_7 = ["Schedule a<br/> Call ", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779
    menu_8 = ["View / Edit<br/> Reservations", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	submenu1_0   = ["conference",94,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",94,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

//    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
//    menu = menu.concat( eval("submenu1_0_2"));
//    for (i=1; i<=3; i++) 
//        menu = menu.concat(eval("submenu1_" + i));

//    addmenu();
	submenu2_0   = ["settings",94,,100,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu2_0_2 = ["settings",94,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	submenu2_1 = ["Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	submenu2_2 = ["Reports","GraphicalReport.aspx",,"Reports",0]
	submenu2_3 = ["Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
    submenu2_4 = ["Groups","ManageGroup.aspx",,"manage group",0]
	

    
    submenu3_0   = ["organization",94,,120,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu3_0_2 = ["organization",94,,120,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	
	submenu3_1  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu3_1_ = ["Hardware","",,,0]
	submenu3_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu3_2_ = ["Locations","",,,0]
	submenu3_3  = ["Users","show-menu=usr",,"Manage users",0]
	submenu3_3_ = ["Users","",,,0]
    submenu3_4  = ["Options","mainadministrator.aspx",,"Edit System Settings",0]
    submenu3_5  = ["Settings <hr/>","OrganisationSettings.aspx",,"Edit Organization Settings",0]
  	submenu3_6  = ["Audiovisual","show-menu=av",,,0]
    submenu3_6_ = ["Audiovisual","",,,0] // FB 2570
	submenu3_7  = ["Catering","show-menu=catr",,,0]
	submenu3_7_ = ["Catering","",,,0]
	submenu3_8 = ["Facility", "show-menu=hk", , , 0]
	submenu3_8_ = ["Facility", "", , , 0] // FB 2570
	
	submenu4_0   = ["site",94,,100,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu4_0_2 = ["site",94,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	submenu4_1  = ["Settings","SuperAdministrator.aspx",,"Edit system settings",0]
	
        submenu3_1_0 = ["mcu",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_0_2 = ["mcu",,,100,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_1 = ["Endpoints","EndpointList.aspx?t=",,"Setup Bridge",0]
		submenu3_1_2 = ["Diagnostics","EventLog.aspx",,"View hardware problem log",0]
		submenu3_1_3 = ["MCUs","ManageBridge.aspx",,"Edit Bridge Management",0]
		submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
		submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

		submenu3_2_0 = ["loc",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_0_2 = ["loc",,,100,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
		submenu3_2_2 = ["Tiers","ManageTiers.aspx",,"Manage Tiers",0]

        
		submenu3_3_0 = ["usr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_0_2 = ["usr",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
		submenu3_3_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
		submenu3_3_3 = ["Departments","ManageDepartment.aspx",,"Manage Department",0]
		submenu3_3_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu3_3_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu3_3_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		submenu3_3_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
		submenu3_3_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
		submenu3_3_8 = ["Templates","ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]

		submenu3_6_0 = ["av",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_6_0_2 = ["av",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_6_1 = ["Audiovisual Inventories","InventoryManagement.aspx?t=1",,"",0] // FB 2570
		submenu3_6_2 = ["Work Orders","ConferenceOrders.aspx?t=1",,"",0]

		
		submenu3_7_0 = ["catr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_7_0_2 = ["catr",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_7_1 = ["Catering Menus","InventoryManagement.aspx?t=2",,"",0] // FB 2570
        submenu3_7_2 = ["Work Orders","ConferenceOrders.aspx?t=2",,"",0]

		submenu3_8_0 = ["hk",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_8_0_2 = ["hk",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
		submenu3_8_2 = ["Work Orders","ConferenceOrders.aspx?t=3",,"",0]

	    cur_munu_no = 1;	cur_munu_str = "2_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	        addmenu();
        }
        cur_munu_no = 2;	cur_munu_str = "3_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	      for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
	        addmenu();
        }
   	
		cur_munu_no = 3;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i = 1; i <= mms_num[cur_munu_no]; i++) {
			    if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
			    if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 4;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 5;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i = 1; i <= mms_num[cur_munu_no]; i++) {
			    if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 6;	cur_munu_str = "3_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 7;	cur_munu_str = "3_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 8;	cur_munu_str = "3_8_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 9;	cur_munu_str = "4_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}
else
{
     menu_0 = ["mainmenu",72,3,90,,,style1,1,"center",effect,,1,,,,,,,,,,]			// 24
    menu_0_2 = ["mainmenu",70,3,90,,,style1_2,1,"center",effect,,1,,,,,,,,,,]		// 24
    menu_1 = menu1Val //["Lobby&nbsp;","SettingSelect2.aspx",,"Go to the lobby page",0]
    menu_2 = ["Calendar&nbsp;","dispatcher/admindispatcher.asp?cmd=ManageConfRoom&f=v&hf=",,"Go to Room Calendar",0]
    //menu_3 = ["List&nbsp;","ConferenceList.aspx?t=2",,"Ongoing Conferences",0]
    menu_3 = menu5Val;
    //menu_4 = ["Search&nbsp;","dispatcher/gendispatcher.asp?cmd=GetSearchConference",,"Search Conference",0]
    menu_4 = ["Settings&nbsp;","show-menu=settings",,"My Settings",0]
    //menu_5 = ["New Conference","aspToaspNet.asp?tp=ConferenceSetup.aspx&t=n",,"Create a new conference",0]
    //menu_5 = menu5Val //["New Conference","show-menu=conference",,"Create a new conference",0]
    menu_5 = ["Organization","show-menu=organization",,"Organization",0]
    menu_6 = ["Site &nbsp;", "show-menu=site", , "Site", 0]
    menu_7 = ["Schedule a<br/> Call ", "ExpressConference.aspx?t=n", , "Schedule a Call", 0] //FB 1779
    menu_8 = ["View / Edit<br/> Reservations", "ConferenceList.aspx?t=3", , "View Reservations", 0] //FB 1779

    menu = new Array();
    menu = (if_str=="2") ? menu.concat(menu_0_2) : menu.concat(menu_0);

    for (i=1; i<=mmm_num; i++) {
	    menu = ( mmm_int & (1 << (mmm_num-i)) ) ? ( menu.concat(eval("menu_" + i)) ) : menu;
    }
    addmenu();
	submenu1_0   = ["conference",94,,180,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu1_0_2 = ["conference",94,,180,1,,style2_2,,"left",effect,,,,,,,,,,,,]
    submenu1_1 = ["Future","ConferenceSetup.aspx?t=n&op=1",,"Create a Future Conference",0]
	submenu1_2 = ["Immediate","ConferenceSetup.aspx?t=n&op=2",,"Create an Immediate Conference",0]
	submenu1_3 = ["From Template","ManageTemplate.aspx",,"Create a conference from Template",0]

//    menu = new Array();
    //menu = menu.concat( eval("submenu1_0"));
//    menu = menu.concat( eval("submenu1_0_2"));
//    for (i=1; i<=3; i++) 
//        menu = menu.concat(eval("submenu1_" + i));

//    addmenu();
	submenu2_0   = ["settings",94,,100,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu2_0_2 = ["settings",94,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	submenu2_1 = ["Preferences","ManageUserProfile.aspx",,"Edit preferences",0]
	submenu2_2 = ["Reports","GraphicalReport.aspx",,"Reports",0]
	submenu2_3 = ["Templates","ManageTemplate.aspx",,"Manage Conference Templates",0]
    submenu2_4 = ["Groups","ManageGroup.aspx",,"manage group",0]
	

    
    submenu3_0   = ["organization",94,,120,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu3_0_2 = ["organization",94,,120,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	
	submenu3_1  = ["Hardware","show-menu=mcu",,"Manage Hardware",0]
	submenu3_1_ = ["Hardware","",,,0]
	submenu3_2  = ["Locations","show-menu=loc",,"Manage locations",0]
	submenu3_2_ = ["Locations","",,,0]
	submenu3_3  = ["Users","show-menu=usr",,"Manage users",0]
	submenu3_3_ = ["Users","",,,0]
    submenu3_4  = ["Options","mainadministrator.aspx",,"Edit System Settings",0]
    submenu3_5  = ["Settings <hr/>","OrganisationSettings.aspx",,"Edit Organization Settings",0]
  	submenu3_6  = ["Audiovisual","show-menu=av",,,0]
    submenu3_6_ = ["Audiovisual","",,,0] // FB 2570
	submenu3_7  = ["Catering","show-menu=catr",,,0]
	submenu3_7_ = ["Catering","",,,0]
	submenu3_8 = ["Facility", "show-menu=hk", , , 0]
	submenu3_8_ = ["Facility", "", , , 0] // FB 2570
	
	submenu4_0   = ["site",94,,100,1,,style2,,"left",effect,,,,,,,,,,,,]
	submenu4_0_2 = ["site",94,,100,1,,style2_2,,"left",effect,,,,,,,,,,,,]
	submenu4_1  = ["Settings","SuperAdministrator.aspx",,"Edit system settings",0]
	
        submenu3_1_0 = ["mcu",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_0_2 = ["mcu",,,100,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_1_1 = ["Endpoints","EndpointList.aspx?t=",,"Setup Bridge",0]
		submenu3_1_2 = ["Diagnostics","EventLog.aspx",,"View hardware problem log",0]
		submenu3_1_3 = ["MCUs","ManageBridge.aspx",,"Edit Bridge Management",0]
		submenu3_1_4 = ["Audio Bridges", "ManageAudioAddOnBridges.aspx", , "Audio Bridge Management", 0] //FB 2023
		submenu3_1_5 = ["EM7", "EM7Dashboard.aspx", , "EM7", 0] //FB 2633

		submenu3_2_0 = ["loc",,,100,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_0_2 = ["loc",,,100,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_2_1 = ["Rooms","dispatcher/admindispatcher.asp?cmd=ManageConfRoom",,"Edit or set up conference room",0]
		submenu3_2_2 = ["Tiers","ManageTiers.aspx",,"Manage Tiers",0]

        
		submenu3_3_0 = ["usr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_0_2 = ["usr",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_3_1 = ["Active Users","ManageUser.aspx?t=1",,"Edit, search or create a new user",0]
		submenu3_3_2 = ["Bulk Tool","dispatcher/admindispatcher.asp?cmd=GetAllocation",,"Manage Bulk User",0]
		submenu3_3_3 = ["Departments","ManageDepartment.aspx",,"Manage Department",0]
		submenu3_3_4 = ["Guests","ManageUser.aspx?t=2",,"Edit, search or create a new guest",0]
		if (sso_int)
			submenu3_3_5 = ["Restore User","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		else
			submenu3_3_5 = ["Inactive Users","ManageUser.aspx?t=3",,"Delete or Replaced a removed user",0]
		submenu3_3_6 = ["LDAP Directory Import","aspToaspNet.asp?tp=LDAPImport.aspx",,"Setup Multiple Users",0]
		submenu3_3_7 = ["Roles","dispatcher/admindispatcher.asp?cmd=GetUserRoles",,"Edit, search or create user roles",0]
		submenu3_3_8 = ["Templates","ManageUserTemplatesList.aspx",,"Edit, search or create user templates",0]

		submenu3_6_0 = ["av",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_6_0_2 = ["av",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_6_1 = ["Audiovisual Inventories","InventoryManagement.aspx?t=1",,"",0] // FB 2570
		submenu3_6_2 = ["Work Orders","ConferenceOrders.aspx?t=1",,"",0]

		
		submenu3_7_0 = ["catr",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_7_0_2 = ["catr",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_7_1 = ["Catering Menus","InventoryManagement.aspx?t=2",,"",0] // FB 2570
        submenu3_7_2 = ["Work Orders","ConferenceOrders.aspx?t=2",,"",0]

		submenu3_8_0 = ["hk",,,150,1,"",style2,,"left",effect,,,,,,,,,,,,]
		submenu3_8_0_2 = ["hk",,,150,1,"",style2_2,,"left",effect,,,,,,,,,,,,]
		submenu3_8_1 = ["Facility Services", "InventoryManagement.aspx?t=3", , "", 0] // FB 2570
		submenu3_8_2 = ["Work Orders","ConferenceOrders.aspx?t=3",,"",0]

	    cur_munu_no = 1;	cur_munu_str = "2_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	        addmenu();
        }
        cur_munu_no = 2;	cur_munu_str = "3_";
        if ( mms_int[cur_munu_no] ) {
	        menu = new Array();
	        menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
	        for (i=1; i<=mms_num[cur_munu_no]-3; i++) {
		        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	        }
	      for (i=mms_num[cur_munu_no]-2; i<=mms_num[cur_munu_no]; i++) {
			    if (rf_int & 1)
			        menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
	    		rf_int = rf_int/2;
		    }
	        addmenu();
        }
   	
		cur_munu_no = 3;	cur_munu_str = "3_1_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i = 1; i <= mms_num[cur_munu_no]; i++) {
			    if (EnableAudioBridge == 0 && i == 4) continue; //FB 2023
			    if (EnableEM7Opt == 0 && i == 5) continue; // FB 2633
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 4;	cur_munu_str = "3_2_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		cur_munu_no = 5;	cur_munu_str = "3_3_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i = 1; i <= mms_num[cur_munu_no]; i++) {
			    if ((EnableCloudInstallation == 1 && EnableAdvancedUserOption == 1) && (UsrCrossAccess == 1 || UsrCrossAccess == 0) && i == 7) continue; //ZD 100164
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 6;	cur_munu_str = "3_6_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 7;	cur_munu_str = "3_7_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}

		cur_munu_no = 8;	cur_munu_str = "3_8_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
		
		cur_munu_no = 9;	cur_munu_str = "4_";
		if ( mms_int[cur_munu_no] ) {
			menu = new Array();
			menu = menu.concat( eval("submenu" + cur_munu_str + "0" + ((if_str=="2") ? "_2" : "")) );
			for (i=1; i<=mms_num[cur_munu_no]; i++) {
				menu = ( mms_int[cur_munu_no] & (1 << (mms_num[cur_munu_no]-i)) ) ? ( menu.concat(eval("submenu" + cur_munu_str + i)) ) : menu;
			}
			addmenu();
		}
}
dumpmenus()
