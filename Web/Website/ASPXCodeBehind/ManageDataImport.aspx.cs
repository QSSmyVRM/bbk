/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
using DevExpress.Utils;
using DevExpress.Web.ASPxGridView.Export;
using System.Drawing;

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class ManageDataImport : System.Web.UI.Page
{
    #region protected Members

    protected System.Web.UI.HtmlControls.HtmlTableRow trDetails;
    protected System.Web.UI.HtmlControls.HtmlGenericControl MainDiv;
    protected System.Web.UI.WebControls.Label lblHeading;
    protected System.Web.UI.WebControls.Label errLabel;
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    protected DevExpress.Web.ASPxGridView.Export.ASPxGridViewExporter gridExport;
    protected DevExpress.Web.ASPxEditors.ASPxComboBox lstDataImportType;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    
    #endregion

    #region protected data members

    private myVRMNet.NETFunctions obj;
    private MyVRMNet.LoginManagement obj1;
    private ns_Logger.Logger log;

    protected Int32 orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataTable rptTable = new DataTable();
    protected DataTable participantTable = new DataTable();
    protected String tmzone = "";
    protected String organizationID = "";
    protected Int32 usrID = 11;

    #endregion

    #region Constructor
    public ManageDataImport()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
        obj1 = new MyVRMNet.LoginManagement();
    }

    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page_init
    /// <summary>
    /// Page_init
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_init(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
                Session["ReportXML"] = null;

            if (Session["organizationID"] != null)
                Int32.TryParse(Session["organizationID"].ToString(), out orgId);

            if (Session["timezoneID"] != null)
                tmzone = Session["timezoneID"].ToString();
            else
                tmzone = "26";

            if (Session["ReportXML"] != null)
                btnOk_Click(null, null);

        }
        catch (Exception ex)
        {
            log.Trace("Page_init" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region Methods Executed on Page Load

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("managedataimport.aspx", Request.Url.AbsoluteUri.ToLower());

            lblHeading.Text = obj.GetTranslatedText("Data Import") + " - " + obj.GetTranslatedText(lstDataImportType.Text);

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    Int32.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (hdnValue.Value == "1")
            {
                Session["ReportXML"] = null;
                hdnValue.Value = "";
            }

            if (!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            log.Trace("Page_Load" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region BindData

    private void BindData()
    {
        try
        {
            btnOk_Click(null, null);
        }
        catch (Exception ex)
        {
            log.Trace("BindData: " + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;
        }
    }

    #endregion

    #region MainGrid_DataBound

    protected void MainGrid_DataBound(object sender, EventArgs e)
    {
        try
        {
            ASPxGridView gridView = sender as ASPxGridView;

            for (Int32 c = 0; c < gridView.Columns.Count; c++)
            {
                gridView.Columns[c].CellStyle.Wrap = DevExpress.Utils.DefaultBoolean.False;
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace("MainGrid_DataBound" + ex.Message);
        }
    }
    #endregion
    
    #region exporter_RenderBrick

    protected void exporter_RenderBrick(object sender, ASPxGridViewExportRenderingEventArgs e)
    {
        GridViewDataColumn dataColumn = e.Column as GridViewDataColumn;
        if (e.RowType == GridViewRowType.Data && dataColumn != null && dataColumn.FieldName == "id")
        {
            //e.BrickStyle.ForeColor = Color.Red;
            //e.BrickStyle.BackColor = Color.LightYellow;
            //e.BrickStyle.
            
        }
    }
    #endregion

    #region btnOk_Click
    protected void btnOk_Click(object sender, EventArgs e)
    {
        StringBuilder inXml = new StringBuilder();
        String outXML = "";
        try
        {
            GenerateInXML(ref inXml);

            if ((Session["ReportXML"] == null) && inXml.ToString() != "")
            {
                outXML = obj.CallMyVRMServer("GetUsageReports", inXml.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                Session["ReportXML"] = outXML;
            }
            else if (Session["ReportXML"] != null && Session["ReportXML"].ToString() != "")
                outXML = Session["ReportXML"].ToString();

            xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outXML);
            ds = new DataSet();
            ds.ReadXml(new XmlNodeReader(xmlDoc));
            MainGrid.Columns.Clear();

            if (ds.Tables.Count > 0)
            {
                CreateTable();
                CreateGridColumns();

                try
                {
                    MainGrid.DataSource = rptTable;
                    MainGrid.DataBind();
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace + " : " + ex.Message);
                }
            }
            else
            {
                errLabel.Visible = true;
                errLabel.Text = obj.GetTranslatedText("No Records");
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region GenerateInXML

    private void GenerateInXML(ref StringBuilder inXml)
    {
        try
        {
            String rptType = "";

            switch (lstDataImportType.Value.ToString())
            {
                case "1":
                    rptType = "DIU";
                    break;
                case "2":
                    rptType = "DIR";
                    break;
                case "3":
                    rptType = "DIE";
                    break;
                case "4":
                    rptType = "DIM";
                    break;
            }

            inXml.Append("<GetUsageReports>");
            inXml.Append(obj.OrgXMLElement());
            inXml.Append("<DateFrom></DateFrom>");
            inXml.Append("<DateTo></DateTo>");
            inXml.Append("<DateFormat></DateFormat>");
            inXml.Append("<UserID>" + usrID + "</UserID>");
            inXml.Append("<ReportType>" + rptType + "</ReportType>");
            inXml.Append("<InputType></InputType>");
            inXml.Append("</GetUsageReports>");

        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();
            log.Trace(ex.Message);
        }
    }

    #endregion

    #region CreateTable

    private void CreateTable()
    {
        try
        {
            String colName = "";
            rptTable = new DataTable();
            
            for (Int32 t1 = 0; t1 < ds.Tables.Count; t1++)
            {
                DataTable colTable = ds.Tables[t1];

                for (Int32 i = 0; i < colTable.Columns.Count; i++)
                {
                    colName = "";
                    colName = colTable.Columns[i].ToString().ToLower();
					//ZD 104091
                    if (colName == obj.GetTranslatedText("Approvers").ToLower())
                    {
                        string tmpColName = "";
                        for (Int32 c = 1; c <= 3; c++)
                        {
                            tmpColName = obj.GetTranslatedText("Conference Approver") + ' ' + c;
                            rptTable.Columns.Add(tmpColName, typeof(String));
                        }
                    }
                    else
                        rptTable.Columns.Add(obj.GetTranslatedText(colTable.Columns[i].ToString()), typeof(String));
                }
            }

            for (Int32 t = 0; t < ds.Tables.Count; t++)
            {
                DataTable detailsTable = ds.Tables[t];

                for (Int32 i = 0; i < detailsTable.Rows.Count; i++)
                {
                    DataRow dataRow = null;
                    dataRow = rptTable.NewRow();

                    if (detailsTable.Rows[0][1].ToString() == "")
                        break;

                    Int32 rCnt = 0;
                    rCnt = detailsTable.Columns.Count;

                    Int32 c = 0;
                    String pwd = "";
                    for (int j = 0; j < rCnt; j++)
                    {
                        colName = detailsTable.Columns[j].ColumnName.ToLower();
						//ZD 104091
                        if (colName == obj.GetTranslatedText("Approvers").ToLower())
                        {
                            dataRow[c] = detailsTable.Rows[i][colName].ToString();
                            string apprString = detailsTable.Rows[i][colName].ToString();
                            string[] tmpColName = apprString.Split('|');

                            for (Int32 cc = 0; cc < 3; cc++)
                            {
                                if (cc + 1 <= tmpColName.Length)
                                    dataRow[c] = tmpColName[cc];
                                else
                                    dataRow[c] = "";

                                c = c + 1;
                            }
                        }
                        else
                        {
                            if (colName == "id" || colName == "pid")
                            {
                                pwd = "";
                                pwd = detailsTable.Rows[i][colName].ToString();
                                obj1.simpleEncrypt(ref pwd);
                                dataRow[c] = "ox" + pwd;
                            }
                            else if (colName == "media (none audio-only audio-video)" || colName == "user role (user/admin/super admin etc)"
                                || colName == "address type" || colName == "preferred dialing option"
                                || colName == "located outside the network" || colName == "projector available"
                                || colName == "outlook" || colName == "notes" || colName == "profile type") //ZD 104091
                            {
                                dataRow[c] = obj.GetTranslatedText(detailsTable.Rows[i][colName].ToString());
                            }
                            else
                                dataRow[c] = detailsTable.Rows[i][colName].ToString();
                        }
                        //colName = obj.GetTranslatedText(detailsTable.Columns[j].ColumnName.ToLower());

                        c = c + 1;
                    }
                    rptTable.Rows.Add(dataRow);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region CreateGridColumns

    private void CreateGridColumns()
    {
        try
        {
            GridViewDataColumn dataColumn = new GridViewDataColumn();
            MainGrid.Columns.Clear();
            for (Int32 i = 0; i < rptTable.Columns.Count; i++)
            {
                String colName = "";
                colName = obj.GetTranslatedText(rptTable.Columns[i].ColumnName);
                dataColumn = new GridViewDataColumn();
                dataColumn.Caption = colName;
                dataColumn.FieldName = colName;
                if (colName.ToLower() == "id" || colName.ToLower() == "pid" || colName.ToLower() == obj.GetTranslatedText("password").ToLower()
                    || colName.ToLower() == obj.GetTranslatedText("initial password").ToLower())
                    dataColumn.Visible = false;
				// ZD 103550
                if (colName.ToLower() == obj.GetTranslatedText("blue jeans user name"))
                {
                    if (Session["EnableBlueJeans"].ToString() == "1")
                        dataColumn.Visible = true;
                    else
                        dataColumn.Visible = false;
                }
                MainGrid.Columns.Add(dataColumn);
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateGridColumns" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion

    #region ExportExcel

    protected void ExportExcel(object sender, EventArgs e)
    {
        try
        {
            //ZD 103896
            //DevExpress.XtraPrinting.XlsExportOptions options = new DevExpress.XtraPrinting.XlsExportOptions();
            string shtname = "";
            switch (lstDataImportType.Value.ToString())
            {
                case "1":
                    shtname = obj.GetTranslatedText("User Data");
                    break;
                case "2":
                    shtname = obj.GetTranslatedText("Room Data");
                    break;
                case "3":
                    shtname = obj.GetTranslatedText("Endpoint Data");
                    break;
                case "4":
                    shtname = obj.GetTranslatedText("MCU Data");
                    break;
            }

            string fileName = "DataImport " + DateTime.Now.ToString("MM-dd-yyyy");
            if (lstDataImportType.Value.ToString() == "3" || lstDataImportType.Value.ToString() == "4")
                MainGrid.Columns[obj.GetTranslatedText("Password")].Visible = true;
            if (lstDataImportType.Value.ToString() == "1")
                MainGrid.Columns[obj.GetTranslatedText("Initial Password")].Visible = true;

            MainGrid.Columns["id"].Visible = true;
            MainGrid.Columns["id"].VisibleIndex = 0;
            if (lstDataImportType.Value.ToString() == "3")
            {
                MainGrid.Columns["pid"].Visible = true;
                MainGrid.Columns["pid"].VisibleIndex = 1;
            }
            MainGrid.DataBind();
            
            String strFooter = "";
            strFooter = Environment.NewLine + Environment.NewLine + obj.GetTranslatedText("Note") + " : " + Environment.NewLine + Environment.NewLine 
                + obj.GetTranslatedText("For New Entry :  Leave the first column ('ID') value as blank (ie., no need to enter any value for the 'ID' field).");
            strFooter = strFooter + Environment.NewLine + Environment.NewLine + obj.GetTranslatedText("For Modifying Existing Record : Please never modify /remove the value in the first column ('ID').");

            gridExport.ReportFooter = strFooter;

            DataTable dt = new DataTable();
            foreach (GridViewColumn col in MainGrid.VisibleColumns)
            {
                GridViewDataColumn dataColumn = col as GridViewDataColumn;
                if (dataColumn == null) continue;
                dt.Columns.Add(dataColumn.FieldName);
            }

            for (int i = 0; i < MainGrid.VisibleRowCount; i++)
            {
                DataRow row = dt.Rows.Add();
                foreach (DataColumn col in dt.Columns)
                    row[col.ColumnName] = MainGrid.GetRowValues(i, col.ColumnName);
            }

            String destFile = Request.MapPath(".").ToString() + "\\image" + "\\" + fileName + ".xls";
            ExportExcel expExcel = new ExportExcel();
            expExcel.GenerateReport(dt, shtname, strFooter, "", destFile);

            //To open the Excel files
            System.IO.FileStream fs = null;
            fs = System.IO.File.Open(destFile, System.IO.FileMode.Open);
            byte[] btFile = new byte[fs.Length];
            fs.Read(btFile, 0, Convert.ToInt32(fs.Length));
            fs.Close();
            Response.AddHeader("Content-disposition", "attachment; filename=" + fileName + ".xls");
            Response.ContentType = "application/octet-stream";
            Response.BinaryWrite(btFile);
            Response.End();
        }
        catch (Exception ex)
        {
            log.Trace(ex.StackTrace);
        }
    }


    #endregion

}