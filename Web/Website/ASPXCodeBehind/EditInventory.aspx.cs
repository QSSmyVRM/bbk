/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Text;

/// <summary>
/// This file is used to create new/manage Inventory Items for Audio/Visual,
/// Menus for Catering and Groups for Housekeeping.
/// txtType is been used to store the type of Inventory
/// 1: A/V
/// 2: Catering
/// 3: Housekeeping
/// </summary>
/// 
namespace ns_EditInventory
{
    public partial class EditInventory : System.Web.UI.Page
    {
        #region Data Members

        protected static string selRooms = ", ";
        protected TextBox itemName;
        protected TextBox itemQuantity;
        protected TextBox itemPrice;
        protected TextBox itemSNo;
        protected TextBox itemComments;
        protected CheckBox itemPortable;
        protected CheckBox itemDeleted;
        protected Image itemImg;
        DataSet ds;
        
        protected System.Web.UI.WebControls.Label lblType;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.TreeView treeRoomSelection;
        protected System.Web.UI.WebControls.CheckBoxList lstRoomSelection;
        protected System.Web.UI.WebControls.DataGrid itemsGrid;
        protected System.Web.UI.WebControls.DataGrid dgCateringMenus;
		//ZD 100420
        //protected System.Web.UI.WebControls.Button btnSubmit;
        protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;
        //protected System.Web.UI.WebControls.Button btnAddNewMenu;
        protected System.Web.UI.HtmlControls.HtmlButton btnAddNewMenu;
        //protected System.Web.UI.HtmlControls.HtmlInputButton btnAddNewItem;
        protected System.Web.UI.HtmlControls.HtmlButton btnAddNewItem;
		//ZD 100420
        protected System.Web.UI.WebControls.HiddenField selItems;
        protected System.Web.UI.WebControls.HiddenField AVInventoryID;
        protected System.Web.UI.WebControls.TextBox txtInvName;
        protected System.Web.UI.WebControls.TextBox txtInvComments;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedloc;
        protected System.Web.UI.HtmlControls.HtmlTableRow trComments1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trComments2;
        protected System.Web.UI.WebControls.TextBox hdnApprover1;
        protected System.Web.UI.WebControls.TextBox txtApprover1;
        protected System.Web.UI.WebControls.CheckBox chkNotify;
        protected System.Web.UI.WebControls.CheckBox chkReminder;
        protected System.Web.UI.WebControls.RadioButtonList rdSelView;
        protected System.Web.UI.WebControls.Panel pnlListView;
        protected System.Web.UI.WebControls.Panel pnlLevelView;
        protected System.Web.UI.WebControls.DropDownList lstDeliveryTypes;
        protected System.Web.UI.WebControls.Panel pnlNoData;//Added for Location Issues
        protected System.Web.UI.HtmlControls.HtmlTableCell tdCom;//Added for Location Issues
        protected System.Web.UI.HtmlControls.HtmlInputButton btnCompare;//Added for Location Issues
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//Code added for Room Search 
        protected System.Web.UI.HtmlControls.HtmlSelect RoomList;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCatMenuID;//ZD 104792
        
        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        ns_Logger.Logger log;
        myVRMNet.ImageUtil imageUtilObj; //Image Project
        byte[] imgArray = null;
        //FB 1830
        protected String language = "";
        protected string currencyFormat = "";        
        CultureInfo cInfo = null;
        decimal tmpVal = 0;
        #endregion

        #region Constructor for EditInventory Class
        public EditInventory()
        {
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
            ds = new DataSet();
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
            selRooms = "";
        }
        #endregion

        #region Methods Executed on Page 

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region validateInventoryItemsData

        //Function added by Vivek as a fix for issue number 308
        /// <summary>
        /// Browse through the grid and check if duplicate Items exist
        // Display error to user and exit
        /// </summary>
        /// <returns></returns>
        private bool validateInventoryItemsData()
        {
            try
            {
                int i, j;
                string name1, name2;
                bool chk1, chk2;

                for (i = 0; i < itemsGrid.Items.Count; i++)
                {
                    DataGridItem dgi1 = itemsGrid.Items[i];
                    name1 = ((TextBox)dgi1.FindControl("txtName")).Text;
                    chk1 = ((CheckBox)dgi1.FindControl("chkDelete")).Checked;

                    for (j = 0; j < itemsGrid.Items.Count; j++)
                    {
                        if (i != j)
                        {
                            DataGridItem dgi2 = itemsGrid.Items[j];
                            name2 = ((TextBox)dgi2.FindControl("txtName")).Text;
                            chk2 = ((CheckBox)dgi2.FindControl("chkDelete")).Checked;
                            if ((name1.Trim() == name2.Trim()) && (chk1 != true) && (chk2 != true))
                            {
                                errLabel.Text =obj.GetTranslatedText("Item") + name1 + obj.GetTranslatedText(" is duplicated!");//FB 1830 - Translation
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;
                return false;
            }
        }
        #endregion

        #region Page_Load
        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = "";
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheck(Request.Url.AbsoluteUri.ToLower());
                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString();
                obj.AccessConformityCheck("editinventory.aspx?t=" + confChkArg);
                // ZD 100263 Ends



                //FB 1830 - Starts
                if (Session["CurrencyFormat"] == null)
                    Session["CurrencyFormat"] = ns_MyVRMNet.vrmCurrencyFormat.dollar;

                currencyFormat = Session["CurrencyFormat"].ToString();
				//ZD 104792
                if (Session["NumberFormat"] == null)
                    Session["NumberFormat"] = ns_MyVRMNet.vrmNumberFormat.american;

                cInfo = new CultureInfo(Session["NumberFormat"].ToString());//FB 1830

                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End
                
                //Response.Write("Page is loading");
                if (!IsPostBack)
                {
                    obj.GetDeliveryTypes(lstDeliveryTypes);
                    Session["DS"] = new DataSet();
                    if (Request.QueryString["id"] != null)
                        if (Request.QueryString["id"].Equals("new"))
                            lblType.Text = obj.GetTranslatedText("Create New");//FB 1830 - Translation
                        else
                            lblType.Text = obj.GetTranslatedText("Edit");//FB 1830 - Translation
                    lblType.Text += " "; //ZD 100288
                    if (Request.QueryString["t"] != null)
                        txtType.Text = Request.QueryString["t"].ToString();
                    if (txtType.Text.Equals("1"))
                        lblType.Text += obj.GetTranslatedText("Audiovisual Inventories");//FB 1830 - Translation  FB 2570
                    else if (txtType.Text.Equals("2"))
                        lblType.Text += obj.GetTranslatedText("Catering Menus"); //FB Case 883 Saima//FB 1830 - Translation  FB 2570
                    else if (txtType.Text.Equals("3"))
                        lblType.Text += obj.GetTranslatedText("Facility Services");//FB 1830 - Translation  FB 2570
                    if (!txtType.Text.Equals("2"))
                    {
                        String inXML = objInXML.GetInventoryDetails(Request.QueryString["id"].ToString(), "", "", "", "", "", txtType.Text, "", "");
                        String outXML = obj.CallMyVRMServer("GetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        //Response.Write(obj.Transfer(outXML));
                        if (outXML.IndexOf("<error>") < 0)
                        {
                            BindData(outXML);
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(outXML);
                            treeRoomSelection.Nodes.Clear();
                            lstRoomSelection.Items.Clear();
                            TreeNode tn = new TreeNode("All", "All");
                            treeRoomSelection.Nodes.Add(tn);
                            tn.Expanded = true;
                            XmlNode Locnode = xmldoc.SelectSingleNode("//Inventory/locationList/selected");
                            if (Locnode != null)
                            {
                                String locndes = "";
                                XmlNodeList locnodes = Locnode.SelectNodes("level1ID");
                                foreach (XmlNode nd in locnodes)
                                {
                                    if (locndes == "")
                                        locndes = nd.InnerText;
                                    else
                                        locndes += "," + nd.InnerText;
                                }

                                if (locndes != "")
                                {
                                    //myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                                    //locstrname.Value = ntfuncs.RoomDetailsString(locndes);
                                    selectedloc.Value = locndes;
                                    //BindRoomToList();
                                   // ntfuncs = null;
                                }

                                locnodes = null;
                                Locnode = null;

                            }

                            /* XmlNodeList nodesLoc = xmldoc.SelectNodes("//Inventory/locationList");
                             GetLocationList(nodesLoc);*/
                            if (itemsGrid.Items.Count.Equals(0))
                                btnSubmit.Disabled = true;//ZD 100420
                                btnSubmit.Attributes.Add("Class", "btndisable");//FB 2664
                            btnAddNewMenu.Visible = false;
                        }
                        else
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                        }
                    }
                    else
                        GetProviderDetails();
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Visible = true;
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        }
                }
                else
                {
                    if (selItems.Value != "" && !txtType.Text.Equals("2"))
                        AddItemsInGrid();
                    else
                        if (Request.Params.Get("__EVENTTARGET").ToString().IndexOf("popup") >= 0)
                            AddCateringItems();
                }
                //////////////////case #285/////////
                /*
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    if (null != dgi.FindControl("txtPrice"))
                    {
                        ((TextBox)dgi.FindControl("txtPrice")).Attributes.Add("onblur", "javascript:ValidateNumeric('" + ((TextBox)dgi.FindControl("txtPrice")).ClientID + "')");

                    }

                    if (null != dgi.FindControl("txtQuantity"))
                    {
                        ((TextBox)dgi.FindControl("txtQuantity")).Attributes.Add("onblur", "javascript:ValidateInteger('" + ((TextBox)dgi.FindControl("txtQuantity")).ClientID + "')");

                    }

                    DataGrid dgType = (DataGrid)dgi.FindControl("dgDeliveryType");
                    if (null != dgType)
                    {
                        foreach (DataGridItem dgt in dgType.Items)
                        {

                            if (null != dgt.FindControl("txtDeliveryCost"))
                            {
                                ((TextBox)dgt.FindControl("txtDeliveryCost")).Attributes.Add("onblur", "javascript:ValidateNumeric('" + ((TextBox)dgt.FindControl("txtDeliveryCost")).ClientID + "')");

                            }
                            if (null != dgt.FindControl("txtServiceCharge"))
                            {
                                ((TextBox)dgt.FindControl("txtServiceCharge")).Attributes.Add("onblur", "javascript:ValidateNumeric('" + ((TextBox)dgt.FindControl("txtServiceCharge")).ClientID + "')");

                            }

                        }
                    }

                }*/
                //////////////////case #285/////////

               /* //Added for FB 1415,1416,1417,1418-- Start
                if (treeRoomSelection.Nodes.Count == 0 && lstRoomSelection.Items.Count == 0)
                {
                    rdSelView.Enabled = false;
                    pnlNoData.Visible = true;
                    pnlListView.Visible = false;
                    pnlLevelView.Visible = false;
                    btnCompare.Disabled = true;
                }
                //Added for FB 1415,1416,1417,1418-- End*/
                locstrname.Value = obj.RoomDetailsString(selectedloc.Value);
                BindRoomToList();

                //FB  2664 start
                if (btnSubmit.Disabled == false)//ZD 100420
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");
                else
                    btnSubmit.Attributes.Add("Class", "btndisable");
                //FB  2664 End
				//ZD 100263 start
                if (Session["admin"].ToString() == "3")
                {
                    btnSubmit.Visible = false;
                    btnAddNewMenu.Visible = false;
                }
				//ZD 100263 End

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region GetProviderDetails
        protected void GetProviderDetails()
        {
            try
            {
                log.Trace("in GetProviderDetails");
                trComments1.Visible = false;
                trComments2.Visible = false;
                btnAddNewItem.Visible = false;
                btnAddNewMenu.Visible = true;
                AVInventoryID.Value = Request.QueryString["id"].ToString();
                XmlDocument xmldoc = new XmlDocument();
                treeRoomSelection.Nodes.Clear();
                lstRoomSelection.Items.Clear();
                TreeNode tn = new TreeNode("All", "All");
                treeRoomSelection.Nodes.Add(tn);
                tn.Expanded = true;
                if (!AVInventoryID.Value.Trim().ToUpper().Equals("NEW"))
                {
                    StringBuilder inXML = new StringBuilder(); //FB 2559 - Stringbuilder
                    inXML.Append("<GetProviderDetails>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("  <ID>" + AVInventoryID.Value + "</ID>");
                    inXML.Append("  <Type>" + txtType.Text + "</Type>");
                    inXML.Append("</GetProviderDetails>");
                    String outXML = obj.CallMyVRMServer("GetProviderDetails", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    xmldoc.LoadXml(outXML);
                    //xmldoc.Load("C:\\VRM\\XML\\OutXML\\GetProviderDetails.xml");
                    //String outXML = xmldoc.OuterXml;
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderDetails/SelectedLocations");
                    GetLocationList(nodes);
                    if (Request.QueryString["n"] != null)
                        if (Request.QueryString["n"].ToString().Equals("1"))
                        {
                            outXML = outXML.Replace("</MenuList>", "<Menu><ID>new</ID><Name></Name><Price>0</Price><CateringServices></CateringServices><ItemsList></ItemsList></Menu></MenuList>");
                        }
                    log.Trace(outXML);
                    xmldoc.LoadXml(outXML);
                    nodes = xmldoc.SelectNodes("//GetProviderDetails/MenuList/Menu");
                    LoadMenus(nodes);
                }
                else
                {
                    String outXML = "<GetProviderDetails><ID>new</ID><Name></Name><Type>2</Type><AdminID></AdminID><AdminName></AdminName><Notify>0</Notify><SelectedLocations></SelectedLocations><MenuList><Menu><ID>new</ID><Name></Name><Price>0</Price><Comments></Comments><CateringServices></CateringServices><ItemsList></ItemsList></Menu></MenuList></GetProviderDetails>";
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetProviderDetails/SelectedLocations");
                    //GetLocationList(nodes);

                    


                    nodes = xmldoc.SelectNodes("//GetProviderDetails/MenuList/Menu");
                    LoadMenus(nodes);
                }

                XmlNode Locnode = xmldoc.SelectSingleNode("//GetProviderDetails/SelectedLocations");
                if (Locnode != null)
                {
                    String locndes = "";
                    XmlNodeList locnodes = Locnode.SelectNodes("Selected/ID");
                    foreach (XmlNode nd in locnodes)
                    {
                        if (locndes == "")
                            locndes = nd.InnerText;
                        else
                            locndes += "," + nd.InnerText;
                    }

                    if (locndes != "")
                    {
                        myVRMNet.NETFunctions ntfuncs = new myVRMNet.NETFunctions();
                        locstrname.Value = ntfuncs.RoomDetailsString(locndes);
                        selectedloc.Value = locndes;
                        BindRoomToList();
                        ntfuncs = null;
                    }

                    locnodes = null;
                    Locnode = null;

                }

                txtInvName.Text = xmldoc.SelectSingleNode("//GetProviderDetails/Name").InnerText;
                chkNotify.Checked = false;
                if (xmldoc.SelectSingleNode("//GetProviderDetails/Notify").InnerText.Trim().Equals("1"))
                    chkNotify.Checked = true;
                txtApprover1.Text = xmldoc.SelectSingleNode("//GetProviderDetails/AdminName").InnerText.Trim();
                hdnApprover1.Text = xmldoc.SelectSingleNode("//GetProviderDetails/AdminID").InnerText.Trim();
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadMenus
        protected void LoadMenus(XmlNodeList nodes)
        {
            try
            {
                ds = new DataSet();
                XmlTextReader xtr;
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataTable dtMenus = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)//FB 1830
                    {
                        String tempString = dr["Price"].ToString();
                        if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                            tempString = tempString.Replace(",", ".");
                        tmpVal = decimal.Parse(Double.Parse(tempString).ToString("0.00"));
                        dr["Price"] = tmpVal.ToString("g", cInfo);     
                    }

                    dgCateringMenus.DataSource = ds.Tables[0];
                    dgCateringMenus.DataBind();
                    foreach (DataGridItem dgi in dgCateringMenus.Items)
                    {
                        DataSet itemDS = new DataSet();
                        foreach (XmlNode node in nodes)
                            if (dgi.Cells[0].Text.Equals(node.SelectSingleNode("ID").InnerText))
                            {
                                XmlNodeList itemNodes = node.SelectNodes("ItemsList/Item");
                                HiddenField selCateringItems = (HiddenField)dgi.FindControl("selCateringItems");
                                selCateringItems.Value = "";
                                //Response.Write(selCateringItems.);
                                foreach (XmlNode itemNode in itemNodes)
                                {
                                    xtr = new XmlTextReader(itemNode.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                    itemDS.ReadXml(xtr, XmlReadMode.Auto);
                                    selCateringItems.Value += itemNode.SelectSingleNode("Name").InnerText + ":" + itemNode.SelectSingleNode("ID").InnerText
                                        + ":" + itemNode.SelectSingleNode("ImageName").InnerText + ":" + itemNode.SelectSingleNode("Image").InnerText + ":2" + ","; //ZD 100175
                                }
                                if (itemDS.Tables.Count > 0)
                                {
                                    DataGrid dgMenuItems = (DataGrid)dgi.FindControl("dgMenuItems");
                                    dgMenuItems.DataSource = itemDS.Tables[0];
                                    dgMenuItems.DataBind();
                                }
                                itemNodes = node.SelectNodes("CateringServices/Service");
                                CheckBoxList chkLstServices = (CheckBoxList)dgi.FindControl("chkLstServices");
                                //Response.Write(selCateringItems.);
                                foreach (XmlNode itemNode in itemNodes)
                                {
                                    foreach (ListItem li in chkLstServices.Items)
                                        if (li.Value.Equals(itemNode.SelectSingleNode("ID").InnerText.Trim())) //ZD 104792
                                            li.Selected = true;
                                }
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region AddNewMenu
        protected void AddNewMenu(Object sender, EventArgs e)
        {
            string strurl = ""; //ZD 104792
            try
            {

                String inXML = GenerateCateringInXML();
                String errMsg = "";
                errMsg = ValidateXML(inXML);
                if (errMsg.Equals(""))
                {
                    String outXML = obj.CallMyVRMServer("SetProviderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
						//ZD 104792
                        strurl = "EditInventory.aspx?id=" + xmldoc.SelectSingleNode("//SetProviderDetails/ID").InnerText + "&t=2&n=1&m=1";

                        RedirectItemPage(strurl);
                        //Response.Redirect("EditInventory.aspx?id=" + xmldoc.SelectSingleNode("//SetProviderDetails/ID").InnerText + "&t=2&n=1&m=1", false);
                    }
                }
                else
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (System.Threading.ThreadAbortException tex)//ZD 104792
            {
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion
		//ZD 104792
        #region RedirectItemPage
        private void RedirectItemPage(string strurl)
        {
            try{

            }
            catch (System.Threading.ThreadAbortException tex)
            {
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
            finally
            {
                Response.Redirect(strurl, false);
            }
        }
        #endregion

        #region BindData
        protected void BindData(string outXML)
        {
            //string mt = "";
            //TableRow tr;
            //TableCell tc;
            XmlDocument xmldoc = new XmlDocument();

            try
            {
                xmldoc.LoadXml(outXML);
                AVInventoryID.Value = Request.QueryString["id"].ToString();
                if (!AVInventoryID.Value.Equals("new"))
                {
                    txtInvName.Text = xmldoc.SelectSingleNode("/Inventory/Name").InnerText;

                    txtInvComments.Text = xmldoc.SelectSingleNode("/Inventory/Comments").InnerText;

                    hdnApprover1.Text = xmldoc.SelectSingleNode("/Inventory/Admin/ID").InnerText;
                    txtApprover1.Text = obj.GetMyVRMUserName(hdnApprover1.Text); //xmldoc.SelectSingleNode("/Inventory/Admin/Name").InnerText;

                    if (xmldoc.SelectSingleNode("//Inventory/Notify").InnerText.Equals("1"))
                        chkNotify.Checked = true;
                    else
                        chkNotify.Checked = false;

                    XmlNodeList nodes = xmldoc.SelectNodes("//Inventory/ItemList/Item");
                    XmlTextReader xtr;
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                    DataTable dt, dtCharge; //, dtCharges;
                    if (ds.Tables.Count == 0)
                    {
                        dt = new DataTable();
                        dtCharge = new DataTable();
                    }
                    else
                    {
                        dt = ds.Tables[0];
                        if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                        if (!dt.Columns.Contains("Item_Id")) dt.Columns.Add("Item_Id");
                        if (!dt.Columns.Contains("ImageId")) dt.Columns.Add("ImageId"); //Image Project
                        if (!dt.Columns.Contains("ImageName")) dt.Columns.Add("ImageName"); //Image Project

                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["Type"] = txtType.Text;
                            //FB 1830
                            decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                            dr["ServiceCharge"] = tmpVal.ToString("g", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                            dr["DeliveryCost"] = tmpVal.ToString("g", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                            dr["Price"] = tmpVal.ToString("g", cInfo);
                            tmpVal = 0;  

                        }
                        ds.Tables[0].TableName = "Items"; //Item_Id
                        if (ds.Tables.Count > 1)
                        {
                            ds.Tables[1].TableName = "Charges"; // Item_Id, Charges_Id
                            ds.Tables[2].TableName = "Charge"; // Charges_Id 
                        }
                    }

                    if (!txtType.Text.Equals("1"))
                    {
                        HideColumns();
                    }
                    Session.Add("DS", ds);
                    ///////case #274 /////////////////////
                    if (ds.Tables.Count > 0)
                    {
                        itemsGrid.DataSource = ds.Tables[0];
                        itemsGrid.DataBind();//FB 1830
                    }
                    
                }
            }

            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.Message + " : " + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message + " : " + ex.StackTrace;
            }
        }
        #endregion

        #region HideColumns
        protected void HideColumns()
        {
            try
            {
                //DataGridItem dgHeader = null;
                //foreach(DataGridItem dgi in itemsGrid.Items)
                //    if (dgi.ItemType.Equals(ListItemType.Header))
                //        dgHeader = dgi;
                //if (txtType.Text.Equals("3"))
                //{
                //    ((Label)dgHeader.FindControl("lblHName")).Text = "Task/Item";
                //    dgHeader.FindControl("tdHSerialNumber").Visible = false;
                //}
                //dgHeader.FindControl("tdHQuantity").Visible = false;
                //dgHeader.FindControl("tdHPortable").Visible = false;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("HideColumns" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region getDeliveryTypesDataSource
        protected DataView getDeliveryTypesDataSource(String ItemID)
        {
            try
            {
                DataView charges = new DataView();
                DataView charge = new DataView();
                //ds = (DataSet)Session["DS"];
                //Response.Write(ItemID);
                if (txtType.Text.Equals("1"))
                    if (ds.Tables.Count > 1)
                    {
                        //FB 1830
                        DataTable temp = ds.Tables[2].Copy();

                        foreach (DataRow dr in temp.Rows)
                        {
                            dr["DeliveryName"] = obj.GetTranslatedText(dr["DeliveryName"].ToString()); // FB 2272

                            decimal.TryParse(dr["DeliveryCost"].ToString(), out tmpVal);
                            dr["DeliveryCost"] = tmpVal.ToString("g", cInfo);
                            tmpVal = 0;

                            decimal.TryParse(dr["ServiceCharge"].ToString(), out tmpVal);
                            dr["ServiceCharge"] = tmpVal.ToString("g", cInfo);
                            tmpVal = 0;
                        }

                        charges = ds.Tables["Charges"].DefaultView;
                        //charge = ds.Tables["Charge"].DefaultView;
                        charge = temp.DefaultView;//FB 1830

                        charges.RowFilter = "Item_Id='" + Int32.Parse(ItemID) + "'";
                        charge.RowFilter = "Charges_Id='" + Int32.Parse(charges.Table.DefaultView[0]["Charges_Id"].ToString()) + "' AND NOT(DeliveryTypeID ='0')";
                    }


                return charge;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("getDeliveryTypesDataSource" + ex.Message + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.Message + ex.StackTrace;
                errLabel.Visible = true;
                return null;
            }
        }
        #endregion

        #region GetLocationList
        public void GetLocationList(XmlNodeList nodesLoc1)
        {
            try
            {
                String inXML = "<GetLocationsList>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID></GetLocationsList>";//Organization Module Fixes
                String outXML = obj.CallMyVRMServer("GetLocationsList", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //log.Trace(outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodesLoc = xmldoc.SelectNodes("//level3List/level3");
                int selLength = 0;
                int length = nodesLoc.Count;
                XmlNodeList selnodes;
                if (txtType.Text.Equals("2"))
                {
                    try
                    {
                        selnodes = nodesLoc1.Item(0).SelectNodes("Selected/ID");
                        selLength = selnodes.Count;
                        if (selLength >= 1)
                            for (int n = 0; n < selLength; n++)
                                selRooms += selnodes.Item(n).InnerText + ",";
                    }
                    catch (Exception ex) { log.Trace(ex.StackTrace); }
                }
                else
                {
                    selnodes = nodesLoc1.Item(0).SelectNodes("selected/level1ID");
                    selLength = selnodes.Count;
                    if (selLength >= 1)
                        for (int n = 0; n < selLength; n++)
                            selRooms += selnodes.Item(n).InnerText + ",";
                }//Response.Write(selLength);
                int cnt = 0;
                //int cnt2 = 0;
                //int cnt3 = 0;
                foreach (XmlNode node3 in nodesLoc)
                {
                    TreeNode tn1 = new TreeNode(node3.SelectSingleNode("level3Name").InnerText, node3.SelectSingleNode("level3ID").InnerText);
                    XmlNodeList nodes2 = node3.SelectNodes("level2List/level2");
                    int cnt2 = 0;
                    foreach (XmlNode node2 in nodes2)
                    {
                        TreeNode tn2 = new TreeNode(node2.SelectSingleNode("level2Name").InnerText, node2.SelectSingleNode("level2ID").InnerText);
                        tn2.Expanded = true;    // fogbugz case 277
                        tn1.ChildNodes.Add(tn2);
                        XmlNodeList nodes1 = node2.SelectNodes("level1List/level1");
                        foreach (XmlNode node1 in nodes1)
                        {
                            if (Convert.ToInt32(node1.SelectSingleNode("disabled").InnerText) != 1) //code added for Fb 1443
                            {
                                String roomID = node1.SelectSingleNode("level1ID").InnerText.Trim();
                                //String roomName = "<a href=\"javascript:chkresource('" + node1.SelectSingleNode("level1ID").InnerText + "');\">" + node1.SelectSingleNode("level1Name").InnerText + "</a>";
                                String roomName = node1.SelectSingleNode("level1Name").InnerText;
                                ListItem li = new ListItem(roomName, roomID);
                                lstRoomSelection.Items.Add(li);
                                TreeNode tn3 = new TreeNode(roomName, roomID);
                                tn3.ToolTip = roomID;
                                tn3.NavigateUrl = @"javascript:chkresource('" + roomID + "');";
                                cnt = 0;
                                //Response.Write("<br>" + selRooms);
                                tn3.Checked = false;
                                for (int i = 0; i < selRooms.Split(',').Length; i++)
                                {
                                    if (selRooms.Split(',')[i].Trim().Equals(roomID) && !roomID.Equals("11"))
                                    {
                                        tn3.Checked = true;
                                        li.Selected = true;
                                        selectedloc.Value += roomID + ",";
                                        cnt++;
                                    }
                                }
                                tn2.ChildNodes.Add(tn3);
                            }  //fb 1443
                        }
                    }
                    treeRoomSelection.Nodes[0].ChildNodes.Add(tn1);
                }
                //fogbugz case 466: Saima starts here
                if (Session["RoomListView"].ToString().ToUpper().Equals("LIST"))
                {
                    rdSelView.Items.FindByValue("2").Selected = true;
                    rdSelView.SelectedIndex = 1;
                    rdSelView_SelectedIndexChanged(new object(), new EventArgs());
                }
                //fogbugz case 466: Saima ends here
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("GetLocationList" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Visible = true;
            }
        }
        #endregion

        #endregion

        #region Methods called on adding items from popup

        #region AddCateringItems
        protected void AddCateringItems()
        {
            try
            {
                //FB Case 853 Saima //ZD 104792
                if (hdnCatMenuID.Value != "")
                {
                    Int32 identified = 0;
                    foreach (DataGridItem dgi in dgCateringMenus.Items)
                    {
                        if (identified == 1)
                            break;
                        if (dgi.ClientID == hdnCatMenuID.Value)
                        {
                            identified = 1;  
                            String selCateringItems = ((HiddenField)dgi.FindControl("selCateringItems")).Value;
                            if (!selCateringItems.Equals(""))
                            {
                                DataTable dt = new DataTable();
                                if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
                                if (!dt.Columns.Contains("Name")) dt.Columns.Add("Name");
                                //ZD 100175
                                if (!dt.Columns.Contains("ImageName")) dt.Columns.Add("ImageName");
                                if (!dt.Columns.Contains("Image")) dt.Columns.Add("Image");
                                if (!dt.Columns.Contains("Type")) dt.Columns.Add("Type");
                                if (selCateringItems != "")
                                {
                                    for (int i = 0; i < selCateringItems.Split(',').Length - 1; i++)
                                    {
                                        if (IsUniqueItem(dt, selCateringItems.Split(',')[i].Split(':')[1])) //FB Case 872 Saima
                                        {
                                            DataRow dr = dt.NewRow();
                                            dr["ID"] = selCateringItems.Split(',')[i].Split(':')[1];
                                            dr["Name"] = selCateringItems.Split(',')[i].Split(':')[0];
                                            //ZD 100175
                                            if (selCateringItems.Split(',')[i].Split(':').Length > 2)
                                            {
                                                dr["ImageName"] = selCateringItems.Split(',')[i].Split(':')[2];
                                                dr["Image"] = selCateringItems.Split(',')[i].Split(':')[3];
                                            }
                                            else
                                            {
                                                dr["ImageName"] = "";
                                                dr["Image"] = "";
                                            }
                                            dr["Type"] = "2";
                                            dt.Rows.Add(dr);
                                        }
                                    }
                                }
                                
                                ((DataGrid)dgi.FindControl("dgMenuItems")).DataSource = dt;
                                ((DataGrid)dgi.FindControl("dgMenuItems")).DataBind();
                            }
                        }
                    }
                    hdnCatMenuID.Value = "";
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region IsUniqueItem
        //FB Case 872 Saima starts here
        protected bool IsUniqueItem(DataTable dtMenuItems, String itemID)
        {
            try
            {
                Boolean flag = true;
                foreach (DataRow dr in dtMenuItems.Rows)
                {
                    if (dr["ID"].ToString().Equals(itemID))
                    { flag = false; break; }
                }
                return flag;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        //FB Case 872 Saima ends here
        #endregion

        #region AddItemsInGrid
        protected void AddItemsInGrid()
        {
            try
            {
                DataTable dtItems = new DataTable();
                DataTable dtCharges = new DataTable();
                DataTable dtCharge = new DataTable();
                //DataRowCollection myRows;
                //save the price - case#275 FogBugz////////////////
                System.Collections.ArrayList arrGrid = new System.Collections.ArrayList();

                //FB 2559 Start
                System.Collections.ArrayList NameArray = new System.Collections.ArrayList();
                System.Collections.ArrayList countArray = new System.Collections.ArrayList();
                System.Collections.ArrayList snoArray = new System.Collections.ArrayList();
                System.Collections.ArrayList CommentsArray = new System.Collections.ArrayList();
                System.Collections.ArrayList DescArray = new System.Collections.ArrayList();
                System.Collections.ArrayList DelCostArray = new System.Collections.ArrayList();
                System.Collections.ArrayList ServiceChrArray = new System.Collections.ArrayList();
                System.Collections.ArrayList ChkPortableArray = new System.Collections.ArrayList();
                System.Collections.ArrayList RecDeleteArray = new System.Collections.ArrayList();
                //FB 2559 End

                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    if (dgi.FindControl("txtPrice") != null)
                        arrGrid.Add(((TextBox)dgi.FindControl("txtPrice")).Text);
                    else
                        arrGrid.Add("0.00"); // FB 1686
                    // FB 2559 Start
                    if (dgi.FindControl("txtQuantity") != null)
                        countArray.Add(((TextBox)dgi.FindControl("txtQuantity")).Text);
                    else
                        countArray.Add("1");

                    if (dgi.FindControl("txtName") != null)
                        NameArray.Add(((TextBox)dgi.FindControl("txtName")).Text);

                    if (dgi.FindControl("txtSno") != null)
                        snoArray.Add(((TextBox)dgi.FindControl("txtSno")).Text);

                    if (dgi.FindControl("txtComments") != null)
                        CommentsArray.Add(((TextBox)dgi.FindControl("txtComments")).Text);

                    if (dgi.FindControl("txtDescription") != null)
                        DescArray.Add(((TextBox)dgi.FindControl("txtDescription")).Text);

                    if (dgi.FindControl("chkPortable") != null)
                        ChkPortableArray.Add((((CheckBox)dgi.FindControl("chkPortable")).Checked) ? "1" : "0");

                    if (dgi.FindControl("chkDelete") != null)
                        RecDeleteArray.Add((((CheckBox)dgi.FindControl("chkDelete")).Checked) ? "1" : "0");


                    DataGrid dgTemp = (DataGrid)dgi.FindControl("dgDeliveryType");
                    foreach (DataGridItem dgiDT in dgTemp.Items)
                    {
                        if (((TextBox)dgiDT.FindControl("txtDeliveryCost")).Text != null)
                            DelCostArray.Add(((TextBox)dgiDT.FindControl("txtDeliveryCost")).Text);

                        if (((TextBox)dgiDT.FindControl("txtServiceCharge")).Text != null)
                            ServiceChrArray.Add(((TextBox)dgiDT.FindControl("txtServiceCharge")).Text);
                    }

                    // FB 2559 End

                }
                //////////////////////////////////
                char[] splitter = { ':', ',' };
                int len = selItems.Value.Split(',').Length;
                if (itemsGrid.Items.Count > 0)
                {
                    ds = (DataSet)Session["ds"];
                    dtItems = ds.Tables[0];
                    //Response.Write(dtItems.Columns.Contains("Item_Id"));
                    if (!dtItems.Columns.Contains("Type")) dtItems.Columns.Add("Type");
                    if (!dtItems.Columns.Contains("Item_Id")) dtItems.Columns.Add("Item_Id");
                    if (!dtItems.Columns.Contains("ImageId")) dtItems.Columns.Add("ImageId"); //Image Project
                    if (!dtItems.Columns.Contains("ImageName")) dtItems.Columns.Add("ImageName"); //Image Project

                    //Response.Write(dtItems.Columns.Contains("Item_Id"));
                    foreach (DataRow dr in dtItems.Rows)
                        dr["Type"] = txtType.Text;
                    if (ds.Tables.Count > 1)
                    {
                        dtCharges = ds.Tables[1];
                        dtCharge = ds.Tables[2];
                    }
                    //Response.Write(dtItems.Rows.Count);
                }
                else
                {
                    dtItems.Columns.Add("ID");
                    dtItems.Columns.Add("Type");
                    dtItems.Columns.Add("Name");
                    dtItems.Columns.Add("Quantity");
                    dtItems.Columns.Add("Price");
                    dtItems.Columns.Add("SerialNumber");
                    dtItems.Columns.Add("Portable");
                    dtItems.Columns.Add("Comments");
                    dtItems.Columns.Add("Image");
                    dtItems.Columns.Add("Description");
                    dtItems.Columns.Add("Item_Id");
                    dtItems.Columns.Add("ImageId"); //Image Project
                    dtItems.Columns.Add("ImageName"); //Image Project

                    dtCharges.Columns.Add("Charges_Id");
                    dtCharges.Columns.Add("Item_Id");

                    dtCharge.Columns.Add("Charges_Id");
                    dtCharge.Columns.Add("DeliveryTypeID");
                    dtCharge.Columns.Add("DeliveryName");
                    dtCharge.Columns.Add("DeliveryCost");
                    dtCharge.Columns.Add("ServiceCharge");
                    ds.Tables.Add(dtItems);
                    ds.Tables.Add(dtCharges);
                    ds.Tables.Add(dtCharge);
                    ds.Tables[0].TableName = "Items"; //Item_Id
                    if (ds.Tables.Count > 1)
                    {
                        ds.Tables[1].TableName = "Charges"; // Item_Id, Charges_Id
                        ds.Tables[2].TableName = "Charge"; // Charges_Id 
                    }
                    DataRelation drP = new DataRelation("ItemChargeP", dtItems.Columns["Item_Id"], dtCharges.Columns["Item_Id"], false);
                    ds.Relations.Add(drP);
                    DataRelation drC = new DataRelation("ItemChargeC", dtCharges.Columns["Charges_Id"], dtCharge.Columns["Charges_Id"], false);
                    ds.Relations.Add(drC);
                }
                for (int i = 0; i < len - 1; i++)
                {
                    string temp = selItems.Value.Split(',')[i];
                    DataRow dr = dtItems.NewRow();
                    DataRow drCharges = dtCharges.NewRow();
                    DataRow drCharge;

                    //id + ":" + iname + ":" + name + ":" + iamge + ",";
                    
                    string imgid = temp.Split(':')[0]; //Image Project
                    string gItemImgName = temp.Split(':')[1];
                    string gItemName = temp.Split(':')[2];
                    string gItemImage = temp.Split(':')[3];
                                        
                    dr["ID"] = "new";
                    dr["Type"] = txtType.Text;
                    dr["Name"] = gItemName;
                    dr["Quantity"] = "1";
                    dr["Price"] = "0.00"; //FB 1686
                    //FB 1830
                    tmpVal = 0;
                    decimal.TryParse(dr["Price"].ToString(), out tmpVal);
                    dr["Price"] = tmpVal.ToString("g", cInfo);
                    
                    dr["SerialNumber"] = "";
                    dr["Portable"] = "0";
                    dr["Comments"] = "";
                    dr["Description"] = "";
                    dr["Image"] = gItemImage;
                    dr["Item_Id"] = itemsGrid.Items.Count + (i);
                    dr["ImageId"] = imgid; //Image Project
                    dr["ImageName"] = gItemImgName; //Image Project

                    dtItems.Rows.Add(dr);
                    //Response.Write(dr["Item_Id"]);
                    if (txtType.Text.Equals("1"))
                    {
                        drCharges["Item_Id"] = itemsGrid.Items.Count + (i); //3
                        drCharges["Charges_Id"] = itemsGrid.Items.Count + (i);
                        dtCharges.Rows.Add(drCharges); //, Int32.Parse(dr["Item_Id"].ToString()));
                        //foreach (DataRow dc in dtCharges.Rows)
                        foreach (ListItem li in lstDeliveryTypes.Items)
                        {
                            if (Int32.Parse(li.Value) >= 1)
                            {
                                drCharge = dtCharge.NewRow();
                                drCharge["Charges_Id"] = itemsGrid.Items.Count + (i);
                                drCharge["DeliveryTypeID"] = li.Value;
                                drCharge["DeliveryName"] = li.Text;
                                drCharge["DeliveryCost"] = "0.00";  //FB 1686
                                drCharge["ServiceCharge"] = "0.00";
                                dtCharge.Rows.Add(drCharge);
                            }
                        }
                    }
                }
                Session["DS"] = ds;
                itemsGrid.DataSource = ds;
                itemsGrid.DataBind();
                if (!txtType.Text.Equals("1"))
                    HideColumns();
                selItems.Value = "";
                //save the price - case#275 FogBugz////////////////
                int valCnt = 0;
                // FB 2559 Start 
                int tempAmountCnt = 0;
                int tempRecCount = 0;
                // FB 2559 End
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    if (null != dgi.FindControl("txtPrice"))
                    {
                        if ((arrGrid.Count) > valCnt)
                        {
                            //FB 1830                            
                            String tempString = arrGrid[valCnt].ToString();
                            if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                                tempString = tempString.Replace(",", ".");
                            tmpVal = decimal.Parse(Double.Parse(tempString).ToString("0.00"));
                            ((TextBox)dgi.FindControl("txtPrice")).Text = tmpVal.ToString("g", cInfo);  

                            //((TextBox)dgi.FindControl("txtPrice")).Text = arrGrid[valCnt].ToString();
                                //Decimal.Parse(arrGrid[valCnt].ToString()).ToString("0.00"); //FB 1686
                        }
                        //((TextBox)dgi.FindControl("txtPrice")).Attributes.Add("onblur", "javascript:CallMe('" + ((TextBox)dgi.FindControl("txtPrice")).ClientID + "')");

                    }
                    else
                    {
                        ((TextBox)dgi.FindControl("txtPrice")).Text = (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound) ? "0,00" : "0.00"; //FB 1686//FB 1830
                    }

                    // FB 2559 Start                    
                    if ((countArray.Count) > valCnt)
                    {
                        if (dgi.FindControl("txtName") != null)
                            ((TextBox)dgi.FindControl("txtName")).Text = NameArray[valCnt].ToString();

                        if (dgi.FindControl("txtQuantity") != null)
                            ((TextBox)dgi.FindControl("txtQuantity")).Text = countArray[valCnt].ToString();

                        if (dgi.FindControl("txtSno") != null)
                            ((TextBox)dgi.FindControl("txtSno")).Text = snoArray[valCnt].ToString();

                        if (dgi.FindControl("txtComments") != null)
                            ((TextBox)dgi.FindControl("txtComments")).Text = CommentsArray[valCnt].ToString();

                        if (dgi.FindControl("txtDescription") != null)
                            ((TextBox)dgi.FindControl("txtDescription")).Text = DescArray[valCnt].ToString();

                        if (ChkPortableArray[valCnt].ToString() == "1")
                            ((CheckBox)dgi.FindControl("chkPortable")).Checked = true;

                        if (RecDeleteArray[valCnt].ToString() == "1")
                            ((CheckBox)dgi.FindControl("chkDelete")).Checked = true;

                        if (tempAmountCnt <= 0)
                            tempRecCount = 0;
                        else
                            tempRecCount = tempAmountCnt;
                        DataGrid dgTemp = (DataGrid)dgi.FindControl("dgDeliveryType");
                        foreach (DataGridItem dgiDT in dgTemp.Items)
                        {
                            if (dgiDT.FindControl("txtDeliveryCost") != null)
                                ((TextBox)dgiDT.FindControl("txtDeliveryCost")).Text = DelCostArray[tempRecCount].ToString();

                            if (dgiDT.FindControl("txtServiceCharge") != null)
                                ((TextBox)dgiDT.FindControl("txtServiceCharge")).Text = ServiceChrArray[tempRecCount].ToString();

                            tempRecCount++;
                            tempAmountCnt = tempRecCount;
                        }
                    }
                    else
                        ((TextBox)dgi.FindControl("txtQuantity")).Text = "1";
                    // FB 2559 End

                    valCnt++;
                }
                Session["DS"] = (DataSet)itemsGrid.DataSource;
                ///////////////////////////////
                btnSubmit.Disabled = false;//ZD 100420
                btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");//FB 2664
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("AddItemsInGrid" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                //errLabel.Text = ex.Message;
            }
        }
        #endregion

        #region BindItemImage - OnItemDatabound
        /// <summary>
        /// BindItemImage - OnItemDatabound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindItemImage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    string imCont = "";
                    string pathName = "resource";
                    string finame = "";
                    string strtype = "";
                    string fullPath = "";

                    if (row["ImageName"] != null)
                        finame = row["ImageName"].ToString().Trim();

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    if (row["Type"] != null)
                        strtype = row["Type"].ToString().Trim();

                    if (strtype == "2")
                        pathName = "food";

                    if (strtype =="3")
                        pathName = "Housekeeping";
                    //ZD 101022
                    fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + "/image/" + pathName + "/" + finame;//FB 1830
                    //fullPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 3) + "/image/" + pathName + "/" + finame;//FB 1830
                     
                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);

                        //ZD 104792 - Start
                        //if (File.Exists(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame))
                        //    File.Delete(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame);

                        //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\" + pathName + "\\" + finame, ref imgArray);

                        Image imgContrl = (Image)e.Item.FindControl("imgItem");
                        //imgContrl.ImageUrl = fullPath;


                        string base64String = Convert.ToBase64String(imgArray, 0, imgArray.Length);
                        imgContrl.ImageUrl = "data:image/png;base64," + base64String;
                        //ZD 104792 - End


                        //myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("imgItem");

                        //MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                        //ms1.Write(imgArray, 0, imgArray.Length);

                        //if (fileExtn.ToLower() == "gif")
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        //else if ((fileExtn.ToLower() == "jpg") || (fileExtn.ToLower() == "jpeg"))
                        //    imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        //imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                        ////imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms1);

                        //imCont = null;
                        //imgCtrl.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindItemImage" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("WriteToFile" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }
        #endregion

        #endregion

        #region Methods execute on Submit Click

        //////// Bug Fix Case # 274 ///////////////////////////
        private bool ValidateInventoryItems()
        {
            //WO Bug Fix
            Boolean isValid = false;
            try
            {
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    log.Trace(((CheckBox)dgi.FindControl("chkDelete")).Checked.ToString());
                    if (!((CheckBox)dgi.FindControl("chkDelete")).Checked)
                        isValid = true;

                    if (txtType.Text == "1")
                    {
                        if (((TextBox)dgi.FindControl("txtQuantity")).Text == "")
                        {
                            errLabel.Text = obj.GetTranslatedText("Please enter a quantity for each selected item. Each item should have a quantity > 0");//FB 1830 - Translation
                            isValid = false;
                            break;
                        }
                        //Onsite dont want the 0 value validations
                        //else if (Convert.ToInt32(((TextBox)dgi.FindControl("txtQuantity")).Text) <= 0)
                        //{
                        //    errLabel.Text = "Please enter a quantity for each selected item. Each item should have a quantity > 0";
                        //    isValid = false;
                        //    break;
                        //}
                    }
                }

                return isValid;
            }
            catch (Exception ex)
            {
                log.Trace("ValidateInventoryItems: " + ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
        //////// Bug Fix Case # 274 ///////////////////////////
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                String outXML = "";
                //Code added by Vivek as a fix for issue number 308
                //First of check if Items are duplicated or not if not then
                //Display error to user and exit
                //Response.Write("About to check for duplicate");

                if (validateInventoryItemsData() == false)
                {
                    errLabel.Visible = true;

                    //return;
                }
                else
                {
                    if (txtType.Text != "2")
                        if (ValidateInventoryItems())
                        {
                            //ZD 100583 Starts
                            string roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                            string crossRoomxml;
                            if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString() != "0")
                            {
                                if (Session["multisiloOrganizationID"].ToString() != "11")
                                {
                                    crossRoomxml = "Organizations/Org_" + Session["multisiloOrganizationID"].ToString() + "/Rooms/Room.xml";
                                    roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + crossRoomxml;
                                }
                            }
                            if (File.Exists(roomxmlPath))
                                 File.Delete(roomxmlPath);
                            //ZD 100583 Ends


                            String inXML = GenerateINXML();
                            //Response.Write(obj.Transfer(inXML));
                            //Response.End();
                            outXML = obj.CallMyVRMServer("SetInventoryDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            if (outXML.IndexOf("<error>") >= 0)
                            {
                                errLabel.Visible = true;
                                errLabel.Text = obj.ShowErrorMessage(outXML);
                            }
                            else
                            {
                                errLabel.Visible = true;
                                selItems.Value = "";
                                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                                Response.Redirect("InventoryManagement.aspx?m=1&t=" + txtType.Text,false);//ZD 104792
                            }
                        }
                        else
                        {
                            //WO Bug Fix
                            if (errLabel.Text == "")
                                errLabel.Text = obj.GetTranslatedText ("At least one Inventory item should NOT be marked for deletion.");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                    else
                    {
                        log.Trace("in else for Submit");
                        String inXML = GenerateCateringInXML();
                        String errMsg = "";
                        errMsg = ValidateXML(inXML);

                        if (errMsg.Equals(""))
                        {
                            outXML = obj.CallMyVRMServer("SetProviderDetails", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                            if (outXML.IndexOf("<error>") >= 0)
                            {
                                errLabel.Visible = true;
                                errLabel.Text = obj.ShowErrorMessage(outXML);
                            }
                            else
                            {
                                errLabel.Visible = true;
                                selItems.Value = "";
                                errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                                Cache.Remove("RoomsXML" + Session["organizationID"]);//Code added for room search
                                Response.Redirect("InventoryManagement.aspx?m=1&t=" + txtType.Text, false);
                            }
                        }
                        else
                        {
                            errLabel.Text = errMsg;
                            errLabel.Visible = true;
                        }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected String GenerateCateringInXML()
        {
            try
            {
                String inXML = "";
                inXML += "<SetProviderDetails>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <ID>" + AVInventoryID.Value + "</ID>";
                inXML += "  <Name>" + txtInvName.Text + "</Name>";
                inXML += "  <Type>2</Type>";
                inXML += "  <AdminID>" + hdnApprover1.Text + "</AdminID>";
                if (chkNotify.Checked)
                    inXML += "  <Notify>1</Notify>";
                else
                    inXML += "  <Notify>0</Notify>";
                inXML += "  <SelectedLocations>";

                if (selectedloc.Value != "")
                {
                    String[] roomslst = selectedloc.Value.Split(',');

                    foreach (String s in roomslst)
                        inXML += "  <Selected><ID>" + s + "</ID></Selected>";

                    roomslst = null;

                }


                /* **** FB 1465 rooms not assigning to inventory sets - start  Commented for room search
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                        if (lstItem.Selected == true)
                        {
                            inXML += "<Selected>";
                            inXML += "<ID>" + lstItem.Value + "</ID>";
                            inXML += "</Selected>";
                        }
                }
                else
                {
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        if (tn.Depth.Equals(3))
                        {
                            inXML += "<Selected>";
                            inXML += "  <ID>" + tn.Value + "</ID>";
                            inXML += "</Selected>";
                        }
                }
               FB 1465 rooms not assigning to inventory sets - end *** */

                
                inXML += "  </SelectedLocations>";
                inXML += "  <MenuList>";
                foreach (DataGridItem dgMenu in dgCateringMenus.Items)
                {
                    {
                        inXML += "    <Menu>";
                        inXML += "      <ID>" + dgMenu.Cells[0].Text + "</ID>";
                        //if (!((CheckBox)dgMenu.FindControl("chkMenuDelete")).Checked)
                        inXML += "  <Deleted>0</Deleted>";
                        //else
                        //    inXML += "  <Deleted>1</Deleted>";
                        inXML += "      <Name>" + ((TextBox)dgMenu.FindControl("txtMenuName")).Text + "</Name>";
                        
                        //FB 1830 - Starts                    
                        String tempString = ((TextBox)dgMenu.FindControl("txtPrice")).Text;
                        if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {   
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                        inXML += "      <Price>" + tempString + "</Price>";
                        //FB 1830 - End                    

                        inXML += "      <Comments>" + ((TextBox)dgMenu.FindControl("txtComments")).Text + "</Comments>";
                        inXML += "      <CateringServices>";
                        foreach (ListItem chkService in ((CheckBoxList)dgMenu.FindControl("chkLstServices")).Items)
                            if (chkService.Selected)
                            {
                                inXML += "        <Service>";
                                inXML += "          <ID>" + chkService.Value + "</ID>";
                                inXML += "        </Service>";
                            }
                        inXML += "      </CateringServices>";
                        inXML += "      <ItemsList>";
                        foreach (DataGridItem dgItem in ((DataGrid)dgMenu.FindControl("dgMenuItems")).Items)
                        {
                            log.Trace(((CheckBox)dgItem.FindControl("chkItemDelete")).ClientID + " : " + ((CheckBox)dgItem.FindControl("chkItemDelete")).Checked.ToString());
                            if (!((CheckBox)dgItem.FindControl("chkItemDelete")).Checked)
                            {
                                inXML += "        <Item>";
                                inXML += "          <ID>" + dgItem.Cells[0].Text + "</ID>";
                                inXML += "        </Item>";
                            }
                        }
                        inXML += "      </ItemsList>";
                        inXML += "    </Menu>";
                    }
                }
                inXML += "  </MenuList>";
                inXML += "</SetProviderDetails>";
                log.Trace("SetProviderDetails" + inXML);
                return inXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        protected String GenerateINXML()
        {
            //FB 1830 
            String tempString = "";
            try
            {
                //FB 2181 - Start
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<Inventory>");
                inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + Session["userID"] + "</userID>");
                inXML.Append("<ID>" + AVInventoryID.Value + "</ID>");
                inXML.Append("<Type>" + txtType.Text + "</Type>");
                inXML.Append("<Name>" + txtInvName.Text + "</Name>");
                inXML.Append("<Comments>" + txtInvComments.Text + "</Comments>");
                inXML.Append("<Admin><ID>" + hdnApprover1.Text + "</ID></Admin>");
                if (chkNotify.Checked.Equals(true))
                    inXML.Append("<Notify>1</Notify>");
                else
                    inXML.Append("<Notify>0</Notify>");
                inXML.Append("<AssignedCostCtr>1</AssignedCostCtr>");
                inXML.Append("<AllowOverBooking>1</AllowOverBooking>");
                inXML.Append("<Deleted>0</Deleted>");
                inXML.Append("<ItemList>");
                foreach (DataGridItem dgi in itemsGrid.Items)
                {
                    inXML.Append("<Item>");
                    inXML.Append("<ID>" + ((Label)dgi.FindControl("lblItemID")).Text + "</ID>");
                    inXML.Append("<Name>" + ((TextBox)dgi.FindControl("txtName")).Text + "</Name>");
                    if (txtType.Text.Equals("1"))
                        inXML.Append("<Quantity>" + ((TextBox)dgi.FindControl("txtQuantity")).Text + "</Quantity>");
                    else
                        inXML.Append("<Quantity>0</Quantity>");
                    //FB 1830 - Starts                    
                    tempString = ((TextBox)dgi.FindControl("txtPrice")).Text;
                    if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                    {
                        decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                        tempString = tmpVal.ToString();
                    }

                    inXML.Append("<Price>" + tempString + "</Price>");

                    inXML.Append("<SerialNumber>" + ((TextBox)dgi.FindControl("txtSno")).Text + "</SerialNumber>");
                    
                    /* Image Project code starts here */

                    inXML.Append("<ImageName>" + ((Label)dgi.FindControl("LblImageName")).Text + "</ImageName>");
                    inXML.Append("<ImageId>" + ((Label)dgi.FindControl("LblImageId")).Text + "</ImageId>");

                    /* Image Project code ends here */

                    if (!txtType.Text.Equals("1"))
                        inXML.Append("<Portable>1</Portable>");
                    else
                        if (((CheckBox)dgi.FindControl("chkPortable")).Checked)
                            inXML.Append("<Portable>1</Portable>");
                        else
                            inXML.Append("<Portable>0</Portable>");
                    if (((CheckBox)dgi.FindControl("chkDelete")).Checked)
                        inXML.Append("<Deleted>1</Deleted>");
                    else
                        inXML.Append("<Deleted>0</Deleted>");

                    inXML.Append("<Comments>" + ((TextBox)dgi.FindControl("txtComments")).Text + "</Comments>");
                    inXML.Append("<Description>" + ((TextBox)dgi.FindControl("txtDescription")).Text + "</Description>");
                    inXML.Append("<Charges>");
                    DataGrid dgTemp = (DataGrid)dgi.FindControl("dgDeliveryType");
                    foreach (DataGridItem dgiDT in dgTemp.Items)
                    {
                        inXML.Append("<Charge>");
                        inXML.Append("<DeliveryTypeID>" + dgiDT.Cells[0].Text + "</DeliveryTypeID>");
                        //FB 1830 - Starts                          
                        tempString = ((TextBox)dgiDT.FindControl("txtDeliveryCost")).Text;
                        if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                        inXML.Append("<DeliveryCost>" + tempString + "</DeliveryCost>");

                        tempString = ((TextBox)dgiDT.FindControl("txtServiceCharge")).Text;
                        if (currencyFormat == ns_MyVRMNet.vrmCurrencyFormat.bound)
                        {
                            decimal.TryParse(tempString, NumberStyles.Any, cInfo, out tmpVal);
                            tempString = tmpVal.ToString();
                        }
                        inXML.Append("<ServiceCharge>" + tempString + "</ServiceCharge>");

                        inXML.Append("</Charge>");
                    }
                    inXML.Append("</Charges>");
                    inXML.Append("</Item>");
                }
                inXML.Append("</ItemList>");
                inXML.Append("<locationList>");
                inXML.Append("<selected>");
                //if (Request.QueryString["id"].Equals("new"))    // WO Bug Fixing
                    //inXML += "          <level1ID>11</level1ID>";
                //Room Search
                if (selectedloc.Value != "")
                {
                    String[] roomslst = selectedloc.Value.Split(',');

                    foreach (String s in roomslst)
                        inXML.Append("<level1ID>" + s + " </level1ID>");

                    roomslst = null;

                }


                /* **** FB 1465 rooms not assigning to inventory sets - start 
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                        if (lstItem.Selected == true)
                        {
                            inXML += "<level1ID>" + lstItem.Value + "</level1ID>";
                        }
                }
                else
                {
                    foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                        if (tn.Depth.Equals(3))
                            inXML += "<level1ID>" + tn.Value + "</level1ID>";
                }
                 FB 1465 rooms not assigning to inventory sets - end *** */
                
                inXML.Append("</selected>");
                inXML.Append("</locationList>");
                inXML.Append("</Inventory>");
                return inXML.ToString();
                //FB 2181 - End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        #endregion

        #region Events related to Room Tree Change View List/Level
        protected void rdSelView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                selRooms = "";
                if (rdSelView.SelectedIndex.Equals(1))
                {
                    pnlListView.Visible = true;
                    pnlLevelView.Visible = false;
                    lstRoomSelection.ClearSelection();
                    foreach (ListItem lstItem in lstRoomSelection.Items)
                    {
                        foreach (TreeNode tn in treeRoomSelection.CheckedNodes)
                            if (tn.Value.Equals(lstItem.Value) && (tn.Depth.Equals(3)))
                            {
                                lstItem.Selected = true;
                                selRooms += lstItem.Value + ",";
                            }
                    }
                }
                else
                {
                    pnlLevelView.Visible = true;
                    pnlListView.Visible = false;

                    foreach (TreeNode tnTop in treeRoomSelection.Nodes[0].ChildNodes)
                        foreach (TreeNode tnMid in tnTop.ChildNodes)
                            foreach (TreeNode tn in tnMid.ChildNodes)
                            {
                                tn.Checked = false;
                                foreach (ListItem lstItem in lstRoomSelection.Items)
                                    if ((lstItem.Selected == true) && (tn.Value.Equals(lstItem.Value)))
                                    {
                                        tn.Checked = true;
                                        selRooms += tn.Value + ",";
                                    }
                            }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
                errLabel.Visible = true;
                errLabel.Text = "Error while changing room display view. Please contact your VRM Administrator.";//FB 1830D - 1881D
            }
        }
        protected void LoadDeliveryTypes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                //if (txtType.Text.Equals("1"))
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))// || (e.Item.ItemType.Equals(ListItemType.Footer)))
                {
                    obj.GetDeliveryTypes((DropDownList)e.Item.FindControl("lstDeliveryType"));
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //ZD 100263
                log.Trace("LoadDeliveryTypes" + ex.StackTrace + " : " + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
            }
        }

        protected void LoadPrerequisite(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                CheckBoxList chkLstTemp = (CheckBoxList)e.Item.FindControl("chkLstServices");
                if (chkLstTemp != null)
                    obj.GetCateringServices(chkLstTemp);
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnMenuDelete");
                    btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Menu?") + "')) {DataLoading(1); return true;} else {DataLoading('0'); return false;}");//FB japnese
                    btnTemp.Visible = (!e.Item.Cells[0].Text.Trim().ToLower().Equals("new"));
                }

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected String ValidateXML(String inXML)
        {
            try
            {
                String errMsg = "";
                XmlDocument xmldoc = new XmlDocument();
                try
                {
                    xmldoc.LoadXml(inXML);
                }
                catch (Exception ex)
                {
                    log.Trace(ex.StackTrace);
                    return "Invalid InXML";
                }

                XmlNodeList nodes = xmldoc.SelectNodes("//SetProviderDetails/SelectedLocations/Selected");
                if (nodes.Count.Equals(0))
                {
                    errMsg += obj.GetTranslatedText("Please associate at least one location with this Provider.");
                }
                int cntMenus = 0;
                nodes = xmldoc.SelectNodes("//SetProviderDetails/MenuList/Menu");
                foreach (XmlNode node in nodes)
                    cntMenus++;
                log.Trace("cntMenus: " + cntMenus);
                if (cntMenus.Equals(0))
                {
                    errMsg += "<br>"+obj.GetTranslatedText("Please associate at least one Menu with this Provider.");
                }
                else
                {
                    foreach (XmlNode node in nodes)
                        if (node.SelectNodes("ItemsList/Item").Count.Equals(0))
                        {
                            errMsg += "<br>"+obj.GetTranslatedText("Please associate at least one Menu Item with all Menus.");
                            break;
                        }
                    foreach (XmlNode node in nodes)
                        if (node.SelectNodes("CateringServices/Service").Count.Equals(0))
                        {
                            errMsg += "<br>"+obj.GetTranslatedText("Please associate at least one Catering Service with all Menus.");
                            break;
                        }
                    String errMsg1 = "";
                    foreach (XmlNode node in nodes)
                    {
                        foreach (XmlNode node1 in nodes)
                            if (!node1.Equals(node) && node1.SelectSingleNode("Name").InnerText.Trim().Equals(node.SelectSingleNode("Name").InnerText.Trim()))
                            {
                                errMsg1 = obj.GetTranslatedText("Please enter unique menu names for all menus.");
                                break;
                            }
                    }
                    if (!errMsg1.Equals(""))
                        errMsg += "<br>" + errMsg1;
                }
                log.Trace(errMsg);
                return errMsg;
            }
            catch (Exception ex)
            {
                log.Trace("ValidateXML:error in validating input" + ex.Message);//FB 1881
                //return "error in validating input";
                return obj.ShowSystemMessage();//FB 1881
            }
        }
        protected void DeleteMenu(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String menuID = e.Item.Cells[0].Text.Trim().ToLower();
                if (!menuID.Equals("new"))
                {
                    String inXML = "";
                    inXML += "<DeleteProviderMenu>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += "  <ProviderID>" + AVInventoryID.Value + "</ProviderID>";
                    inXML += "  <MenuID>" + menuID + "</MenuID>";
                    inXML += "</DeleteProviderMenu>";
                    log.Trace("DeleteMenuProvider inxml: " + inXML);
                    String outXML = obj.CallMyVRMServer("DeleteProviderMenu", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    log.Trace("DeleteMenuProvider outxml: " + outXML);
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Visible = true;
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    }
                    else
                    {
                        //errLabel.Visible = true;
                        //errLabel.Text = "Operation Successful";
                        //foreach (DataGridItem dgi in dgCateringMenus.Items)
                        //{

                        //}
                        Response.Redirect("EditInventory.aspx?id=" + AVInventoryID.Value + "&t=2&m=1", false);//ZD 104792
                    }
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        protected void CancelMenu(Object sender, EventArgs e)
        {
            try
            {
            }
            catch (System.Exception ex)
            { }
            finally
            {
                Response.Redirect("InventoryManagement.aspx?t=" + txtType.Text, true);
            } 
        }
        //protected DataTable CreateMenuDataTable()
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        if (!dt.Columns.Contains("ID")) dt.Columns.Add("ID");
        //        if (!dt.Columns.Contains("Name")) dt.Columns.Add("Name");
        //        if (!dt.Columns.Contains("RoomId")) dt.Columns.Add("RoomId");
        //        if (!dt.Columns.Contains("SelectedService")) dt.Columns.Add("SelectedService");
        //        if (!dt.Columns.Contains("ServiceName")) dt.Columns.Add("ServiceName");
        //        if (!dt.Columns.Contains("RoomName")) dt.Columns.Add("RoomName");
        //        if (!dt.Columns.Contains("strMenus")) dt.Columns.Add("strMenus");
        //        if (!dt.Columns.Contains("Price")) dt.Columns.Add("Price");
        //        if (!dt.Columns.Contains("Comments")) dt.Columns.Add("Comments");
        //        if (!dt.Columns.Contains("DeliverByDate")) dt.Columns.Add("DeliverByDate");
        //        if (!dt.Columns.Contains("DeliverByTime")) dt.Columns.Add("DeliverByTime");
        //        return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Trace(ex.StackTrace + " : " + ex.Message);
        //        return null;
        //    }
        //}
        #endregion

        #region Bind room to list

        public void BindRoomToList()
        {
            String[] locsName = null;
            try
            {
                if (locstrname.Value != "")
                {

                    locsName = locstrname.Value.Split('+');
                    RoomList.Items.Clear();  
                    foreach (String s in locsName)
                    {
                        if (s != "")
                        {

                            if (s.Split('|').Length > 1)
                                RoomList.Items.Add(new ListItem(s.Split('|')[1], s.Split('|')[0]));
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        #endregion
        
    }
}