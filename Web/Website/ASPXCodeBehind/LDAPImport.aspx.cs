/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_LDAPImport
{
    public partial class LDAPImport : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        
        protected System.Web.UI.WebControls.Label lblType;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Label lblErrorInImport;
        protected System.Web.UI.WebControls.TextBox txtType;
        protected System.Web.UI.WebControls.TextBox txtSearchUsers;
        protected System.Web.UI.WebControls.TextBox txtLDAPUsers;
        protected System.Web.UI.WebControls.Label lblCount;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.WebControls.Table tblAlphabet;
        protected System.Web.UI.WebControls.Table tblNoLDAPUsers;
        protected System.Web.UI.WebControls.DataGrid dgLDAPUsers;
        protected System.Web.UI.WebControls.Button btnSubmitUsers;
        protected System.Web.UI.WebControls.DropDownList lstLDAPTemplates;
        protected System.Web.UI.WebControls.DropDownList lstSortBy;
        protected System.Web.UI.WebControls.DropDownList lstDelimiter;
        ns_Logger.Logger log;//FB 1881
        
        public LDAPImport()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();//FB 1881
            //
            // TODO: Add constructor logic here
            //
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("LDAPImport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Text = "";

                //dgLDAPUsers.Attributes.Add("onclick", "javascript:checkClicks()");
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Visible = true;
                    }
                if (Session["errMsg"] != null)
                {
                    errLabel.Visible = true;
                    errLabel.Text = Session["errMsg"].ToString();
                    Session["errMsg"] = null;
                }
                if (!IsPostBack)
                    BindData();
            }
            catch (Exception ex)
            {
                //                Response.Write(ex.Message);
                //ZD 100263
                log.Trace("Page_Load" + ex.Message);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }

        }

        private void BindData()
        {
            try
            {
                //btnSubmitUsers.Enabled = false;
                string sortBy = "2";
                string alphabet = "a";
                string currentPageNo = "1";
                if (Request.QueryString["sortBy"] != null)
                    sortBy = Request.QueryString["sortBy"].ToString();
                if (Request.QueryString["alphabet"] != null)
                    alphabet = Request.QueryString["alphabet"].ToString();
                if (Request.QueryString["pageNo"] != null)
                    currentPageNo = Request.QueryString["pageNo"].ToString();

                //                Response.Write(sortBy);
                lstSortBy.ClearSelection();
                lstSortBy.Items.FindByValue(sortBy).Selected = true;
                string inXML = "";
                DisplayAlphabets(tblAlphabet);
                if (txtSearchUsers.Text != "")
                    currentPageNo = "1";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <searchFor>" + txtSearchUsers.Text + "</searchFor>";
                inXML += "  <sortBy>" + sortBy + "</sortBy>";
                inXML += "  <alphabet>" + alphabet + "</alphabet>";
                inXML += "  <pageNo>" + currentPageNo + "</pageNo>";
                inXML += "</login>";
                string outXML = obj.CallMyVRMServer("GetMultipleUsers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //                Response.Write(obj.Transfer(inXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//getMultipleUsers/users/user");
                    if (nodes.Count > 0)
                    {
                        btnSubmitUsers.Enabled = true;
                        dgLDAPUsers.Visible = true;
                        tblNoLDAPUsers.Visible = false;
                        LoadLDAPUsersGrid(nodes);
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgLDAPUsers.Controls[0].Controls[dgLDAPUsers.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        lblTemp.Text += xmldoc.SelectSingleNode("//getMultipleUsers/totalNumber").InnerText;
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//getMultipleUsers/totalPages").InnerText);
                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//getMultipleUsers/pageNo").InnerText);
                        if (totalPages > 1)
                        {
                            //  Request.QueryString.Remove("pageNo");
                            string qString = Request.QueryString.ToString();
                            if (Request.QueryString["pageNo"] != null)
                                qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                            if (Request.QueryString["m"] != null)
                                qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                            obj.DisplayPaging(totalPages, pageNo, tblPage, "LDAPImport.aspx?" + qString);
                        }
                    }
                    else
                    {
                        tblNoLDAPUsers.Visible = true;
                        btnSubmitUsers.Enabled = false;
                        dgLDAPUsers.Visible = false;
                    }
                    nodes = xmldoc.SelectNodes("//getMultipleUsers/templates/template");
                    if (nodes.Count > 0)
                    {
                        btnSubmitUsers.Enabled = true;
                        LoadTemplates(nodes);
                    }
                    else
                    {
                        btnSubmitUsers.Enabled = false;
                        errLabel.Text += "<br>" + obj.GetTranslatedText("There are no user templates available. Please create at least one user template and proceed with LDAP Import");
                        errLabel.Visible = true;
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("BindData" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;                
            }
        }
        #endregion

        protected bool LoadTemplates(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    lstLDAPTemplates.DataSource = dt;
                    lstLDAPTemplates.DataBind();
                }
                else
                    return false;

                return true;

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("LoadTemplates" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return false;
            }
        }
        protected void LoadLDAPUsersGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                }
                dgLDAPUsers.DataSource = dt;
                dgLDAPUsers.DataBind();
                DataGridItem dgFooter = (DataGridItem)dgLDAPUsers.Controls[0].Controls[dgLDAPUsers.Controls[0].Controls.Count - 1];
                DataGridItem dgHeader = (DataGridItem)dgLDAPUsers.Controls[0].Controls[0];
                CheckBox cbTemp = (CheckBox)dgHeader.FindControl("chkSelectAllUSers");
                TextBox lblTemp = (TextBox)dgFooter.FindControl("lblTotalSelected");

                //                Response.Write(cbTemp.ClientID + " : " + lblTemp.ClientID);
                dgLDAPUsers.Attributes.Add("onclick", "javascript:checkClicks('" + cbTemp.ClientID + "','" + lblTemp.ClientID + "',event)");
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in Loading Users. Please contact your VRM administrator." + ex.StackTrace;
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                log.Trace("LoadLDAPUsersGrid:Error in Loading Users." + ex.Message);//FB 1881
            }

        }

        protected void DeleteLDAPUser(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string sortBy = "2";
                string alphabet = "a";
                string currentPageNo = "1";
                if (Request.QueryString["soryBy"] != null)
                    sortBy = Request.QueryString["sortBy"].ToString();
                if (Request.QueryString["alphabet"] != null)
                    alphabet = Request.QueryString["alphabet"].ToString();
                if (Request.QueryString["pageNo"] != null)
                    currentPageNo = Request.QueryString["pageNo"].ToString();
                string inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <LDAPUserID>" + e.Item.Cells[0].Text.ToString() + "</LDAPUserID>";
                inXML += "</login>";
                //Response.Write("Inxml: " + obj.Transfer(inXML));
                //Response.End();
                String outXML = obj.CallMyVRMServer("DeleteLDAPUser", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    Session.Add("errMsg", obj.ShowErrorMessage(outXML));
                    Response.Redirect("LDAPImport.aspx?" + Request.QueryString.ToString());
                }
                else
                {
                    string url = "LDAPImport.aspx?t=&m=1&pageNo=" + currentPageNo.ToString() + "&sortBy=" + sortBy.ToString() + "&alphabet=" + alphabet.ToString();
                    Response.Redirect(url);
                }

            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DeleteLDAPUser" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void SearchLDAPUsers(Object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "Error in Loading Users. Please contact your VRM administrator." + ex.StackTrace;
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                //log.Trace("SearchLDAPUsers:" + ex.Message);//FB 1881
            }
        }

        protected void SubmitLDAPUsers(Object sender, EventArgs e)
        {
            try
            {
                bool cbFlag = false;
                String inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <userTemplateID> " + lstLDAPTemplates.SelectedValue.ToString() + "</userTemplateID>";
                inXML += "  <users>";
                foreach (DataGridItem dgi in dgLDAPUsers.Items)
                {
                    CheckBox cbTemp = (CheckBox)dgi.FindControl("chkSelectThisUser");
                    if (cbTemp.Checked)
                    {
                        inXML += "<userID>" + dgi.Cells[0].Text + "</userID>";
                        cbFlag = true;
                    }
                }
                inXML += "  </users>";
                inXML += "</login>";
                if (cbFlag)
                {
                    //Response.End();
                    String outXML = obj.CallMyVRMServer("SetMultipleUsers", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                    //Response.Write(obj.Transfer(inXML));
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        Session.Add("errMsg", obj.ShowErrorMessage(outXML));
                        Response.Redirect("LDAPImport.aspx?" + Request.QueryString.ToString());
                    }
                    else
                    {
                        string qString = Request.QueryString.ToString();
                        if (Request.QueryString["m"] == null)
                            qString += "&m=1";
                        Response.Redirect("LDAPImport.aspx?" + qString);
                    }
                }
                else
                {
                    Session.Add("errMsg", obj.GetTranslatedText("Please select at least one user to import"));
                    Response.Redirect("LDAPImport.aspx?" + Request.QueryString.ToString());
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SubmitLDAPUsers" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        protected void ResetLDAPUsers(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("LDAPImport.aspx");
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ResetLDAPUsers" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void SynchronizeLDAPUsers(Object sender, EventArgs e)
        {
            try
            {
                String inxml = "<login><userID>" + Session["userID"].ToString() + "</userID></login>";
                String outxml = obj.CallCOM2("SyncLdapNow", inxml, Application["RTC_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") >= 0)
                {
                    Session.Add("errMsg", obj.ShowErrorMessage(outxml));
                    Response.Redirect("LDAPImport.aspx?" + Request.QueryString.ToString());
                }
                else
                {
                    Response.Redirect("LDAPImport.aspx?m=1");
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SynchronizeLDAPUsers" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }

        protected void SelectAllUsers(Object sender, EventArgs e)
        {
            try
            {
                CheckBox cbTop = (CheckBox)sender;
                //Response.Write("in function");
                foreach (DataGridItem dgi in dgLDAPUsers.Items)
                {
                    CheckBox cbTemp = (CheckBox)dgi.FindControl("chkSelectThisUser");
                    cbTemp.Checked = cbTop.Checked;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("SelectAllUsers" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void ChangeAlphabets(Object sender, EventArgs e)
        {
            //            DisplayAlphabets(tblAlphabet);
            String qString = Request.QueryString.ToString();
            if (Request.QueryString["sortBy"] == null)
                qString += "&sortBy=" + lstSortBy.SelectedValue;
            else
                qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "&sortBy=" + lstSortBy.SelectedValue);
            if (Request.QueryString["pageNo"] == null)
                qString += "&pageNo=1";
            else
                qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "&pageNo=1");
            Response.Redirect("LDAPImport.aspx?" + qString);
        }
        protected void DisplayAlphabets(Table tblAlpha)
        {
            try
            {
                String[] Alphabets = { "#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "All" };
                TableRow tr = new TableRow();
                TableCell tc;
                tblAlpha.Rows.Clear();
                string alpha = "";
                if (Request.QueryString["alphabet"] != null)
                    alpha = Request.QueryString["alphabet"].ToString();
                else alpha = "#"; //fogbugz case 403
                if (alpha.Equals("a"))
                    alpha = "#";//fogbugz case 403
                else if (alpha.Equals("all"))
                    alpha = "All";
                for (int i = 0; i < Alphabets.Length; i++)
                {
                    tc = new TableCell();
                    if (alpha.Equals(Alphabets[i]))
                    {
                        if (i == 0)
                            Alphabets[i] = "#";//fogbugz case 403
                        tc.Text = Alphabets[i];
                    }
                    else
                    {
                        string qString = "";
                        if (i == Alphabets.Length - 1)
                            qString += "alphabet=" + Alphabets[i].ToLower();
                        else if (i==0)
                            qString += "alphabet=a";//fogbugz case 403
                        else
                            qString += "alphabet=" + Alphabets[i];
                        qString += "&sortBy=" + lstSortBy.SelectedValue;

                        tc.Text = "<a href='LDAPImport.aspx?" + qString + "'>" + Alphabets[i] + "</a>";
                    }
                    tr.Cells.Add(tc);
                }
                tblAlpha.Rows.Add(tr);
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("DisplayAlphabets" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
            {
                LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDeleteLDAPUser");
                btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this user ?") + "')"); //FB japnese
            }
        }
        protected void ImportUsers(Object sender, EventArgs e)
        {
            try
            {
                lblErrorInImport.Visible = false;
                String inXML = "";
                int totalRecords = 0;
                String[] Users = txtLDAPUsers.Text.Split('\n');
                totalRecords = Users.Length;
                //Response.Write(totalRecords);
                string temp = "";
                for (int i = 0; i < totalRecords; i++)
                {
                    String[] User = Users[i].Split(',');
                    //Response.Write(User.Length.ToString() + " : " + ValidateLogin(User[3]) + " : " + ValidateEmail(User[0]));
                    if (!User.Equals(""))
                        if (User.Length.Equals(5) && ValidateLogin(User[3]) && ValidateEmail(User[0]))
                        {
                            inXML = "<BulkLoadUsers>";
                            inXML += "<User>";
                            inXML += "     <Email>" + User[0].ToString().Trim() + "</Email>";
                            inXML += "     <FirstName>" + User[1].ToString().Trim() + "</FirstName>";
                            inXML += "     <LastName>" + User[2].ToString().Trim() + "</LastName>";
                            inXML += "     <Login>" + User[3].ToString().Trim() + "</Login>";
                            inXML += "     <Telephone>" + User[4].ToString().Trim() + "</Telephone>";
                            inXML += "</User>";
                            inXML += "</BulkLoadUsers>";
                            //Response.Write("<hr>here: " + obj.Transfer(inXML));
                            String outXML = obj.CallCOM2("BulkLoadUsers", inXML, Application["RTC_ConfigPath"].ToString());
                            //Response.Write("<br>" + obj.Transfer(outXML));
                            if (outXML.IndexOf("<error>") >= 0)
                            {
                                errLabel.Text += obj.ShowErrorMessage(outXML);
                                temp += Users[i] + "\n";
                            }
                            else
                            {
                                //    Response.Write("in else");
                                temp = temp.Replace(User.ToString(), "");
                            }
                        }
                        else
                        {
                            temp += Users[i] + "\n";
                        }
                }
                //Response.Write(temp);

                if ((temp != "") && (temp != "\n"))
                {
                    errLabel.Text += "<br>Some of the following Users have formatting problems and some already exist. All others are imported successfully.";
                    errLabel.Visible = true;
                    txtLDAPUsers.Text = temp;
                    lblErrorInImport.Visible = true;
                }
                else
                {
                    //errLabel.Text = "Operation Successful!";
                    //errLabel.Visible = true;
                    Response.Redirect("LDAPImport.aspx?m=1&t="); // fogbugz case 392
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ImportUsers" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
            }
        }
        protected bool ValidateEmail(String loginName)
        {
            try
            {
                if (loginName == "")
                    return false;
                else
                {
                    Regex regVal = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
                    if (regVal.IsMatch(loginName))
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ValidateEmail" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return false;
            }
        }

        protected bool ValidateLogin(String loginName)
        {
            try
            {
                if (loginName == "")
                    return false;
                else
                {
                    if (loginName.Length <= 2)
                        return false;
                    if ((loginName.IndexOf("@") >= 0) || (loginName.IndexOf("'") >= 0) || (loginName.IndexOf("\"") >= 0) || (loginName.IndexOf("-") >= 0) || (loginName.IndexOf("+") >= 0))
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                //ZD 100263
                log.Trace("ValidateLogin" + ex.StackTrace);
                errLabel.Text = obj.ShowSystemMessage();                
                errLabel.Visible = true;
                return false;
            }
        }
    }
}