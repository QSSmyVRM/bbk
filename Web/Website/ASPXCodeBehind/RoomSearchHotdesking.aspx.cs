/* Copyright (C) 2014 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Text;//ZD 100621
using DevExpress.Web.ASPxPager;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace ns_MyVRM
{
    public partial class en_RoomSearchHotdesking : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        myVRMNet.ImageUtil imageUtilObj = null;
        ns_Logger.Logger log;
        DataRow[] filtrs = null;
        DataTable dtSel = null;
        DataTable dtDisp = null;
        DataSet DSdisp = null;
        ArrayList rmlist = null;

        DataSet ds = null;
        public String confID = "";
        public String duration = "";
        public String Parentframe = "";
        String outXML = "";
        String outXMLDept = "";
        String tzone = "";
        string immediate = "0";
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        XmlTextReader txtrd = null;
        String roomEdit = "N";
        protected String favRooms = "";
        protected String GuestRooms = "";
        String pathName = "";
        string filePath = "";
        byte[] imgArray = null;
        protected String language = "";
        protected String EnableRoomServiceType = "";
        protected String VMR = "";
        protected string roomVMR = "0";
        protected int Cloud = 0, pageIndex = 0;
        string Tier1ID = "";
        string confOrgID = "";
        protected string HotdeskingRooms = "";
        protected string ConfType = "";
        const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";
        protected int CloudConferencing = 0;
        protected int enableWaitList = 0;
        protected int isRecurConference = 0;


        StringBuilder inXML = new StringBuilder();
        # region prviate DataMember

        protected DevExpress.Web.ASPxGridView.ASPxGridView grid2;
        protected DevExpress.Web.ASPxPager.ASPxPager ASPxPager2;
        protected System.Web.UI.WebControls.DataGrid SelectedGrid;
        protected System.Web.UI.WebControls.Label LblError;
        protected System.Web.UI.WebControls.Label vidLbl;
        protected System.Web.UI.WebControls.Label nvidLbl;
        protected System.Web.UI.WebControls.Label vmrvidLbl;
        protected System.Web.UI.WebControls.Label ROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label VCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlVCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label ttlnvidLbl;
        protected System.Web.UI.WebControls.Label lblVMRRooms;
        protected System.Web.UI.WebControls.Label tntvmrrooms;
        protected System.Web.UI.WebControls.Label lblPublicRoom;
        protected System.Web.UI.WebControls.Label ttlPublicLbl;
        protected System.Web.UI.WebControls.Label totalNumber;
        protected System.Web.UI.WebControls.Label lblTotalRecords1;
        protected System.Web.UI.WebControls.CheckBox chkHotdesking;
        protected System.Web.UI.WebControls.CheckBox Available;
        protected System.Web.UI.WebControls.CheckBox chkIsVMR;
        protected System.Web.UI.WebControls.CheckBox chkGuestRooms;
        protected System.Web.UI.WebControls.CheckBox chkFavourites;
        protected System.Web.UI.WebControls.CheckBox MediaNone;
        protected System.Web.UI.WebControls.CheckBox MediaAudio;
        protected System.Web.UI.WebControls.CheckBox MediaVideo;
        protected System.Web.UI.WebControls.CheckBox PhotosOnly;
        protected System.Web.UI.WebControls.CheckBox HandiCap;

        protected System.Web.UI.WebControls.CheckBoxList AVlist;

        protected System.Web.UI.WebControls.DropDownList DrpDwnListView;
        protected System.Web.UI.WebControls.DropDownList DrpActDct;
        protected System.Web.UI.WebControls.DropDownList lstCountry;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.DropDownList lstStates2;
        protected System.Web.UI.WebControls.DropDownList lstStates3;
        protected System.Web.UI.WebControls.DropDownList DrpRecordsperPage;

        protected System.Web.UI.HtmlControls.HtmlTableRow trDateFromTo;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrRoomAvaible;
        protected System.Web.UI.HtmlControls.HtmlTableRow trActDct;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrLicense;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAvlChk;
        protected System.Web.UI.HtmlControls.HtmlTableRow ListView;

        protected System.Web.UI.HtmlControls.HtmlTableCell TDSelectedRoom;

        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomEndTime;
        protected System.Web.UI.WebControls.ImageMap ImageMap1;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMRRoomadded;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Tierslocstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnServiceType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeZone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedlocframe;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnView;
        protected System.Web.UI.HtmlControls.HtmlInputHidden addroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden cmd;
        protected System.Web.UI.HtmlControls.HtmlInputHidden helpPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomIDs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityH;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAV;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMedia;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLoc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnZipCode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAvailable;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnStartTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSubmit;


        protected MetaBuilders.WebControls.ComboBox confRoomStartTime;
        protected MetaBuilders.WebControls.ComboBox confRoomEndTime;

        protected System.Web.UI.WebControls.TextBox txtRoomDateFrom;
        protected System.Web.UI.WebControls.TextBox txtRoomDateTo;
        protected System.Web.UI.WebControls.TextBox TxtNameSearch;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.WebControls.TextBox txtTier1;
        protected System.Web.UI.WebControls.TextBox txtTier2;
        protected System.Web.UI.WebControls.TextBox txtRoomName;

        protected System.Web.UI.HtmlControls.HtmlTableRow trHotdesking;
        protected System.Web.UI.HtmlControls.HtmlButton btnClose;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnImportErr;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton Av_and;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton Av_or;

        protected System.Web.UI.WebControls.Label lbliControlRooms;
        protected System.Web.UI.WebControls.Label tntiControlRooms;
        protected System.Web.UI.WebControls.Label icontrolvidlbl;

        string VMRdialOUTID = "";
        protected System.Web.UI.HtmlControls.HtmlInputHidden SelectedRoomNameValue;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchtier1;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchtier2;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchRoomname;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchMaxCapacity;
        #endregion

        const string PageSizeSession = "ed5e843d-cff7-47a7-815e-832923f7fb10";

        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil();
            String stDate = "";
            String enDate = "";
            String serType = "";

            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("RoomSearch.aspx", Request.Url.AbsoluteUri.ToLower());
                LblError.Visible = false;
                LblError.Text = "";

                grid2.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grid2.Settings.ShowVerticalScrollBar = true;
                grid2.Settings.VerticalScrollableHeight = 450;

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                if (Session["timeFormat"].ToString() == "2") //FB 2588
                    tformat = "HHmmZ";

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if (tformat == "HH:mm")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                }
                else if (tformat == "HHmmZ")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                }

                if (Session["DtDisp"] != null)
                {
                    grid2.DataSource = (DataTable)Session["DtDisp"];
                }

                if (Session["systemTimezoneID"] != null)
                {
                    if (Session["systemTimezoneID"].ToString() != "")
                    {
                        tzone = Session["systemTimezoneID"].ToString();
                    }
                }

                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Session["EnableRoomServiceType"] == null)
                    Session["EnableRoomServiceType"] = "0";
                if (Session["EnableRoomServiceType"].ToString() != "")
                    EnableRoomServiceType = Session["EnableRoomServiceType"].ToString();

                if (Session["Cloud"] != null)
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }

                if (Session["EnableWaitList"] != null && Session["EnableWaitList"].ToString() != "")
                {
                    int.TryParse(Session["EnableWaitList"].ToString(), out enableWaitList);
                }

                String searchType = "";
                if (Request.QueryString["type"] != null)
                    searchType = Request.QueryString["type"].ToString().Trim();

                if (searchType.ToLower() == "h")
                {
                    chkHotdesking.Checked = true;
                    chkHotdesking.Enabled = false;

                    Available.Enabled = false;
                    chkIsVMR.Enabled = false;
                    chkGuestRooms.Enabled = false;
                    chkFavourites.Enabled = false;
                }

                if (Request.QueryString["conforgID"] != null)
                {
                    confOrgID = Request.QueryString["conforgID"].ToString();
                    Session.Add("multisiloOrganizationID", confOrgID);
                }
                if (Request.QueryString["frm"] != null)
                    Parentframe = Request.QueryString["frm"].ToString().Trim();

                if (Request.QueryString["rmEdit"] != null)
                    roomEdit = Request.QueryString["rmEdit"].ToString().Trim();

                if (roomEdit == "Y")
                {
                    Session["multisiloOrganizationID"] = null;
                    TrLicense.Attributes.Add("style", "display:block");
                    trActDct.Attributes.Add("style", "display:block");
                }

                if (Parentframe == "frmCalendarRoom" || roomEdit == "Y")
                {
                    TrRoomAvaible.Attributes.Add("style", "display:none;");
                    trAvlChk.Attributes.Add("style", "display:none;");
                    trDateFromTo.Attributes.Add("style", "display:none;");
                }

                if (roomEdit == "N")
                {
                    chkGuestRooms.Attributes.Add("style", "display:none");
                    chkIsVMR.Attributes.Add("style", "display:none");
                }

                if (Request.QueryString["CloudConf"] != null)
                {
                    int.TryParse(Request.QueryString["CloudConf"].ToString(), out CloudConferencing);
                }

                if (Cloud == 1 && CloudConferencing == 1)
                {
                    chkHotdesking.Checked = false;
                    trHotdesking.Attributes.Add("style", "display:none;");
                }

                if (Request.QueryString["ConfType"] != null)
                {
                    if (Request.QueryString["ConfType"] != "")
                        ConfType = Request.QueryString["ConfType"].ToString();
                }

                if (Request.QueryString["hdnLnkBtnId"] != null && Request.QueryString["hdnLnkBtnId"].ToString() == "1")
                {
                    MediaNone.Checked = false;
                    MediaNone.Enabled = false;
                }

                if (Request.QueryString["immediate"] != null)
                    immediate = Request.QueryString["immediate"].ToString();

                //ZD 102963
                if (Request.QueryString["isRecurrence"] != null)
                    int.TryParse(Request.QueryString["isRecurrence"].ToString(), out isRecurConference);

                if (!IsPostBack)
                {
                    //if (Request.QueryString["View"] != null)
                    //{
                    //    DrpDwnListView.SelectedValue = Request.QueryString["View"].ToString();
                    //}
                    DrpDwnListView.Items.FindByValue("3").Selected = true;

                    if (Request.QueryString["rmsframe"] != null)
                    {
                        selectedlocframe.Value += Request.QueryString["rmsframe"].ToString().Trim();

                        if (Request.QueryString["isVMR"] != null)
                        {
                            if (Request.QueryString["isVMR"] != "")
                                roomVMR = Request.QueryString["isVMR"].ToString();
                        }

                        String selrooms = "";


                        if (selectedlocframe.Value != "")
                        {
                            foreach (String s in selectedlocframe.Value.Split(','))
                            {
                                if (s != "")
                                {
                                    if (selrooms == "")
                                        selrooms = s.Trim();
                                    else
                                        selrooms += "," + s.Trim();
                                }
                            }
                        }

                        selectedlocframe.Value = selrooms;
                    }

                    confRoomStartTime.Items.Clear();
                    confRoomEndTime.Items.Clear();
                    obj.BindTimeToListBox(confRoomStartTime, true, true);
                    obj.BindTimeToListBox(confRoomEndTime, true, true);

                    if (selectedlocframe.Value != "" || selectedlocframe.Value != " ")
                        GetselectedLocations();
                    else
                        selectedlocframe.Value = "";

                    if (Request.QueryString["confID"] != null)
                    {
                        confID = Request.QueryString["confID"].ToString();
                        //ZD 102963 Starts
                        if (enableWaitList == 1 && ConfType == ns_MyVRMNet.vrmConfType.HotDesking && isRecurConference == 0)
                        {
                            Available.Checked = false;
                            Available.Enabled = false;
                        }
                        else
                        {
                            Available.Checked = true;
                            Available.Enabled = false;
                        }
                        //ZD 102963 End

                    }

                    if (Request.QueryString["stDate"] != null)
                        stDate = Request.QueryString["stDate"].ToString();

                    if (Request.QueryString["enDate"] != null)
                        enDate = Request.QueryString["enDate"].ToString();

                    if (Request.QueryString["serType"] != null)
                    {
                        if (Request.QueryString["serType"] != "")
                            serType = Request.QueryString["serType"].ToString();

                        if (serType != "")
                            hdnServiceType.Value = serType;
                    }

                    DateTime dStart, dEnd;
                    DateTime.TryParse(stDate, out dStart);
                    DateTime.TryParse(enDate, out dEnd);

                    if (dStart == DateTime.MinValue || dEnd == DateTime.MinValue)
                    {
                        int defConfDur = 60;
                        Int32.TryParse(Session["DefaultConfDuration"].ToString(), out defConfDur);
                        dStart = DateTime.Now;
                        dEnd = dStart.AddMinutes(defConfDur);
                    }

                    txtRoomDateFrom.Text = dStart.ToString(format);
                    txtRoomDateTo.Text = dEnd.ToString(format);

                    confRoomStartTime.Text = dStart.ToString(tformat);
                    confRoomEndTime.Text = dEnd.ToString(tformat);


                    if (Request.QueryString["tzone"] != null)
                    {
                        if (Request.QueryString["tzone"].ToString() != "")
                            tzone = Request.QueryString["tzone"].ToString();

                        if (tzone != "")
                            hdnTimeZone.Value = tzone;
                    }

                    if (roomVMR == "1")
                        chkIsVMR.Attributes.Add("style", "display:block");

                    BindAv();
                    GetAllData(null, null);
                }

                if (roomEdit == "Y")
                    TDSelectedRoom.Attributes.Add("style", "display:none");

                if (Request.QueryString["hdnLnkBtnId"] != null && Request.QueryString["hdnLnkBtnId"].ToString() != "1")
                {
                    string lnkbtnid = Request.QueryString["hdnLnkBtnId"].ToString();
                    btnClose.Attributes.Add("onclick", "javascript:ClosePopup();window.parent.fnTriggerFromPopup('" + lnkbtnid + "')");
                }
                if (Request.QueryString["hdnLnkBtnId"] != null)
                {
                    trDateFromTo.Attributes.Add("style", "display:none;");
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }

        }
        #endregion


        protected void SetRoomAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {

                    HyperLink lnkRoom = (HyperLink)e.Item.FindControl("btnViewDetails");
                    if (lnkRoom != null)
                        lnkRoom.Attributes.Add("onclick", "javascript:chkresources('" + e.Item.Cells[0].Text + "');");

                    /* HyperLink lnkRooms = (HyperLink)e.Item.FindControl("btnEdit");
                     if (lnkRooms != null)
                         lnkRooms.Attributes.Add("onclick", "javascript:EditRoom('" + e.Item.Cells[0].Text + "');");
                     if (roomEdit == "Y")
                         lnkRooms.Attributes.Add("style", "display:block");*/



                    HtmlImage imageDel = (HtmlImage)e.Item.FindControl("ImageDel");

                    if (imageDel != null)
                        imageDel.Attributes.Add("onclick", "javascript:Delroms('" + e.Item.Cells[0].Text.Trim() + "');");
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }


        private void BindAv()
        {

            String itemInXML = "";
            String itemOutXML = "<ItemList></ItemList>";
            XmlDocument xmldoc = null;
            try
            {


                if (!IsPostBack)
                {
                    if (Session["roomModule"].ToString() == "1")
                    {
                        itemInXML = "<login><userID>11</userID>" + obj.OrgXMLElement() + "<Type>1</Type>" + obj.OrgXMLElement() + "</login>";
                        itemOutXML = obj.CallMyVRMServer("GetItemsList", itemInXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(itemOutXML);

                    AVlist.Items.Add(new ListItem(obj.GetTranslatedText("None"), "None")); //ZD 101344

                    if (AVlist.Items.FindByText(obj.GetTranslatedText("None")) != null)
                        AVlist.Items.FindByText(obj.GetTranslatedText("None")).Selected = true;

                    XmlNodeList nodes = xmldoc.SelectNodes("//ItemList/Item");
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("Name") != null)
                            AVlist.Items.Add(new ListItem(node.SelectSingleNode("Name").InnerText, node.SelectSingleNode("Name").InnerText));//ZD 101344

                        if (AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText) != null)
                            AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText).Selected = true;
                    }

                    obj.GetCountryCodes(lstCountry);

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Selected = true;

                    BindStates();
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void BindStates()
        {
            try
            {
                if (hdnLoc.Value == "0")
                {
                    hdnLoc.Value = "1";

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");

                    lstStates.Enabled = true;
                    lstStates2.Enabled = true;
                    lstStates3.Enabled = true;

                    obj.GetCountryStatesSearch(lstStates, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates2, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates3, lstCountry.SelectedValue);

                    if (lstStates.Items.FindByValue("-1") != null)
                    {
                        lstStates.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates2.Items.FindByValue("-1") != null)
                    {
                        lstStates2.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates2.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates3.Items.FindByValue("-1") != null)
                    {
                        lstStates3.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates3.Items.FindByValue("-1").Selected = true;
                    }
                }


            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void CreateTable()
        {
            try
            {
                dtSel = new DataTable();
                dtSel.Columns.Add("RoomID");
                dtSel.Columns.Add("RoomName");
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

        }

        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
                {
                    string identifiedLoc = "", Status = "", FloorPlanName = "", Tier1Name = "", Tier2Name = ""; ;
                    ImageMap Image1 = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "ImageMap1") as ImageMap;
                    if (Image1 != null)
                    {
                        Image1.ImageUrl = e.GetValue("FloorPlanImage").ToString();
                        identifiedLoc = e.GetValue("IdentifiedLocations").ToString();
                        Status = e.GetValue("PlanRoomIDStatus").ToString();
                        FloorPlanName = e.GetValue("FloorPlanName").ToString();
                        Tier1Name = e.GetValue("Tier1Name").ToString();
                        Tier2Name = e.GetValue("Tier2Name").ToString();

                        string[] statList = Status.Split(new string[] { "||" }, StringSplitOptions.None);
                        int isBusyRoom = 0;
                        string roomsArg = "", TierName = ""; //ZD Latest Issue
                        string[] roomStat = null;

                        for (int i = 0; i < identifiedLoc.Split(',').Length; i++)
                        {
                            isBusyRoom = 0;

                            roomStat = statList[i].Split(new string[] { "~~" }, StringSplitOptions.None);

                            CircleHotSpot Circle1 = new CircleHotSpot();
                            Circle1.HotSpotMode = HotSpotMode.Navigate;

                            if (roomStat[2] == "0")
                                isBusyRoom = 1;

                            roomsArg = roomStat[0] + ";;;" + roomStat[1] + ";" + isBusyRoom + ";" + ConfType + ";" + roomStat[2];
                            TierName = Tier1Name + ">" + Tier2Name; //ZD Latest Issue
                            if (Parentframe == "frmConferenceSetUp" || Parentframe == "frmExpress")
                                Circle1.NavigateUrl = "javascript:Addroms('" + roomsArg + "');";
                            else
                                Circle1.NavigateUrl = "javascript:fnInitiateConf('" + roomStat[0] + "','" + roomStat[1] + "','" + roomStat[2] + "','" + TierName + "');"; //ZD Latest Issue
                            Circle1.X = Convert.ToInt32(identifiedLoc.Split(',')[i].Split('x')[0]);
                            Circle1.Y = Convert.ToInt32(identifiedLoc.Split(',')[i].Split('x')[1]);
                            Circle1.Radius = 5;
                            Circle1.AlternateText = roomStat[1];
                            //Circle1.AlternateText = roomStat[1] + "" + "Status " + roomStat[2];
                            Image1.HotSpots.Add(Circle1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }
        }

        public void GetselectedLocations()
        {
            HttpContext.Current.Session["locstr"] = null;
            DataTable dtselected = null;
            string selrooms = "";
            string selinXML = "", selOutXML = "";
            DataSet ds1 = new DataSet();
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
            try
            {
                if (selectedlocframe.Value != "")
                {
                    foreach (String s in selectedlocframe.Value.Split(','))
                    {
                        if (s != "")
                        {
                            if (selrooms == "")
                                selrooms = s.Trim();
                            else
                                selrooms += "," + s.Trim();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(selrooms))
                {
                    selinXML = "<SelectedRooms>" + func.OrgXMLElement() + "<RoomID>" + selrooms + "</RoomID></SelectedRooms>";
                    selOutXML = obj.CallMyVRMServer("SelectedRooms", selinXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (selOutXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(selOutXML);
                        LblError.Visible = true;
                        return;
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(selOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Rooms/Room");
                        ds1 = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            txtrd = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds1.ReadXml(txtrd, XmlReadMode.InferSchema);
                        }

                        if (ds1.Tables.Count > 0)
                        {

                            if (ds1.Tables[0] != null)
                            {
                                Tierslocstr.Value = "";
                                locstr.Value = "";
                                dtselected = ds1.Tables[0].Clone();
                                foreach (DataRow rw in ds1.Tables[0].Rows)
                                {
                                    if (Tierslocstr.Value == "")
                                        Tierslocstr.Value = rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString();
                                    else
                                        Tierslocstr.Value += "�" + rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString();

                                    if (locstr.Value == "")
                                        locstr.Value = rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString();
                                    else
                                        locstr.Value += "++" + rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString();


                                    dtselected.ImportRow(rw);
                                }
                            }
                        }
                        else

                            dtselected = new DataTable();

                        HttpContext.Current.Session["locstr"] = locstr.Value;
                        SelectedRoomNameValue.Value = locstr.Value.Replace("++", "�"); //ALT + 147
                        HttpContext.Current.Session["Tierslocstr"] = Tierslocstr.Value;
                        SelectedGrid.DataSource = dtselected;
                        SelectedGrid.DataBind();


                    }
                }
                else
                {
                    dtselected = new DataTable();
                    SelectedRoomNameValue.Value = "";
                    HttpContext.Current.Session["locstr"] = null;
                    HttpContext.Current.Session["Tierslocstr"] = null;
                    SelectedGrid.DataSource = dtselected;
                    SelectedGrid.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        public Boolean GetDeactRooms(String rmids)
        {
            String deacRooms = "";
            Boolean retrn = false;
            try
            {
                myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";
                if (rmlist == null)
                {

                    String outXML = func.CallMyVRMServer("GetDeactivatedRooms", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument docdata = new XmlDocument();
                    docdata.LoadXml(outXML);

                    XmlNode favlist = docdata.SelectSingleNode("roomIDs");
                    if (favlist != null)
                        deacRooms = favlist.InnerText;

                    if (deacRooms != "")
                    {
                        rmlist = new ArrayList();
                        foreach (String s in deacRooms.Split(','))
                        {
                            if (s != "")
                                rmlist.Add(s);

                        }
                    }
                }

                if (rmlist != null)
                    retrn = rmlist.Contains(rmids);

                func = null;

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

            return retrn;

        }

        #region BindCountry

        protected void BindCountry(Object sender, EventArgs e)
        {
            try
            {
                BindStates();
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        #endregion

        #region Custom Call

        protected void Grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            grid2.DataBind();
        }

        protected void Grid2_DataBound(object sender, EventArgs e)
        {

            grid2.JSProperties["cpPageCount"] = grid2.PageCount;
        }

        #endregion

        #region Paging and Filtering
        /// <summary>
        /// Paging and Filtering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ASPxPager_PageIndexChanged2(object sender, EventArgs e)
        {
            ASPxPager pager = sender as ASPxPager;
            pageIndex = pager.PageIndex;

            GetAllData(null, null);

        }
        #endregion

        #region DrpRecordsperPage
        /// <summary>
        /// DrpRecordsperPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DrpRecordsperPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAllData(null, null);
            }
            catch (Exception ex)
            {
                log.Trace("ListViewChange: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetAllDatas
        /// <summary>
        /// Get Floor Rooms Information
        /// </summary>
        public void GetAllData(object sender, EventArgs e)
        {
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
            XmlWriter xWriter = null;
            XmlWriterSettings xSettings = new XmlWriterSettings();
            StringBuilder FloorPlanXML = new StringBuilder();
            DataView dv = null;
            DataTable dt = new DataTable();
            string avItemQueryAnd = "", avItemQueryAndorOr = "", rmName = "", AVItemNone = "0";
            try
            {
                GetselectedLocations();
                if (addroom.Value == "-1")
                {
                    addroom.Value = "0";
                    //GetselectedLocations();
                }
                else
                {
                    DateTime dStart = DateTime.MinValue;
                    DateTime dEnd = DateTime.MinValue;
                    try
                    {
                        dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateFrom.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomStartTime.Text)); //FB 2588
                        dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateTo.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomEndTime.Text)); //FB 2588
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(func.GetTranslatedText("Please enter a valid date time."));
                    }

                    TimeSpan confDuration = dEnd.Subtract(dStart);
                    dStart = dStart.AddSeconds(45);

                    xSettings = new XmlWriterSettings();
                    xSettings.OmitXmlDeclaration = true;
                    using (xWriter = XmlWriter.Create(FloorPlanXML, xSettings))
                    {
                        xWriter.WriteStartElement("GetFloorPlans");
                        xWriter.WriteElementString("UserID", Session["userID"].ToString());
                        xWriter.WriteString(func.OrgXMLElement());

                        int isAdmin = 0;
                        if (Session["admin"].ToString() == "2" || Session["admin"].ToString() == "3")
                            isAdmin = 1;
                        xWriter.WriteElementString("isAdmin", isAdmin.ToString());

                        xWriter.WriteElementString("StartDate", dStart.ToString());
                        xWriter.WriteElementString("ConfDuration", confDuration.TotalMinutes.ToString());
                        xWriter.WriteElementString("TimeZone", tzone);

                        xWriter.WriteStartElement("SearchCriteria");

                        if (hdnSubmit.Value == "1")
                        {
                            if (txtLsearchtier1 != null && txtLsearchtier1.Value != "")
                                xWriter.WriteElementString("Tier1Name", txtLsearchtier1.Value);

                            if (txtLsearchtier2 != null && txtLsearchtier2.Value != "")
                                xWriter.WriteElementString("Tier2Name", txtLsearchtier2.Value);

                            if (txtLsearchRoomname != null && txtLsearchRoomname.Value != "")
                                xWriter.WriteElementString("RoomName", txtLsearchRoomname.Value);
                        }
                        else
                        {
                            if (txtTier1 != null && txtTier1.Text != "")
                                xWriter.WriteElementString("Tier1Name", txtTier1.Text);

                            if (txtTier2 != null && txtTier2.Text != "")
                                xWriter.WriteElementString("Tier2Name", txtTier2.Text);

                            if (txtRoomName != null && txtRoomName.Text != "")
                                xWriter.WriteElementString("RoomName", txtRoomName.Text);
                        }

                        if (HandiCap.Checked)
                            xWriter.WriteElementString("Handicappedaccess", "1");
                        else
                            xWriter.WriteElementString("Handicappedaccess", "0");

                        if (PhotosOnly.Checked)
                            xWriter.WriteElementString("PhotosOnly", "1");
                        else
                            xWriter.WriteElementString("PhotosOnly", "0");

                        xWriter.WriteElementString("ConfType", ConfType);

                        if (MediaNone.Checked)
                            xWriter.WriteElementString("MediaNone", "1");
                        else
                            xWriter.WriteElementString("MediaNone", "0");

                        if (MediaAudio.Checked)
                            xWriter.WriteElementString("MediaAudio", "1");
                        else
                            xWriter.WriteElementString("MediaAudio", "0");

                        if (MediaVideo.Checked)
                            xWriter.WriteElementString("MediaVideo", "1");
                        else
                            xWriter.WriteElementString("MediaVideo", "0");

                        if (hdnZipCode.Value == "1")
                        {
                            xWriter.WriteElementString("hdnZipCode", hdnZipCode.Value);
                            xWriter.WriteElementString("ZipCode", txtZipCode.Text);
                        }

                        xWriter.WriteElementString("Country", lstCountry.SelectedValue);
                        xWriter.WriteElementString("State", lstStates.SelectedValue);
                        xWriter.WriteElementString("State2", lstStates2.SelectedValue);
                        xWriter.WriteElementString("State3", lstStates3.SelectedValue);

                        if (Av_and.Checked)
                            avItemQueryAndorOr = "and";
                        else
                            avItemQueryAndorOr = "or";

                        if (hdnAV.Value == "1")
                        {
                            foreach (ListItem lstItem in AVlist.Items)
                            {

                                rmName = lstItem.Value.Trim();

                                if (lstItem.Value.Trim().Split(' ').Length > 1)
                                    rmName = lstItem.Value.Trim().Split(' ')[0] + lstItem.Value.Trim().Split(' ')[1];


                                if (lstItem.Selected)
                                {
                                    if (lstItem.Text != "None")
                                    {
                                        if (avItemQueryAnd == "")
                                            avItemQueryAnd = " name = '" + lstItem.Text + "' ";
                                        else
                                            avItemQueryAnd += avItemQueryAndorOr + " name = '" + lstItem.Text + "' ";
                                    }
                                    else
                                        AVItemNone = "1";
                                }
                            }
                        }

                        xWriter.WriteElementString("isEnableAVItem", hdnAV.Value);
                        xWriter.WriteElementString("ListAVItem", avItemQueryAnd);
                        xWriter.WriteElementString("AVItemNone", AVItemNone);
                        xWriter.WriteElementString("avItemQuery", avItemQueryAndorOr);



                        xWriter.WriteFullEndElement();
                        xWriter.WriteElementString("PageNo", pageIndex.ToString());
                        xWriter.WriteElementString("MaxRecords", DrpRecordsperPage.SelectedValue);
                        xWriter.WriteFullEndElement();
                        xWriter.Flush();
                    }

                    FloorPlanXML = FloorPlanXML.Replace("&lt;", "<")
                                   .Replace("&gt;", ">");

                    outXML = obj.CallMyVRMServer("GetFloorPlans", FloorPlanXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(outXML);
                        LblError.Visible = true;
                        return;
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//GetFloorPlans/FloorPlan");
                        ds = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            txtrd = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(txtrd, XmlReadMode.InferSchema);
                        }

                        int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//GetFloorPlans/PageNo").InnerText);
                        int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//GetFloorPlans/TotalPages").InnerText);
                        lblTotalRecords1.Text = xmldoc.SelectSingleNode("//GetFloorPlans/TotalRecords").InnerText;
                        ASPxPager2.ItemCount = totalPages;
                    }

                    if (ds.Tables.Count == 0)
                        dt = new DataTable();

                    if (ds.Tables.Count > 0)
                    {
                        dtDisp = ds.Tables[0].Clone();


                        foreach (DataRow rw in ds.Tables[0].Rows)
                        {
                            string planStatus = rw["PlanRoomIDStatus"].ToString();
                            string planMarkedLoc = rw["IdentifiedLocations"].ToString();

                            MemoryStream mem = new MemoryStream(Convert.FromBase64String((rw["FloorPlanImage"]).ToString()));
                            MemoryStream ms = MarkImageLocation(mem, planStatus, planMarkedLoc);
                            mem.Dispose();
                            rw["FloorPlanImage"] = "data:image/jpg;base64," + Convert.ToBase64String(ms.ToArray(), 0, ms.ToArray().Length);

                            ms.Dispose();


                            dtDisp.ImportRow(rw);
                        }

                        dv = new DataView(dtDisp);

                        if (Session["DtDisp"] != null)
                            Session["DtDisp"] = dv.ToTable();
                        else
                            Session.Add("DtDisp", dv.ToTable());

                        dt = new DataTable();
                        dt = (DataTable)Session["DtDisp"];

                    }
                    grid2.DataSource = dt;
                    grid2.DataBind();
                }


                #region License update

                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";

                String outXMLlicen = obj.CallMyVRMServer("GetRoomLicenseDetails", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmld = new XmlDocument();
                xmld.LoadXml(outXMLlicen);

                XmlNode nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideoRemaining");

                if (nde != null)
                    nvidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideoRemaining");

                if (nde != null)
                    vidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMRRemaining");

                if (nde != null)
                    vmrvidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && nvidLbl.Text != "")
                    {
                        ttlnvidLbl.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(nvidLbl.Text)).ToString();
                    }

                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vidLbl.Text != "")
                    {
                        totalNumber.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vidLbl.Text)).ToString();
                    }
                }


                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxiControlRemaining");

                if (nde != null)
                    icontrolvidlbl.Text = nde.InnerText;


                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMR");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vmrvidLbl.Text != "")
                    {
                        tntvmrrooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vmrvidLbl.Text)).ToString();
                    }
                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxiControl");

                if (nde != null)
                {
                    if (nde.InnerText != "" && icontrolvidlbl.Text != "")
                    {
                        tntiControlRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(icontrolvidlbl.Text)).ToString();
                    }
                }


                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxPublicRoom");

                if (nde != null)
                {
                    if (nde.InnerText != "")
                        ttlPublicLbl.Text = nde.InnerText;
                }
                lblPublicRoom.Visible = false;
                ttlPublicLbl.Visible = false;
                if (Session["EnablePublicRooms"].ToString() == "1")
                {
                    lblPublicRoom.Visible = true;
                    ttlPublicLbl.Visible = true;
                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdeskingRemaining");
                if (nde != null)
                    ROHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && ROHotdeskingRooms.Text != "")
                    {
                        ttlROHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(ROHotdeskingRooms.Text)).ToString();
                    }
                }
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdeskingRemaining");
                if (nde != null)
                    VCHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && VCHotdeskingRooms.Text != "")
                    {
                        ttlVCHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(VCHotdeskingRooms.Text)).ToString();
                    }
                }

                nde = null;

                #endregion



            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
            finally
            {
                if (txtrd != null)
                {
                    txtrd.Close();
                }
                txtrd = null;
                func = null;

            }
        }
        #endregion


        #region searchresult
        /// <summary>
        /// searchresult
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void searchresult(object sender, EventArgs e)
        {
            try
            {
                GetAllData(null, null);
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }
        #endregion


        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }


        #region MarkImageLocation
        /// <summary>
        /// MarkImageLocation
        /// </summary>
        /// <param name="imageString"></param>
        /// <param name="fname"></param>
        /// <param name="ImageID"></param>
        protected MemoryStream MarkImageLocation(MemoryStream memStream, string stat, string locs)
        {
            MemoryStream ms = new MemoryStream();
            try
            {

                System.Drawing.Image img = System.Drawing.Image.FromStream(memStream);
                memStream.Dispose();

                Bitmap bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format24bppRgb);
                bmp.SetResolution(72, 72);


                Graphics gfx = Graphics.FromImage(bmp);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.Clear(Color.Gray);

                gfx.DrawImage(img, 0, 0, img.Width, img.Height);
                img.Dispose();

                string roomids = selectedlocframe.Value;
                string[] roomidlist = null;
                string[] roomidarr = new string[1];
                if (roomids != "")
                {
                    roomids = roomids + ",";
                    roomidlist = roomids.Split(',');
                }

                Brush brshRed = new SolidBrush(Color.Red);
                Brush brshGreen = new SolidBrush(Color.Green);
                Brush brshYellow = new SolidBrush(Color.FromArgb(255, 204, 0));
                Brush brshGray = new SolidBrush(Color.Gray);

                string[] markLocs = locs.Split(',');
                string[] statList = stat.Split(new string[] { "||" }, StringSplitOptions.None);
                string[] subMarkLocs = null;
                string[] subStatList = null;
                int cnt;
                for (int i = 0; i < markLocs.Length; i++)
                {
                    subMarkLocs = markLocs[i].Split('x');
                    subStatList = statList[i].Split(new string[] { "~~" }, StringSplitOptions.None);

                    cnt = 0;
                    if (roomids != "")
                    {
                        roomidarr[0] = subStatList[0];
                        var intersec = roomidlist.Intersect(roomidarr);
                        cnt = intersec.Count();
                    }

                    if (cnt > 0)
                    {
                        gfx.FillEllipse(brshYellow, Int32.Parse(subMarkLocs[0]) - 5, Int32.Parse(subMarkLocs[1]) - 5, 10, 10);
                    }
                    else if (subStatList[2] == "-1")
                    {
                        gfx.FillEllipse(brshGray, Int32.Parse(subMarkLocs[0]) - 5, Int32.Parse(subMarkLocs[1]) - 5, 10, 10);
                    }
                    else if (subStatList[2] == "0")
                    {
                        gfx.FillEllipse(brshRed, Int32.Parse(subMarkLocs[0]) - 5, Int32.Parse(subMarkLocs[1]) - 5, 10, 10);
                    }
                    else
                    {
                        gfx.FillEllipse(brshGreen, Int32.Parse(subMarkLocs[0]) - 5, Int32.Parse(subMarkLocs[1]) - 5, 10, 10);
                    }

                }

                gfx.Dispose();


                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                bmp.Dispose();

            }
            catch (Exception ex)
            {
                log.Trace("SetRoomImage: " + ex.StackTrace + " : " + ex.Message);
            }
            return ms;
        }
        #endregion

    }
}
