/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Xml;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.UI.HtmlControls;//ZD 100420

namespace ns_MyVRM
{
   public partial class Group : System.Web.UI.Page
    {
 
        myVRMNet.NETFunctions obj;
        ns_Logger.Logger log; //FB 2027
       
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.Table tblNoGroups;
        protected System.Web.UI.WebControls.DataGrid dgGroups;
        protected System.Web.UI.WebControls.TextBox txtSortBy;
        protected System.Web.UI.WebControls.TextBox txtSGroupName;
        protected System.Web.UI.WebControls.TextBox txtSMember;
        protected System.Web.UI.WebControls.TextBox txtSDescription;
       
       
       public Boolean searchgroup = false;

        public Group()
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger(); //FB 2027
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Methods Executed on Page Load

        private void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageGroup.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                errLabel.Attributes.Add("style", "display:none"); //FB 2921
                errLabel.Text = "";
                if (Request.QueryString["m"] != null)
                    if (Request.QueryString["m"].ToString().Equals("1"))
                    {
                        errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                        errLabel.Attributes.Add("style", "display:block"); //FB 2921
                    }
                if (!IsPostBack)
                    BindDataWithInXML();
              
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("Page_Load" + ex.Message);//ZD 100263
            }

        }
        protected void BindDataWithInXML()
        {
            Session["multisiloOrganizationID"] = null; //FB 2274
            //FB 2027
            String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><sortBy>" + txtSortBy.Text + "</sortBy></login>";
            BindData(inXML, "GetGroup");
        }
        private void BindData(String inXML, String cmd)
        {
            try
            {
                //Response.Write("<br>" + obj.Transfer(inXML));
                //FB 2027
                //String outXML = obj.CallCOM(cmd, inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer(cmd, inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
               
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");

                    if (nodes.Count > 0)
                    {

                        LoadGroupGrid(nodes);

                        foreach (DataGridItem dgroup in dgGroups.Items)
                        {
                            //ZD 100420
                            //((Button)dgroup.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:OpenDetails('" + dgroup.Cells[0].Text + "');return false;");
                            ((HtmlButton)dgroup.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:OpenDetails('" + dgroup.Cells[0].Text + "');return false;");
                        }


                        dgGroups.Visible = true;
                        tblNoGroups.Visible = false;

                    }
                    else
                    {
                        dgGroups.Visible = false;
                        tblNoGroups.Visible = true;

                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Attributes.Add("style", "display:block"); //FB 2921
                }
            }
            catch (Exception ex)
            {
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
                log.Trace("BindData" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            }
        }

        protected void LoadGroupGrid(XmlNodeList nodes)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                //Response.Write(ds.Tables[1].Columns[3].ColumnName);
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["public"].ToString().Trim().Equals("1"))
                            dr["public"] = obj.GetTranslatedText("Public");//FB 2272
                        else
                            dr["public"] = obj.GetTranslatedText("Private");
                    }
                    dgGroups.DataSource = dt;
                    dgGroups.DataBind();
                }
               
            }
            catch (Exception ex)
            {
                log.Trace("LoadGroupGrid" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
            }
        }
       //code for show the view details grid -revathi
      protected void LoadGroupDetailsGrid(XmlNodeList nodes, DataGrid dgViewDetails)
       {
           try
           {
               //Response.Write(dgViewDetails.ClientID);
               XmlTextReader xtr;
               DataSet dset = new DataSet();

               foreach (XmlNode node in nodes)
               {
                   xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                   dset.ReadXml(xtr, XmlReadMode.InferSchema);
               }
               DataTable dta = new DataTable();
               DataTable dta1 = new DataTable();
              
               if (dset.Tables.Count > 0)
               {
                   dta = dset.Tables[0];
                   //Response.Write(dta.Columns.Count);
                   //Response.End();
                   dgViewDetails.DataSource = dta;
                   dgViewDetails.DataBind();
                   dgViewDetails.Attributes.Add("style", "display:none");
               }
           }
           catch (Exception ex)
           {
               log.Trace("LoadGroupDetailsGrid" + ex.Message);//ZD 100263
               errLabel.Text = obj.ShowSystemMessage();//ZD 100263
               errLabel.Attributes.Add("style", "display:block"); //FB 2921
           }
       }

        #endregion

        protected void DeleteGroup(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                String inXML = ""; // <login><userID>11</userID><edit/><delete><departmentID>1</departmentID></delete></login>
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <groups>";
                inXML += "      <group>";
                inXML += "          <groupID>" + e.Item.Cells[0].Text + "</groupID>";
                inXML += "      </group>";
                inXML += "  </groups>";
                inXML += "</login>";
                String outXML = obj.CallMyVRMServer("DeleteGroup", inXML, Application["COM_ConfigPath"].ToString()); //FB 2027
                //Response.Write(obj.Transfer(outXML));
                //Response.End();
                if (outXML.IndexOf("<error>") < 0)
                {
                    Response.Redirect("ManageGroup.aspx?m=1");
                }
                else
                {
                    errLabel.Attributes.Add("style", "display:block");//FB 2921
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("DeleteGroup" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
            }
        }
        protected void EditGroup(object sender, DataGridCommandEventArgs e)
        {
            try
            {
                //Response.Redirect("dispatcher/userdispatcher.asp?cmd=GetGroup&opr=MODIFY&groupID=" + e.Item.Cells[0].Text); //Login Management
                Response.Redirect("managegroup2.aspx?groupID=" + e.Item.Cells[0].Text);

            }
            catch (System.Threading.ThreadAbortException) { }//Login Management
            catch (Exception ex)
            {
                log.Trace("Editgroup" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263;
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
            }
        }
      //FogBuz 1754 Fixed AS 09/23/2010 - Start
        protected void SearchGroup(Object sender, EventArgs e)
        {
            if ((txtSGroupName.Text == "") && (txtSMember.Text == "") && (txtSDescription.Text == ""))
            {
                BindDataWithInXML();//FB Fix AS 09/23/2010
            }
            else
            {
                //FB 2027 
                StringBuilder inXML = new StringBuilder();
                searchgroup = true;
                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>"); 
                inXML.Append("<groupName>" + txtSGroupName.Text + "</groupName>");
                inXML.Append("<includedParticipant>" + txtSMember.Text + "</includedParticipant>");
                inXML.Append("<descriptionIncludes>" + txtSDescription.Text + "</descriptionIncludes>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                BindData(inXML.ToString(), "SearchGroup");
                txtSGroupName.Text = "";
                txtSMember.Text = "";
                txtSDescription.Text = "";
            }


        }
       //Fixed End
        
       protected void ResetGroup(Object sender, EventArgs e)
       {
           Response.Redirect("ManageGroup.aspx?opr=''&tid=''"); //Reset Bug Fix AS 09/23/2010
                
           // Response.Redirect("dispatcher/userdispatcher.asp?cmd=GetGroup&opr=''&tid=''");
          
       }

        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Group ?") + "')"); //FB japnese

                   
                  
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindRowsDeleteMessage" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
            }
        }

      
        protected void CreateNewGroup(Object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("dispatcher/userdispatcher.asp?cmd=GetGroup&opr=NEW&tid=new"); //Login Management
                Response.Redirect("managegroup2.aspx");

            }
            catch (System.Threading.ThreadAbortException) { }//Login Management
            catch (Exception ex)
            {
                log.Trace("CreateNewGroup" + ex.Message);//ZD 100263
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                errLabel.Attributes.Add("style", "display:block"); //FB 2921
            }
        }
       
        protected void SortGroups(Object sender, DataGridSortCommandEventArgs e)
        {
            txtSortBy.Text = e.SortExpression;
            BindDataWithInXML();
        }
    }
}
