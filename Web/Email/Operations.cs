//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/// <remarks>
/// FILE : Operations.cs
/// DESCRIPTION : This is the controller. All operations are directed from here. 
/// AUTHOR : Kapil M
/// </remarks>

#region References 
using System;
using System.Xml;
using System.Collections;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
#endregion 

namespace NS_OPERATIONS
{
	/// <summary>
	/// All event-triggered operations are stored in this class. 
	/// Each of the methods perform a specific task.
	/// This class is usually called by the ASP/ASPX pages directly.
	/// </summary>
	class Operations
	{
		/// <summary>
		/// Public class attributes
		/// </summary>
		internal string errMsg = null;

		/// <summary>
		/// private class attributes
		/// </summary>
		private NS_LOGGER.Log logger;
		private NS_MESSENGER.Party masterParty = new NS_MESSENGER.Party();
		private NS_MESSENGER.ConfigParams configParams;

        #region Operations
        /// <summary>
		/// Constructor
		/// </summary>
		/// <param name="config"></param>
		internal Operations(NS_MESSENGER.ConfigParams config)
		{
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
        }
        #endregion

        #region LogEvent
        /// <summary>
		/// Log an event in Windows Event Logs (Not used so far)
		/// </summary>
		/// <param name="message"></param>
		/// <returns></returns>
		internal bool LogEvent(string message)
		{
			try 
			{
				// log a event in the event viewer 
				NS_LOGGER.EvtLog evtLog = new NS_LOGGER.EvtLog(configParams);
				string sSource = "myVRM";
				string sLog = "Application";				
				bool ret = evtLog.Log(sSource,sLog,message);
				if (!ret)
				{
					this.errMsg = evtLog.errMsg;
					return false;
				}
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				return false;
			}
        }
        #endregion

       


    }

class SendEmails
{
	private NS_LOGGER.Log logger ;
	internal string errMsg = null;
	private NS_MESSENGER.ConfigParams configParams;
	internal SendEmails (NS_MESSENGER.ConfigParams config)
	{
		configParams = new NS_MESSENGER.ConfigParams();
		configParams = config;
		logger = new NS_LOGGER.Log(configParams);
	}

	internal bool Send()
	{
		try
		{
			logger.Trace ("In the Email Sub-system...");
            logger.Trace("*** Entering Send Email Sub-system ***");
			NS_EMAIL.Email email = new NS_EMAIL.Email(configParams);
			bool ret = email.SendAllEmails();
			if (!ret)
			{
				return false;
			}
            // update LastRunTime status in db
            NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);
            db.UpdateMailServerLastRunDateTime();

			return true;
		}
		catch (Exception e)
		{
			logger.Exception(100,e.Message);
			return false;
		}
	}
} 
     

/// <summary>
/// Misc tasks which need to be performed at the back-end.
/// </summary>
class MiscTasks	
{
	/// <summary>
	/// Public class attribute
	/// </summary>
	internal string errMsg = null;

	/// <summary>
	/// Private class attribute
	/// </summary>
	private NS_MESSENGER.ConfigParams configParams;
	private NS_LOGGER.Log logger;

	/// <summary>
	/// Delete unsent emails
	/// </summary>
	/// <returns></returns>	
	internal bool DeleteUnsentOldEmails()
	{
		// If emails are more than 7 days old delete them from the queue
		NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
		bool ret = db.DeleteUnsentOldEmails();
		if (!ret)return false;
		return true;
	}

	/// <summary>
	/// Shrink database transaction log file
	/// </summary>
	/// <returns></returns>
	internal bool ShrinkDbTransLogFile()
	{
		// Transaction log file increases over time and can space can run out on hard disk.				
		// Run this every day to keep the log file under 1 GB. 
		if ((DateTime.Now.Hour > 0 && DateTime.Now.Hour < 2) && (DateTime.Now.Minute > 0 && DateTime.Now.Minute < 3))
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			bool ret = db.ShrinkDbTransLogFile();
			if (!ret) return false;
		}
				
		return true;
	}
		
	/// <summary>
	/// Delete log records
	/// </summary>
	/// <returns></returns>
	internal bool DeleteLogRecords()
	{
		// TODO: Delete log records depending on log preference settings.				
		if ((DateTime.Now.Hour == 3) && (DateTime.Now.Minute > 0 && DateTime.Now.Minute < 5))
		{
			NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
			bool ret = db.DeleteLogRecords();
			if (!ret) return false;
		}					
		return true;
	}


    

}	



}
